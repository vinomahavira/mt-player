﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using mtvplayer;
using System.Threading;
using System.Globalization;

namespace mtvplayer
{
    /// <summary>
    /// this form is used to divert screen, while record progress is happen
    /// </summary>
    public partial class ProgressRecord : Form
    {

        /// <summary> call function from MainForm class</summary>
        MainForm main = new MainForm();
        /// <summary> get full path of the application</summary>
        //string pathdirect = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        //string pathdirect = "C:\\";
        string pathdirect = System.Environment.GetEnvironmentVariable("temp");

        /// <summary> initialize ProgressRecord component. </summary>
        public ProgressRecord()
        {
            InitializeComponent();
        }

        /// <summary> close ProgressRecord form when done. </summary>
        private void btDone_Click(object sender, EventArgs e)
        {
           // Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
           // Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            //Process.Start(Path.Combine(pathdirect, "Maestronic\\recording"));
            Process.Start(Properties.Settings.Default.recPath);
            this.Close();
        }
    }
}
   