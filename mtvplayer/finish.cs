﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace mtvplayer
{
    public partial class finish : Form
    {
        MainForm mainForm;
        public finish(MainForm forms1)
        {
            InitializeComponent();
            mainForm = forms1;
        }

        private void btReload_Click(object sender, EventArgs e)
        {
            mainForm.videoTrackBar.Value = 0;
            mainForm.SeqNo1 = 0;
            mainForm.SeqNo2 = 0;
            mainForm.SeqNo3 = 0;
            mainForm.SeqNo4 = 0;
            mainForm.SeqNo5 = 0;

            mainForm.a = 0;
            mainForm.b = 0;
            mainForm.c = 0;
            mainForm.d = 0;
            mainForm.e5 = 0;

            mainForm.lbStartDur.Text = "00:00:00";
            mainForm.ticky = 0;
            mainForm.btPlay.Enabled = true;
            mainForm.btFast4x.Enabled = true;
            mainForm.btFast8x.Enabled = true;
            mainForm.countCh = 0;
            mainForm.timer1.Stop();

            mainForm.CloseInterfaces();
            mainForm.CloseInterfaces2();
            mainForm.CloseInterfaces3();
            mainForm.CloseInterfaces4();
            mainForm.CloseInterfaces5();

            mainForm.splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.rady;
            mainForm.splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.rady;
            mainForm.splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.rady;
            mainForm.splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.rady;
            mainForm.splitContainer4.Panel1.BackgroundImage = mtvplayer.Properties.Resources.rady;
            this.Close();
        }
    }
}
