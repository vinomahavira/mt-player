﻿namespace mtvplayer
{
    partial class finish
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btReload = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btReload
            // 
            this.btReload.BackgroundImage = global::mtvplayer.Properties.Resources.icon_refresh_128;
            this.btReload.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btReload.Location = new System.Drawing.Point(23, 23);
            this.btReload.Name = "btReload";
            this.btReload.Size = new System.Drawing.Size(80, 80);
            this.btReload.TabIndex = 0;
            this.btReload.UseVisualStyleBackColor = true;
            this.btReload.Click += new System.EventHandler(this.btReload_Click);
            // 
            // finish
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(125, 125);
            this.Controls.Add(this.btReload);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "finish";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "finish";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btReload;
    }
}