﻿namespace mtvplayer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.btCam5 = new System.Windows.Forms.Button();
            this.rbIPCAM = new System.Windows.Forms.RadioButton();
            this.btCam3 = new System.Windows.Forms.Button();
            this.btCam4 = new System.Windows.Forms.Button();
            this.btAll4 = new System.Windows.Forms.Button();
            this.btcam1 = new System.Windows.Forms.Button();
            this.btcam2 = new System.Windows.Forms.Button();
            this.btRecStop = new System.Windows.Forms.Button();
            this.btOpenVideo = new System.Windows.Forms.Button();
            this.btSnapshots = new System.Windows.Forms.Button();
            this.btRec = new System.Windows.Forms.Button();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.splitContainer8 = new System.Windows.Forms.SplitContainer();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.splitContainer9 = new System.Windows.Forms.SplitContainer();
            this.splitContainer7 = new System.Windows.Forms.SplitContainer();
            this.splitContainer10 = new System.Windows.Forms.SplitContainer();
            this.splitContainer11 = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.doPopulate = new System.ComponentModel.BackgroundWorker();
            this.doRecord = new System.ComponentModel.BackgroundWorker();
            this.backwardTimer = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lbStartDur = new System.Windows.Forms.Label();
            this.lbEndDur = new System.Windows.Forms.Label();
            this.btFast4x = new System.Windows.Forms.CheckBox();
            this.btFast8x = new System.Windows.Forms.CheckBox();
            this.btFastBackward4x = new System.Windows.Forms.CheckBox();
            this.btFastBackward8x = new System.Windows.Forms.CheckBox();
            this.btPause = new System.Windows.Forms.CheckBox();
            this.btFB = new System.Windows.Forms.CheckBox();
            this.btPlay = new System.Windows.Forms.CheckBox();
            this.trackBarPanel = new System.Windows.Forms.Panel();
            this.videoTrackBar = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.doCapture = new System.ComponentModel.BackgroundWorker();
            this.browsePath = new System.Windows.Forms.FolderBrowserDialog();
            this.rbNVS = new System.Windows.Forms.RadioButton();
            this.cbRate = new System.Windows.Forms.ComboBox();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.splitContainer8.Panel1.SuspendLayout();
            this.splitContainer8.Panel2.SuspendLayout();
            this.splitContainer8.SuspendLayout();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            this.splitContainer9.SuspendLayout();
            this.splitContainer7.Panel1.SuspendLayout();
            this.splitContainer7.SuspendLayout();
            this.splitContainer10.SuspendLayout();
            this.splitContainer11.Panel1.SuspendLayout();
            this.splitContainer11.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.trackBarPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer2
            // 
            resources.ApplyResources(this.splitContainer2, "splitContainer2");
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            resources.ApplyResources(this.splitContainer2.Panel1, "splitContainer2.Panel1");
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer5);
            this.splitContainer2.TabStop = false;
            // 
            // splitContainer5
            // 
            resources.ApplyResources(this.splitContainer5, "splitContainer5");
            this.splitContainer5.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer5.Name = "splitContainer5";
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer5.Panel1.Controls.Add(this.btCam5);
            this.splitContainer5.Panel1.Controls.Add(this.rbIPCAM);
            this.splitContainer5.Panel1.Controls.Add(this.btCam3);
            this.splitContainer5.Panel1.Controls.Add(this.btCam4);
            this.splitContainer5.Panel1.Controls.Add(this.btAll4);
            this.splitContainer5.Panel1.Controls.Add(this.btcam1);
            this.splitContainer5.Panel1.Controls.Add(this.btcam2);
            this.splitContainer5.Panel1.Controls.Add(this.btRecStop);
            this.splitContainer5.Panel1.Controls.Add(this.btOpenVideo);
            this.splitContainer5.Panel1.Controls.Add(this.btSnapshots);
            this.splitContainer5.Panel1.Controls.Add(this.btRec);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.splitContainer3);
            // 
            // btCam5
            // 
            resources.ApplyResources(this.btCam5, "btCam5");
            this.btCam5.ForeColor = System.Drawing.Color.Black;
            this.btCam5.Name = "btCam5";
            this.btCam5.TabStop = false;
            this.btCam5.UseVisualStyleBackColor = true;
            this.btCam5.Click += new System.EventHandler(this.btCam5_Click);
            // 
            // rbIPCAM
            // 
            resources.ApplyResources(this.rbIPCAM, "rbIPCAM");
            this.rbIPCAM.ForeColor = System.Drawing.Color.Black;
            this.rbIPCAM.Name = "rbIPCAM";
            this.rbIPCAM.TabStop = true;
            this.rbIPCAM.UseVisualStyleBackColor = true;
            this.rbIPCAM.CheckedChanged += new System.EventHandler(this.rbIPCAM_CheckedChanged);
            // 
            // btCam3
            // 
            resources.ApplyResources(this.btCam3, "btCam3");
            this.btCam3.ForeColor = System.Drawing.Color.Black;
            this.btCam3.Name = "btCam3";
            this.btCam3.TabStop = false;
            this.btCam3.UseVisualStyleBackColor = true;
            this.btCam3.Click += new System.EventHandler(this.btCH3_Click);
            // 
            // btCam4
            // 
            resources.ApplyResources(this.btCam4, "btCam4");
            this.btCam4.ForeColor = System.Drawing.Color.Black;
            this.btCam4.Name = "btCam4";
            this.btCam4.TabStop = false;
            this.btCam4.UseVisualStyleBackColor = true;
            this.btCam4.Click += new System.EventHandler(this.btCH4_Click);
            // 
            // btAll4
            // 
            resources.ApplyResources(this.btAll4, "btAll4");
            this.btAll4.Image = global::mtvplayer.Properties.Resources.cross;
            this.btAll4.Name = "btAll4";
            this.btAll4.TabStop = false;
            this.btAll4.UseVisualStyleBackColor = true;
            this.btAll4.Click += new System.EventHandler(this.btAll4_Click);
            // 
            // btcam1
            // 
            resources.ApplyResources(this.btcam1, "btcam1");
            this.btcam1.ForeColor = System.Drawing.Color.Black;
            this.btcam1.Name = "btcam1";
            this.btcam1.TabStop = false;
            this.btcam1.UseVisualStyleBackColor = true;
            this.btcam1.Click += new System.EventHandler(this.btCH1_Click);
            // 
            // btcam2
            // 
            resources.ApplyResources(this.btcam2, "btcam2");
            this.btcam2.ForeColor = System.Drawing.Color.Black;
            this.btcam2.Name = "btcam2";
            this.btcam2.TabStop = false;
            this.btcam2.UseVisualStyleBackColor = true;
            this.btcam2.Click += new System.EventHandler(this.btCH2_Click);
            // 
            // btRecStop
            // 
            resources.ApplyResources(this.btRecStop, "btRecStop");
            this.btRecStop.Image = global::mtvplayer.Properties.Resources.stop8;
            this.btRecStop.Name = "btRecStop";
            this.btRecStop.TabStop = false;
            this.btRecStop.UseVisualStyleBackColor = true;
            this.btRecStop.Click += new System.EventHandler(this.btRecStop_Click);
            // 
            // btOpenVideo
            // 
            this.btOpenVideo.Image = global::mtvplayer.Properties.Resources.search102;
            resources.ApplyResources(this.btOpenVideo, "btOpenVideo");
            this.btOpenVideo.Name = "btOpenVideo";
            this.btOpenVideo.TabStop = false;
            this.btOpenVideo.UseVisualStyleBackColor = true;
            this.btOpenVideo.Click += new System.EventHandler(this.btOpenVideo_Click);
            // 
            // btSnapshots
            // 
            resources.ApplyResources(this.btSnapshots, "btSnapshots");
            this.btSnapshots.Image = global::mtvplayer.Properties.Resources.small35;
            this.btSnapshots.Name = "btSnapshots";
            this.btSnapshots.TabStop = false;
            this.btSnapshots.UseVisualStyleBackColor = true;
            this.btSnapshots.Click += new System.EventHandler(this.btSnapshots_Click);
            // 
            // btRec
            // 
            resources.ApplyResources(this.btRec, "btRec");
            this.btRec.Image = global::mtvplayer.Properties.Resources.record;
            this.btRec.Name = "btRec";
            this.btRec.TabStop = false;
            this.btRec.UseVisualStyleBackColor = true;
            this.btRec.Click += new System.EventHandler(this.btRec_Click);
            // 
            // splitContainer3
            // 
            resources.ApplyResources(this.splitContainer3, "splitContainer3");
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.splitContainer8);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer11);
            // 
            // splitContainer8
            // 
            this.splitContainer8.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.splitContainer8, "splitContainer8");
            this.splitContainer8.Name = "splitContainer8";
            // 
            // splitContainer8.Panel1
            // 
            this.splitContainer8.Panel1.Controls.Add(this.splitContainer6);
            // 
            // splitContainer8.Panel2
            // 
            this.splitContainer8.Panel2.Controls.Add(this.splitContainer7);
            this.splitContainer8.TabStop = false;
            // 
            // splitContainer6
            // 
            resources.ApplyResources(this.splitContainer6, "splitContainer6");
            this.splitContainer6.Name = "splitContainer6";
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.Controls.Add(this.splitContainer9);
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.BackColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.splitContainer6.Panel2, "splitContainer6.Panel2");
            this.splitContainer6.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer6_Panel2_Paint);
            this.splitContainer6.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer6_SplitterMoved);
            // 
            // splitContainer9
            // 
            resources.ApplyResources(this.splitContainer9, "splitContainer9");
            this.splitContainer9.Name = "splitContainer9";
            // 
            // splitContainer9.Panel1
            // 
            this.splitContainer9.Panel1.BackColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.splitContainer9.Panel1, "splitContainer9.Panel1");
            // 
            // splitContainer9.Panel2
            // 
            this.splitContainer9.Panel2.BackColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.splitContainer9.Panel2, "splitContainer9.Panel2");
            this.splitContainer9.TabStop = false;
            // 
            // splitContainer7
            // 
            resources.ApplyResources(this.splitContainer7, "splitContainer7");
            this.splitContainer7.Name = "splitContainer7";
            // 
            // splitContainer7.Panel1
            // 
            this.splitContainer7.Panel1.Controls.Add(this.splitContainer10);
            // 
            // splitContainer7.Panel2
            // 
            this.splitContainer7.Panel2.BackColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.splitContainer7.Panel2, "splitContainer7.Panel2");
            // 
            // splitContainer10
            // 
            resources.ApplyResources(this.splitContainer10, "splitContainer10");
            this.splitContainer10.Name = "splitContainer10";
            // 
            // splitContainer10.Panel1
            // 
            this.splitContainer10.Panel1.BackColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.splitContainer10.Panel1, "splitContainer10.Panel1");
            // 
            // splitContainer10.Panel2
            // 
            this.splitContainer10.Panel2.BackColor = System.Drawing.Color.Gray;
            this.splitContainer10.Panel2.BackgroundImage = global::mtvplayer.Properties.Resources.logo;
            resources.ApplyResources(this.splitContainer10.Panel2, "splitContainer10.Panel2");
            this.splitContainer10.TabStop = false;
            // 
            // splitContainer11
            // 
            resources.ApplyResources(this.splitContainer11, "splitContainer11");
            this.splitContainer11.Name = "splitContainer11";
            // 
            // splitContainer11.Panel1
            // 
            this.splitContainer11.Panel1.Controls.Add(this.splitContainer4);
            // 
            // splitContainer11.Panel2
            // 
            this.splitContainer11.Panel2.BackColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.splitContainer11.Panel2, "splitContainer11.Panel2");
            // 
            // splitContainer4
            // 
            resources.ApplyResources(this.splitContainer4, "splitContainer4");
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.BackColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.splitContainer4.Panel1, "splitContainer4.Panel1");
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.BackColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.splitContainer4.Panel2, "splitContainer4.Panel2");
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // doPopulate
            // 
            this.doPopulate.WorkerSupportsCancellation = true;
            this.doPopulate.DoWork += new System.ComponentModel.DoWorkEventHandler(this.doPopulate_DoWork);
            this.doPopulate.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.doPopulate_ProgressChanged);
            this.doPopulate.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.doPopulate_RunWorkerCompleted);
            // 
            // doRecord
            // 
            this.doRecord.WorkerSupportsCancellation = true;
            this.doRecord.DoWork += new System.ComponentModel.DoWorkEventHandler(this.doRecord_DoWork);
            this.doRecord.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.doRecord_RunWorkerCompleted);
            // 
            // backwardTimer
            // 
            this.backwardTimer.Interval = 1000;
            this.backwardTimer.Tick += new System.EventHandler(this.backwardTimer_Tick);
            // 
            // splitContainer1
            // 
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.ForeColor = System.Drawing.Color.Transparent;
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lbStartDur);
            this.splitContainer1.Panel2.Controls.Add(this.lbEndDur);
            this.splitContainer1.Panel2.Controls.Add(this.btFast4x);
            this.splitContainer1.Panel2.Controls.Add(this.btFast8x);
            this.splitContainer1.Panel2.Controls.Add(this.btFastBackward4x);
            this.splitContainer1.Panel2.Controls.Add(this.btFastBackward8x);
            this.splitContainer1.Panel2.Controls.Add(this.btPause);
            this.splitContainer1.Panel2.Controls.Add(this.btFB);
            this.splitContainer1.Panel2.Controls.Add(this.btPlay);
            this.splitContainer1.Panel2.Controls.Add(this.trackBarPanel);
            // 
            // lbStartDur
            // 
            resources.ApplyResources(this.lbStartDur, "lbStartDur");
            this.lbStartDur.BackColor = System.Drawing.Color.Transparent;
            this.lbStartDur.ForeColor = System.Drawing.Color.Black;
            this.lbStartDur.Name = "lbStartDur";
            // 
            // lbEndDur
            // 
            resources.ApplyResources(this.lbEndDur, "lbEndDur");
            this.lbEndDur.BackColor = System.Drawing.Color.Transparent;
            this.lbEndDur.ForeColor = System.Drawing.Color.Black;
            this.lbEndDur.Name = "lbEndDur";
            // 
            // btFast4x
            // 
            resources.ApplyResources(this.btFast4x, "btFast4x");
            this.btFast4x.FlatAppearance.BorderSize = 0;
            this.btFast4x.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.Highlight;
            this.btFast4x.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btFast4x.Image = global::mtvplayer.Properties.Resources.fast18;
            this.btFast4x.Name = "btFast4x";
            this.btFast4x.TabStop = false;
            this.btFast4x.UseVisualStyleBackColor = true;
            this.btFast4x.Click += new System.EventHandler(this.btFast4x_Click);
            // 
            // btFast8x
            // 
            resources.ApplyResources(this.btFast8x, "btFast8x");
            this.btFast8x.FlatAppearance.BorderSize = 0;
            this.btFast8x.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.Highlight;
            this.btFast8x.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btFast8x.Name = "btFast8x";
            this.btFast8x.TabStop = false;
            this.btFast8x.UseVisualStyleBackColor = true;
            this.btFast8x.Click += new System.EventHandler(this.btFast8x_Click);
            // 
            // btFastBackward4x
            // 
            resources.ApplyResources(this.btFastBackward4x, "btFastBackward4x");
            this.btFastBackward4x.FlatAppearance.BorderSize = 0;
            this.btFastBackward4x.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.Highlight;
            this.btFastBackward4x.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btFastBackward4x.Image = global::mtvplayer.Properties.Resources.backf;
            this.btFastBackward4x.Name = "btFastBackward4x";
            this.btFastBackward4x.TabStop = false;
            this.btFastBackward4x.UseVisualStyleBackColor = true;
            this.btFastBackward4x.Click += new System.EventHandler(this.btFastBackward4x_Click);
            // 
            // btFastBackward8x
            // 
            resources.ApplyResources(this.btFastBackward8x, "btFastBackward8x");
            this.btFastBackward8x.FlatAppearance.BorderSize = 0;
            this.btFastBackward8x.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.Highlight;
            this.btFastBackward8x.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btFastBackward8x.Image = global::mtvplayer.Properties.Resources.backff;
            this.btFastBackward8x.Name = "btFastBackward8x";
            this.btFastBackward8x.TabStop = false;
            this.btFastBackward8x.UseVisualStyleBackColor = true;
            this.btFastBackward8x.Click += new System.EventHandler(this.btFastBackward8x_Click);
            // 
            // btPause
            // 
            resources.ApplyResources(this.btPause, "btPause");
            this.btPause.FlatAppearance.BorderSize = 0;
            this.btPause.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.Highlight;
            this.btPause.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btPause.Name = "btPause";
            this.btPause.TabStop = false;
            this.btPause.UseVisualStyleBackColor = true;
            this.btPause.Click += new System.EventHandler(this.btPause_Click);
            // 
            // btFB
            // 
            resources.ApplyResources(this.btFB, "btFB");
            this.btFB.FlatAppearance.BorderSize = 0;
            this.btFB.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.Highlight;
            this.btFB.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btFB.Image = global::mtvplayer.Properties.Resources.back;
            this.btFB.Name = "btFB";
            this.btFB.TabStop = false;
            this.btFB.UseVisualStyleBackColor = true;
            this.btFB.Click += new System.EventHandler(this.btFB_Click);
            // 
            // btPlay
            // 
            resources.ApplyResources(this.btPlay, "btPlay");
            this.btPlay.BackColor = System.Drawing.Color.Transparent;
            this.btPlay.FlatAppearance.BorderSize = 0;
            this.btPlay.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.Highlight;
            this.btPlay.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btPlay.Name = "btPlay";
            this.btPlay.TabStop = false;
            this.btPlay.UseVisualStyleBackColor = false;
            this.btPlay.Click += new System.EventHandler(this.btPlay_Click);
            // 
            // trackBarPanel
            // 
            resources.ApplyResources(this.trackBarPanel, "trackBarPanel");
            this.trackBarPanel.Controls.Add(this.videoTrackBar);
            this.trackBarPanel.Controls.Add(this.label1);
            this.trackBarPanel.Name = "trackBarPanel";
            // 
            // videoTrackBar
            // 
            this.videoTrackBar.Cursor = System.Windows.Forms.Cursors.Arrow;
            resources.ApplyResources(this.videoTrackBar, "videoTrackBar");
            this.videoTrackBar.LargeChange = 1;
            this.videoTrackBar.Name = "videoTrackBar";
            this.videoTrackBar.TabStop = false;
            this.videoTrackBar.TickFrequency = 3;
            this.videoTrackBar.Scroll += new System.EventHandler(this.videoTrackBar_Scroll);
            this.videoTrackBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.videoTrackBar_MouseDown);
            this.videoTrackBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.videoTrackBar_MouseUp);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Name = "label1";
            // 
            // doCapture
            // 
            this.doCapture.WorkerSupportsCancellation = true;
            this.doCapture.DoWork += new System.ComponentModel.DoWorkEventHandler(this.doCapture_DoWork);
            this.doCapture.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.doCapture_RunWorkerCompleted);
            // 
            // rbNVS
            // 
            resources.ApplyResources(this.rbNVS, "rbNVS");
            this.rbNVS.Checked = true;
            this.rbNVS.Name = "rbNVS";
            this.rbNVS.TabStop = true;
            this.rbNVS.UseVisualStyleBackColor = true;
            this.rbNVS.CheckedChanged += new System.EventHandler(this.rbNVS_CheckedChanged);
            // 
            // cbRate
            // 
            this.cbRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRate.FormattingEnabled = true;
            resources.ApplyResources(this.cbRate, "cbRate");
            this.cbRate.Name = "cbRate";
            this.cbRate.SelectedIndexChanged += new System.EventHandler(this.cbRate_SelectedIndexChanged);
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.cbRate);
            this.Controls.Add(this.rbNVS);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Tag = "MT Video Player";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel1.PerformLayout();
            this.splitContainer5.Panel2.ResumeLayout(false);
            this.splitContainer5.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer8.Panel1.ResumeLayout(false);
            this.splitContainer8.Panel2.ResumeLayout(false);
            this.splitContainer8.ResumeLayout(false);
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.ResumeLayout(false);
            this.splitContainer9.ResumeLayout(false);
            this.splitContainer7.Panel1.ResumeLayout(false);
            this.splitContainer7.ResumeLayout(false);
            this.splitContainer10.ResumeLayout(false);
            this.splitContainer11.Panel1.ResumeLayout(false);
            this.splitContainer11.ResumeLayout(false);
            this.splitContainer4.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.trackBarPanel.ResumeLayout(false);
            this.trackBarPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>background worker to load video file. </summary>
        private System.ComponentModel.BackgroundWorker doPopulate;
        /// <summary> set video timer when play forward. </summary>
        public System.Windows.Forms.Timer timer1;
        private System.ComponentModel.BackgroundWorker doRecord;
        /// <summary> set video timer when play backward. </summary>
        private System.Windows.Forms.Timer backwardTimer;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.Button btOpenVideo;
        /// <summary> background worker to capture video file. </summary>
        private System.ComponentModel.BackgroundWorker doCapture;
        /// <summary> set channel 3 full screen windows. </summary>
        private System.Windows.Forms.Button btCam3;
        /// <summary> set channel 4 full screen windows. </summary>
        private System.Windows.Forms.Button btCam4;
        /// <summary> set video screen to 4 screen windows. </summary>
        private System.Windows.Forms.Button btAll4;
        /// <summary> set channel 1 full screen windows. </summary>
        private System.Windows.Forms.Button btcam1;
        /// <summary> set channel 2 full screen windows. </summary>
        private System.Windows.Forms.Button btcam2;
        /// <summary> stop recording. </summary>
        private System.Windows.Forms.Button btRecStop;
        /// <summary> create snapshot. </summary>
        private System.Windows.Forms.Button btSnapshots;
        /// <summary> start recording. </summary>
        private System.Windows.Forms.Button btRec;
        /// <summary> show start duration to end user. </summary>
        public System.Windows.Forms.Label lbStartDur;
        /// <summary> show end duration to end user. </summary>
        public System.Windows.Forms.Label lbEndDur;
        /// <summary> set video to play fast backward 4x. </summary>
        private System.Windows.Forms.CheckBox btFastBackward4x;
        /// <summary> set video to play fast backward 8x. </summary>
        private System.Windows.Forms.CheckBox btFastBackward8x;
        /// <summary> pause video. </summary>
        private System.Windows.Forms.CheckBox btPause;
        /// <summary>set video to play backward normal speed. </summary>
        private System.Windows.Forms.CheckBox btFB;
        private System.Windows.Forms.Panel trackBarPanel;
        /// <summary>helping label for trackbar. </summary>
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer8;
        public System.Windows.Forms.TrackBar videoTrackBar;
        public System.Windows.Forms.CheckBox btFast4x;
        public System.Windows.Forms.CheckBox btFast8x;
        public System.Windows.Forms.CheckBox btPlay;
        public System.Windows.Forms.SplitContainer splitContainer9;
        public System.Windows.Forms.SplitContainer splitContainer10;
        private System.Windows.Forms.FolderBrowserDialog browsePath;
        private System.Windows.Forms.RadioButton rbIPCAM;
        private System.Windows.Forms.RadioButton rbNVS;
        private System.Windows.Forms.ComboBox cbRate;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Button btCam5;
        public System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private System.Windows.Forms.SplitContainer splitContainer7;
        private System.Windows.Forms.SplitContainer splitContainer11;
    }
}

