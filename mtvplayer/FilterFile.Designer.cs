﻿namespace mtvplayer
{
    partial class FilterFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txFromDate = new System.Windows.Forms.DateTimePicker();
            this.timePickToDefault = new System.Windows.Forms.DateTimePicker();
            this.timePickFromDefault = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btCalendar = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.timePickToSelect = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.timePickFromSelect = new System.Windows.Forms.DateTimePicker();
            this.lbDate = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btOpen = new System.Windows.Forms.Button();
            this.cbSingleFile = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.btOK = new System.Windows.Forms.Button();
            this.btClose = new System.Windows.Forms.Button();
            this.btDefault = new System.Windows.Forms.Button();
            this.BrowseFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.lbBus = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txFromDate);
            this.panel1.Controls.Add(this.timePickToDefault);
            this.panel1.Controls.Add(this.timePickFromDefault);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btCalendar);
            this.panel1.Location = new System.Drawing.Point(3, 18);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(224, 104);
            this.panel1.TabIndex = 0;
            this.panel1.Click += new System.EventHandler(this.panel1_Click);
            // 
            // txFromDate
            // 
            this.txFromDate.Checked = false;
            this.txFromDate.CustomFormat = "dd/MM/yyyy";
            this.txFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txFromDate.Location = new System.Drawing.Point(73, 41);
            this.txFromDate.Name = "txFromDate";
            this.txFromDate.ShowUpDown = true;
            this.txFromDate.Size = new System.Drawing.Size(101, 20);
            this.txFromDate.TabIndex = 37;
            this.txFromDate.Value = new System.DateTime(2015, 3, 26, 0, 0, 0, 0);
            this.txFromDate.ValueChanged += new System.EventHandler(this.txFromDate_ValueChanged);
            // 
            // timePickToDefault
            // 
            this.timePickToDefault.CustomFormat = "HH:mm:ss";
            this.timePickToDefault.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.timePickToDefault.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timePickToDefault.Location = new System.Drawing.Point(120, 79);
            this.timePickToDefault.Name = "timePickToDefault";
            this.timePickToDefault.ShowUpDown = true;
            this.timePickToDefault.Size = new System.Drawing.Size(100, 20);
            this.timePickToDefault.TabIndex = 36;
            this.timePickToDefault.Value = new System.DateTime(2014, 12, 12, 1, 0, 0, 0);
            this.timePickToDefault.ValueChanged += new System.EventHandler(this.timePickToDefault_ValueChanged);
            // 
            // timePickFromDefault
            // 
            this.timePickFromDefault.CustomFormat = "HH:mm:ss";
            this.timePickFromDefault.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.timePickFromDefault.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timePickFromDefault.Location = new System.Drawing.Point(3, 79);
            this.timePickFromDefault.Name = "timePickFromDefault";
            this.timePickFromDefault.ShowUpDown = true;
            this.timePickFromDefault.Size = new System.Drawing.Size(100, 20);
            this.timePickFromDefault.TabIndex = 32;
            this.timePickFromDefault.Value = new System.DateTime(2014, 12, 24, 0, 0, 0, 0);
            this.timePickFromDefault.ValueChanged += new System.EventHandler(this.timePickFromDefault_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(221, 15);
            this.label2.TabIndex = 34;
            this.label2.Text = "Select date and time from default folder";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(104, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "to";
            // 
            // btCalendar
            // 
            this.btCalendar.BackgroundImage = global::mtvplayer.Properties.Resources.small58;
            this.btCalendar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCalendar.Location = new System.Drawing.Point(180, 33);
            this.btCalendar.Name = "btCalendar";
            this.btCalendar.Size = new System.Drawing.Size(40, 40);
            this.btCalendar.TabIndex = 33;
            this.btCalendar.UseVisualStyleBackColor = true;
            this.btCalendar.Click += new System.EventHandler(this.btCalendar_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.timePickToSelect);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.timePickFromSelect);
            this.panel2.Controls.Add(this.lbDate);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.btOpen);
            this.panel2.Controls.Add(this.cbSingleFile);
            this.panel2.Location = new System.Drawing.Point(3, 143);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(224, 92);
            this.panel2.TabIndex = 1;
            this.panel2.Click += new System.EventHandler(this.panel2_Click);
            // 
            // timePickToSelect
            // 
            this.timePickToSelect.CustomFormat = "HH:mm:ss";
            this.timePickToSelect.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.timePickToSelect.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timePickToSelect.Location = new System.Drawing.Point(120, 67);
            this.timePickToSelect.Name = "timePickToSelect";
            this.timePickToSelect.ShowUpDown = true;
            this.timePickToSelect.Size = new System.Drawing.Size(100, 20);
            this.timePickToSelect.TabIndex = 37;
            this.timePickToSelect.Value = new System.DateTime(2014, 12, 12, 1, 0, 0, 0);
            this.timePickToSelect.ValueChanged += new System.EventHandler(this.timePickToSelect_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(101, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "to";
            // 
            // timePickFromSelect
            // 
            this.timePickFromSelect.CustomFormat = "HH:mm:ss";
            this.timePickFromSelect.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.timePickFromSelect.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timePickFromSelect.Location = new System.Drawing.Point(0, 67);
            this.timePickFromSelect.Name = "timePickFromSelect";
            this.timePickFromSelect.ShowUpDown = true;
            this.timePickFromSelect.Size = new System.Drawing.Size(100, 20);
            this.timePickFromSelect.TabIndex = 35;
            this.timePickFromSelect.Value = new System.DateTime(2014, 12, 24, 0, 0, 0, 0);
            this.timePickFromSelect.ValueChanged += new System.EventHandler(this.timePickFromSelect_ValueChanged);
            // 
            // lbDate
            // 
            this.lbDate.AutoSize = true;
            this.lbDate.Location = new System.Drawing.Point(49, 25);
            this.lbDate.Name = "lbDate";
            this.lbDate.Size = new System.Drawing.Size(0, 13);
            this.lbDate.TabIndex = 34;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(31, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(169, 15);
            this.label4.TabIndex = 31;
            this.label4.Text = "Select folder and choose time";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btOpen
            // 
            this.btOpen.BackgroundImage = global::mtvplayer.Properties.Resources.folder;
            this.btOpen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btOpen.Location = new System.Drawing.Point(3, 21);
            this.btOpen.Name = "btOpen";
            this.btOpen.Size = new System.Drawing.Size(40, 40);
            this.btOpen.TabIndex = 32;
            this.btOpen.UseVisualStyleBackColor = true;
            this.btOpen.Click += new System.EventHandler(this.btOpen_Click);
            // 
            // cbSingleFile
            // 
            this.cbSingleFile.AutoSize = true;
            this.cbSingleFile.Location = new System.Drawing.Point(49, 44);
            this.cbSingleFile.Name = "cbSingleFile";
            this.cbSingleFile.Size = new System.Drawing.Size(74, 17);
            this.cbSingleFile.TabIndex = 33;
            this.cbSingleFile.Text = "Single File";
            this.cbSingleFile.UseVisualStyleBackColor = true;
            this.cbSingleFile.CheckedChanged += new System.EventHandler(this.cbSingleFile_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(106, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 15);
            this.label3.TabIndex = 40;
            this.label3.Text = "or";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(230, 280);
            this.shapeContainer1.TabIndex = 41;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderWidth = 3;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 2;
            this.lineShape2.X2 = 91;
            this.lineShape2.Y1 = 132;
            this.lineShape2.Y2 = 132;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderWidth = 3;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 136;
            this.lineShape1.X2 = 225;
            this.lineShape1.Y1 = 132;
            this.lineShape1.Y2 = 132;
            // 
            // btOK
            // 
            this.btOK.BackgroundImage = global::mtvplayer.Properties.Resources.ok4;
            this.btOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btOK.Enabled = false;
            this.btOK.Location = new System.Drawing.Point(150, 238);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(40, 40);
            this.btOK.TabIndex = 43;
            this.btOK.UseVisualStyleBackColor = true;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // btClose
            // 
            this.btClose.BackgroundImage = global::mtvplayer.Properties.Resources.prohibited;
            this.btClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btClose.Location = new System.Drawing.Point(190, 238);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(40, 40);
            this.btClose.TabIndex = 42;
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // btDefault
            // 
            this.btDefault.BackgroundImage = global::mtvplayer.Properties.Resources.monotone_cog_settings_gear;
            this.btDefault.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btDefault.Location = new System.Drawing.Point(200, 0);
            this.btDefault.Name = "btDefault";
            this.btDefault.Size = new System.Drawing.Size(30, 30);
            this.btDefault.TabIndex = 39;
            this.btDefault.Text = " ";
            this.btDefault.UseVisualStyleBackColor = true;
            this.btDefault.Click += new System.EventHandler(this.btDefault_Click);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(1, 96);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 44;
            this.monthCalendar1.Visible = false;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // lbBus
            // 
            this.lbBus.AutoSize = true;
            this.lbBus.Location = new System.Drawing.Point(3, 0);
            this.lbBus.Name = "lbBus";
            this.lbBus.Size = new System.Drawing.Size(0, 13);
            this.lbBus.TabIndex = 45;
            // 
            // FilterFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(230, 280);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.lbBus);
            this.Controls.Add(this.btOK);
            this.Controls.Add(this.btClose);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btDefault);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.shapeContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(20, 30);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FilterFile";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FilterFile";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FilterFile_FormClosing);
            this.Load += new System.EventHandler(this.FilterFile_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker txFromDate;
        private System.Windows.Forms.DateTimePicker timePickToDefault;
        private System.Windows.Forms.DateTimePicker timePickFromDefault;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btCalendar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btDefault;
        private System.Windows.Forms.Label label3;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btOpen;
        private System.Windows.Forms.CheckBox cbSingleFile;
        private System.Windows.Forms.Label lbDate;
        private System.Windows.Forms.DateTimePicker timePickToSelect;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker timePickFromSelect;
        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.Button btClose;
        private System.Windows.Forms.FolderBrowserDialog BrowseFolder;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Label lbBus;
    }
}