﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace mtvplayer
{
    /// <summary> this form is used to load the video files. Can choose one from two method user needed, default method and select method.
    /// Default method it used for selected file from default folder uses set in the first time.
    /// Select method it used for selected the other file, the value in this method is not saving by program</summary>

    public partial class FilterFile : Form
    {
        /// <summary> call function from MainForm class</summary>
        MainForm mainForm;

        /// <summary> set status for video has been load not</summary>
        int i = 0;

        /// <summary> default ipcam for start name. </summary>
        string ipcam = "ipcam_10.13.201.10";
        /// <summary> default format date string. </summary>
        string formatString = "MM dd yyyy";

        /// <summary> set starting time filter to load video files </summary>
        TimeSpan timeFrom = TimeSpan.Parse("00:00:00");
        /// <summary> set to time filter to load video files </summary>
        TimeSpan endtime = TimeSpan.Parse("00:00:00");

        /// <summary> check the video filter is crossing day or not</summary>
        int crossDaydef = 0;
        int crossDay = 0;

        /// <summary> get days range between two time (startTime and endTime) </summary>
        TimeSpan daysRangedef = TimeSpan.Parse("00:00:00");
        TimeSpan daysRange = TimeSpan.Parse("00:00:00");

        /// <summary> get full path of video files for single play method</summary>
        string dirSingle;
        CultureInfo MyCultureInfo = new CultureInfo("en-US");



        /// <summary> initialize FilterFile class component. </summary>
        public FilterFile(MainForm forms1)
        {
            InitializeComponent();
            mainForm = forms1;
            System.Globalization.CultureInfo cultureinformation = new System.Globalization.CultureInfo("en-US");

            System.Globalization.DateTimeFormatInfo dateTimeInfo = new System.Globalization.DateTimeFormatInfo();

            dateTimeInfo.DateSeparator = "/";
            dateTimeInfo.LongDatePattern = "dddd, MMMM dd,yyyy";
            dateTimeInfo.ShortDatePattern = "M/d/yyyy";
            dateTimeInfo.ShortTimePattern = "HH:mm";
            dateTimeInfo.LongTimePattern = "HH:mm:ss";

            //cultureinformation.DateTimeFormat = dateTimeInfo;

            Application.CurrentCulture = cultureinformation;

            Thread.CurrentThread.CurrentCulture = cultureinformation;
            Thread.CurrentThread.CurrentUICulture = cultureinformation;
        }

        /// <summary> load first action when newly opening form. </summary>
        private void FilterFile_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            //setcbRate();
            System.Globalization.CultureInfo cultureinformation = new System.Globalization.CultureInfo("en-US");

            System.Globalization.DateTimeFormatInfo dateTimeInfo = new System.Globalization.DateTimeFormatInfo();

            dateTimeInfo.DateSeparator = "/";
            dateTimeInfo.LongDatePattern = "dd MMMMM yyyy";
            dateTimeInfo.ShortDatePattern = "dd/MM/yyyy";
            dateTimeInfo.ShortTimePattern = "HH:mm";
            dateTimeInfo.LongTimePattern = "HH:mm:ss";

           // cultureinformation.DateTimeFormat = dateTimeInfo;

            Application.CurrentCulture = cultureinformation;

            Thread.CurrentThread.CurrentCulture = cultureinformation;
            Thread.CurrentThread.CurrentUICulture = cultureinformation;

            DriveInfo[] ListDrives = DriveInfo.GetDrives();
            foreach (DriveInfo Drive in ListDrives)
            {
                if (Drive.DriveType == DriveType.Fixed)
                {
                    try
                    {
                        if (Drive.DriveFormat == "EXT3")
                        {
                            BrowseFolder.SelectedPath = Drive + "ftp\\";
                        }
                    }
                    catch
                    {
                        Console.WriteLine(Drive + " gagal");
                        continue;
                    }
                }
            }

            if (!string.IsNullOrEmpty(Properties.Settings.Default.DateDefFF))
            {
                Console.WriteLine("DateDefFF " + Properties.Settings.Default.DateDefFF);
                try
                {
                    DateTime d = DateTime.ParseExact(Properties.Settings.Default.DateDefFF, "yyyyMMdd", CultureInfo.InvariantCulture);
                    txFromDate.Text = d.ToString("MM/dd/yyyy");
                }
                catch
                { }
            }

            if (!string.IsNullOrEmpty(Properties.Settings.Default.SelectedPathFF))
            {
                Console.WriteLine("selectedPath " + Properties.Settings.Default.SelectedPathFF);
                BrowseFolder.SelectedPath = Properties.Settings.Default.SelectedPathFF;
            }

            if (!string.IsNullOrEmpty(Properties.Settings.Default.SelectedPathSingFF))
            {
                Console.WriteLine("selectedPath " + Properties.Settings.Default.SelectedPathSingFF);
                dirSingle = Properties.Settings.Default.SelectedPathSingFF;
                lbDate.Text = Properties.Settings.Default.lbdatesingle;
            }

            if (!string.IsNullOrEmpty(Properties.Settings.Default.TimeFrSaveDefFF))
            {
                Console.WriteLine("TimeFrSaveDefFF " + Properties.Settings.Default.TimeFrSaveDefFF);
                timePickFromDefault.Text = Properties.Settings.Default.TimeFrSaveDefFF;
            }
            else
            {
                timePickFromDefault.Enabled = false;
            }

            if (!string.IsNullOrEmpty(Properties.Settings.Default.TimeToSaveDefFF))
            {
                Console.WriteLine("TimeToSaveDefFF " + Properties.Settings.Default.TimeToSaveDefFF);
                timePickToDefault.Text = Properties.Settings.Default.TimeToSaveDefFF;
            }
            else
            {
                timePickToDefault.Enabled = false;
            }

            if (!string.IsNullOrEmpty(Properties.Settings.Default.dateMultipleFF))
            {
                Console.WriteLine("dateMultipleFF " + Properties.Settings.Default.dateMultipleFF);
                lbDate.Text = Properties.Settings.Default.dateMultipleFF;

                if (!string.IsNullOrEmpty(Properties.Settings.Default.TimeFrSaveSelFF))
                {
                    Console.WriteLine("TimeFrSaveSelFF " + Properties.Settings.Default.TimeFrSaveSelFF);
                    timePickFromSelect.Text = Properties.Settings.Default.TimeFrSaveSelFF;
                }
                if (!string.IsNullOrEmpty(Properties.Settings.Default.TimeToSaveSelFF))
                {
                    Console.WriteLine("TimeToSaveSelFF " + Properties.Settings.Default.TimeToSaveSelFF);
                    timePickToSelect.Text = Properties.Settings.Default.TimeToSaveSelFF;
                }
            }

            if (!string.IsNullOrEmpty(Properties.Settings.Default.defselectFF))
            {
                if (Properties.Settings.Default.defselectFF == "default")
                {
                    Console.WriteLine("panel1");
                    Properties.Settings.Default.defselectFF = "default";
                    panel1.BackColor = SystemColors.ControlLight;
                    panel2.BackColor = SystemColors.Control;

                    btOK.Enabled = false;
                    if (!string.IsNullOrEmpty(Properties.Settings.Default.SelectedPathFF))
                    {
                        btOK.Enabled = true;
                        timePickFromDefault.Enabled = true;
                        timePickToDefault.Enabled = true;
                    }
                    else
                    {
                        timePickFromDefault.Enabled = false;
                        timePickToDefault.Enabled = false;
                    }

                    Console.WriteLine(Properties.Settings.Default.SelectedPathFF);
                    btOpen.Enabled = false;
                    cbSingleFile.Enabled = false;

                    btDefault.Enabled = true;
                    txFromDate.Enabled = true;
                    btCalendar.Enabled = true;
                    timePickFromSelect.Enabled = false;
                    timePickToSelect.Enabled = false;
                }
                else if (Properties.Settings.Default.defselectFF == "select")
                {
                    Console.WriteLine("panel2");
                    Properties.Settings.Default.defselectFF = "select";
                    panel1.BackColor = SystemColors.Control;
                    panel2.BackColor = SystemColors.ControlLight;

                    btOK.Enabled = false;
                    if (!string.IsNullOrEmpty(Properties.Settings.Default.playMethod))
                    {
                        if (Properties.Settings.Default.playMethod == "single")
                        {
                            mainForm.countCh = 1;
                            cbSingleFile.Checked = true;
                            timePickFromSelect.Enabled = false;
                            timePickToSelect.Enabled = false;

                            if (!string.IsNullOrEmpty(Properties.Settings.Default.SelectedPathSingFF))
                            {
                                btOK.Enabled = true;
                            }
                        }
                        else if (Properties.Settings.Default.playMethod == "multiple")
                        {
                            cbSingleFile.Checked = false;
                            mainForm.countCh = 0;
                            timePickFromSelect.Enabled = true;
                            timePickToSelect.Enabled = true;

                            if (!string.IsNullOrEmpty(Properties.Settings.Default.SelectedPathMulFF))
                            {
                                btOK.Enabled = true;
                            }
                            else
                            {
                                btOK.Enabled = false;
                            }
                        }
                    }

                    btOpen.Enabled = true;
                    cbSingleFile.Enabled = true;

                    btDefault.Enabled = false;
                    txFromDate.Enabled = false;
                    btCalendar.Enabled = false;
                    timePickFromDefault.Enabled = false;
                    timePickToDefault.Enabled = false;
                }
            }

            BrowseFolder.ShowNewFolderButton = false;
        }

        /// <summary> select video directory used for default method</summary>
        private void btDefault_Click(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            //setcbRate();
            System.Globalization.CultureInfo cultureinformation = new System.Globalization.CultureInfo("en-US");

            System.Globalization.DateTimeFormatInfo dateTimeInfo = new System.Globalization.DateTimeFormatInfo();

            dateTimeInfo.DateSeparator = "/";
            dateTimeInfo.LongDatePattern = "dd MMMMM yyyy";
            dateTimeInfo.ShortDatePattern = "dd/MM/yyyy";
            dateTimeInfo.ShortTimePattern = "HH:mm";
            dateTimeInfo.LongTimePattern = "HH:mm:ss";

            cultureinformation.DateTimeFormat = dateTimeInfo;

            Application.CurrentCulture = cultureinformation;

            Thread.CurrentThread.CurrentCulture = cultureinformation;
            Thread.CurrentThread.CurrentUICulture = cultureinformation;

            DialogResult dr;
            string dirName = "";
            string filename = "";
            BrowseFolder.ShowNewFolderButton = false;

            if (i != 0)
            {
                monthCalendar1.Hide();
                i = 0;
            }

            if (!string.IsNullOrEmpty(Properties.Settings.Default.SelectedPathFF))
            {
                BrowseFolder.SelectedPath = Properties.Settings.Default.SelectedPathFF;
            }

            DateTime datefr = DateTime.ParseExact(txFromDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);

            if (!string.IsNullOrEmpty(Properties.Settings.Default.busidDefFF))
            {
                Console.WriteLine("SelectedPathFF " + Path.Combine(Properties.Settings.Default.defParentFF, datefr.ToString("yyyyMMdd")));
                if (Directory.Exists(Properties.Settings.Default.defParentFF + "\\" + datefr.ToString("yyyyMMdd")))
                {
                    BrowseFolder.SelectedPath = Properties.Settings.Default.defParentFF + "\\" + datefr.ToString("yyyyMMdd");
                    dr = BrowseFolder.ShowDialog();
                }
                else
                {
                    Console.WriteLine("SelectedPathFF " + Properties.Settings.Default.defParentFF + "\\" + datefr.ToString("yyyyMMdd"));
                    BrowseFolder.SelectedPath = Properties.Settings.Default.defParentFF;
                    dr = BrowseFolder.ShowDialog();
                }
                Console.WriteLine(BrowseFolder.SelectedPath);
            }
            else
            {
                dr = BrowseFolder.ShowDialog();
            }

            if (dr == DialogResult.OK)
            {
                mainForm.mediactrl();
                mainForm.stop();
                mainForm.timer1.Stop();
                mainForm.SeqNo1 = 0;
                mainForm.SeqNo2 = 0;
                mainForm.SeqNo3 = 0;
                mainForm.SeqNo4 = 0;
                mainForm.SeqNo5 = 0;
                mainForm.SeqNo6 = 0;
                mainForm.SeqNo7 = 0;
                mainForm.SeqNo8 = 0;
                mainForm.videoTrackBar.Value = 0;
                mainForm.lbStartDur.Text = "00:00:00";
                mainForm.lbEndDur.Text = "00:00:00";
                mainForm.a = 0;
                mainForm.b = 0;
                mainForm.c = 0;
                mainForm.d = 0;
                mainForm.e5 = 0;
                mainForm.f = 0;
                mainForm.g = 0;
                mainForm.h = 0;
                mainForm.ticky = 0;
                string iMinute, iHour, iSec;

                try
                {
                    Console.WriteLine("busid " + Path.GetFileName(BrowseFolder.SelectedPath));
                    Console.WriteLine("Length " + Path.GetFileName(BrowseFolder.SelectedPath).Length);

                    DirectoryInfo dinfo = null;
                    if (Path.GetFileName(BrowseFolder.SelectedPath).Any(char.IsDigit) && Path.GetFileName(BrowseFolder.SelectedPath).Length == 8)
                    {
                        Properties.Settings.Default.busidDefFF = Path.GetFileName(Path.GetDirectoryName(BrowseFolder.SelectedPath));
                        Console.WriteLine("busidDefFF " + Properties.Settings.Default.busidDefFF);

                        dinfo = new DirectoryInfo(BrowseFolder.SelectedPath);
                        dirName = Path.GetFileName(BrowseFolder.SelectedPath);
                    }
                    else
                    {
                        Properties.Settings.Default.busidDefFF = Path.GetFileName(BrowseFolder.SelectedPath);
                        Console.WriteLine("busidDefFF " + Properties.Settings.Default.busidDefFF);

                        string[] subdirs = Directory.GetDirectories(BrowseFolder.SelectedPath);
                        dirName = Path.GetFileName(subdirs[0].ToString());
                        dinfo = new DirectoryInfo(subdirs[0].ToString());
                    }

                    Console.WriteLine("Path GetDirectoryName " + dinfo);
                    FileInfo[] Files = dinfo.GetFiles();
                    List<string> primes = new List<string>();

                    foreach (FileInfo file in Files.OrderBy(d => d.Name))
                    {
                        string time = Path.GetFileNameWithoutExtension(file.ToString());
                        string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);

                        int setind = time.IndexOf('_');
                        string tt = time.Substring(0, setind);

                        if (file.ToString().Contains(ipcam))
                        {
                            setind = ttime.IndexOf('_');
                            tt = ttime.Substring(0, setind);
                            char cht = tt[tt.Length - 1];
                            tt = Convert.ToString(cht);
                            ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                        }

                        primes.Add(ttime);
                        primes.Sort();
                    }

                    if (Files.Length > 0)
                    {
                        filename = Path.GetFileName(primes[0].ToString());
                        iHour = filename.Substring(0, 2);
                        iMinute = filename.Substring(2, 2);
                        iSec = filename.Substring(4, 2);
                        filename = iHour + ":" + iMinute + ":" + iSec;

                        var oDate = DateTime.ParseExact(dirName, "yyyyMMdd", CultureInfo.InvariantCulture)
                        .ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);

                        Properties.Settings.Default.DateDefFF = dirName;
                        Properties.Settings.Default.SelectedPathFF = dinfo.ToString();
                        Properties.Settings.Default.defParentFF = Path.GetDirectoryName(dinfo.ToString());

                        Console.WriteLine("time " + oDate);
                        txFromDate.Value = Convert.ToDateTime(oDate);

                        timePickFromDefault.Text = filename;

                        timePickFromDefault.Enabled = true;
                        timePickToDefault.Enabled = true;
                        btOK.Enabled = true;
                    }
                    else
                    {
                        DirSearch(dinfo);
                    }
                }
                catch
                {
                    btOK.Enabled = false;
                }
            }
        }

        /// <summary> used to select date filter to load the video files for defaul method</summary>
        private void txFromDate_ValueChanged(object sender, EventArgs e)
        {
            RegionInfo LocalReg = System.Globalization.RegionInfo.CurrentRegion;
            String Region = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            String Language = System.Globalization.CultureInfo.CurrentUICulture.NativeName;

            monthCalendar1.SetDate(DateTime.ParseExact(txFromDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));

            string iMinute, iHour, iSec;
            string dirName = "";
            string filename = "";

            try
            {
                DirectoryInfo dinfo = null;

                dirName = DateTime.ParseExact(txFromDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                .ToString("yyyyMMdd", CultureInfo.InvariantCulture);

                Properties.Settings.Default.DateDefFF = dirName;
                Console.WriteLine("SelectedPathFF " + Properties.Settings.Default.SelectedPathFF);
                String dirNames = Path.Combine(Path.GetDirectoryName(Properties.Settings.Default.SelectedPathFF), dirName);

                dinfo = new DirectoryInfo(dirNames);
                FileInfo[] Files = dinfo.GetFiles();
                List<string> primes = new List<string>();

                foreach (FileInfo file in Files.OrderBy(d => d.Name))
                {
                    string time = Path.GetFileNameWithoutExtension(file.ToString());
                    string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);

                    int setind = time.IndexOf('_');
                    string tt = time.Substring(0, setind);

                    if (file.ToString().Contains(ipcam))
                    {
                        setind = ttime.IndexOf('_');
                        tt = ttime.Substring(0, setind);
                        char cht = tt[tt.Length - 1];
                        tt = Convert.ToString(cht);
                        ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                    }

                    primes.Add(ttime);
                    primes.Sort();
                }

                if (Files.Length > 0)
                {
                    filename = Path.GetFileName(primes[0].ToString());
                    iHour = filename.Substring(0, 2);
                    iMinute = filename.Substring(2, 2);
                    iSec = filename.Substring(4, 2);
                    filename = iHour + ":" + iMinute + ":" + iSec;

                    if (Path.GetFileName(BrowseFolder.SelectedPath).Length == 8)
                    {
                        dirName = Path.GetFileName(BrowseFolder.SelectedPath);
                    }

                    Properties.Settings.Default.DateDefFF = dirName;
                    Properties.Settings.Default.SelectedPathFF = dirNames;
                    timePickFromDefault.Text = filename;
                    btOK.Enabled = true;
                    timePickFromDefault.Enabled = true;
                    timePickToDefault.Enabled = true;
                }
                else if (Files.Length == 0)
                {
                    timePickFromDefault.Enabled = false;
                    timePickToDefault.Enabled = false;
                    btOK.Enabled = false;
                }
            }
            catch
            {
                btOK.Enabled = false;
                timePickFromDefault.Enabled = false;
                timePickToDefault.Enabled = false;
            }
        }

        /// <summary> check the panel 1 to activate default method</summary>
        private void panel1_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.defselectFF = "default";
            panel1.BackColor = SystemColors.ControlLight;
            panel2.BackColor = SystemColors.Control;

            btOpen.Enabled = false;
            cbSingleFile.Enabled = false;
            timePickFromSelect.Enabled = false;
            timePickToSelect.Enabled = false;

            btDefault.Enabled = true;
            txFromDate.Enabled = true;
            btCalendar.Enabled = true;
            timePickFromDefault.Enabled = true;
            timePickToDefault.Enabled = true;
            Console.WriteLine("SelectedPathFF " + Properties.Settings.Default.SelectedPathFF);

            if (!string.IsNullOrEmpty(Properties.Settings.Default.SelectedPathFF))
            {
                btOK.Enabled = true;
            }
            else
            {
                btOK.Enabled = false;
            }
            txFromDate_ValueChanged(sender, e);
        }

        /// <summary> check the panel 2 to activate select method</summary>
        private void panel2_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.defselectFF = "select";
            panel1.BackColor = SystemColors.Control;
            panel2.BackColor = SystemColors.ControlLight;

            btDefault.Enabled = false;
            txFromDate.Enabled = false;
            btCalendar.Enabled = false;
            timePickFromDefault.Enabled = false;
            timePickToDefault.Enabled = false;

            btOpen.Enabled = true;
            cbSingleFile.Enabled = true;
            timePickFromSelect.Enabled = true;
            timePickToSelect.Enabled = true;

            if (!string.IsNullOrEmpty(Properties.Settings.Default.SelectedPathMulFF) || !string.IsNullOrEmpty(Properties.Settings.Default.SelectedPathSingFF))
            {
                btOK.Enabled = true;
            }
            else
            {
                btOK.Enabled = false;
            }

            if (cbSingleFile.Checked)
            {
                timePickFromSelect.Enabled = false;
                timePickToSelect.Enabled = false;
            }
            else
            {
                timePickFromSelect.Enabled = true;
                timePickToSelect.Enabled = true;
            }
        }

        /// <summary> used to selecting start time filter to load the video files in default method</summary>
        private void timePickFromDefault_ValueChanged(object sender, EventArgs e)
        {
            if (timePickFromDefault.Text != timePickToDefault.Text)
            {
                TimeSpan t1 = TimeSpan.Parse(timePickFromDefault.Value.ToString("HH:mm:ss")).Add(TimeSpan.FromHours(1));
                TimeSpan t2 = TimeSpan.Parse(timePickToDefault.Value.ToString("HH:mm:ss"));

                double day2 = t2.TotalHours;
                int hour2 = t2.Hours;
                int minutes2 = t2.Minutes;
                int seconds2 = t2.Seconds;

                btOK.Enabled = true;

                if (t1 >= TimeSpan.Parse("1.00:00:01"))
                {
                    double day = t1.TotalHours;
                    int hour = t1.Hours;
                    int minutes = t1.Minutes;
                    int seconds = t1.Seconds;
                    t1 = new TimeSpan(hour, minutes, seconds);

                    if (t2 > TimeSpan.Parse("00:00:00") && t2 <= TimeSpan.Parse("01:00:00"))
                    {
                        crossDaydef = 1;
                    }
                    else
                    {
                        crossDaydef = 0;
                    }
                }
                else
                {
                    crossDaydef = 0;
                    if (t1 >= TimeSpan.Parse("1.00:00:00") && t2 <= TimeSpan.Parse("01:00:00"))
                    {
                        double day = t1.TotalHours;
                        int hour = t1.Hours;
                        int minutes = t1.Minutes;
                        int seconds = t1.Seconds;
                        t1 = new TimeSpan(hour, minutes, seconds);
                    }
                    else if (t1 >= TimeSpan.Parse("1.00:00:00") && t1.Subtract(TimeSpan.FromHours(1)) > t2)
                    {
                        Console.WriteLine("end time must not be longer than first time");
                        btOK.Enabled = false;
                        Console.WriteLine("not allowed ");
                    }
                }

                timePickToDefault.Text = t1.ToString();
            }
            else
            {
                btOK.Enabled = false;
            }

            Console.WriteLine("crossDaydef " + crossDaydef);
        }

        /// <summary> used to selecting end time filter to load the video files in default method</summary>
        private void timePickToDefault_ValueChanged(object sender, EventArgs e)
        {
            if (timePickFromDefault.Text != timePickToDefault.Text)
            {
                TimeSpan t1 = TimeSpan.Parse(timePickFromDefault.Value.ToString("HH:mm:ss")).Add(TimeSpan.FromHours(1));
                TimeSpan t2 = TimeSpan.Parse(timePickToDefault.Value.ToString("HH:mm:ss"));

                double day2 = t2.TotalHours;
                int hour2 = t2.Hours;
                int minutes2 = t2.Minutes;
                int seconds2 = t2.Seconds;

                btOK.Enabled = true;

                if (t1 >= TimeSpan.Parse("1.00:00:01"))
                {
                    crossDaydef = 1;
                    t2 = new TimeSpan(hour2, minutes2, seconds2);
                    if (t2 > TimeSpan.Parse("00:00:00") && t2 <= TimeSpan.Parse("01:00:00"))
                    {
                        timePickToDefault.Text = t2.ToString();
                        crossDaydef = 1;
                    }
                    else
                    {
                        crossDaydef = 0;
                    }
                }
                else
                {
                    crossDaydef = 0;
                    t2 = new TimeSpan(hour2, minutes2, seconds2);
                    if (t1 >= TimeSpan.Parse("23:00:00") && t2 > TimeSpan.Parse("00:00:00") && t2 <= TimeSpan.Parse("01:00:00"))
                    {
                        timePickToDefault.Text = t2.ToString();
                        crossDaydef = 1;
                    }
                    else if (t2 > TimeSpan.Parse("01:00:00"))
                    {
                        if (t1.Subtract(TimeSpan.FromHours(1)) > t2)
                        {
                            Console.WriteLine("end time must not be longer than first time");
                            btOK.Enabled = false;
                            Console.WriteLine("not allowed ");
                        }
                    }
                }
            }
            else
            {
                btOK.Enabled = false;
            }
            Console.WriteLine("crossDaydef " + crossDaydef);
        }

        /// <summary> select video directory used for select method</summary>
        private void btOpen_Click(object sender, EventArgs e)
        {
            DialogResult dr;
            string filename = "";

            if (cbSingleFile.Checked)
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Title = "Select video file..";
                openFileDialog.DefaultExt = ".mp4";
                openFileDialog.Filter = "Media Files|*.avi;*.mp4";
                openFileDialog.InitialDirectory = Properties.Settings.Default.SelectedPathSingFF;

                if (DialogResult.OK == openFileDialog.ShowDialog())
                {
                    mainForm.stop();
                    mainForm.timer1.Stop();
                    if (!string.IsNullOrEmpty(openFileDialog.FileName))
                    {
                        dirSingle = openFileDialog.FileName;
                        Properties.Settings.Default.SelectedPathSingFF = openFileDialog.FileName;
                        Properties.Settings.Default.dateSingleFF = Path.GetFileName(Path.GetDirectoryName(@openFileDialog.FileName));
                        lbDate.Text = Path.Combine("...\\" + Path.GetFileName(Path.GetDirectoryName(openFileDialog.FileName)), Path.GetFileName(openFileDialog.FileName));
                        Properties.Settings.Default.lbdatesingle = lbDate.Text;

                        string time = Path.GetFileNameWithoutExtension(openFileDialog.FileName);
                        string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                        var pos = ttime.IndexOf('_');
                        var lengt = ttime.Length;
                        int cek = lengt - pos;

                        if (cek != 7)
                        {
                            return;
                        }

                        int setind = time.IndexOf('_');
                        string tt = time.Substring(0, setind);
                        if (time.Contains(ipcam))
                        {
                            setind = ttime.IndexOf('_');
                            tt = ttime.Substring(0, setind);
                            char cht = tt[tt.Length - 1];
                            tt = Convert.ToString(cht);
                            ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                        }

                        string iHour = ttime.Substring(0, 2);
                        string iMinute = ttime.Substring(2, 2);
                        string iSec = ttime.Substring(4, 2);
                        time = iHour + ":" + iMinute + ":" + iSec;
                        timeFrom = TimeSpan.Parse(time);

                        DateTime MyDateTime = DateTime.ParseExact(Properties.Settings.Default.dateSingleFF, "yyyyMMdd", null);
                        //lbDate.Text = MyDateTime.ToString("MM/dd/yyyy");
                        btOK.Enabled = true;
                    }

                    Console.WriteLine("SelectedPathSingFF " + Properties.Settings.Default.SelectedPathSingFF);
                    Console.WriteLine("dateSingleFF " + Properties.Settings.Default.dateSingleFF);
                }
            }
            else
            {
                string dirName = "";

                if (!string.IsNullOrEmpty(Properties.Settings.Default.SelectedPathFF))
                {
                    BrowseFolder.SelectedPath = Properties.Settings.Default.SelectedPathFF;
                }

                Console.WriteLine("parent. " + Properties.Settings.Default.busidFF);
                if (!string.IsNullOrEmpty(Properties.Settings.Default.busidFF))
                {
                    Console.WriteLine("That path is exists.");
                    BrowseFolder.SelectedPath = Properties.Settings.Default.SelectedPathMulFF;
                    BrowseFolder.ShowNewFolderButton = false;
                    dr = BrowseFolder.ShowDialog();
                    Console.WriteLine(BrowseFolder.SelectedPath);
                }
                else
                {
                    dr = BrowseFolder.ShowDialog();
                }

                if (dr == DialogResult.OK)
                {
                    mainForm.mediactrl();
                    mainForm.stop();
                    mainForm.timer1.Stop();
                    mainForm.SeqNo1 = 0;
                    mainForm.SeqNo2 = 0;
                    mainForm.SeqNo3 = 0;
                    mainForm.SeqNo4 = 0;
                    mainForm.SeqNo5 = 0;
                    mainForm.SeqNo6 = 0;
                    mainForm.SeqNo7 = 0;
                    mainForm.SeqNo8 = 0;
                    mainForm.videoTrackBar.Value = 0;
                    mainForm.lbStartDur.Text = "00:00:00";
                    mainForm.lbEndDur.Text = "00:00:00";
                    mainForm.a = 0;
                    mainForm.b = 0;
                    mainForm.c = 0;
                    mainForm.d = 0;
                    mainForm.e5 = 0;
                    mainForm.f = 0;
                    mainForm.g = 0;
                    mainForm.h = 0;
                    mainForm.ticky = 0;
                    string iMinute, iHour, iSec;


                    try
                    {
                        Console.WriteLine("busid " + Path.GetFileName(BrowseFolder.SelectedPath));
                        Console.WriteLine("Length " + Path.GetFileName(BrowseFolder.SelectedPath).Length);

                        DirectoryInfo dinfo = null;
                        if (Path.GetFileName(BrowseFolder.SelectedPath).Any(char.IsDigit) && Path.GetFileName(BrowseFolder.SelectedPath).Length == 8)
                        {
                            Properties.Settings.Default.busidFF = Path.GetFileName(Path.GetDirectoryName(BrowseFolder.SelectedPath));
                            Console.WriteLine("busidFF " + Properties.Settings.Default.busidFF);

                            dinfo = new DirectoryInfo(BrowseFolder.SelectedPath);
                            Console.WriteLine("Path GetDirectoryName " + Path.GetDirectoryName(BrowseFolder.SelectedPath));
                            dirName = Path.GetFileName(BrowseFolder.SelectedPath);
                        }
                        else
                        {
                            Properties.Settings.Default.busidFF = Path.GetFileName(BrowseFolder.SelectedPath);
                            Console.WriteLine("busidFF " + Properties.Settings.Default.busidFF);

                            string[] subdirs = Directory.GetDirectories(BrowseFolder.SelectedPath);
                            dirName = Path.GetFileName(subdirs[0].ToString());
                            dinfo = new DirectoryInfo(subdirs[0].ToString());
                        }

                        Console.WriteLine("Path GetDirectoryName " + dinfo);
                        FileInfo[] Files = dinfo.GetFiles();
                        List<string> primes = new List<string>();

                        foreach (FileInfo file in Files.OrderBy(d => d.Name))
                        {
                            string time = Path.GetFileNameWithoutExtension(file.ToString());
                            string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);

                            int setind = time.IndexOf('_');
                            string tt = time.Substring(0, setind);

                            if (file.ToString().Contains(ipcam))
                            {
                                setind = ttime.IndexOf('_');
                                tt = ttime.Substring(0, setind);
                                char cht = tt[tt.Length - 1];
                                tt = Convert.ToString(cht);
                                ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                            }

                            primes.Add(ttime);
                            primes.Sort();
                        }

                        if (Files.Length > 0)
                        {
                            filename = Path.GetFileName(primes[0].ToString());
                            iHour = filename.Substring(0, 2);
                            iMinute = filename.Substring(2, 2);
                            iSec = filename.Substring(4, 2);
                            filename = iHour + ":" + iMinute + ":" + iSec;

                            var oDate = DateTime.ParseExact(dirName, "yyyyMMdd", CultureInfo.InvariantCulture)
                              .ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);

                            lbDate.Text = oDate.ToString();
                            //Modification on 2017-Feb-08
                            txFromDate.Text = oDate.ToString();

                            Properties.Settings.Default.dateMultipleFF = lbDate.Text;
                            Properties.Settings.Default.SelectedPathMulFF = dinfo.ToString();
                            Console.WriteLine("SelectedPathMulFF " + dinfo.ToString());
                            timePickFromSelect.Text = filename;
                            btOK.Enabled = true;
                        }
                        else
                        {
                            DirSearch(dinfo);
                        }
                    }
                    catch
                    {
                        btOK.Enabled = false;
                        lbDate.Text = "Invalid Folder Selected";
                    }
                }
            }
        }

        /// <summary> function used when the folder has multiple sub-subfolder</summary>
        void DirSearch(DirectoryInfo sDir)
        {
            List<string> primes = new List<string>();
            string filename = "";
            string iMinute, iHour, iSec;

            Properties.Settings.Default.busidDefFF = Path.GetFileName(sDir.ToString());
            Console.WriteLine("busidDefFF " + Properties.Settings.Default.busidDefFF);

            try
            {
                FileInfo[] Files = sDir.GetFiles();

                foreach (string d in Directory.GetDirectories(sDir.ToString()))
                {
                    string[] subdirs = Directory.GetDirectories(sDir.ToString());
                    string dirName = Path.GetFileName(subdirs[0].ToString());
                    sDir = new DirectoryInfo(subdirs[0].ToString());

                    Files = sDir.GetFiles();
                    foreach (FileInfo file in Files.OrderBy(e => e.Name))
                    {
                        string time = Path.GetFileNameWithoutExtension(file.ToString());
                        string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);

                        int setind = time.IndexOf('_');
                        string tt = time.Substring(0, setind);

                        if (file.ToString().Contains(ipcam))
                        {
                            setind = ttime.IndexOf('_');
                            tt = ttime.Substring(0, setind);
                            char cht = tt[tt.Length - 1];
                            tt = Convert.ToString(cht);
                            ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                        }

                        primes.Add(ttime);
                        primes.Sort();
                    }

                    if (Files.Length > 0)
                    {
                        filename = Path.GetFileName(primes[0].ToString());
                        iHour = filename.Substring(0, 2);
                        iMinute = filename.Substring(2, 2);
                        iSec = filename.Substring(4, 2);
                        filename = iHour + ":" + iMinute + ":" + iSec;

                        if (Path.GetFileName(sDir.ToString()).Length == 8)
                        {
                            dirName = Path.GetFileName(sDir.ToString());
                        }

                        string ddirName = dirName + "000000";
                        DateTime dt = DateTime.ParseExact(ddirName, formatString, null);
                        Console.WriteLine("time " + dt);
                        DateTime MyDateTime = DateTime.ParseExact(dirName, "yyyyMMdd", null);

                        if (Properties.Settings.Default.defselectFF == "default")
                        {
                            Properties.Settings.Default.DateDefFF = dirName;
                            Properties.Settings.Default.SelectedPathFF = sDir.ToString();
                            Properties.Settings.Default.defParentFF = Path.GetDirectoryName(sDir.ToString());
                            txFromDate.Text = MyDateTime.ToString("MM/dd/yyyy");
                            timePickFromDefault.Text = filename;
                        }
                        else if (Properties.Settings.Default.defselectFF == "select")
                        {
                            Properties.Settings.Default.dateMultipleFF = MyDateTime.ToString("dd MM yyyy");
                            Properties.Settings.Default.SelectedPathMulFF = sDir.ToString();
                            lbDate.Text = MyDateTime.ToString("MM/dd/yyyy");
                            timePickFromSelect.Text = filename;
                        }

                        Console.WriteLine("DateDefFF " + Properties.Settings.Default.DateDefFF);
                        Console.WriteLine("SelectedPathFF " + Properties.Settings.Default.SelectedPathFF);
                        Console.WriteLine("defParentFF " + Properties.Settings.Default.defParentFF);

                        Console.WriteLine("dateMultipleFF " + Properties.Settings.Default.dateMultipleFF);
                        Console.WriteLine("SelectedPathMulFF " + Properties.Settings.Default.SelectedPathMulFF);

                        timePickFromDefault.Enabled = true;
                        timePickToDefault.Enabled = true;
                        btOK.Enabled = true;
                    }
                }

                if (Files.Length == 0)
                {
                    DateTime MyDateTime = DateTime.ParseExact(Path.GetFileName(sDir.ToString()), "yyyyMMdd", null);
                    if (Properties.Settings.Default.defselectFF == "default")
                    {
                        txFromDate.Text = MyDateTime.ToString("MM/dd/yyyy");

                    }
                    else if (Properties.Settings.Default.defselectFF == "select")
                    {
                        lbDate.Text = MyDateTime.ToString("MM/dd/yyyy");
                    }

                    timePickFromDefault.Enabled = false;
                    timePickToDefault.Enabled = false;
                    btOK.Enabled = false;
                }
            }
            catch (System.Exception excpt)
            {
                Console.WriteLine(excpt);
                DirSearch(sDir);
            }
        }

        /// <summary> load filter from selected directory, date and time to main form</summary>
        private void btOK_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.defselectFF == "default")
            {
                Properties.Settings.Default.TimeFrSaveDefFF = timePickFromDefault.Text;
                Properties.Settings.Default.TimeToSaveDefFF = timePickToDefault.Text;
                Properties.Settings.Default.defselectFF = "default";
                mainForm.countCh = 0;

                Properties.Settings.Default.DateDefFF = DateTime.ParseExact(txFromDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                                                        .ToString("yyyyMMdd");
                Console.WriteLine("DateDefFF " + Properties.Settings.Default.DateDefFF);
                Console.WriteLine("SelectedPathFF " + Properties.Settings.Default.SelectedPathFF);
                Console.WriteLine("defParentFF " + Properties.Settings.Default.defParentFF);

                Console.WriteLine("SelectedPathMulFF " + Path.GetFileName(Properties.Settings.Default.SelectedPathFF));
                Properties.Settings.Default.defParentFF = Path.GetDirectoryName(Properties.Settings.Default.SelectedPathFF);
                Console.WriteLine("Path GetDirectoryName " + Path.Combine(Path.GetDirectoryName(Properties.Settings.Default.SelectedPathFF), Properties.Settings.Default.DateDefFF));

                string pathCombine = Path.Combine(Properties.Settings.Default.defParentFF, Properties.Settings.Default.DateDefFF);
                Properties.Settings.Default.SelectedPathFF = pathCombine;

                mainForm.bgProcessUpdate(pathCombine, TimeSpan.Parse(timePickFromDefault.Text), TimeSpan.Parse(timePickToDefault.Text), Properties.Settings.Default.DateDefFF, crossDaydef, "multiple");
                Properties.Settings.Default.loadStatusFF = "load";
                this.Close();
            }
            else if (Properties.Settings.Default.defselectFF == "select")
            {
                //Properties.Settings.Default.defselectFF = "select";
                if (cbSingleFile.Checked)
                {
                    if (!string.IsNullOrEmpty(Properties.Settings.Default.SelectedPathSingFF))
                    {
                        Properties.Settings.Default.playMethod = "single";
                        mainForm.SeqNo1 = 0;
                        mainForm.SeqNo2 = 0;
                        mainForm.SeqNo3 = 0;
                        mainForm.SeqNo4 = 0;
                        mainForm.SeqNo5 = 0;
                        mainForm.SeqNo6 = 0;
                        mainForm.SeqNo7 = 0;
                        mainForm.SeqNo8 = 0;
                        mainForm.videoTrackBar.Value = 0;
                        mainForm.lbStartDur.Text = "00:00:00";
                        mainForm.lbEndDur.Text = "00:00:00";
                        mainForm.a = 0;
                        mainForm.b = 0;
                        mainForm.c = 0;
                        mainForm.d = 0;
                        mainForm.e5 = 0;
                        mainForm.f = 0;
                        mainForm.g = 0;
                        mainForm.h = 0;
                        mainForm.countCh = 1;
                        mainForm.ticky = 0;
                        mainForm.setFullScreen();

                        Properties.Settings.Default.SelectedPathSingFF = dirSingle;
                        mainForm.bgProcessUpdate(dirSingle, timeFrom, endtime, Properties.Settings.Default.dateSingleFF, crossDay, "single");
                        Properties.Settings.Default.loadStatusFF = "load";
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show(this, "select file first");
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Properties.Settings.Default.SelectedPathMulFF))
                    {
                        Properties.Settings.Default.playMethod = "multiple";
                        Properties.Settings.Default.TimeFrSaveSelFF = timePickFromSelect.Text;
                        Properties.Settings.Default.TimeToSaveSelFF = timePickToSelect.Text;

                        mainForm.countCh = 0;
                        timeFrom = TimeSpan.Parse(timePickFromSelect.Value.ToString("HH:mm:ss"));
                        endtime = TimeSpan.Parse(timePickToSelect.Value.ToString("HH:mm:ss"));

                        var oDate = DateTime.ParseExact(txFromDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                                                        .ToString("yyyyMMdd");
                        //lbDate.Text = oDate.ToString();

                        //DateTime dtimes = Convert.ToDateTime(lbDate.Text);

                        Console.WriteLine(Properties.Settings.Default.SelectedPathMulFF);
                        Console.WriteLine(Properties.Settings.Default.dateMultipleFF);
                        //Console.WriteLine(dtimes.ToString("yyyyMMdd"));

                        mainForm.bgProcessUpdate(Properties.Settings.Default.SelectedPathMulFF, timeFrom, endtime, oDate, crossDay, "multiple");
                        Properties.Settings.Default.loadStatusFF = "load";
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show(this, "select folder first");
                    }
                }
            }
        }

        /// <summary> used to selecting start time filter to load the video files in select method</summary>
        private void timePickFromSelect_ValueChanged(object sender, EventArgs e)
        {
            if (timePickFromSelect.Text != timePickToSelect.Text)
            {
                TimeSpan t1 = TimeSpan.Parse(timePickFromSelect.Value.ToString("HH:mm:ss")).Add(TimeSpan.FromHours(1));
                TimeSpan t2 = TimeSpan.Parse(timePickToSelect.Value.ToString("HH:mm:ss"));

                double day2 = t2.TotalHours;
                int hour2 = t2.Hours;
                int minutes2 = t2.Minutes;
                int seconds2 = t2.Seconds;

                btOK.Enabled = true;

                if (t1 >= TimeSpan.Parse("1.00:00:01"))
                {
                    double day = t1.TotalHours;
                    int hour = t1.Hours;
                    int minutes = t1.Minutes;
                    int seconds = t1.Seconds;
                    t1 = new TimeSpan(hour, minutes, seconds);

                    if (t2 > TimeSpan.Parse("00:00:00") && t2 <= TimeSpan.Parse("01:00:00"))
                    {
                        crossDay = 1;
                    }
                    else
                    {
                        crossDay = 0;
                    }
                }
                else
                {
                    crossDay = 0;
                    if (t1 >= TimeSpan.Parse("1.00:00:00") && t2 <= TimeSpan.Parse("01:00:00"))
                    {
                        double day = t1.TotalHours;
                        int hour = t1.Hours;
                        int minutes = t1.Minutes;
                        int seconds = t1.Seconds;
                        t1 = new TimeSpan(hour, minutes, seconds);
                    }
                    else if (t1 >= TimeSpan.Parse("1.00:00:00") && t1.Subtract(TimeSpan.FromHours(1)) > t2)
                    {
                        Console.WriteLine("end time must not be longer than first time");
                        btOK.Enabled = false;
                        Console.WriteLine("not allowed ");
                    }
                }

                Console.WriteLine("t1.ToString() " + t1.ToString());
                timePickToSelect.Text = t1.ToString();
                Console.WriteLine("crossDay" + crossDay);
            }
            else
            {
                btOK.Enabled = false;
            }
        }

        /// <summary> used to selecting end time filter to load the video files in default method</summary>
        private void timePickToSelect_ValueChanged(object sender, EventArgs e)
        {
            if (timePickFromSelect.Text != timePickToSelect.Text)
            {
                TimeSpan t1 = TimeSpan.Parse(timePickFromSelect.Value.ToString("HH:mm:ss")).Add(TimeSpan.FromHours(1));
                TimeSpan t2 = TimeSpan.Parse(timePickToSelect.Value.ToString("HH:mm:ss"));

                double day2 = t2.TotalHours;
                int hour2 = t2.Hours;
                int minutes2 = t2.Minutes;
                int seconds2 = t2.Seconds;

                btOK.Enabled = true;

                if (t1 >= TimeSpan.Parse("1.00:00:01"))
                {
                    crossDay = 1;
                    t2 = new TimeSpan(hour2, minutes2, seconds2);
                    if (t2 > TimeSpan.Parse("00:00:00") && t2 <= TimeSpan.Parse("01:00:00"))
                    {
                        timePickToSelect.Text = t2.ToString();
                        crossDay = 1;
                    }
                    else
                    {
                        crossDay = 0;
                    }
                }
                else
                {
                    crossDay = 0;
                    t2 = new TimeSpan(hour2, minutes2, seconds2);
                    if (t1 >= TimeSpan.Parse("23:00:00") && t2 > TimeSpan.Parse("00:00:00") && t2 <= TimeSpan.Parse("01:00:00"))
                    {
                        timePickToSelect.Text = t2.ToString();
                        crossDay = 1;
                    }
                    else if (t2 > TimeSpan.Parse("01:00:00"))
                    {
                        if (t1.Subtract(TimeSpan.FromHours(1)) > t2)
                        {
                            Console.WriteLine("end time must not be longer than first time");
                            btOK.Enabled = false;
                            Console.WriteLine("not allowed ");
                        }
                    }
                }
            }
            else
            {
                btOK.Enabled = false;
            }

            Console.WriteLine("crossDay" + crossDay);
        }

        /// <summary> select video file for single video</summary>
        private void cbSingleFile_CheckedChanged(object sender, EventArgs e)
        {
            if (cbSingleFile.Checked)
            {
                Properties.Settings.Default.playMethod = "single";
                timePickFromSelect.Enabled = false;
                timePickToSelect.Enabled = false;

                if (!string.IsNullOrEmpty(Properties.Settings.Default.lbdatesingle))
                {
                    lbDate.Text = Properties.Settings.Default.lbdatesingle;
                    btOK.Enabled = true;
                }
                else
                {
                    lbDate.Text = "";
                    btOK.Enabled = false;
                }
            }
            else
            {
                Properties.Settings.Default.playMethod = "multiple";
                timePickFromSelect.Enabled = true;
                timePickToSelect.Enabled = true;
                if (!string.IsNullOrEmpty(Properties.Settings.Default.dateMultipleFF))
                {
                    lbDate.Text = Properties.Settings.Default.dateMultipleFF;
                    btOK.Enabled = true;
                }
                else
                {
                    lbDate.Text = "";
                    btOK.Enabled = false;
                }
            }
        }

        /// <summary> used to open/close calendar</summary>
        private void btCalendar_Click(object sender, EventArgs e)
        {
            if (i == 0)
            {
                monthCalendar1.Show();
                i = 1;
            }
            else if (i != 0)
            {
                monthCalendar1.Hide();
                i = 0;
            }
        }

        /// <summary> used to change date from calendar</summary>
        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            txFromDate.Value = monthCalendar1.SelectionStart;
        }

        /// <summary> cancel loading the video</summary>
        private void btClose_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.loadStatusFF = "not load";
            if (Properties.Settings.Default.defselectFF == "default")
            {
                Properties.Settings.Default.DateDefFF = DateTime.ParseExact(txFromDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                                                        .ToString("yyyyMMdd");

                Properties.Settings.Default.TimeFrSaveDefFF = timePickFromDefault.Text;
                Properties.Settings.Default.TimeToSaveDefFF = timePickToDefault.Text;
                Properties.Settings.Default.defselectFF = "default";

                Console.WriteLine(Properties.Settings.Default.DateDefFF);

                mainForm.countCh = 0;
                Properties.Settings.Default.loadStatusFF = "not load";

                if (string.IsNullOrEmpty(Properties.Settings.Default.SelectedPathFF))
                {
                    mainForm.btPlay.Enabled = false;
                    mainForm.btFast4x.Enabled = false;
                    mainForm.btFast8x.Enabled = false;
                }
            }
            else if (Properties.Settings.Default.defselectFF == "select")
            {
                Properties.Settings.Default.TimeFrSaveSelFF = timePickFromSelect.Text;
                Properties.Settings.Default.TimeToSaveSelFF = timePickToSelect.Text;
                Properties.Settings.Default.dateMultipleFF = lbDate.Text;
                Console.WriteLine("lbdate " + lbDate.Text);

                if (cbSingleFile.Checked)
                {
                    Properties.Settings.Default.playMethod = "single";
                    if (string.IsNullOrEmpty(Properties.Settings.Default.SelectedPathSingFF))
                    {
                        mainForm.btPlay.Enabled = false;
                        mainForm.btFast4x.Enabled = false;
                        mainForm.btFast8x.Enabled = false;
                    }
                }
                else
                {
                    Properties.Settings.Default.playMethod = "multiple";
                    if (string.IsNullOrEmpty(Properties.Settings.Default.SelectedPathMulFF))
                    {
                        mainForm.btPlay.Enabled = false;
                        mainForm.btFast4x.Enabled = false;
                        mainForm.btFast8x.Enabled = false;
                    }
                }
            }


            this.Close();
        }

        private void FilterFile_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Properties.Settings.Default.Save();
        }
    }
}
