using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace mtvplayer
{
    /// <summary>
    /// Class used to windows local library
    /// </summary>
    public class WinApi
    {
        //[DllImport("user32.dll")]
        //public static extern void DisableProcessWindowsGhosting();

        [DllImport("user32.dll", EntryPoint = "GetSystemMetrics")]
        public static extern int GetSystemMetrics(int which);

        [DllImport("user32.dll")]
        public static extern void 
            SetWindowPos(IntPtr hwnd, IntPtr hwndInsertAfter,
                         int X, int Y, int width, int height, uint flags);        
        
        private const int SM_CXSCREEN = 0;
	    private const int SM_CYSCREEN = 1;
        private static IntPtr HWND_TOP = IntPtr.Zero;
        private const int SWP_SHOWWINDOW = 64; // 0x0040
        
        public static int ScreenX
        {
            get { return GetSystemMetrics(SM_CXSCREEN);}
        }
        
        public static int ScreenY
        {
            get { return GetSystemMetrics(SM_CYSCREEN);}
        }
        
        public static void SetWinFullScreen(IntPtr hwnd)
        {
            SetWindowPos(hwnd, HWND_TOP, 0, 0, ScreenX, ScreenY, SWP_SHOWWINDOW);
        }
    }
    
    /// <summary>
    /// Class used to preserve / restore state of the form
    /// </summary>
    public class FormState
    {
        public void Maximize(Form targetForm)
        {
                targetForm.WindowState = FormWindowState.Maximized;
                targetForm.FormBorderStyle = FormBorderStyle.None;
                WinApi.SetWinFullScreen(targetForm.Handle);
        }
        
        public void Restore(Form targetForm)
        {
            targetForm.WindowState = FormWindowState.Normal;
            targetForm.FormBorderStyle = FormBorderStyle.FixedSingle;
        }
    }
}
