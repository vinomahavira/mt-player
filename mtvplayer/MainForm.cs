﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using DShowNET;
using System.Collections.Generic;
using DirectShowLib;
using DirectShowLib.DES;
using Microsoft.VisualBasic.PowerPacks;
using System.Diagnostics;
using mtvplayer;
using System.Globalization;

namespace mtvplayer
{
    /// <summary>
    /// this is the main form of the MT Video Player Application
    /// </summary>
    public partial class MainForm : Form
    {
        /// <summary> get full path of the application</summary>
        //string pathdirect = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        //string pathdirect = "C:\\";



        #region Variable
        string pathdirect = System.Environment.GetEnvironmentVariable("temp");
        public string snapPath = null;
        public string recPath = null;

        /// <summary> call empty video resource</summary>
        string EmptyVideo;

        /// <summary> detect first video is empty or not</summary>
        bool IsEmpty = true;

        /// <summary> call function from OpenFile class</summary>
        //OpenFile open = null;
        FilterFile open = null;
        finish finish = null;

        /// <summary> call function from FormState class</summary>
        FormState formState = new FormState();

        /// <summary> call function from ProgressRecord class</summary>
        ProgressRecord progressDialog = null;

        /// <summary> call function from progressLoad class </summary>
        progressLoad pLoad = null;

        /// <summary> put status of the video </summary>
        public string VidStatus = "";

        /// <summary> set video rate </summary>
        double rate;
        double drate;

        /// <summary> set start video duration in double </summary>
        double startDuration;
        /// <summary> set start video duration in string </summary>
        String strTimeTB;

        /// <summary> set end video duration in double </summary>
        double endDuration;
        /// <summary> set end video duration in string </summary>
        String endDurStr;

        /// <summary> set first video sequence for channel 1 </summary>
        int strSeq1;
        /// <summary> set first video sequence for channel 2 </summary>
        int strSeq2;
        /// <summary> set first video sequence for channel 3 </summary>
        int strSeq3;
        /// <summary> set first video sequence for channel 4 </summary>
        int strSeq4;
        /// <summary> set first video sequence for channel 5 </summary>
        int strSeq5;
        /// <summary> set first video sequence for channel 6 </summary>
        int strSeq6;
        /// <summary> set first video sequence for channel 7 </summary>
        int strSeq7;
        /// <summary> set first video sequence for channel 8 </summary>
        int strSeq8;

        /// <summary> set video path source for snapshot for channel 1 </summary>
        string pathSnap1;
        /// <summary> set video path source for snapshot for channel 2 </summary>
        string pathSnap2;
        /// <summary> set video path source for snapshot for channel 3 </summary>
        string pathSnap3;
        /// <summary> set video path source for snapshot for channel 4 </summary>
        string pathSnap4;
        /// <summary> set video path source for snapshot for channel 5 </summary>
        string pathSnap5;
        /// <summary> set video path source for snapshot for channel 6 </summary>
        string pathSnap6;
        /// <summary> set video path source for snapshot for channel 7 </summary>
        string pathSnap7;
        /// <summary> set video path source for snapshot for channel 8 </summary>
        string pathSnap8;

        /// <summary> get current video time position for snapshot time </summary>
        double snaptime;

        /// <summary> set video snapshot time </summary>
        String snapTimeTB;

        /// <summary> detect many channel to taking snapshots </summary>
        public int countCh = 0;

        /// <summary> set video path source for start video recording for channel 1</summary>
        string pathrec1a;
        /// <summary> set video path source for start video recording for channel 2</summary>
        string pathrec2a;
        /// <summary> set video path source for start video recording for channel 3</summary>
        string pathrec3a;
        /// <summary> set video path source for start video recording for channel 4</summary>
        string pathrec4a;
        /// <summary> set video path source for start video recording for channel 5</summary>
        string pathrec5a;
        /// <summary> set video path source for start video recording for channel 6</summary>
        string pathrec6a;
        /// <summary> set video path source for start video recording for channel 7</summary>
        string pathrec7a;
        /// <summary> set video path source for start video recording for channel 8</summary>
        string pathrec8a;

        /// <summary> set video path source for next video recording for channel 1</summary>
        string pathrec1b;
        /// <summary> set video path source for next video recording for channel 2</summary>
        string pathrec2b;
        /// <summary> set video path source for next video recording for channel 3</summary>
        string pathrec3b;
        /// <summary> set video path source for next video recording for channel 4</summary>
        string pathrec4b;
        /// <summary> set video path source for next video recording for channel 5</summary>
        string pathrec5b;
        /// <summary> set video path source for next video recording for channel 6</summary>
        string pathrec6b;
        /// <summary> set video path source for next video recording for channel 7</summary>
        string pathrec7b;
        /// <summary> set video path source for next video recording for channel 8</summary>
        string pathrec8b;

        /// <summary> setting target video recording path. </summary>
        string recFolder;

        /// <summary> setting target video recording path. </summary>
        string inPath;

        /// <summary> setting target video with long path. </summary>
        string directpath;

        /// <summary> setting target video recording path with busID. </summary>
        string numberPath;

        /// <summary> setting target video recording path from dir. </summary>
        string dPath;

        /// <summary> get name of subdirectory videopath, name formatted by date ddMMyyyy. </summary>
        string dateRec;

        /// <summary> get player mode single file or multiple. </summary>
        string playmode = "";

        /// <summary> variable to check time filter through the day or not. </summary>
        int crossday;

        /// <summary> variable to check folder path through the day or not. </summary>
        int FolderName, FolderNameExt;

        /// <summary> variable to storing the video duration get from media info. </summary>
        double durat;

        /// <summary> variable to storing the video duration filter time range. </summary>
        double duration = 0.0;
        int numStat = 0;

        /// <summary> storing start time of video in channel 1</summary>
        TimeSpan strtm1 = TimeSpan.Parse("00:00:00");
        /// <summary> storing start time of video in channel 2</summary>
        TimeSpan strtm2 = TimeSpan.Parse("00:00:00");
        /// <summary> storing start time of video in channel 3</summary>
        TimeSpan strtm3 = TimeSpan.Parse("00:00:00");
        /// <summary> storing start time of video in channel 4</summary>
        TimeSpan strtm4 = TimeSpan.Parse("00:00:00");
        /// <summary> storing start time of video in channel 5</summary>
        TimeSpan strtm5 = TimeSpan.Parse("00:00:00");
        /// <summary> storing start time of video in channel 6</summary>
        TimeSpan strtm6 = TimeSpan.Parse("00:00:00");
        /// <summary> storing start time of video in channel 7</summary>
        TimeSpan strtm7 = TimeSpan.Parse("00:00:00");
        /// <summary> storing start time of video in channel 8</summary>
        TimeSpan strtm8 = TimeSpan.Parse("00:00:00");

        /// <summary> storing start video recording in timespan format, select time from trackbar</summary>
        TimeSpan strDurName = TimeSpan.Parse("00:00:00");

        /// <summary> storing end video recording in timespan format, select time from trackbar</summary>
        TimeSpan endDurName = TimeSpan.Parse("00:00:00");

        /// <summary> storing start video for recording in timespan format for channel 1</summary>
        TimeSpan strRec1a = TimeSpan.Parse("00:00:00");
        /// <summary> storing start video for recording in timespan format for channel 2</summary>
        TimeSpan strRec2a = TimeSpan.Parse("00:00:00");
        /// <summary> storing start video for recording in timespan format for channel 3</summary>
        TimeSpan strRec3a = TimeSpan.Parse("00:00:00");
        /// <summary> storing start video for recording in timespan format for channel 4</summary>
        TimeSpan strRec4a = TimeSpan.Parse("00:00:00");
        /// <summary> storing start video for recording in timespan format for channel 5</summary>
        TimeSpan strRec5a = TimeSpan.Parse("00:00:00");
        /// <summary> storing start video for recording in timespan format for channel 6</summary>
        TimeSpan strRec6a = TimeSpan.Parse("00:00:00");
        /// <summary> storing start video for recording in timespan format for channel 7</summary>
        TimeSpan strRec7a = TimeSpan.Parse("00:00:00");
        /// <summary> storing start video for recording in timespan format for channel 8</summary>
        TimeSpan strRec8a = TimeSpan.Parse("00:00:00");

        /// <summary> storing start next video for recording in timespan format for channel 1</summary>
        TimeSpan strRec1b = TimeSpan.Parse("00:00:00");
        /// <summary> storing start next video for recording in timespan format for channel 2</summary>
        TimeSpan strRec2b = TimeSpan.Parse("00:00:00");
        /// <summary> storing start next video for recording in timespan format for channel 3</summary>
        TimeSpan strRec3b = TimeSpan.Parse("00:00:00");
        /// <summary> storing start next video for recording in timespan format for channel 4</summary>
        TimeSpan strRec4b = TimeSpan.Parse("00:00:00");
        /// <summary> storing start next video for recording in timespan format for channel 5</summary>
        TimeSpan strRec5b = TimeSpan.Parse("00:00:00");
        /// <summary> storing start next video for recording in timespan format for channel 6</summary>
        TimeSpan strRec6b = TimeSpan.Parse("00:00:00");
        /// <summary> storing start next video for recording in timespan format for channel 7</summary>
        TimeSpan strRec7b = TimeSpan.Parse("00:00:00");
        /// <summary> storing start next video for recording in timespan format for channel 8</summary>
        TimeSpan strRec8b = TimeSpan.Parse("00:00:00");

        /// <summary> storing duration video recording in timespan format for channel 1</summary>
        TimeSpan durRec1 = TimeSpan.Parse("00:00:00");
        /// <summary> storing duration video recording in timespan format for channel 2</summary>
        TimeSpan durRec2 = TimeSpan.Parse("00:00:00");
        /// <summary> storing duration video recording in timespan format for channel 3</summary>
        TimeSpan durRec3 = TimeSpan.Parse("00:00:00");
        /// <summary> storing duration video recording in timespan format for channel 4</summary>
        TimeSpan durRec4 = TimeSpan.Parse("00:00:00");
        /// <summary> storing duration video recording in timespan format for channel 5</summary>
        TimeSpan durRec5 = TimeSpan.Parse("00:00:00");
        /// <summary> storing duration video recording in timespan format for channel 6</summary>
        TimeSpan durRec6 = TimeSpan.Parse("00:00:00");
        /// <summary> storing duration video recording in timespan format for channel 7</summary>
        TimeSpan durRec7 = TimeSpan.Parse("00:00:00");
        /// <summary> storing duration video recording in timespan format for channel 8</summary>
        TimeSpan durRec8 = TimeSpan.Parse("00:00:00");

        /// <summary> storing snpshot time in timespan format, select from trackbar</summary>
        TimeSpan snap = TimeSpan.Parse("00:00:00");

        /// <summary> start filter time to load video</summary>
        TimeSpan fromTime;

        /// <summary> end filter time to load video</summary>
        TimeSpan endtime;
        TimeSpan endVtime;

        /// <summary> check the video type ipcam or nvs</summary>
        public string type = "";

        /// <summary> set the initial sequence state of video, used for trackbar only in channel 1</summary>
        public int a = 0;
        /// <summary> set the initial sequence state of video, used for trackbar only in channel 2</summary>
        public int b = 0;
        /// <summary> set the initial sequence state of video, used for trackbar only in channel 3</summary>
        public int c = 0;
        /// <summary> set the initial sequence state of video, used for trackbar only in channel 4</summary>
        public int d = 0;
        /// <summary> set the initial sequence state of video, used for trackbar only in channel 5</summary>
        public int e5 = 0;
        /// <summary> set the initial sequence state of video, used for trackbar only in channel 6</summary>
        public int f = 0;
        /// <summary> set the initial sequence state of video, used for trackbar only in channel 7</summary>
        public int g = 0;
        /// <summary> set the initial sequence state of video, used for trackbar only in channel 8</summary>
        public int h = 0;

        /// <summary> set the initial sequence state of video in channel 1</summary>
        public int SeqNo1 = 0;
        /// <summary> set the initial sequence state of video in channel 2 </summary>
        public int SeqNo2 = 0;
        /// <summary> set the initial sequence state of video in channel 3 </summary>
        public int SeqNo3 = 0;
        /// <summary> set the initial sequence state of video in channel 4 </summary>
        public int SeqNo4 = 0;
        /// <summary> set the initial sequence state of video in channel 4 </summary>
        public int SeqNo5 = 0;
        /// <summary> set the initial sequence state of video in channel 6 </summary>
        public int SeqNo6 = 0;
        /// <summary> set the initial sequence state of video in channel 7 </summary>
        public int SeqNo7 = 0;
        /// <summary> set the initial sequence state of video in channel 8 </summary>
        public int SeqNo8 = 0;


        /// <summary> set video's date selected. </summary>
        string dateFrom;

        /// <summary> set video path directory. </summary>
        public string pathDir;

        /// <summary> set video path directory combine with busID. </summary>
        public string combPath;

        /// <summary> set tick value of timer </summary>
        public int ticky = 0;

        /// <summary> set timestart of video files </summary>
        TimeSpan timestart = TimeSpan.Parse("23:59:59");

        /// <summary> file name of clip in channel 1. </summary>
        private string clipFile1;
        /// <summary> file name of clip in channel 2. </summary>
        private string clipFile2;
        /// <summary> file name of clip in channel 3. </summary>
        private string clipFile3;
        /// <summary> file name of clip in channel 4. </summary>
        private string clipFile4;
        /// <summary> file name of clip in channel 5. </summary>
        private string clipFile5;
        /// <summary> file name of clip in channel 6. </summary>
        private string clipFile6;
        /// <summary> file name of clip in channel 7. </summary>
        private string clipFile7;
        /// <summary> file name of clip in channel 8. </summary>
        private string clipFile8;

        /// <summary> type of clip, video / audio in channel 1. </summary>
        private ClipType clipType1;
        /// <summary> type of clip, video / audio in channel 2. </summary>
        private ClipType clipType2;
        /// <summary> type of clip, video / audio in channel 3. </summary>
        private ClipType clipType3;
        /// <summary> type of clip, video / audio in channel 4. </summary>
        private ClipType clipType4;
        /// <summary> type of clip, video / audio in channel 5. </summary>
        private ClipType clipType5;
        /// <summary> type of clip, video / audio in channel 6. </summary>
        private ClipType clipType6;
        /// <summary> type of clip, video / audio in channel 8. </summary>
        private ClipType clipType7;
        /// <summary> type of clip, video / audio in channel 9. </summary>
        private ClipType clipType8;

        /// <summary> state of when playing in channel 1. </summary>
        private PlayState playState1;
        /// <summary> state of when playing in channel 2. </summary>
        private PlayState playState2;
        /// <summary> state of when playing in channel 3. </summary>
        private PlayState playState3;
        /// <summary> state of when playing in channel 4. </summary>
        private PlayState playState4;
        /// <summary> state of when playing in channel 5. </summary>
        private PlayState playState5;
        /// <summary> state of when playing in channel 6. </summary>
        private PlayState playState6;
        /// <summary> state of when playing in channel 7. </summary>
        private PlayState playState7;
        /// <summary> state of when playing in channel 8. </summary>
        private PlayState playState8;

        /// <summary> set the basic current video state. </summary>
        public VideoState currentVideoState = VideoState.Stopped;

        /// <summary> filter graph that renders the specified file. </summary>
        private DShowNET.IGraphBuilder graphBuilder;
        /// <summary> filter graph that renders the specified file in channel 1. </summary>
        private DShowNET.IGraphBuilder graphBuilder1;
        /// <summary> filter graph that renders the specified file in channel 2. </summary>
        private DShowNET.IGraphBuilder graphBuilder2;
        /// <summary> filter graph that renders the specified file in channel 3. </summary>
        private DShowNET.IGraphBuilder graphBuilder3;
        /// <summary> filter graph that renders the specified file in channel 4. </summary>
        private DShowNET.IGraphBuilder graphBuilder4;
        /// <summary> filter graph that renders the specified file in channel 5. </summary>
        private DShowNET.IGraphBuilder graphBuilder5;
        /// <summary> filter graph that renders the specified file in channel 6. </summary>
        private DShowNET.IGraphBuilder graphBuilder6;
        /// <summary> filter graph that renders the specified file in channel 7. </summary>
        private DShowNET.IGraphBuilder graphBuilder7;
        /// <summary> filter graph that renders the specified file in channel 8. </summary>
        private DShowNET.IGraphBuilder graphBuilder8;

        /// <summary> control interface for channel 1. </summary>
        private DShowNET.IMediaControl mediaCtrl1;
        /// <summary> control interface for channel 2. </summary>
        private DShowNET.IMediaControl mediaCtrl2;
        /// <summary> control interface for channel 3. </summary>
        private DShowNET.IMediaControl mediaCtrl3;
        /// <summary> control interface for channel 4. </summary>
        private DShowNET.IMediaControl mediaCtrl4;
        /// <summary> control interface for channel 5. </summary>
        private DShowNET.IMediaControl mediaCtrl5;
        /// <summary> control interface for channel 6. </summary>
        private DShowNET.IMediaControl mediaCtrl6;
        /// <summary> control interface for channel 7. </summary>
        private DShowNET.IMediaControl mediaCtrl7;
        /// <summary> control interface for channel 8. </summary>
        private DShowNET.IMediaControl mediaCtrl8;

        /// <summary> graph event interface for channel 1. </summary>
        private DShowNET.IMediaEventEx mediaEvt1;
        /// <summary> graph event interface for channel 2. </summary>
        private DShowNET.IMediaEventEx mediaEvt2;
        /// <summary> graph event interface for channel 3. </summary>
        private DShowNET.IMediaEventEx mediaEvt3;
        /// <summary> graph event interface for channel 4. </summary>
        private DShowNET.IMediaEventEx mediaEvt4;
        /// <summary> graph event interface for channel 5. </summary>
        private DShowNET.IMediaEventEx mediaEvt5;
        /// <summary> graph event interface for channel 6. </summary>
        private DShowNET.IMediaEventEx mediaEvt6;
        /// <summary> graph event interface for channel 7. </summary>
        private DShowNET.IMediaEventEx mediaEvt7;
        /// <summary> graph event interface for channel 8. </summary>
        private DShowNET.IMediaEventEx mediaEvt8;

        /// <summary> seek interface for positioning in stream for channel 1. </summary>
        private DShowNET.IMediaSeeking mediaSeek1;
        /// <summary> seek interface for positioning in stream for channel 2. </summary>
        private DShowNET.IMediaSeeking mediaSeek2;
        /// <summary> seek interface for positioning in stream for channel 3. </summary>
        private DShowNET.IMediaSeeking mediaSeek3;
        /// <summary> seek interface for positioning in stream for channel 4. </summary>
        private DShowNET.IMediaSeeking mediaSeek4;
        /// <summary> seek interface for positioning in stream for channel 5. </summary>
        private DShowNET.IMediaSeeking mediaSeek5;
        /// <summary> seek interface for positioning in stream for channel 6. </summary>
        private DShowNET.IMediaSeeking mediaSeek6;
        /// <summary> seek interface for positioning in stream for channel 7. </summary>
        private DShowNET.IMediaSeeking mediaSeek7;
        /// <summary> seek interface for positioning in stream for channel 8. </summary>
        private DShowNET.IMediaSeeking mediaSeek8;

        /// <summary> seek interface to set position in stream. </summary>
        private DShowNET.IMediaPosition mediaPos;
        /// <summary> seek interface to set position in stream in channel 1. </summary>
        private DShowNET.IMediaPosition mediaPos1;
        /// <summary> seek interface to set position in stream in channel 2. </summary>
        private DShowNET.IMediaPosition mediaPos2;
        /// <summary> seek interface to set position in stream in channel 3. </summary>
        private DShowNET.IMediaPosition mediaPos3;
        /// <summary> seek interface to set position in stream in channel 4. </summary>
        private DShowNET.IMediaPosition mediaPos4;
        /// <summary> seek interface to set position in stream in channel 5. </summary>
        private DShowNET.IMediaPosition mediaPos5;
        /// <summary> seek interface to set position in stream in channel 6. </summary>
        private DShowNET.IMediaPosition mediaPos6;
        /// <summary> seek interface to set position in stream in channel 7. </summary>
        private DShowNET.IMediaPosition mediaPos7;
        /// <summary> seek interface to set position in stream in channel 8. </summary>
        private DShowNET.IMediaPosition mediaPos8;

        /// <summary> video preview window interface for channel 1. </summary>
        private DShowNET.IVideoWindow videoWin1;
        /// <summary> video preview window interface for channel 2. </summary>
        private DShowNET.IVideoWindow videoWin2;
        /// <summary> video preview window interface for channel 3. </summary>
        private DShowNET.IVideoWindow videoWin3;
        /// <summary> video preview window interface for channel 4. </summary>
        private DShowNET.IVideoWindow videoWin4;
        /// <summary> video preview window interface for channel 5. </summary>
        private DShowNET.IVideoWindow videoWin5;
        /// <summary> video preview window interface for channel 6. </summary>
        private DShowNET.IVideoWindow videoWin6;
        /// <summary> video preview window interface for channel 7. </summary>
        private DShowNET.IVideoWindow videoWin7;
        /// <summary> video preview window interface for channel 8. </summary>
        private DShowNET.IVideoWindow videoWin8;

        /// <summary> interface to get information and control video for channel 1. </summary>
        private DShowNET.IBasicVideo2 basicVideo1;
        /// <summary> interface to get information and control video for channel 2. </summary>
        private DShowNET.IBasicVideo2 basicVideo2;
        /// <summary> interface to get information and control video for channel 3. </summary>
        private DShowNET.IBasicVideo2 basicVideo3;
        /// <summary> interface to get information and control video for channel 4. </summary>
        private DShowNET.IBasicVideo2 basicVideo4;
        /// <summary> interface to get information and control video for channel 5. </summary>
        private DShowNET.IBasicVideo2 basicVideo5;
        /// <summary> interface to get information and control video for channel 6. </summary>
        private DShowNET.IBasicVideo2 basicVideo6;
        /// <summary> interface to get information and control video for channel 7. </summary>
        private DShowNET.IBasicVideo2 basicVideo7;
        /// <summary> interface to get information and control video for channel 8. </summary>
        private DShowNET.IBasicVideo2 basicVideo8;

        /// <summary> interface to single-step video in channel 1. </summary>
        private DShowNET.IVideoFrameStep videoStep1;
        /// <summary> interface to single-step video in channel 2. </summary>
        private DShowNET.IVideoFrameStep videoStep2;
        /// <summary> interface to single-step video in channel 3. </summary>
        private DShowNET.IVideoFrameStep videoStep3;
        /// <summary> interface to single-step video in channel 4. </summary>
        private DShowNET.IVideoFrameStep videoStep4;
        /// <summary> interface to single-step video in channel 5. </summary>
        private DShowNET.IVideoFrameStep videoStep5;
        /// <summary> interface to single-step video in channel 6. </summary>
        private DShowNET.IVideoFrameStep videoStep6;
        /// <summary> interface to single-step video in channel 7. </summary>
        private DShowNET.IVideoFrameStep videoStep7;
        /// <summary> interface to single-step video in channel 8. </summary>
        private DShowNET.IVideoFrameStep videoStep8;

        /// <summary>  the get StreamLength method retrieves the duration of the current stream in channel 1. </summary>
        public IMediaDet mediaDet1 = null;
        /// <summary>  the get StreamLength method retrieves the duration of the current stream in channel 2. </summary>
        public IMediaDet mediaDet2 = null;
        /// <summary>  the get StreamLength method retrieves the duration of the current stream in channel 3. </summary>
        public IMediaDet mediaDet3 = null;
        /// <summary>  the get StreamLength method retrieves the duration of the current stream in channel 4. </summary>
        public IMediaDet mediaDet4 = null;
        /// <summary>  the get StreamLength method retrieves the duration of the current stream in channel 4. </summary>
        public IMediaDet mediaDet5 = null;
        /// <summary>  the get StreamLength method retrieves the duration of the current stream in channel 6. </summary>
        public IMediaDet mediaDet6 = null;
        /// <summary>  the get StreamLength method retrieves the duration of the current stream in channel 7. </summary>
        public IMediaDet mediaDet7 = null;
        /// <summary>  the get StreamLength method retrieves the duration of the current stream in channel 8. </summary>
        public IMediaDet mediaDet8 = null;

        /// <summary> audio interface used to control volume. </summary>
        private DShowNET.IBasicAudio basicAudio;

        /// <summary> message from graph. </summary>
        private const int WM_GRAPHNOTIFY = 0x00008001;

        /// <summary> The window is a child window. </summary>
        private const int WS_CHILD = 0x40000000;
        /// <summary> Excludes the area occupied by child windows when drawing occurs within the parent window. This style is used when creating the parent window. </summary>
        private const int WS_CLIPCHILDREN = 0x02000000;
        /// <summary> Clips child windows relative to each other. </summary>
        private const int WS_CLIPSIBLINGS = 0x04000000;

        /// <summary> general last video list from convert for channel 1. </summary>
        List<string> cam1 = new List<string>();
        /// <summary> general last video list from convert for channel 2. </summary>
        List<string> cam2 = new List<string>();
        /// <summary> general last video list from convert for channel 3. </summary>
        List<string> cam3 = new List<string>();
        /// <summary> general last video list from convert for channel 4. </summary>
        List<string> cam4 = new List<string>();
        /// <summary> general last video list from convert for channel 4. </summary>
        List<string> cam5 = new List<string>();
        /// <summary> general last video list from convert for channel 6. </summary>
        List<string> cam6 = new List<string>();
        /// <summary> general last video list from convert for channel 7. </summary>
        List<string> cam7 = new List<string>();
        /// <summary> general last video list from convert for channel 8. </summary>
        List<string> cam8 = new List<string>();


        /// <summary> first video list filter from convert for channel 1. </summary>
        List<string> cam1a = new List<string>();
        /// <summary> first video list filter from convert for channel 2. </summary>
        List<string> cam2a = new List<string>();
        /// <summary> first video list filter from convert for channel 3. </summary>
        List<string> cam3a = new List<string>();
        /// <summary> first video list filter from convert for channel 4. </summary>
        List<string> cam4a = new List<string>();
        /// <summary> first video list filter from convert for channel 4. </summary>
        List<string> cam5a = new List<string>();
        /// <summary> first video list filter from convert for channel 6. </summary>
        List<string> cam6a = new List<string>();
        /// <summary> first video list filter from convert for channel 7. </summary>
        List<string> cam7a = new List<string>();
        /// <summary> first video list filter from convert for channel 8. </summary>
        List<string> cam8a = new List<string>();

        /// <summary> second video list filter from convert for channel 1. </summary>
        List<string> cam1b = new List<string>();
        /// <summary> second video list filter from convert for channel 2. </summary>
        List<string> cam2b = new List<string>();
        /// <summary> second video list filter from convert for channel 3. </summary>
        List<string> cam3b = new List<string>();
        /// <summary> second video list filter from convert for channel 4. </summary>
        List<string> cam4b = new List<string>();
        /// <summary> second video list filter from convert for channel 5. </summary>
        List<string> cam5b = new List<string>();
        /// <summary> second video list filter from convert for channel 6. </summary>
        List<string> cam6b = new List<string>();
        /// <summary> second video list filter from convert for channel 7. </summary>
        List<string> cam7b = new List<string>();
        /// <summary> second video list filter from convert for channel 8. </summary>
        List<string> cam8b = new List<string>();

        /// <summary> extended video list filter from convert for channel 1. </summary>
        List<string> cam1c = new List<string>();
        /// <summary> extended video list filter from convert for channel 2. </summary>
        List<string> cam2c = new List<string>();
        /// <summary> extended video list filter from convert for channel 3. </summary>
        List<string> cam3c = new List<string>();
        /// <summary> extended video list filter from convert for channel 4. </summary>
        List<string> cam4c = new List<string>();
        /// <summary> extended video list filter from convert for channel 5. </summary>
        List<string> cam5c = new List<string>();
        /// <summary> extended video list filter from convert for channel 6. </summary>
        List<string> cam6c = new List<string>();
        /// <summary> extended video list filter from convert for channel 7. </summary>
        List<string> cam7c = new List<string>();
        /// <summary> extended video list filter from convert for channel 8. </summary>
        List<string> cam8c = new List<string>();

        /// <summary> count value for video in channel 1 </summary>
        int cam1Count = 0;
        /// <summary> count value for video in channel 2 </summary>
        int cam2Count = 0;
        /// <summary> count value for video in channel 3 </summary>
        int cam3Count = 0;
        /// <summary> count value for video in channel 4 </summary>
        int cam4Count = 0;
        /// <summary> count value for video in channel 5 </summary>
        int cam5Count = 0;
        /// <summary> count value for video in channel 6 </summary>
        int cam6Count = 0;
        /// <summary> count value for video in channel 7 </summary>
        int cam7Count = 0;
        /// <summary> count value for video in channel 8 </summary>
        int cam8Count = 0;

        /// <summary> list video duration in each channel for channel 1. </summary>
        List<double> duration1 = new List<double>();
        /// <summary> list video duration in each channel for channel 2. </summary>
        List<double> duration2 = new List<double>();
        /// <summary> list video duration in each channel for channel 3. </summary>
        List<double> duration3 = new List<double>();
        /// <summary> list video duration in each channel for channel 4. </summary>
        List<double> duration4 = new List<double>();
        /// <summary> list video duration in each channel for channel 4. </summary>
        List<double> duration5 = new List<double>();
        /// <summary> list video duration in each channel for channel 6. </summary>
        List<double> duration6 = new List<double>();
        /// <summary> list video duration in each channel for channel 7. </summary>
        List<double> duration7 = new List<double>();
        /// <summary> list video duration in each channel for channel 8. </summary>
        List<double> duration8 = new List<double>();

        /// <summary> list video starttime in each channel for channel 1. </summary>
        List<TimeSpan> starttime1 = new List<TimeSpan>();
        /// <summary> list video starttime in each channel for channel 2. </summary>
        List<TimeSpan> starttime2 = new List<TimeSpan>();
        /// <summary> list video starttime in each channel for channel 3. </summary>
        List<TimeSpan> starttime3 = new List<TimeSpan>();
        /// <summary> list video starttime in each channel for channel 4. </summary>
        List<TimeSpan> starttime4 = new List<TimeSpan>();
        /// <summary> list video starttime in each channel for channel 5. </summary>
        List<TimeSpan> starttime5 = new List<TimeSpan>();
        /// <summary> list video starttime in each channel for channel 6. </summary>
        List<TimeSpan> starttime6 = new List<TimeSpan>();
        /// <summary> list video starttime in each channel for channel 7. </summary>
        List<TimeSpan> starttime7 = new List<TimeSpan>();
        /// <summary> list video starttime in each channel for channel 8. </summary>
        List<TimeSpan> starttime8 = new List<TimeSpan>();

        /// <summary> list video endtime in channel 1. </summary>
        List<TimeSpan> endtime1 = new List<TimeSpan>();
        /// <summary> list video endtime in channel 2. </summary>
        List<TimeSpan> endtime2 = new List<TimeSpan>();
        /// <summary> list video endtime in channel 3. </summary>
        List<TimeSpan> endtime3 = new List<TimeSpan>();
        /// <summary> list video endtime in channel 4. </summary>
        List<TimeSpan> endtime4 = new List<TimeSpan>();
        /// <summary> list video endtime in channel 4. </summary>
        List<TimeSpan> endtime5 = new List<TimeSpan>();
        /// <summary> list video endtime in channel 6. </summary>
        List<TimeSpan> endtime6 = new List<TimeSpan>();
        /// <summary> list video endtime in channel 7. </summary>
        List<TimeSpan> endtime7 = new List<TimeSpan>();
        /// <summary> list video endtime in channel 8. </summary>
        List<TimeSpan> endtime8 = new List<TimeSpan>();

        /// <summary> list video endtime in channel 1. </summary>
        List<TimeSpan> endtime1F = new List<TimeSpan>();
        /// <summary> list video endtime in channel 2. </summary>
        List<TimeSpan> endtime2F = new List<TimeSpan>();
        /// <summary> list video endtime in channel 3. </summary>
        List<TimeSpan> endtime3F = new List<TimeSpan>();
        /// <summary> list video endtime in channel 4. </summary>
        List<TimeSpan> endtime4F = new List<TimeSpan>();
        /// <summary> list video endtime in channel 5. </summary>
        List<TimeSpan> endtime5F = new List<TimeSpan>();
        /// <summary> list video endtime in channel 6. </summary>
        List<TimeSpan> endtime6F = new List<TimeSpan>();
        /// <summary> list video endtime in channel 7. </summary>
        List<TimeSpan> endtime7F = new List<TimeSpan>();
        /// <summary> list video endtime in channel 8. </summary>
        List<TimeSpan> endtime8F = new List<TimeSpan>();

        /// <summary> joint list convert video files from cam1a, cam1b for channel 1. </summary>
        List<string> convertCam1 = new List<string>();
        /// <summary> joint list convert video files from cam2a, cam2b for channel 2. </summary>
        List<string> convertCam2 = new List<string>();
        /// <summary> joint list convert video files from cam3a, cam3b for channel 3. </summary>
        List<string> convertCam3 = new List<string>();
        /// <summary> joint list convert video files from cam3a, cam3b for channel 4. </summary>
        List<string> convertCam4 = new List<string>();
        /// <summary> joint list convert video files from cam3a, cam3b for channel 5. </summary>
        List<string> convertCam5 = new List<string>();
        /// <summary> joint list convert video files from cam3a, cam3b for channel 6. </summary>
        List<string> convertCam6 = new List<string>();
        /// <summary> joint list convert video files from cam3a, cam3b for channel 7. </summary>
        List<string> convertCam7 = new List<string>();
        /// <summary> joint list convert video files from cam3a, cam3b for channel 8. </summary>
        List<string> convertCam8 = new List<string>();

        /// <summary> joint all video list from last from convertCam1,convertCam2,convertCam3,convertCam4 for channel 1. </summary>
        List<string> allVideoList = new List<string>();

        /// <summary> get each snapshot video's argument in each channel. </summary>
        List<string> listCapture = new List<string>();

        /// <summary> get each record video's argument in each channel. </summary>
        List<string> listRec = new List<string>();
        #endregion

        /// <summary> Set clip type from video. </summary>
        internal enum ClipType
        {
            None, AudioVideo, VideoOnly, AudioOnly
        }

        /// <summary> Set play state from video. </summary>
        internal enum PlayState
        {
            Stopped, Running, Paused
        }

        /// <summary> Set video state from button clicked. </summary>
        public enum VideoState
        {
            Backwards,
            Forwarding,
            Stopped,
            FastBackwards4x, FastBackwards8x,
            Forwarding4x, Forwarding8x
        }

        /// <summary> initialize MainForm class component. </summary>
        public MainForm()
        {

            
            InitializeComponent();
        }

        /// <summary> load first action when newly opening form. </summary>
        private void Form1_Load(object sender, EventArgs e)
        {

            splitContainer6.Panel2.DoubleClick += splitContainer6_DoubleClick;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            //setcbRate();
            System.Globalization.CultureInfo cultureinformation = new System.Globalization.CultureInfo("en-US");

            System.Globalization.NumberFormatInfo numberFormat = new System.Globalization.NumberFormatInfo();

            numberFormat.NumberDecimalSeparator = ".";

            System.Globalization.DateTimeFormatInfo dateTimeInfo = new System.Globalization.DateTimeFormatInfo();

            dateTimeInfo.DateSeparator = "/";
            dateTimeInfo.LongDatePattern = "dd MMMMM yyyy";
            dateTimeInfo.ShortDatePattern = "dd/MM/yyyy";
            dateTimeInfo.ShortTimePattern = "HH:mm";
            dateTimeInfo.LongTimePattern = "HH:mm:ss";

            //cultureinformation.DateTimeFormat = dateTimeInfo;

            cultureinformation.NumberFormat = numberFormat;

            Application.CurrentCulture = cultureinformation;

            Thread.CurrentThread.CurrentCulture = cultureinformation;
            Thread.CurrentThread.CurrentUICulture = cultureinformation;


            

            btSnapshots.Location = new Point(1, 3);
            btOpenVideo.Location = new Point(35, 3);

            MaximizeBox = true;
            videoTrackBar.Value = 0;
            videoTrackBar.Enabled = false;

            System.Drawing.Drawing2D.GraphicsPath myGraphicsPath = new System.Drawing.Drawing2D.GraphicsPath();
            myGraphicsPath.AddEllipse(3, 3, 23, 23);
        }

        /// <summary> this function is used for set action when timer running in each interval. </summary>
        private void timer1_Tick(object sender, EventArgs e)
        {
            /// <summary> timer for single channel play. </summary>
            if (playmode == "single")
            {
                TimeSpan eTime = TimeSpan.FromSeconds(ticky);
                var posi = fromTime.Add(eTime);

                if (currentVideoState == VideoState.Backwards || currentVideoState == VideoState.FastBackwards4x || currentVideoState == VideoState.FastBackwards8x)
                {
                    if (ticky < duration)
                    {
                        if (posi == fromTime.Subtract(TimeSpan.FromSeconds(1)))
                            //splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;

                            if (posi == fromTime)
                            {
                                playSingleVideo();
                            }

                        ticky--;
                        videoTrackBar.Value = ticky;
                        lbStartDur.Text = fromTime.Add(TimeSpan.Parse(CalculateTime(ticky))).ToString();
                    }
                }
                else if (currentVideoState == VideoState.Forwarding || currentVideoState == VideoState.Forwarding4x || currentVideoState == VideoState.Forwarding8x)
                {
                    ticky++;

                    if (posi == fromTime)//.Subtract(TimeSpan.FromSeconds(1)))
                        playSingleVideo();

                    lbStartDur.Text = fromTime.Add(TimeSpan.Parse(CalculateTime(ticky))).ToString();
                    videoTrackBar.Value = ticky;

                    if (lbStartDur.Text == lbEndDur.Text)
                    {
                        btPause_Click(null, null);
                        btFB.Enabled = false;
                        btFastBackward4x.Enabled = false;
                        btFastBackward8x.Enabled = false;
                        splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                        splitContainer3.Panel2Collapsed = true;

                        finish = new finish(this);
                        finish.ShowDialog(this);
                    }
                }
            }
            /// <summary> timer for multiple channel play. </summary>
            
            else if (playmode == "multiple")
            {
                if (currentVideoState == VideoState.Backwards || currentVideoState == VideoState.FastBackwards4x || currentVideoState == VideoState.FastBackwards8x)
                {
                    if (ticky > 0)
                    {
                        ticky--;
                        videoTrackBar.Value = ticky;
                        lbStartDur.Text = fromTime.Add(TimeSpan.Parse(CalculateTime(ticky))).ToString();
                    }
                    if (lbStartDur.Text == fromTime.ToString())
                    {
                        btFB.Enabled = false;
                        btFastBackward4x.Enabled = false;
                        btFastBackward8x.Enabled = false;

                        timer1.Stop();
                        backwardTimer.Stop();
                        btPause_Click(null, null);

                        if (mediaPos1 != null)
                        {
                            mediaPos1.put_CurrentPosition(0.0);
                        }

                        if (mediaPos2 != null)
                        {
                            mediaPos2.put_CurrentPosition(0.0);
                        }

                        if (mediaPos3 != null)
                        {
                            mediaPos3.put_CurrentPosition(0.0);
                        }

                        if (mediaPos4 != null)
                        {
                            mediaPos4.put_CurrentPosition(0.0);
                        }
                        if (mediaPos5 != null)
                        {
                            mediaPos5.put_CurrentPosition(0.0);
                        }
                        if (mediaPos6 != null)
                        {
                            mediaPos6.put_CurrentPosition(0.0);
                        }
                        if (mediaPos7 != null)
                        {
                            mediaPos7.put_CurrentPosition(0.0);
                        }
                        if (mediaPos8 != null)
                        {
                            mediaPos8.put_CurrentPosition(0.0);
                        }
                    }
                    else if (ticky > 0)
                    {
                        ticky--;
                        videoTrackBar.Value = ticky;
                        lbStartDur.Text = fromTime.Add(TimeSpan.Parse(CalculateTime(ticky))).ToString();
                    }
                }
                else if (currentVideoState == VideoState.Forwarding || currentVideoState == VideoState.Forwarding4x || currentVideoState == VideoState.Forwarding8x)
                {
                    if (ticky < duration)
                    {
                        ticky++;
                        Console.WriteLine("ticky timer " + ticky);

                        TimeSpan eTime = TimeSpan.FromSeconds(ticky);
                        var posi = fromTime.Add(eTime);

                        Console.WriteLine("etime " + eTime);
                        Console.WriteLine("position " + posi);

                        if (cam1Count != 0)
                        {
                            if (SeqNo1 <= cam1Count - 1)
                            {
                                if (posi == starttime1[SeqNo1].Subtract(TimeSpan.FromSeconds(1)))
                                {
                                    splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                }
                                else if (posi == starttime1[SeqNo1])
                                {
                                    Initializevid1(sender, e);
                                }
                            }
                        }

                        if (cam2Count != 0)
                        {
                            if (SeqNo2 <= cam2Count - 1)
                            {
                                if (posi == starttime2[SeqNo2].Subtract(TimeSpan.FromSeconds(1)))
                                {
                                    splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                }
                                else if (posi == starttime2[SeqNo2])
                                {
                                    Initializevid2(sender, e);
                                }
                            }
                        }

                        if (cam3Count != 0)
                        {
                            if (SeqNo3 <= cam3Count - 1)
                            {
                                if (posi == starttime3[SeqNo3].Subtract(TimeSpan.FromSeconds(1)))
                                {
                                    splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                }
                                else if (posi == starttime3[SeqNo3])
                                {
                                    Initializevid3(sender, e);
                                }
                            }
                        }

                        if (cam4Count != 0)
                        {
                            if (SeqNo4 <= cam4Count - 1)
                            {
                                if (posi == starttime4[SeqNo4].Subtract(TimeSpan.FromSeconds(1)))
                                {
                                    splitContainer4.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                }
                                else if (posi == starttime4[SeqNo4])
                                {
                                    Initializevid4(sender, e);
                                }
                            }
                        }

                        if (cam5Count != 0)
                        {
                            if (SeqNo5 <= cam5Count - 1)
                            {
                                if (posi == starttime5[SeqNo5].Subtract(TimeSpan.FromSeconds(1)))
                                {
                                    splitContainer4.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                }
                                    //checkpoint
                                else if (posi == starttime5[SeqNo5])
                                {
                                    Initializevid5(sender, e);
                                }
                            }
                        }

                        if (cam6Count != 0)
                        {
                            if (SeqNo6 <= cam6Count - 1)
                            {
                                if (posi == starttime6[SeqNo6].Subtract(TimeSpan.FromSeconds(1)))
                                {
                                    splitContainer6.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                }
                                //checkpoint
                                else if (posi == starttime6[SeqNo6])
                                {
                                    Initializevid6(sender, e);
                                }
                            }
                        }

                        if (cam7Count != 0)
                        {
                            if (SeqNo7 <= cam7Count - 1)
                            {
                                if (posi == starttime7[SeqNo7].Subtract(TimeSpan.FromSeconds(1)))
                                {
                                    splitContainer7.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                }
                                //checkpoint
                                else if (posi == starttime7[SeqNo7])
                                {
                                    Initializevid7(sender, e);
                                }
                            }
                        }

                        if (cam8Count != 0)
                        {
                            if (SeqNo8 <= cam8Count - 1)
                            {
                                if (posi == starttime8[SeqNo8].Subtract(TimeSpan.FromSeconds(1)))
                                {
                                    splitContainer11.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                }
                                //checkpoint
                                else if (posi == starttime8[SeqNo8])
                                {
                                    Initializevid8(sender, e);
                                }
                            }
                        }


                        videoTrackBar.Value = ticky;
                        lbStartDur.Text = posi.ToString();
                    }
                    if (lbStartDur.Text == lbEndDur.Text)
                    {
                        btPause_Click(null, null);
                        btFB.Enabled = false;
                        btFastBackward4x.Enabled = false;
                        btFastBackward8x.Enabled = false;
                        timer1.Stop();

                        splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                        splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                        splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                        splitContainer4.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                        splitContainer4.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                        splitContainer6.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                        splitContainer7.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                        splitContainer11.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                        finish = new finish(this);
                        finish.ShowDialog(this);
                    }
                }
            }
        }

        //private void timer1_Tick(object sender, EventArgs e)
        //{
        //    if (playmode == "single")
        //    {
        //        if (currentVideoState == VideoState.Backwards || currentVideoState == VideoState.FastBackwards4x || currentVideoState == VideoState.FastBackwards8x)
        //        {
        //            if (ticky > 0)
        //            {
        //                ticky--;
        //                videoTrackBar.Value = ticky;
        //                lbStartDur.Text = TimeSpan.Parse(CalculateTime(ticky)).ToString();
        //            }
        //        }
        //        else
        //            if (currentVideoState == VideoState.Forwarding)
        //            {
        //                ticky++;
        //                Console.WriteLine("ticky timer " + ticky);
        //                TimeSpan eTime = TimeSpan.FromSeconds(ticky);
        //                lbStartDur.Text = Convert.ToString(eTime);
        //                videoTrackBar.Value = ticky;
        //            }
        //    }
        //    else if (playmode == "multiple")
        //    {
        //        if (currentVideoState == VideoState.Backwards)
        //        {
        //            if (ticky > 0)
        //            {
        //                ticky--;
        //                //Backwards();

        //                videoTrackBar.Value = ticky;
        //                lbStartDur.Text = TimeSpan.Parse(CalculateTime(ticky)).ToString();
        //            }
        //        }
        //        else if (currentVideoState == VideoState.FastBackwards4x || currentVideoState == VideoState.FastBackwards8x)
        //        {
        //            if (ticky > 0)
        //            {
        //                ticky--;
        //                //FastBackwards();

        //                videoTrackBar.Value = ticky;
        //                lbStartDur.Text = TimeSpan.Parse(CalculateTime(ticky)).ToString();
        //            }
        //        }
        //        else if (currentVideoState == VideoState.Forwarding)
        //        {
        //            if (ticky < duration)
        //            {
        //                TimeSpan eTime = TimeSpan.FromSeconds(ticky);
        //                var posi = fromTime.Add(eTime);

        //                Console.WriteLine("etime " + eTime);
        //                Console.WriteLine("position " + posi);

        //                if (cam1Count != 0)
        //                {
        //                    if (SeqNo1 <= cam1Count - 1)
        //                    {
        //                        if (posi == starttime1[SeqNo1].Subtract(TimeSpan.FromSeconds(1)))
        //                            splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;

        //                        Console.WriteLine("starttime1[SeqNo1] " + starttime1[SeqNo1]);
        //                        if (posi == starttime1[SeqNo1])
        //                        {
        //                            Console.WriteLine("starttime1[SeqNo1] " + starttime1[SeqNo1]);
        //                            Initializevid1(null, null);
        //                        }
        //                    }
        //                }

        //                if (cam2Count != 0)
        //                {
        //                    if (SeqNo2 <= cam2Count - 1)
        //                    {
        //                        if (posi == starttime2[SeqNo2].Subtract(TimeSpan.FromSeconds(1)))
        //                            splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;

        //                        if (posi == starttime2[SeqNo2])
        //                        {
        //                            Initializevid2(null, null);
        //                        }
        //                    }
        //                }

        //                if (cam3Count != 0)
        //                {
        //                    if (SeqNo3 <= cam3Count - 1)
        //                    {
        //                        if (posi == starttime3[SeqNo3].Subtract(TimeSpan.FromSeconds(1)))
        //                            splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;

        //                        if (posi == starttime3[SeqNo3])
        //                        {
        //                            Initializevid3(null, null);
        //                        }
        //                    }
        //                }

        //                if (cam4Count != 0)
        //                {
        //                    if (SeqNo4 <= cam4Count - 1)
        //                    {
        //                        if (posi == starttime4[SeqNo4].Subtract(TimeSpan.FromSeconds(1)))
        //                            splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;

        //                        if (posi == starttime4[SeqNo4])
        //                        {
        //                            Initializevid4(null, null);
        //                        }
        //                    }
        //                }

        //                lbStartDur.Text = Convert.ToString(eTime);
        //                videoTrackBar.Value = ticky;

        //                ticky++;
        //                Console.WriteLine("ticky timer " + ticky);
        //            }
        //        }
        //    }
        //}

        /// <summary> used to control stop video after load new video files. </summary>
        public void control()
        {
            timer1.Stop();
            ticky = 0;

            playState1 = PlayState.Stopped;
            playState2 = PlayState.Stopped;
            playState3 = PlayState.Stopped;
            playState4 = PlayState.Stopped;
            playState5 = PlayState.Stopped;
            playState6 = PlayState.Stopped;
            playState7 = PlayState.Stopped;
            playState8 = PlayState.Stopped;

            currentVideoState = VideoState.Stopped;
        }

        /// <summary> used to load all the video files to be played. </summary>
        public void bgProcessUpdate(string directory, TimeSpan start, TimeSpan end, string date, int cday, string playmodee)
        {
            videoTrackBar.Value = 0;
            videoTrackBar.Enabled = false;

            btPlay.Enabled = false;
            btPause.Enabled = false;
            btFB.Enabled = false;
            btFastBackward4x.Enabled = false;
            btFastBackward8x.Enabled = false;
            btFast4x.Enabled = false;
            btFast8x.Enabled = false;

            btSnapshots.Enabled = false;
            btAll4.Enabled = false;
            btcam1.Enabled = false;
            btcam2.Enabled = false;
            btCam3.Enabled = false;
            btCam4.Enabled = false;
            btCam5.Enabled = false;
            //button not added yet


            btOpenVideo.Enabled = false;
            playmode = playmodee;

            mediaDet1 = null;
            mediaDet2 = null;
            mediaDet3 = null;
            mediaDet4 = null;
            mediaDet5 = null;
            mediaDet6 = null;
            mediaDet7 = null;
            mediaDet8 = null;
            a = 0;
            b = 0;
            c = 0;
            d = 0;
            e5 = 0;
            f = 0;
            g = 0;
            h = 0;

            fromTime = TimeSpan.Parse("00:00:00");
            endtime = TimeSpan.Parse("00:00:00");
            endVtime = TimeSpan.Parse("00:00:00");
            //videoTrackBar.Width = 801;

            /// <summary> single channel play. </summary>
            if (playmodee == "single")
            {
                countCh = 1;
                if (mediaCtrl1 != null || mediaSeek1 != null)
                    CloseInterfaces();

                //logo.Location = new Point(1045, 3);
                pathDir = directory;
                fromTime = start;//.Subtract(TimeSpan.FromSeconds(1));
                splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                splitContainer3.Panel2Collapsed = true;

                pLoad = new progressLoad();
                open.Close();
                doPopulate.RunWorkerAsync();
                pLoad.Show(this);
            }
            /// <summary> multiple channel play. </summary>
            else if (playmodee == "multiple")
            {
                countCh = 0;
                //logo.Location = new Point(825, 2);
                pathDir = directory;
                fromTime = start.Subtract(TimeSpan.FromSeconds(1));
                endtime = end;
                dateFrom = date;
                crossday = cday;
                btOpenVideo.Enabled = false;

                if (mediaCtrl1 != null || mediaSeek1 != null)
                    CloseInterfaces();

                if (mediaCtrl2 != null || mediaSeek2 != null)
                    CloseInterfaces2();

                if (mediaCtrl3 != null || mediaSeek3 != null)
                    CloseInterfaces3();

                if (mediaCtrl4 != null || mediaSeek4 != null)
                    CloseInterfaces4();

                if (mediaCtrl5 != null || mediaSeek5 != null)
                    CloseInterfaces5();

                if (mediaCtrl6 != null || mediaSeek6 != null)
                    CloseInterfaces6();

                if (mediaCtrl7 != null || mediaSeek7 != null)
                    CloseInterfaces7();

                if (mediaCtrl8 != null || mediaSeek8 != null)
                    CloseInterfaces8();

                splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                splitContainer4.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                splitContainer4.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                splitContainer6.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                splitContainer7.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                splitContainer11.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;

                pLoad = new progressLoad();
                open.Close();
                doPopulate.RunWorkerAsync();
                pLoad.Show(this);
            }
        }

        /// <summary> used to convert single channel video. </summary>
        void singleconvert(string direct)
        {
            string ipcam = "ipcam_10.13.201.10";
            string result = "";
            //string result2 = "";
            string ttime = "";
            int pos = 0;
            int lengt = 0;
            int cek = 0;
            int setind = 0;
            string tt = "";
            string file;

            long fileSize = new System.IO.FileInfo(direct).Length;
            string sizeReadable = GetSizeReadable(fileSize);
            var firsout = sizeReadable.Substring(0, sizeReadable.Length - 3);

            int index = sizeReadable.LastIndexOf(' ');
            string lastout = sizeReadable.Substring(index + 1);
            numStat = 1;

            if (lastout == "KB" && Convert.ToDouble(firsout) < 100)
            {
                VidStatus = "not valid video";
                numStat = 0;
                return;
            }

            dateRec = Path.GetDirectoryName(Path.GetDirectoryName(direct));
            string date = Path.GetFileName(Path.GetDirectoryName(direct));
            string busID = Path.GetFileName(dateRec);
            result = Path.GetFileNameWithoutExtension(direct);
            //result = result.Substring(result.Length - 8).ToString();

            ttime = result.ToString().Substring(result.ToString().IndexOf('_') + 1);
            pos = ttime.IndexOf('_');
            lengt = ttime.Length;
            cek = lengt - pos;

            if (cek != 7)
            {
                return;
            }

            setind = result.IndexOf('_');
            tt = result.Substring(0, setind);
            type = "nvs";
            EmptyVideo = "novidnvs.avi";
            rate = 6;
            //rate = drate;

            if (direct.Contains(ipcam))
            {
                setind = ttime.IndexOf('_');
                tt = ttime.Substring(0, setind);
                char cht = tt[tt.Length - 1];
                tt = Convert.ToString(cht);
                ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                type = "ipcam";
                EmptyVideo = "novidipcam.avi";
                rate = 7.27;
                //rate = drate;
            }

            directpath = Path.Combine(pathdirect, "avi");
            directpath = Path.Combine(directpath, type);
            directpath = Path.Combine(directpath, busID);
            numberPath = directpath;

            directpath = Path.Combine(numberPath, date);
            //string result_dir = result.Substring(result.Length - 8);
            //string result_test = result_dir;
            file = "-f" + rate + " -i \"" + direct + "\" -o \"" + directpath + "\\" + result + ".avi\"";

            try
            {
                if (!Directory.Exists(directpath))
                {
                    DirectoryInfo di = Directory.CreateDirectory(directpath);
                    Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(directpath));
                }
            }
            catch (Exception et)
            {
                Console.WriteLine("The process failed: {0}", et.ToString());
            }

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            startInfo.FileName = "thirdParty\\avc2avi.exe";
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.Arguments = file;
            Console.WriteLine("file " + file);

            string comb = Path.Combine(directpath, result + ".avi");
            pathDir = comb;

            try
            {
                if (!File.Exists(comb))
                {
                    using (Process exeProcess = Process.Start(startInfo))
                    {
                        Console.WriteLine("Create file " + result + ".avi");
                        exeProcess.WaitForExit();
                    }
                }
                else
                {
                    Console.WriteLine(File.Exists(comb) ? "File exists." : "File does not exist.");
                }
            }
            catch
            {
                // Log error.
            }

            mediaInfo1(comb);
        }

        /// <summary> used to filter video files load when used date input. </summary>
        void DirSearch(string sDir)
        {
            int startdate = Convert.ToInt32(dateFrom);

            try
            {
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    string dirpath = Path.GetFileName(d);
                    if (Convert.ToInt32(dirpath) == startdate)
                    {
                        FilterVideoList(sDir, dirpath);
                        Console.WriteLine("Populate video files in : " + dirpath);
                    }
                }
            }
            catch (System.Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }
        }

        /// <summary> background process action to load video files. </summary>
        private void doPopulate_DoWork(object sender, DoWorkEventArgs e)
        {
            //this.Invoke(new MethodInvoker(delegate()
            //{
            //    //this.Enabled = false;
            //    //pLoad.TopMost = true;
            //}));

            cam1.Clear();
            cam2.Clear();
            cam3.Clear();
            cam4.Clear();
            cam5.Clear();
            cam6.Clear();
            cam7.Clear();
            cam8.Clear();

            cam1a.Clear();
            cam2a.Clear();
            cam3a.Clear();
            cam4a.Clear();
            cam5a.Clear();
            cam6a.Clear();
            cam7a.Clear();
            cam8a.Clear();

            cam1b.Clear();
            cam2b.Clear();
            cam3b.Clear();
            cam4b.Clear();
            cam5b.Clear();
            cam6b.Clear();
            cam7b.Clear();
            cam8b.Clear();

            cam1c.Clear();
            cam2c.Clear();
            cam3c.Clear();
            cam4c.Clear();
            cam5c.Clear();
            cam6c.Clear();
            cam7c.Clear();
            cam8c.Clear();

            convertCam1.Clear();
            convertCam2.Clear();
            convertCam3.Clear();
            convertCam4.Clear();
            convertCam5.Clear();
            convertCam6.Clear();
            convertCam7.Clear();
            convertCam8.Clear();

            allVideoList.Clear();
            duration1.Clear();
            duration2.Clear();
            duration3.Clear();
            duration4.Clear();
            duration5.Clear();
            duration6.Clear();
            duration7.Clear();
            duration8.Clear();

            starttime1.Clear();
            starttime2.Clear();
            starttime3.Clear();
            starttime4.Clear();
            starttime5.Clear();
            starttime6.Clear();
            starttime7.Clear();
            starttime8.Clear();

            endtime1.Clear();
            endtime2.Clear();
            endtime3.Clear();
            endtime4.Clear();
            endtime5.Clear();
            endtime6.Clear();
            endtime7.Clear();
            endtime8.Clear();

            timestart = TimeSpan.Parse("23:59:59");
            strtm1 = TimeSpan.Parse("00:00:00");
            strtm2 = TimeSpan.Parse("00:00:00");
            strtm3 = TimeSpan.Parse("00:00:00");
            strtm4 = TimeSpan.Parse("00:00:00");
            strtm5 = TimeSpan.Parse("00:00:00");
            strtm6 = TimeSpan.Parse("00:00:00");
            strtm7 = TimeSpan.Parse("00:00:00");
            strtm8 = TimeSpan.Parse("00:00:00");

            strDurName = TimeSpan.Parse("00:00:00");
            endDurName = TimeSpan.Parse("00:00:00");
            strRec1a = TimeSpan.Parse("00:00:00");
            strRec2a = TimeSpan.Parse("00:00:00");
            strRec3a = TimeSpan.Parse("00:00:00");
            strRec4a = TimeSpan.Parse("00:00:00");
            strRec5a = TimeSpan.Parse("00:00:00");
            strRec6a = TimeSpan.Parse("00:00:00");
            strRec7a = TimeSpan.Parse("00:00:00");
            strRec8a = TimeSpan.Parse("00:00:00");

            strRec1b = TimeSpan.Parse("00:00:00");
            strRec2b = TimeSpan.Parse("00:00:00");
            strRec3b = TimeSpan.Parse("00:00:00");
            strRec4b = TimeSpan.Parse("00:00:00");
            strRec5b = TimeSpan.Parse("00:00:00");
            strRec6b = TimeSpan.Parse("00:00:00");
            strRec7b = TimeSpan.Parse("00:00:00");
            strRec8b = TimeSpan.Parse("00:00:00");

            durRec1 = TimeSpan.Parse("00:00:00");
            durRec2 = TimeSpan.Parse("00:00:00");
            durRec3 = TimeSpan.Parse("00:00:00");
            durRec4 = TimeSpan.Parse("00:00:00");
            durRec5 = TimeSpan.Parse("00:00:00");
            durRec6 = TimeSpan.Parse("00:00:00");
            durRec7 = TimeSpan.Parse("00:00:00");
            durRec8 = TimeSpan.Parse("00:00:00");
            snap = TimeSpan.Parse("00:00:00");

            cam1Count = 0;
            cam2Count = 0;
            cam3Count = 0;
            cam4Count = 0;
            cam5Count = 0;
            cam6Count = 0;
            cam7Count = 0;
            cam8Count = 0;

            endtime1.Clear();
            endtime2.Clear();
            endtime3.Clear();
            endtime4.Clear();
            endtime5.Clear();
            endtime6.Clear();
            endtime7.Clear();
            endtime8.Clear();

            endtime1F.Clear();
            endtime2F.Clear();
            endtime3F.Clear();
            endtime4F.Clear();
            endtime5F.Clear();
            endtime6F.Clear();
            endtime7F.Clear();
            endtime8F.Clear();

            convertCam1.Clear();
            convertCam2.Clear();
            convertCam3.Clear();
            convertCam4.Clear();
            convertCam5.Clear();
            convertCam6.Clear();
            convertCam7.Clear();
            convertCam8.Clear();

            allVideoList.Clear();
            listCapture.Clear();
            listRec.Clear();

            duration = 0;
            VidStatus = "";

            SeqNo1 = 0;
            SeqNo2 = 0;
            SeqNo3 = 0;
            SeqNo4 = 0;
            SeqNo5 = 0;
            SeqNo6 = 0;
            SeqNo7 = 0;
            SeqNo8 = 0;
            ticky = 0;
            //countCh = 0;

            if (!string.IsNullOrEmpty(pathDir))
            {
                if (playmode == "single")
                {
                    timer1.Stop();
                    //lbStartDur.Text = "00:00:00";
                    //lbStartDur.Text = CalculateTime(fromTime.Seconds);
                    videoTrackBar.Value = 0;
                    btPlay.Checked = false;

                    VidStatus = "Load Done ";
                    singleconvert(pathDir);
                }
                else if (playmode == "multiple")
                {
                    this.Invoke(new MethodInvoker(delegate()
                    {
                        timer1.Stop();
                        //lbStartDur.Text = CalculateTime(fromTime.Seconds);
                        videoTrackBar.Value = 0;
                        btPlay.Checked = false;
                    }));

                    if (Directory.Exists(@pathDir))
                    {
                        if (System.IO.Directory.GetDirectories(pathDir).Length > 0)
                        {
                            DirSearch(pathDir);
                        }
                        else
                        {
                            string dirpath = Path.GetFileName(@pathDir);
                            DirectoryInfo d = new DirectoryInfo(@pathDir);
                            var getdir = d.Parent.FullName;

                            FilterVideoList(getdir, dirpath);
                        }

                        cam1Count = cam1.Count;
                        Console.WriteLine("->> cam1Count " + cam1Count);
                        cam2Count = cam2.Count;
                        Console.WriteLine("->> cam2Count " + cam2Count);
                        cam3Count = cam3.Count;
                        Console.WriteLine("->> cam3Count " + cam3Count);
                        cam4Count = cam4.Count;
                        Console.WriteLine("->> cam4Count " + cam4Count);
                        cam5Count = cam5.Count;
                        Console.WriteLine("->> cam5Count " + cam5Count);
                        cam6Count = cam6.Count;
                        Console.WriteLine("->> cam6Count " + cam6Count);
                        cam7Count = cam7.Count;
                        Console.WriteLine("->> cam7Count " + cam7Count);
                        cam8Count = cam8.Count;
                        Console.WriteLine("->> cam8Count " + cam8Count);

                        if (cam1Count != 0 || cam2Count != 0 || cam3Count != 0 || cam4Count != 0 || cam5Count != 0 || cam6Count != 0 || cam7Count != 0 || cam8Count != 0)
                        {
                            this.Invoke(new MethodInvoker(delegate()
                            {
                                foreach (object o in cam1)
                                {
                                    Console.WriteLine("->> cam1 " + o + "\n");
                                }
                                foreach (object o in cam2)
                                {
                                    Console.WriteLine("->> cam2 " + o + "\n");
                                }
                                foreach (object o in cam3)
                                {
                                    Console.WriteLine("->> cam3 " + o + "\n");
                                }
                                foreach (object o in cam4)
                                {
                                    Console.WriteLine("->> cam4 " + o + "\n");
                                }
                                foreach (object o in cam5)
                                {
                                    Console.WriteLine("->> cam5 " + o + "\n");
                                }
                                foreach (object o in cam6)
                                {
                                    Console.WriteLine("->> cam6 " + o + "\n");
                                }
                                foreach (object o in cam7)
                                {
                                    Console.WriteLine("->> cam7 " + o + "\n");
                                }
                                foreach (object o in cam8)
                                {
                                    Console.WriteLine("->> cam8 " + o + "\n");
                                }


                                foreach (object o in starttime1)
                                {
                                    Console.WriteLine("->> starttime1 " + o + "\n");
                                }
                                foreach (object o in starttime2)
                                {
                                    Console.WriteLine("->> starttime2 " + o + "\n");
                                }
                                foreach (object o in starttime3)
                                {
                                    Console.WriteLine("->> starttime3 " + o + "\n");
                                }
                                foreach (object o in starttime4)
                                {
                                    Console.WriteLine("->> starttime4 " + o + "\n");
                                }

                                foreach (object o in starttime5)
                                {
                                    Console.WriteLine("->> starttime5 " + o + "\n");
                                }
                                foreach (object o in starttime6)
                                {
                                    Console.WriteLine("->> starttime6 " + o + "\n");
                                }
                                foreach (object o in starttime7)
                                {
                                    Console.WriteLine("->> starttime7 " + o + "\n");
                                }
                                foreach (object o in starttime8)
                                {
                                    Console.WriteLine("->> starttime8 " + o + "\n");
                                }

                                foreach (object o in duration1)
                                {
                                    Console.WriteLine("->> duration1 " + o + "\n");
                                }
                                foreach (object o in duration2)
                                {
                                    Console.WriteLine("->> duration2 " + o + "\n");
                                }
                                foreach (object o in duration3)
                                {
                                    Console.WriteLine("->> duration3 " + o + "\n");
                                }
                                foreach (object o in duration4)
                                {
                                    Console.WriteLine("->> duration4 " + o + "\n");
                                }
                                foreach (object o in duration5)
                                {
                                    Console.WriteLine("->> duration5 " + o + "\n");
                                }
                                foreach (object o in duration6)
                                {
                                    Console.WriteLine("->> duration6 " + o + "\n");
                                }
                                foreach (object o in duration7)
                                {
                                    Console.WriteLine("->> duration7 " + o + "\n");
                                }
                                foreach (object o in duration8)
                                {
                                    Console.WriteLine("->> duration8 " + o + "\n");
                                }

                                cekTimeStart();
                                cektimeduration();

                                VidStatus = "load from folder " + dateFrom;
                                string check = pathDir;
                                numStat = 1;
                            }));
                        }
                        else
                        {
                            VidStatus = "No File Loaded";
                            numStat = 0;
                        }
                    }
                    else
                    {
                        VidStatus = "Invalid Folder Select";
                        numStat = 0;
                    }
                }
            }
            else
            {
                VidStatus = "Select Folder First";
                numStat = 0;
            }
        }

        /// <summary> filter load video process then convert file to avi format. </summary>
        public void FilterVideoList(string Folder, string dir)
        {
            numStat = 1;
            string ipcam = "ipcam_10.13.201.10";
            FolderName = Convert.ToInt32(dir);

            inPath = Path.Combine(Folder, dir);
            if (inPath.Contains("avi"))
            {
                FilterAvi();
            }
            else
            {
                DirectoryInfo dinfo = new DirectoryInfo(inPath);
                FileInfo[] Files = dinfo.GetFiles();

                string iMinute, iHour, iSec;
                TimeSpan nameTime = TimeSpan.Parse("00:00:00");

                endVtime = endtime;
                if (crossday == 1)
                    endVtime = TimeSpan.Parse("23:59:59");

                if (fromTime <= endVtime)
                {
                    Console.WriteLine("-- start primary filter --");

                    /*-start first filter-*/
                    Console.WriteLine("start first filter ");
                    foreach (FileInfo file in Files.OrderBy(d => d.Name))
                    {
                        dPath = Path.Combine(dir, file.ToString());
                        combPath = Path.Combine(Folder, dPath);

                        string time = Path.GetFileNameWithoutExtension(file.ToString());
                        string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                        var pos = ttime.IndexOf('_');
                        var lengt = ttime.Length;
                        int cek = lengt - pos;

                        if (cek != 7)
                        {
                            VidStatus = "wrong file name format";
                            numStat = 0;
                        }

                        int setind = time.IndexOf('_');
                        string tt = time.Substring(0, setind);
                        type = "nvs";
                        EmptyVideo = "novidnvs.avi";

                        if (file.ToString().Contains(ipcam))
                        {
                            setind = ttime.IndexOf('_');
                            tt = ttime.Substring(0, setind);
                            char cht = tt[tt.Length - 1];
                            tt = Convert.ToString(cht);
                            ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);

                            type = "ipcam";
                            EmptyVideo = "novidipcam.avi";
                        }

                        iHour = ttime.Substring(0, 2);
                        iMinute = ttime.Substring(2, 2);
                        iSec = ttime.Substring(4, 2);
                        time = iHour + ":" + iMinute + ":" + iSec;

                        if (TimeSpan.Parse(time) < endtime)
                        {
                            try
                            {
                                long fileSize = new System.IO.FileInfo(combPath).Length;
                                string sizeReadable = GetSizeReadable(fileSize);
                                var firsout = sizeReadable.Substring(0, sizeReadable.Length - 3);

                                int index = sizeReadable.LastIndexOf(' ');
                                string lastout = sizeReadable.Substring(index + 1);

                                if (lastout == "MB")
                                {
                                    if (tt == "1")
                                    {
                                        cam1a.Add(file.ToString());
                                    }
                                    else if (tt == "2")
                                    {
                                        cam2a.Add(file.ToString());
                                    }
                                    else if (tt == "3")
                                    {
                                        cam3a.Add(file.ToString());
                                    }
                                    else if (tt == "4")
                                    {
                                        cam4a.Add(file.ToString());
                                    }
                                    else if (tt == "5")
                                    {
                                        cam5a.Add(file.ToString());
                                    }
                                    else if (tt == "6")
                                    {
                                        cam6a.Add(file.ToString());
                                    }
                                    else if (tt == "7")
                                    {
                                        cam7a.Add(file.ToString());
                                    }
                                    else if (tt == "8")
                                    {
                                        cam8a.Add(file.ToString());
                                    }
                                }
                                else if (lastout == "KB")
                                {
                                    if (Convert.ToDouble(firsout) >= 100)
                                    {
                                        if (tt == "1")
                                        {
                                            cam1a.Add(file.ToString());
                                        }
                                        else if (tt == "2")
                                        {
                                            cam2a.Add(file.ToString());
                                        }
                                        else if (tt == "3")
                                        {
                                            cam3a.Add(file.ToString());
                                        }
                                        else if (tt == "4")
                                        {
                                            cam4a.Add(file.ToString());
                                        }
                                        else if (tt == "5")
                                        {
                                            cam5a.Add(file.ToString());
                                        }
                                        else if (tt == "6")
                                        {
                                            cam6a.Add(file.ToString());
                                        }
                                        else if (tt == "7")
                                        {
                                            cam7a.Add(file.ToString());
                                        }
                                        else if (tt == "8")
                                        {
                                            cam8a.Add(file.ToString());
                                        }
                                    }
                                }
                            }
                            catch (System.Exception excpt)
                            {
                                Console.WriteLine(excpt.Message);
                            }
                        }
                    }

                    /*-start second filter-*/
                    Console.WriteLine("start second filter ");
                    if (cam1a.Count != 0)
                    {
                        for (int i = 0; i < cam1a.Count; i++)
                        {
                            string time = Path.GetFileNameWithoutExtension(cam1a[i]);
                            string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                            var pos = ttime.IndexOf('_');
                            var lengt = ttime.Length;
                            int cek = lengt - pos;

                            if (cek != 7)
                            {
                                VidStatus = "wrong file name format";
                                numStat = 0;
                            }

                            int setind = time.IndexOf('_');
                            string tt = time.Substring(0, setind);
                            type = "nvs";
                            EmptyVideo = "novidnvs.avi";

                            if (cam1a[i].Contains(ipcam))
                            {
                                setind = ttime.IndexOf('_');
                                tt = ttime.Substring(0, setind);
                                char cht = tt[tt.Length - 1];
                                tt = Convert.ToString(cht);
                                ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                type = "ipcam";
                                EmptyVideo = "novidipcam.avi";
                            }

                            iHour = ttime.Substring(0, 2);
                            iMinute = ttime.Substring(2, 2);
                            iSec = ttime.Substring(4, 2);
                            time = iHour + ":" + iMinute + ":" + iSec;

                            nameTime = TimeSpan.Parse(time);

                            if (nameTime >= fromTime && nameTime <= endVtime)
                            {
                                dPath = Path.Combine(dir, cam1a[i]);
                                combPath = Path.Combine(Folder, dPath);

                                time = Path.GetFileNameWithoutExtension(cam1a[i]);
                                ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                                pos = ttime.IndexOf('_');
                                lengt = ttime.Length;
                                cek = lengt - pos;

                                if (cek != 7)
                                {
                                    VidStatus = "wrong file name format";
                                    numStat = 0;
                                }

                                setind = time.IndexOf('_');
                                tt = time.Substring(0, setind);
                                type = "nvs";
                                EmptyVideo = "novidipcam.avi";

                                if (cam1a[i].Contains(ipcam))
                                {
                                    setind = ttime.IndexOf('_');
                                    tt = ttime.Substring(0, setind);
                                    char cht = tt[tt.Length - 1];
                                    tt = Convert.ToString(cht);
                                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                    type = "ipcam";
                                    EmptyVideo = "novidipcam.avi";
                                }

                                iHour = ttime.Substring(0, 2);
                                iMinute = ttime.Substring(2, 2);
                                iSec = ttime.Substring(4, 2);
                                time = iHour + ":" + iMinute + ":" + iSec;
                                nameTime = TimeSpan.Parse(time);

                                try
                                {
                                    cam1b.Add(dPath);
                                    mediaInfo1(combPath);
                                    endtime1.Add(nameTime.Add(TimeSpan.FromSeconds(durat)));

                                    Console.WriteLine("file " + cam1a[i]);
                                    Console.WriteLine("nameTime " + nameTime);
                                }
                                catch (System.Exception excpt)
                                {
                                    Console.WriteLine(excpt.Message);
                                }
                            }
                        }

                        if (cam1b.Count() != 0)
                        {
                            string name1b = cam1b[0];

                            name1b = Path.GetFileName(name1b);
                            int listIndex = cam1a.IndexOf(name1b);
                            listIndex = listIndex - 1;
                            if (listIndex != -1)
                            {
                                name1b = cam1a[listIndex];
                                dPath = Path.Combine(dir, name1b);

                                string time = Path.GetFileNameWithoutExtension(name1b);
                                string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                                int pos = ttime.IndexOf('_');
                                int lengt = ttime.Length;
                                int cek = lengt - pos;

                                if (cek != 7)
                                {
                                    VidStatus = "wrong file name format";
                                    numStat = 0;
                                }

                                int setind = time.IndexOf('_');
                                string tt = time.Substring(0, setind);
                                type = "nvs";
                                EmptyVideo = "novidnvs.avi";

                                if (name1b.Contains(ipcam))
                                {
                                    setind = ttime.IndexOf('_');
                                    tt = ttime.Substring(0, setind);
                                    char cht = tt[tt.Length - 1];
                                    tt = Convert.ToString(cht);
                                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                    type = "ipcam";
                                    EmptyVideo = "novidipcam.avi";
                                }

                                iHour = ttime.Substring(0, 2);
                                iMinute = ttime.Substring(2, 2);
                                iSec = ttime.Substring(4, 2);
                                time = iHour + ":" + iMinute + ":" + iSec;

                                nameTime = TimeSpan.Parse(time);

                                if (nameTime <= endVtime)
                                {
                                    try
                                    {
                                        cam1b.Add(dPath);
                                        mediaInfo1(combPath);
                                        endtime1.Add(nameTime.Add(TimeSpan.FromSeconds(durat)));

                                        Console.WriteLine("file " + name1b);
                                        Console.WriteLine("nameTime " + nameTime);
                                    }
                                    catch (System.Exception excpt)
                                    {
                                        Console.WriteLine(excpt.Message);
                                    }
                                }
                            }
                        }
                        cam1b.Sort();
                        endtime1.Sort();
                        foreach (object o in cam1b)
                        {
                            Console.WriteLine("->> cam1b " + o + "\n");
                        }
                        foreach (object o in endtime1)
                        {
                            Console.WriteLine("->> endtime1 " + o + "\n");
                        }
                    }

                    if (cam2a.Count != 0)
                    {
                        for (int i = 0; i < cam2a.Count; i++)
                        {
                            string time = Path.GetFileNameWithoutExtension(cam2a[i]);
                            string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                            var pos = ttime.IndexOf('_');
                            var lengt = ttime.Length;
                            int cek = lengt - pos;

                            if (cek != 7)
                            {
                                VidStatus = "wrong file name format";
                                numStat = 0;
                            }

                            int setind = time.IndexOf('_');
                            string tt = time.Substring(0, setind);
                            type = "nvs";
                            EmptyVideo = "novidnvs.avi";

                            if (cam2a[i].Contains(ipcam))
                            {
                                setind = ttime.IndexOf('_');
                                tt = ttime.Substring(0, setind);
                                char cht = tt[tt.Length - 1];
                                tt = Convert.ToString(cht);
                                ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                type = "ipcam";
                                EmptyVideo = "novidipcam.avi";
                            }

                            iHour = ttime.Substring(0, 2);
                            iMinute = ttime.Substring(2, 2);
                            iSec = ttime.Substring(4, 2);
                            time = iHour + ":" + iMinute + ":" + iSec;

                            nameTime = TimeSpan.Parse(time);

                            if (nameTime >= fromTime && nameTime <= endVtime)
                            {
                                dPath = Path.Combine(dir, cam2a[i]);
                                combPath = Path.Combine(Folder, dPath);

                                time = Path.GetFileNameWithoutExtension(cam2a[i]);
                                ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                                pos = ttime.IndexOf('_');
                                lengt = ttime.Length;
                                cek = lengt - pos;

                                if (cek != 7)
                                {
                                    VidStatus = "wrong file name format";
                                    numStat = 0;
                                }

                                setind = time.IndexOf('_');
                                tt = time.Substring(0, setind);
                                type = "nvs";
                                EmptyVideo = "novidnvs.avi";

                                if (cam2a[i].Contains(ipcam))
                                {
                                    setind = ttime.IndexOf('_');
                                    tt = ttime.Substring(0, setind);
                                    char cht = tt[tt.Length - 1];
                                    tt = Convert.ToString(cht);
                                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                    type = "ipcam";
                                    EmptyVideo = "novidipcam.avi";
                                }

                                iHour = ttime.Substring(0, 2);
                                iMinute = ttime.Substring(2, 2);
                                iSec = ttime.Substring(4, 2);
                                time = iHour + ":" + iMinute + ":" + iSec;
                                nameTime = TimeSpan.Parse(time);

                                try
                                {
                                    cam2b.Add(dPath);
                                    mediaInfo1(combPath);
                                    endtime2.Add(nameTime.Add(TimeSpan.FromSeconds(durat)));

                                    Console.WriteLine("file " + cam2a[i]);
                                    Console.WriteLine("nameTime " + nameTime);
                                }
                                catch (System.Exception excpt)
                                {
                                    Console.WriteLine(excpt.Message);
                                }
                            }
                        }

                        if (cam2b.Count() != 0)
                        {
                            string name2b = cam2b[0];
                            name2b = Path.GetFileName(name2b);

                            int listIndex = cam2a.IndexOf(name2b);
                            listIndex = listIndex - 1;
                            if (listIndex != -1)
                            {
                                name2b = cam2a[listIndex];
                                dPath = Path.Combine(dir, name2b);

                                string time = Path.GetFileNameWithoutExtension(name2b);
                                string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                                int pos = ttime.IndexOf('_');
                                int lengt = ttime.Length;
                                int cek = lengt - pos;

                                if (cek != 7)
                                {
                                    VidStatus = "wrong file name format";
                                    numStat = 0;
                                }

                                int setind = time.IndexOf('_');
                                string tt = time.Substring(0, setind);
                                type = "nvs";
                                EmptyVideo = "novidnvs.avi";

                                if (name2b.Contains(ipcam))
                                {
                                    setind = ttime.IndexOf('_');
                                    tt = ttime.Substring(0, setind);
                                    char cht = tt[tt.Length - 1];
                                    tt = Convert.ToString(cht);
                                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                    type = "ipcam";
                                    EmptyVideo = "novidipcam.avi";
                                }

                                iHour = ttime.Substring(0, 2);
                                iMinute = ttime.Substring(2, 2);
                                iSec = ttime.Substring(4, 2);
                                time = iHour + ":" + iMinute + ":" + iSec;
                                nameTime = TimeSpan.Parse(time);

                                if (nameTime <= endVtime)
                                {
                                    try
                                    {
                                        cam2b.Add(dPath);
                                        mediaInfo1(combPath);
                                        endtime2.Add(nameTime.Add(TimeSpan.FromSeconds(durat)));

                                        Console.WriteLine("file " + name2b);
                                        Console.WriteLine("nameTime " + nameTime);
                                    }
                                    catch (System.Exception excpt)
                                    {
                                        Console.WriteLine(excpt.Message);
                                    }
                                }
                            }
                        }
                        cam2b.Sort();
                        endtime2.Sort();

                        foreach (object o in cam2b)
                        {
                            Console.WriteLine("->> cam2b " + o + "\n");
                        }
                        foreach (object o in endtime2)
                        {
                            Console.WriteLine("->> endtime2 " + o + "\n");
                        }
                    }

                    if (cam3a.Count != 0)
                    {
                        for (int i = 0; i < cam3a.Count; i++)
                        {
                            string time = Path.GetFileNameWithoutExtension(cam3a[i]);
                            string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                            var pos = ttime.IndexOf('_');
                            var lengt = ttime.Length;
                            int cek = lengt - pos;

                            if (cek != 7)
                            {
                                VidStatus = "wrong file name format";
                                numStat = 0;
                            }

                            int setind = time.IndexOf('_');
                            string tt = time.Substring(0, setind);
                            type = "nvs";
                            EmptyVideo = "novidnvs.avi";

                            if (cam3a[i].Contains(ipcam))
                            {
                                setind = ttime.IndexOf('_');
                                tt = ttime.Substring(0, setind);
                                char cht = tt[tt.Length - 1];
                                tt = Convert.ToString(cht);
                                ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                type = "ipcam";
                                EmptyVideo = "novidipcam.avi";
                            }

                            iHour = ttime.Substring(0, 2);
                            iMinute = ttime.Substring(2, 2);
                            iSec = ttime.Substring(4, 2);
                            time = iHour + ":" + iMinute + ":" + iSec;
                            nameTime = TimeSpan.Parse(time);

                            if (nameTime >= fromTime && nameTime <= endVtime)
                            {
                                dPath = Path.Combine(dir, cam3a[i]);
                                combPath = Path.Combine(Folder, dPath);

                                time = Path.GetFileNameWithoutExtension(cam3a[i]);
                                ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                                pos = ttime.IndexOf('_');
                                lengt = ttime.Length;
                                cek = lengt - pos;

                                if (cek != 7)
                                {
                                    VidStatus = "wrong file name format";
                                    numStat = 0;
                                }

                                setind = time.IndexOf('_');
                                tt = time.Substring(0, setind);
                                type = "nvs";
                                EmptyVideo = "novidnvs.avi";

                                if (cam3a[i].Contains(ipcam))
                                {
                                    setind = ttime.IndexOf('_');
                                    tt = ttime.Substring(0, setind);
                                    char cht = tt[tt.Length - 1];
                                    tt = Convert.ToString(cht);
                                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                    type = "ipcam";
                                    EmptyVideo = "novidipcam.avi";
                                }

                                iHour = ttime.Substring(0, 2);
                                iMinute = ttime.Substring(2, 2);
                                iSec = ttime.Substring(4, 2);
                                time = iHour + ":" + iMinute + ":" + iSec;

                                nameTime = TimeSpan.Parse(time);

                                try
                                {
                                    cam3b.Add(dPath);
                                    mediaInfo1(combPath);
                                    endtime3.Add(nameTime.Add(TimeSpan.FromSeconds(durat)));

                                    Console.WriteLine("file " + cam3a[i]);
                                    Console.WriteLine("nameTime " + nameTime);
                                }
                                catch (System.Exception excpt)
                                {
                                    Console.WriteLine(excpt.Message);
                                }
                            }
                        }

                        if (cam3b.Count() != 0)
                        {
                            string name3b = cam3b[0];
                            name3b = Path.GetFileName(name3b);

                            int listIndex = cam3a.IndexOf(name3b);
                            listIndex = listIndex - 1;
                            if (listIndex != -1)
                            {
                                name3b = cam3a[listIndex];
                                dPath = Path.Combine(dir, name3b);

                                string time = Path.GetFileNameWithoutExtension(name3b);
                                string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                                int pos = ttime.IndexOf('_');
                                int lengt = ttime.Length;
                                int cek = lengt - pos;

                                if (cek != 7)
                                {
                                    VidStatus = "wrong file name format";
                                    numStat = 0;
                                }

                                int setind = time.IndexOf('_');
                                string tt = time.Substring(0, setind);
                                type = "nvs";
                                EmptyVideo = "novidnvs.avi";

                                if (name3b.Contains(ipcam))
                                {
                                    setind = ttime.IndexOf('_');
                                    tt = ttime.Substring(0, setind);
                                    char cht = tt[tt.Length - 1];
                                    tt = Convert.ToString(cht);
                                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                    type = "ipcam";
                                    EmptyVideo = "novidipcam.avi";
                                }

                                iHour = ttime.Substring(0, 2);
                                iMinute = ttime.Substring(2, 2);
                                iSec = ttime.Substring(4, 2);
                                time = iHour + ":" + iMinute + ":" + iSec;
                                nameTime = TimeSpan.Parse(time);

                                if (nameTime <= endVtime)
                                {
                                    try
                                    {
                                        cam3b.Add(dPath);
                                        mediaInfo1(combPath);
                                        endtime3.Add(nameTime.Add(TimeSpan.FromSeconds(durat)));

                                        Console.WriteLine("file " + name3b);
                                        Console.WriteLine("nameTime " + nameTime);
                                    }
                                    catch (System.Exception excpt)
                                    {
                                        Console.WriteLine(excpt.Message);
                                    }
                                }
                            }
                        }
                        cam3b.Sort();
                        endtime3.Sort();

                        foreach (object o in cam3b)
                        {
                            Console.WriteLine("->> cam3b " + o + "\n");
                        }
                        foreach (object o in endtime3)
                        {
                            Console.WriteLine("->> endtime3 " + o + "\n");
                        }
                    }

                    if (cam4a.Count != 0)
                    {
                        for (int i = 0; i < cam4a.Count; i++)
                        {
                            string time = Path.GetFileNameWithoutExtension(cam4a[i]);
                            string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                            var pos = ttime.IndexOf('_');
                            var lengt = ttime.Length;
                            int cek = lengt - pos;

                            if (cek != 7)
                            {
                                VidStatus = "wrong file name format";
                                numStat = 0;

                            }

                            int setind = time.IndexOf('_');
                            string tt = time.Substring(0, setind);
                            type = "nvs";
                            EmptyVideo = "novidnvs.avi";

                            if (cam4a[i].Contains(ipcam))
                            {
                                setind = ttime.IndexOf('_');
                                tt = ttime.Substring(0, setind);
                                char cht = tt[tt.Length - 1];
                                tt = Convert.ToString(cht);
                                ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                type = "ipcam";
                                EmptyVideo = "novidipcam.avi";
                            }

                            iHour = ttime.Substring(0, 2);
                            iMinute = ttime.Substring(2, 2);
                            iSec = ttime.Substring(4, 2);
                            time = iHour + ":" + iMinute + ":" + iSec;
                            nameTime = TimeSpan.Parse(time);

                            if (nameTime >= fromTime && nameTime <= endVtime)
                            {
                                dPath = Path.Combine(dir, cam4a[i]);
                                combPath = Path.Combine(Folder, dPath);

                                time = Path.GetFileNameWithoutExtension(cam4a[i]);
                                ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                                pos = ttime.IndexOf('_');
                                lengt = ttime.Length;
                                cek = lengt - pos;

                                if (cek != 7)
                                {
                                    VidStatus = "wrong file name format";
                                    numStat = 0;
                                }

                                setind = time.IndexOf('_');
                                tt = time.Substring(0, setind);
                                type = "nvs";
                                EmptyVideo = "novidnvs.avi";

                                if (cam4a[i].Contains(ipcam))
                                {
                                    setind = ttime.IndexOf('_');
                                    tt = ttime.Substring(0, setind);
                                    char cht = tt[tt.Length - 1];
                                    tt = Convert.ToString(cht);
                                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                    type = "ipcam";
                                    EmptyVideo = "novidipcam.avi";
                                }

                                iHour = ttime.Substring(0, 2);
                                iMinute = ttime.Substring(2, 2);
                                iSec = ttime.Substring(4, 2);
                                time = iHour + ":" + iMinute + ":" + iSec;

                                nameTime = TimeSpan.Parse(time);

                                try
                                {
                                    cam4b.Add(dPath);
                                    mediaInfo1(combPath);
                                    endtime4.Add(nameTime.Add(TimeSpan.FromSeconds(durat)));

                                    Console.WriteLine("file " + cam4a[i]);
                                    Console.WriteLine("nameTime " + nameTime);
                                }
                                catch (System.Exception excpt)
                                {
                                    Console.WriteLine(excpt.Message);
                                }
                            }
                        }

                        if (cam4b.Count() != 0)
                        {
                            string name4b = cam4b[0];
                            name4b = Path.GetFileName(name4b);

                            int listIndex = cam4a.IndexOf(name4b);
                            listIndex = listIndex - 1;
                            if (listIndex != -1)
                            {
                                name4b = cam4a[listIndex];
                                dPath = Path.Combine(dir, name4b);

                                string time = Path.GetFileNameWithoutExtension(name4b);
                                string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                                int pos = ttime.IndexOf('_');
                                int lengt = ttime.Length;
                                int cek = lengt - pos;

                                if (cek != 7)
                                {
                                    VidStatus = "wrong file name format";
                                    numStat = 0;
                                }

                                int setind = time.IndexOf('_');
                                string tt = time.Substring(0, setind);
                                type = "nvs";
                                EmptyVideo = "novidnvs.avi";

                                if (name4b.Contains(ipcam))
                                {
                                    setind = ttime.IndexOf('_');
                                    tt = ttime.Substring(0, setind);
                                    char cht = tt[tt.Length - 1];
                                    tt = Convert.ToString(cht);
                                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                    type = "ipcam";
                                    EmptyVideo = "novidipcam.avi";
                                }

                                iHour = ttime.Substring(0, 2);
                                iMinute = ttime.Substring(2, 2);
                                iSec = ttime.Substring(4, 2);
                                time = iHour + ":" + iMinute + ":" + iSec;
                                nameTime = TimeSpan.Parse(time);

                                if (nameTime <= endVtime)
                                {
                                    try
                                    {
                                        cam4b.Add(dPath);
                                        mediaInfo1(combPath);
                                        endtime4.Add(nameTime.Add(TimeSpan.FromSeconds(durat)));

                                        Console.WriteLine("file " + name4b);
                                        Console.WriteLine("nameTime " + nameTime);
                                    }
                                    catch (System.Exception excpt)
                                    {
                                        Console.WriteLine(excpt.Message);
                                    }
                                }
                            }
                        }
                        cam4b.Sort();
                        endtime4.Sort();

                        foreach (object o in cam4b)
                        {
                            Console.WriteLine("->> cam4b " + o + "\n");
                        }
                        foreach (object o in endtime4)
                        {
                            Console.WriteLine("->> endtime4 " + o + "\n");
                        }
                    }


                    if (cam5a.Count != 0)
                    {
                        for (int i = 0; i < cam5a.Count; i++)
                        {
                            string time = Path.GetFileNameWithoutExtension(cam5a[i]);
                            string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                            var pos = ttime.IndexOf('_');
                            var lengt = ttime.Length;
                            int cek = lengt - pos;

                            if (cek != 7)
                            {
                                VidStatus = "wrong file name format";
                                numStat = 0;

                            }

                            int setind = time.IndexOf('_');
                            string tt = time.Substring(0, setind);
                            type = "nvs";
                            EmptyVideo = "novidnvs.avi";

                            if (cam5a[i].Contains(ipcam))
                            {
                                setind = ttime.IndexOf('_');
                                tt = ttime.Substring(0, setind);
                                char cht = tt[tt.Length - 1];
                                tt = Convert.ToString(cht);
                                ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                type = "ipcam";
                                EmptyVideo = "novidipcam.avi";
                            }

                            iHour = ttime.Substring(0, 2);
                            iMinute = ttime.Substring(2, 2);
                            iSec = ttime.Substring(4, 2);
                            time = iHour + ":" + iMinute + ":" + iSec;
                            nameTime = TimeSpan.Parse(time);

                            if (nameTime >= fromTime && nameTime <= endVtime)
                            {
                                dPath = Path.Combine(dir, cam5a[i]);
                                combPath = Path.Combine(Folder, dPath);

                                time = Path.GetFileNameWithoutExtension(cam5a[i]);
                                ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                                pos = ttime.IndexOf('_');
                                lengt = ttime.Length;
                                cek = lengt - pos;

                                if (cek != 7)
                                {
                                    VidStatus = "wrong file name format";
                                    numStat = 0;
                                }

                                setind = time.IndexOf('_');
                                tt = time.Substring(0, setind);
                                type = "nvs";
                                EmptyVideo = "novidnvs.avi";

                                if (cam5a[i].Contains(ipcam))
                                {
                                    setind = ttime.IndexOf('_');
                                    tt = ttime.Substring(0, setind);
                                    char cht = tt[tt.Length - 1];
                                    tt = Convert.ToString(cht);
                                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                    type = "ipcam";
                                    EmptyVideo = "novidipcam.avi";
                                }

                                iHour = ttime.Substring(0, 2);
                                iMinute = ttime.Substring(2, 2);
                                iSec = ttime.Substring(4, 2);
                                time = iHour + ":" + iMinute + ":" + iSec;

                                nameTime = TimeSpan.Parse(time);

                                try
                                {
                                    cam5b.Add(dPath);
                                    mediaInfo1(combPath);
                                    endtime5.Add(nameTime.Add(TimeSpan.FromSeconds(durat)));

                                    Console.WriteLine("file " + cam5a[i]);
                                    Console.WriteLine("nameTime " + nameTime);
                                }
                                catch (System.Exception excpt)
                                {
                                    Console.WriteLine(excpt.Message);
                                }
                            }
                        }

                        if (cam5b.Count() != 0)
                        {
                            string name5b = cam5b[0];
                            name5b = Path.GetFileName(name5b);

                            int listIndex = cam5a.IndexOf(name5b);
                            listIndex = listIndex - 1;
                            if (listIndex != -1)
                            {
                                name5b = cam5a[listIndex];
                                dPath = Path.Combine(dir, name5b);

                                string time = Path.GetFileNameWithoutExtension(name5b);
                                string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                                int pos = ttime.IndexOf('_');
                                int lengt = ttime.Length;
                                int cek = lengt - pos;

                                if (cek != 7)
                                {
                                    VidStatus = "wrong file name format";
                                    numStat = 0;
                                }

                                int setind = time.IndexOf('_');
                                string tt = time.Substring(0, setind);
                                type = "nvs";
                                EmptyVideo = "novidnvs.avi";

                                if (name5b.Contains(ipcam))
                                {
                                    setind = ttime.IndexOf('_');
                                    tt = ttime.Substring(0, setind);
                                    char cht = tt[tt.Length - 1];
                                    tt = Convert.ToString(cht);
                                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                    type = "ipcam";
                                    EmptyVideo = "novidipcam.avi";
                                }

                                iHour = ttime.Substring(0, 2);
                                iMinute = ttime.Substring(2, 2);
                                iSec = ttime.Substring(4, 2);
                                time = iHour + ":" + iMinute + ":" + iSec;
                                nameTime = TimeSpan.Parse(time);

                                if (nameTime <= endVtime)
                                {
                                    try
                                    {
                                        cam5b.Add(dPath);
                                        mediaInfo1(combPath);
                                        endtime5.Add(nameTime.Add(TimeSpan.FromSeconds(durat)));

                                        Console.WriteLine("file " + name5b);
                                        Console.WriteLine("nameTime " + nameTime);
                                    }
                                    catch (System.Exception excpt)
                                    {
                                        Console.WriteLine(excpt.Message);
                                    }
                                }
                            }
                        }
                        cam5b.Sort();
                        endtime5.Sort();

                        foreach (object o in cam5b)
                        {
                            Console.WriteLine("->> cam5b " + o + "\n");
                        }
                        foreach (object o in endtime5)
                        {
                            Console.WriteLine("->> endtime5 " + o + "\n");
                        }
                    }


                    if (cam6a.Count != 0)
                    {
                        for (int i = 0; i < cam6a.Count; i++)
                        {
                            string time = Path.GetFileNameWithoutExtension(cam6a[i]);
                            string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                            var pos = ttime.IndexOf('_');
                            var lengt = ttime.Length;
                            int cek = lengt - pos;

                            if (cek != 7)
                            {
                                VidStatus = "wrong file name format";
                                numStat = 0;

                            }

                            int setind = time.IndexOf('_');
                            string tt = time.Substring(0, setind);
                            type = "nvs";
                            EmptyVideo = "novidnvs.avi";

                            if (cam6a[i].Contains(ipcam))
                            {
                                setind = ttime.IndexOf('_');
                                tt = ttime.Substring(0, setind);
                                char cht = tt[tt.Length - 1];
                                tt = Convert.ToString(cht);
                                ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                type = "ipcam";
                                EmptyVideo = "novidipcam.avi";
                            }

                            iHour = ttime.Substring(0, 2);
                            iMinute = ttime.Substring(2, 2);
                            iSec = ttime.Substring(4, 2);
                            time = iHour + ":" + iMinute + ":" + iSec;
                            nameTime = TimeSpan.Parse(time);

                            if (nameTime >= fromTime && nameTime <= endVtime)
                            {
                                dPath = Path.Combine(dir, cam6a[i]);
                                combPath = Path.Combine(Folder, dPath);

                                time = Path.GetFileNameWithoutExtension(cam6a[i]);
                                ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                                pos = ttime.IndexOf('_');
                                lengt = ttime.Length;
                                cek = lengt - pos;

                                if (cek != 7)
                                {
                                    VidStatus = "wrong file name format";
                                    numStat = 0;
                                }

                                setind = time.IndexOf('_');
                                tt = time.Substring(0, setind);
                                type = "nvs";
                                EmptyVideo = "novidnvs.avi";

                                if (cam6a[i].Contains(ipcam))
                                {
                                    setind = ttime.IndexOf('_');
                                    tt = ttime.Substring(0, setind);
                                    char cht = tt[tt.Length - 1];
                                    tt = Convert.ToString(cht);
                                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                    type = "ipcam";
                                    EmptyVideo = "novidipcam.avi";
                                }

                                iHour = ttime.Substring(0, 2);
                                iMinute = ttime.Substring(2, 2);
                                iSec = ttime.Substring(4, 2);
                                time = iHour + ":" + iMinute + ":" + iSec;

                                nameTime = TimeSpan.Parse(time);

                                try
                                {
                                    cam6b.Add(dPath);
                                    mediaInfo1(combPath);
                                    endtime6.Add(nameTime.Add(TimeSpan.FromSeconds(durat)));

                                    Console.WriteLine("file " + cam6a[i]);
                                    Console.WriteLine("nameTime " + nameTime);
                                }
                                catch (System.Exception excpt)
                                {
                                    Console.WriteLine(excpt.Message);
                                }
                            }
                        }

                        if (cam6b.Count() != 0)
                        {
                            string name6b = cam6b[0];
                            name6b = Path.GetFileName(name6b);

                            int listIndex = cam6a.IndexOf(name6b);
                            listIndex = listIndex - 1;
                            if (listIndex != -1)
                            {
                                name6b = cam6a[listIndex];
                                dPath = Path.Combine(dir, name6b);

                                string time = Path.GetFileNameWithoutExtension(name6b);
                                string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                                int pos = ttime.IndexOf('_');
                                int lengt = ttime.Length;
                                int cek = lengt - pos;

                                if (cek != 7)
                                {
                                    VidStatus = "wrong file name format";
                                    numStat = 0;
                                }

                                int setind = time.IndexOf('_');
                                string tt = time.Substring(0, setind);
                                type = "nvs";
                                EmptyVideo = "novidnvs.avi";

                                if (name6b.Contains(ipcam))
                                {
                                    setind = ttime.IndexOf('_');
                                    tt = ttime.Substring(0, setind);
                                    char cht = tt[tt.Length - 1];
                                    tt = Convert.ToString(cht);
                                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                    type = "ipcam";
                                    EmptyVideo = "novidipcam.avi";
                                }

                                iHour = ttime.Substring(0, 2);
                                iMinute = ttime.Substring(2, 2);
                                iSec = ttime.Substring(4, 2);
                                time = iHour + ":" + iMinute + ":" + iSec;
                                nameTime = TimeSpan.Parse(time);

                                if (nameTime <= endVtime)
                                {
                                    try
                                    {
                                        cam6b.Add(dPath);
                                        mediaInfo1(combPath);
                                        endtime6.Add(nameTime.Add(TimeSpan.FromSeconds(durat)));

                                        Console.WriteLine("file " + name6b);
                                        Console.WriteLine("nameTime " + nameTime);
                                    }
                                    catch (System.Exception excpt)
                                    {
                                        Console.WriteLine(excpt.Message);
                                    }
                                }
                            }
                        }
                        cam6b.Sort();
                        endtime6.Sort();

                        foreach (object o in cam6b)
                        {
                            Console.WriteLine("->> cam6b " + o + "\n");
                        }
                        foreach (object o in endtime6)
                        {
                            Console.WriteLine("->> endtime6 " + o + "\n");
                        }
                    }

                    if (cam7a.Count != 0)
                    {
                        for (int i = 0; i < cam7a.Count; i++)
                        {
                            string time = Path.GetFileNameWithoutExtension(cam7a[i]);
                            string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                            var pos = ttime.IndexOf('_');
                            var lengt = ttime.Length;
                            int cek = lengt - pos;

                            if (cek != 7)
                            {
                                VidStatus = "wrong file name format";
                                numStat = 0;

                            }

                            int setind = time.IndexOf('_');
                            string tt = time.Substring(0, setind);
                            type = "nvs";
                            EmptyVideo = "novidnvs.avi";

                            if (cam7a[i].Contains(ipcam))
                            {
                                setind = ttime.IndexOf('_');
                                tt = ttime.Substring(0, setind);
                                char cht = tt[tt.Length - 1];
                                tt = Convert.ToString(cht);
                                ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                type = "ipcam";
                                EmptyVideo = "novidipcam.avi";
                            }

                            iHour = ttime.Substring(0, 2);
                            iMinute = ttime.Substring(2, 2);
                            iSec = ttime.Substring(4, 2);
                            time = iHour + ":" + iMinute + ":" + iSec;
                            nameTime = TimeSpan.Parse(time);

                            if (nameTime >= fromTime && nameTime <= endVtime)
                            {
                                dPath = Path.Combine(dir, cam7a[i]);
                                combPath = Path.Combine(Folder, dPath);

                                time = Path.GetFileNameWithoutExtension(cam7a[i]);
                                ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                                pos = ttime.IndexOf('_');
                                lengt = ttime.Length;
                                cek = lengt - pos;

                                if (cek != 7)
                                {
                                    VidStatus = "wrong file name format";
                                    numStat = 0;
                                }

                                setind = time.IndexOf('_');
                                tt = time.Substring(0, setind);
                                type = "nvs";
                                EmptyVideo = "novidnvs.avi";

                                if (cam7a[i].Contains(ipcam))
                                {
                                    setind = ttime.IndexOf('_');
                                    tt = ttime.Substring(0, setind);
                                    char cht = tt[tt.Length - 1];
                                    tt = Convert.ToString(cht);
                                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                    type = "ipcam";
                                    EmptyVideo = "novidipcam.avi";
                                }

                                iHour = ttime.Substring(0, 2);
                                iMinute = ttime.Substring(2, 2);
                                iSec = ttime.Substring(4, 2);
                                time = iHour + ":" + iMinute + ":" + iSec;

                                nameTime = TimeSpan.Parse(time);

                                try
                                {
                                    cam7b.Add(dPath);
                                    mediaInfo1(combPath);
                                    endtime7.Add(nameTime.Add(TimeSpan.FromSeconds(durat)));

                                    Console.WriteLine("file " + cam7a[i]);
                                    Console.WriteLine("nameTime " + nameTime);
                                }
                                catch (System.Exception excpt)
                                {
                                    Console.WriteLine(excpt.Message);
                                }
                            }
                        }

                        if (cam7b.Count() != 0)
                        {
                            string name7b = cam7b[0];
                            name7b = Path.GetFileName(name7b);

                            int listIndex = cam7a.IndexOf(name7b);
                            listIndex = listIndex - 1;
                            if (listIndex != -1)
                            {
                                name7b = cam7a[listIndex];
                                dPath = Path.Combine(dir, name7b);

                                string time = Path.GetFileNameWithoutExtension(name7b);
                                string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                                int pos = ttime.IndexOf('_');
                                int lengt = ttime.Length;
                                int cek = lengt - pos;

                                if (cek != 7)
                                {
                                    VidStatus = "wrong file name format";
                                    numStat = 0;
                                }

                                int setind = time.IndexOf('_');
                                string tt = time.Substring(0, setind);
                                type = "nvs";
                                EmptyVideo = "novidnvs.avi";

                                if (name7b.Contains(ipcam))
                                {
                                    setind = ttime.IndexOf('_');
                                    tt = ttime.Substring(0, setind);
                                    char cht = tt[tt.Length - 1];
                                    tt = Convert.ToString(cht);
                                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                    type = "ipcam";
                                    EmptyVideo = "novidipcam.avi";
                                }

                                iHour = ttime.Substring(0, 2);
                                iMinute = ttime.Substring(2, 2);
                                iSec = ttime.Substring(4, 2);
                                time = iHour + ":" + iMinute + ":" + iSec;
                                nameTime = TimeSpan.Parse(time);

                                if (nameTime <= endVtime)
                                {
                                    try
                                    {
                                        cam7b.Add(dPath);
                                        mediaInfo1(combPath);
                                        endtime7.Add(nameTime.Add(TimeSpan.FromSeconds(durat)));

                                        Console.WriteLine("file " + name7b);
                                        Console.WriteLine("nameTime " + nameTime);
                                    }
                                    catch (System.Exception excpt)
                                    {
                                        Console.WriteLine(excpt.Message);
                                    }
                                }
                            }
                        }
                        cam7b.Sort();
                        endtime7.Sort();

                        foreach (object o in cam7b)
                        {
                            Console.WriteLine("->> cam7b " + o + "\n");
                        }
                        foreach (object o in endtime7)
                        {
                            Console.WriteLine("->> endtime7 " + o + "\n");
                        }
                    }


                    if (cam8a.Count != 0)
                    {
                        for (int i = 0; i < cam8a.Count; i++)
                        {
                            string time = Path.GetFileNameWithoutExtension(cam8a[i]);
                            string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                            var pos = ttime.IndexOf('_');
                            var lengt = ttime.Length;
                            int cek = lengt - pos;

                            if (cek != 7)
                            {
                                VidStatus = "wrong file name format";
                                numStat = 0;

                            }

                            int setind = time.IndexOf('_');
                            string tt = time.Substring(0, setind);
                            type = "nvs";
                            EmptyVideo = "novidnvs.avi";

                            if (cam8a[i].Contains(ipcam))
                            {
                                setind = ttime.IndexOf('_');
                                tt = ttime.Substring(0, setind);
                                char cht = tt[tt.Length - 1];
                                tt = Convert.ToString(cht);
                                ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                type = "ipcam";
                                EmptyVideo = "novidipcam.avi";
                            }

                            iHour = ttime.Substring(0, 2);
                            iMinute = ttime.Substring(2, 2);
                            iSec = ttime.Substring(4, 2);
                            time = iHour + ":" + iMinute + ":" + iSec;
                            nameTime = TimeSpan.Parse(time);

                            if (nameTime >= fromTime && nameTime <= endVtime)
                            {
                                dPath = Path.Combine(dir, cam8a[i]);
                                combPath = Path.Combine(Folder, dPath);

                                time = Path.GetFileNameWithoutExtension(cam8a[i]);
                                ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                                pos = ttime.IndexOf('_');
                                lengt = ttime.Length;
                                cek = lengt - pos;

                                if (cek != 7)
                                {
                                    VidStatus = "wrong file name format";
                                    numStat = 0;
                                }

                                setind = time.IndexOf('_');
                                tt = time.Substring(0, setind);
                                type = "nvs";
                                EmptyVideo = "novidnvs.avi";

                                if (cam8a[i].Contains(ipcam))
                                {
                                    setind = ttime.IndexOf('_');
                                    tt = ttime.Substring(0, setind);
                                    char cht = tt[tt.Length - 1];
                                    tt = Convert.ToString(cht);
                                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                    type = "ipcam";
                                    EmptyVideo = "novidipcam.avi";
                                }

                                iHour = ttime.Substring(0, 2);
                                iMinute = ttime.Substring(2, 2);
                                iSec = ttime.Substring(4, 2);
                                time = iHour + ":" + iMinute + ":" + iSec;

                                nameTime = TimeSpan.Parse(time);

                                try
                                {
                                    cam8b.Add(dPath);
                                    mediaInfo1(combPath);
                                    endtime8.Add(nameTime.Add(TimeSpan.FromSeconds(durat)));

                                    Console.WriteLine("file " + cam8a[i]);
                                    Console.WriteLine("nameTime " + nameTime);
                                }
                                catch (System.Exception excpt)
                                {
                                    Console.WriteLine(excpt.Message);
                                }
                            }
                        }

                        if (cam8b.Count() != 0)
                        {
                            string name8b = cam8b[0];
                            name8b = Path.GetFileName(name8b);

                            int listIndex = cam8a.IndexOf(name8b);
                            listIndex = listIndex - 1;
                            if (listIndex != -1)
                            {
                                name8b = cam8a[listIndex];
                                dPath = Path.Combine(dir, name8b);

                                string time = Path.GetFileNameWithoutExtension(name8b);
                                string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                                int pos = ttime.IndexOf('_');
                                int lengt = ttime.Length;
                                int cek = lengt - pos;

                                if (cek != 7)
                                {
                                    VidStatus = "wrong file name format";
                                    numStat = 0;
                                }

                                int setind = time.IndexOf('_');
                                string tt = time.Substring(0, setind);
                                type = "nvs";
                                EmptyVideo = "novidnvs.avi";

                                if (name8b.Contains(ipcam))
                                {
                                    setind = ttime.IndexOf('_');
                                    tt = ttime.Substring(0, setind);
                                    char cht = tt[tt.Length - 1];
                                    tt = Convert.ToString(cht);
                                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                                    type = "ipcam";
                                    EmptyVideo = "novidipcam.avi";
                                }

                                iHour = ttime.Substring(0, 2);
                                iMinute = ttime.Substring(2, 2);
                                iSec = ttime.Substring(4, 2);
                                time = iHour + ":" + iMinute + ":" + iSec;
                                nameTime = TimeSpan.Parse(time);

                                if (nameTime <= endVtime)
                                {
                                    try
                                    {
                                        cam8b.Add(dPath);
                                        mediaInfo1(combPath);
                                        endtime8.Add(nameTime.Add(TimeSpan.FromSeconds(durat)));

                                        Console.WriteLine("file " + name8b);
                                        Console.WriteLine("nameTime " + nameTime);
                                    }
                                    catch (System.Exception excpt)
                                    {
                                        Console.WriteLine(excpt.Message);
                                    }
                                }
                            }
                        }
                        cam8b.Sort();
                        endtime8.Sort();

                        foreach (object o in cam8b)
                        {
                            Console.WriteLine("->> cam8b " + o + "\n");
                        }
                        foreach (object o in endtime8)
                        {
                            Console.WriteLine("->> endtime8 " + o + "\n");
                        }
                    }



                    /*-start third filter-*/
                    for (int i = 0; i < cam1b.Count; i++)
                    {
                        string time = Path.GetFileNameWithoutExtension(cam1b[i].ToString());
                        string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                        int pos = ttime.IndexOf('_');
                        int lengt = ttime.Length;
                        int cek = lengt - pos;

                        if (cek != 7)
                        {
                            VidStatus = "wrong file name format";
                            numStat = 0;
                        }

                        int setind = time.IndexOf('_');
                        string tt = time.Substring(0, setind);
                        type = "nvs";
                        EmptyVideo = "novidnvs.avi";

                        if (cam1b[i].Contains(ipcam))
                        {
                            setind = ttime.IndexOf('_');
                            tt = ttime.Substring(0, setind);
                            char cht = tt[tt.Length - 1];
                            tt = Convert.ToString(cht);
                            ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                            type = "ipcam";
                            EmptyVideo = "novidipcam.avi";
                        }

                        iHour = ttime.Substring(0, 2);
                        iMinute = ttime.Substring(2, 2);
                        iSec = ttime.Substring(4, 2);
                        time = iHour + ":" + iMinute + ":" + iSec;
                        nameTime = TimeSpan.Parse(time);

                        //if (endtime1[i] >= fromTime.Add(TimeSpan.FromSeconds(30)) && nameTime <= endVtime)
                        //{ }
                        //else
                        //{
                        // //   cam1b.RemoveAt(i);
                        //}
                    }

                    for (int i = 0; i < cam2b.Count; i++)
                    {
                        string time = Path.GetFileNameWithoutExtension(cam2b[i].ToString());
                        string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);

                        int pos = ttime.IndexOf('_');
                        int lengt = ttime.Length;
                        int cek = lengt - pos;

                        if (cek != 7)
                        {
                            VidStatus = "wrong file name format";
                            numStat = 0;
                        }

                        int setind = time.IndexOf('_');
                        string tt = time.Substring(0, setind);
                        type = "nvs";
                        EmptyVideo = "novidnvs.avi";

                        if (cam2b[i].Contains(ipcam))
                        {
                            setind = ttime.IndexOf('_');
                            tt = ttime.Substring(0, setind);
                            char cht = tt[tt.Length - 1];
                            tt = Convert.ToString(cht);
                            ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                            type = "ipcam";
                            EmptyVideo = "novidipcam.avi";
                        }

                        iHour = ttime.Substring(0, 2);
                        iMinute = ttime.Substring(2, 2);
                        iSec = ttime.Substring(4, 2);
                        time = iHour + ":" + iMinute + ":" + iSec;
                        nameTime = TimeSpan.Parse(time);

                        //if (endtime2[i] >= fromTime.Add(TimeSpan.FromSeconds(30)) && nameTime <= endVtime)
                        //{ }
                        //else
                        //{
                        //  //  cam2b.RemoveAt(i);
                        //}
                    }

                    for (int i = 0; i < cam3b.Count; i++)
                    {
                        string time = Path.GetFileNameWithoutExtension(cam3b[i].ToString());
                        string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);

                        int pos = ttime.IndexOf('_');
                        int lengt = ttime.Length;
                        int cek = lengt - pos;

                        if (cek != 7)
                        {
                            VidStatus = "wrong file name format";
                            numStat = 0;
                        }

                        int setind = time.IndexOf('_');
                        string tt = time.Substring(0, setind);
                        type = "nvs";
                        EmptyVideo = "novidnvs.avi";

                        if (cam3b[i].Contains(ipcam))
                        {
                            setind = ttime.IndexOf('_');
                            tt = ttime.Substring(0, setind);
                            char cht = tt[tt.Length - 1];
                            tt = Convert.ToString(cht);
                            ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                            type = "ipcam";
                            EmptyVideo = "novidipcam.avi";
                        }

                        iHour = ttime.Substring(0, 2);
                        iMinute = ttime.Substring(2, 2);
                        iSec = ttime.Substring(4, 2);
                        time = iHour + ":" + iMinute + ":" + iSec;
                        nameTime = TimeSpan.Parse(time);

                        //if (endtime3[i] >= fromTime.Add(TimeSpan.FromSeconds(30)) && nameTime <= endVtime)
                        //{ }
                        //else
                        //{
                        //    //cam3b.RemoveAt(i);
                        //}
                    }

                    for (int i = 0; i < cam4b.Count; i++)
                    {
                        string time = Path.GetFileNameWithoutExtension(cam4b[i].ToString());
                        string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);

                        int pos = ttime.IndexOf('_');
                        int lengt = ttime.Length;
                        int cek = lengt - pos;

                        if (cek != 7)
                        {
                            VidStatus = "wrong file name format";
                            numStat = 0;
                        }

                        int setind = time.IndexOf('_');
                        string tt = time.Substring(0, setind);
                        type = "nvs";
                        EmptyVideo = "novidnvs.avi";

                        if (cam4b[i].Contains(ipcam))
                        {
                            setind = ttime.IndexOf('_');
                            tt = ttime.Substring(0, setind);
                            char cht = tt[tt.Length - 1];
                            tt = Convert.ToString(cht);
                            ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                            type = "ipcam";
                            EmptyVideo = "novidipcam.avi";
                        }

                        iHour = ttime.Substring(0, 2);
                        iMinute = ttime.Substring(2, 2);
                        iSec = ttime.Substring(4, 2);
                        time = iHour + ":" + iMinute + ":" + iSec;
                        nameTime = TimeSpan.Parse(time);

                        //if (endtime4[i] >= fromTime.Add(TimeSpan.FromSeconds(30)) && nameTime <= endVtime)
                        //{ }
                        //else
                        //{
                        //    //cam4b.RemoveAt(i);
                        //}
                    }

                    for (int i = 0; i < cam5b.Count; i++)
                    {
                        string time = Path.GetFileNameWithoutExtension(cam5b[i].ToString());
                        string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);

                        int pos = ttime.IndexOf('_');
                        int lengt = ttime.Length;
                        int cek = lengt - pos;

                        if (cek != 7)
                        {
                            VidStatus = "wrong file name format";
                            numStat = 0;
                        }

                        int setind = time.IndexOf('_');
                        string tt = time.Substring(0, setind);
                        type = "nvs";
                        EmptyVideo = "novidnvs.avi";

                        if (cam5b[i].Contains(ipcam))
                        {
                            setind = ttime.IndexOf('_');
                            tt = ttime.Substring(0, setind);
                            char cht = tt[tt.Length - 1];
                            tt = Convert.ToString(cht);
                            ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                            type = "ipcam";
                            EmptyVideo = "novidipcam.avi";
                        }

                        iHour = ttime.Substring(0, 2);
                        iMinute = ttime.Substring(2, 2);
                        iSec = ttime.Substring(4, 2);
                        time = iHour + ":" + iMinute + ":" + iSec;
                        nameTime = TimeSpan.Parse(time);

                        //if (endtime4[i] >= fromTime.Add(TimeSpan.FromSeconds(30)) && nameTime <= endVtime)
                        //{ }
                        //else
                        //{
                        //    //cam4b.RemoveAt(i);
                        //}
                    }

                    for (int i = 0; i < cam6b.Count; i++)
                    {
                        string time = Path.GetFileNameWithoutExtension(cam6b[i].ToString());
                        string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);

                        int pos = ttime.IndexOf('_');
                        int lengt = ttime.Length;
                        int cek = lengt - pos;

                        if (cek != 7)
                        {
                            VidStatus = "wrong file name format";
                            numStat = 0;
                        }

                        int setind = time.IndexOf('_');
                        string tt = time.Substring(0, setind);
                        type = "nvs";
                        EmptyVideo = "novidnvs.avi";

                        if (cam6b[i].Contains(ipcam))
                        {
                            setind = ttime.IndexOf('_');
                            tt = ttime.Substring(0, setind);
                            char cht = tt[tt.Length - 1];
                            tt = Convert.ToString(cht);
                            ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                            type = "ipcam";
                            EmptyVideo = "novidipcam.avi";
                        }

                        iHour = ttime.Substring(0, 2);
                        iMinute = ttime.Substring(2, 2);
                        iSec = ttime.Substring(4, 2);
                        time = iHour + ":" + iMinute + ":" + iSec;
                        nameTime = TimeSpan.Parse(time);

                    }

                    for (int i = 0; i < cam7b.Count; i++)
                    {
                        string time = Path.GetFileNameWithoutExtension(cam7b[i].ToString());
                        string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);

                        int pos = ttime.IndexOf('_');
                        int lengt = ttime.Length;
                        int cek = lengt - pos;

                        if (cek != 7)
                        {
                            VidStatus = "wrong file name format";
                            numStat = 0;
                        }

                        int setind = time.IndexOf('_');
                        string tt = time.Substring(0, setind);
                        type = "nvs";
                        EmptyVideo = "novidnvs.avi";

                        if (cam7b[i].Contains(ipcam))
                        {
                            setind = ttime.IndexOf('_');
                            tt = ttime.Substring(0, setind);
                            char cht = tt[tt.Length - 1];
                            tt = Convert.ToString(cht);
                            ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                            type = "ipcam";
                            EmptyVideo = "novidipcam.avi";
                        }

                        iHour = ttime.Substring(0, 2);
                        iMinute = ttime.Substring(2, 2);
                        iSec = ttime.Substring(4, 2);
                        time = iHour + ":" + iMinute + ":" + iSec;
                        nameTime = TimeSpan.Parse(time);

                    }

                    for (int i = 0; i < cam8b.Count; i++)
                    {
                        string time = Path.GetFileNameWithoutExtension(cam8b[i].ToString());
                        string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);

                        int pos = ttime.IndexOf('_');
                        int lengt = ttime.Length;
                        int cek = lengt - pos;

                        if (cek != 7)
                        {
                            VidStatus = "wrong file name format";
                            numStat = 0;
                        }

                        int setind = time.IndexOf('_');
                        string tt = time.Substring(0, setind);
                        type = "nvs";
                        EmptyVideo = "novidnvs.avi";

                        if (cam8b[i].Contains(ipcam))
                        {
                            setind = ttime.IndexOf('_');
                            tt = ttime.Substring(0, setind);
                            char cht = tt[tt.Length - 1];
                            tt = Convert.ToString(cht);
                            ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                            type = "ipcam";
                            EmptyVideo = "novidipcam.avi";
                        }

                        iHour = ttime.Substring(0, 2);
                        iMinute = ttime.Substring(2, 2);
                        iSec = ttime.Substring(4, 2);
                        time = iHour + ":" + iMinute + ":" + iSec;
                        nameTime = TimeSpan.Parse(time);

                    }

                    foreach (object o in cam1b)
                    {
                        Console.WriteLine("->> cam1b. " + o + "\n");
                    }
                    foreach (object o in cam2b)
                    {
                        Console.WriteLine("->> cam2b. " + o + "\n");
                    }
                    foreach (object o in cam3b)
                    {
                        Console.WriteLine("->> cam3b. " + o + "\n");
                    }
                    foreach (object o in cam4b)
                    {
                        Console.WriteLine("->> cam4b. " + o + "\n");
                    }
                    foreach (object o in cam5b)
                    {
                        Console.WriteLine("->> cam5b. " + o + "\n");
                    }
                    foreach (object o in cam6b)
                    {
                        Console.WriteLine("->> cam6b. " + o + "\n");
                    }
                    foreach (object o in cam7b)
                    {
                        Console.WriteLine("->> cam7b. " + o + "\n");
                    }
                    foreach (object o in cam8b)
                    {
                        Console.WriteLine("->> cam8b. " + o + "\n");
                    }
                }

                cam1a.Clear();
                cam2a.Clear();
                cam3a.Clear();
                cam4a.Clear();
                cam5a.Clear();
                cam6a.Clear();
                cam7a.Clear();
                cam8a.Clear();

                endtime1.Clear();
                endtime2.Clear();
                endtime3.Clear();
                endtime4.Clear();
                endtime5.Clear();
                endtime6.Clear();
                endtime7.Clear();
                endtime8.Clear();

                Console.WriteLine("-- start extended filter --");
                if (crossday == 1 && endtime <= TimeSpan.Parse("01:00:00"))
                {
                    FolderNameExt = FolderName + 1;
                    dinfo = new DirectoryInfo(Path.Combine(Folder, FolderNameExt.ToString()));

                    Files = dinfo.GetFiles();
                    //fls = Files.Count();

                    Console.WriteLine("-- start primary filter --");

                    /*-start first filter-*/
                    Console.WriteLine("start first filter ");
                    foreach (FileInfo file in Files.OrderBy(d => d.Name))
                    {
                        if (doPopulate.CancellationPending)
                        {
                            return;
                        }

                        dPath = Path.Combine(FolderNameExt.ToString(), file.ToString());
                        combPath = Path.Combine(Folder, dPath);

                        string time = Path.GetFileNameWithoutExtension(file.ToString());
                        string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                        var pos = ttime.IndexOf('_');
                        var lengt = ttime.Length;
                        int cek = lengt - pos;

                        if (cek != 7)
                        {
                            VidStatus = "wrong file name format";
                            numStat = 0;
                        }

                        int setind = time.IndexOf('_');
                        string tt = time.Substring(0, setind);
                        type = "nvs";
                        EmptyVideo = "novidnvs.avi";
                        if (file.ToString().Contains(ipcam))
                        {
                            setind = ttime.IndexOf('_');
                            tt = ttime.Substring(0, setind);
                            char cht = tt[tt.Length - 1];
                            tt = Convert.ToString(cht);
                            type = "ipcam";
                            EmptyVideo = "novidipcam.avi";
                        }

                        iHour = ttime.Substring(0, 2);
                        iMinute = ttime.Substring(2, 2);
                        iSec = ttime.Substring(4, 2);
                        time = iHour + ":" + iMinute + ":" + iSec;

                        //nameTime = TimeSpan.Parse(time);

                        //if (nameTime >= TimeSpan.Parse("00:00:00") && nameTime < endtime)
                        if (TimeSpan.Parse(time) < endtime)
                        {
                            try
                            {
                                long fileSize = new System.IO.FileInfo(combPath).Length;
                                string sizeReadable = GetSizeReadable(fileSize);
                                var firsout = sizeReadable.Substring(0, sizeReadable.Length - 3);

                                int index = sizeReadable.LastIndexOf(' ');
                                string lastout = sizeReadable.Substring(index + 1);

                                if (lastout == "MB")
                                {
                                    if (tt == "1")
                                    {
                                        cam1c.Add(dPath);
                                    }
                                    else if (tt == "2")
                                    {
                                        cam2c.Add(dPath);
                                    }
                                    else if (tt == "3")
                                    {
                                        cam3c.Add(dPath);
                                    }
                                    else if (tt == "4")
                                    {
                                        cam4c.Add(dPath);
                                    }
                                    else if (tt == "5")
                                    {
                                        cam5c.Add(dPath);
                                    }
                                    else if (tt == "6")
                                    {
                                        cam6c.Add(dPath);
                                    }
                                    else if (tt == "7")
                                    {
                                        cam7c.Add(dPath);
                                    }
                                    else if (tt == "8")
                                    {
                                        cam8c.Add(dPath);
                                    }
                                }
                                else if (lastout == "KB")
                                {
                                    if (Convert.ToDouble(firsout) >= 100)
                                    {
                                        if (tt == "1")
                                        {
                                            cam1c.Add(file.ToString());
                                        }
                                        else if (tt == "2")
                                        {
                                            cam2c.Add(file.ToString());
                                        }
                                        else if (tt == "3")
                                        {
                                            cam3c.Add(file.ToString());
                                        }
                                        else if (tt == "4")
                                        {
                                            cam4c.Add(file.ToString());
                                        }
                                        else if (tt == "5")
                                        {
                                            cam5c.Add(file.ToString());
                                        }
                                        else if (tt == "6")
                                        {
                                            cam6c.Add(file.ToString());
                                        }
                                        else if (tt == "7")
                                        {
                                            cam7c.Add(file.ToString());
                                        }
                                        else if (tt == "8")
                                        {
                                            cam8c.Add(file.ToString());
                                        }
                                    }
                                }
                                Console.WriteLine("file a " + file);
                                Console.WriteLine("nameTime a " + nameTime);
                            }
                            catch (System.Exception excpt)
                            {
                                Console.WriteLine(excpt.Message);
                            }
                        }
                    }
                }

                foreach (object o in cam1c)
                {
                    Console.WriteLine("->> cam1c. " + o + "\n");
                }
                foreach (object o in cam2c)
                {
                    Console.WriteLine("->> cam2c. " + o + "\n");
                }
                foreach (object o in cam3c)
                {
                    Console.WriteLine("->> cam3c. " + o + "\n");
                }
                foreach (object o in cam4c)
                {
                    Console.WriteLine("->> cam4c. " + o + "\n");
                }
                foreach (object o in cam5c)
                {
                    Console.WriteLine("->> cam5c. " + o + "\n");
                }
                foreach (object o in cam6c)
                {
                    Console.WriteLine("->> cam6c. " + o + "\n");
                }
                foreach (object o in cam7c)
                {
                    Console.WriteLine("->> cam7c. " + o + "\n");
                }
                foreach (object o in cam8c)
                {
                    Console.WriteLine("->> cam8c. " + o + "\n");
                }

                convertCam1 = cam1b.Concat(cam1c).ToList();
                convertCam2 = cam2b.Concat(cam2c).ToList();
                convertCam3 = cam3b.Concat(cam3c).ToList();
                convertCam4 = cam4b.Concat(cam4c).ToList();
                convertCam5 = cam5b.Concat(cam5c).ToList();
                convertCam6 = cam6b.Concat(cam6c).ToList();
                convertCam7 = cam7b.Concat(cam7c).ToList();
                convertCam8 = cam8b.Concat(cam8c).ToList();

                Console.WriteLine("->>>>>>>>>>>>>>>>>> ");

                foreach (object o in convertCam1)
                {
                    Console.WriteLine("->> convertCam1. " + o + "\n");
                }
                foreach (object o in convertCam2)
                {
                    Console.WriteLine("->> convertCam2. " + o + "\n");
                }
                foreach (object o in convertCam3)
                {
                    Console.WriteLine("->> convertCam3. " + o + "\n");
                }
                foreach (object o in convertCam4)
                {
                    Console.WriteLine("->> convertCam4. " + o + "\n");
                }

                foreach (object o in convertCam5)
                {
                    Console.WriteLine("->> convertCam5. " + o + "\n");
                }


                foreach (object o in convertCam6)
                {
                    Console.WriteLine("->> convertCam6. " + o + "\n");
                }


                foreach (object o in convertCam7)
                {
                    Console.WriteLine("->> convertCam7. " + o + "\n");
                }


                foreach (object o in convertCam8)
                {
                    Console.WriteLine("->> convertCam8. " + o + "\n");
                }
                Console.WriteLine("->>>>>>>>>>>>>>>>>> ");

                allVideoList = new List<string>(convertCam1.Count +
                            convertCam2.Count +
                            convertCam3.Count +
                            convertCam4.Count +
                            convertCam5.Count +
                            convertCam6.Count +
                            convertCam7.Count +
                            convertCam8.Count
                            );

                allVideoList.AddRange(convertCam1);
                allVideoList.AddRange(convertCam2);
                allVideoList.AddRange(convertCam3);
                allVideoList.AddRange(convertCam4);
                allVideoList.AddRange(convertCam5);
                allVideoList.AddRange(convertCam6);
                allVideoList.AddRange(convertCam7);
                allVideoList.AddRange(convertCam8);

                foreach (object o in allVideoList)
                {
                    Console.WriteLine("->> allVideoList. " + o + "\n");
                }

                foreach (string tmpdat in allVideoList)
                {
                    string dirp = "";
                    string result = "";
                    string ttime = "";
                    int pos = 0;
                    int lengt = 0;
                    int cek = 0;
                    int setind = 0;
                    string tt = "";
                    string file;

                    dirp = Path.Combine(Folder, tmpdat);
                    dateRec = Path.GetFileName(Path.GetDirectoryName(dirp));
                    string busID = Path.GetFileName(Folder);
                    result = Path.GetFileNameWithoutExtension(dirp);

                    ttime = result.ToString().Substring(result.ToString().IndexOf('_') + 1);
                    pos = ttime.IndexOf('_');
                    lengt = ttime.Length;
                    cek = lengt - pos;

                    if (cek != 7)
                    {
                        VidStatus = "wrong file name format";
                        numStat = 0;
                    }

                    setind = result.IndexOf('_');
                    tt = result.Substring(0, setind);
                    type = "nvs";
                    EmptyVideo = "novidnvs.avi";
                    rate = 6;
                    //rate = drate;

                    if (tmpdat.Contains(ipcam))
                    {
                        setind = ttime.IndexOf('_');
                        tt = ttime.Substring(0, setind);
                        char cht = tt[tt.Length - 1];
                        tt = Convert.ToString(cht);
                        ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                        type = "ipcam";
                        rate = 7.27;
                        //rate = drate;
                        EmptyVideo = "novidipcam.avi";
                    }

                    directpath = Path.Combine(pathdirect, "avi");
                    directpath = Path.Combine(directpath, type);
                    directpath = Path.Combine(directpath, busID);
                    numberPath = directpath;

                    directpath = Path.Combine(directpath, dateRec);
                    file = "-f" + rate + " -i \"" + dirp + "\" -o \"" + directpath + "\\" + result + ".avi\"";

                    try
                    {
                        if (!Directory.Exists(directpath))
                        {
                            Console.WriteLine("Create Directory " + Directory.GetCreationTime(directpath));
                            DirectoryInfo di = Directory.CreateDirectory(directpath);
                            Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(directpath));
                        }
                    }
                    catch (Exception et)
                    {
                        Console.WriteLine("The process failed: {0}", et.ToString());
                    }

                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.CreateNoWindow = true;
                    startInfo.UseShellExecute = false;
                    startInfo.FileName = "thirdParty\\avc2avi.exe";
                    startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    startInfo.Arguments = file;
                    Console.WriteLine("File " + file);

                    string comb = Path.Combine(directpath, result + ".avi");
                    try
                    {
                        if (!File.Exists(comb))
                        {
                            using (Process exeProcess = Process.Start(startInfo))
                            {
                                Console.WriteLine("Create File " + comb);
                                exeProcess.WaitForExit();
                            }
                        }
                        else
                        {
                            Console.WriteLine("That File " + comb + " exists already.");
                        }
                    }
                    catch
                    {
                        // Log error.
                    }
                }

                if (allVideoList.Count > 0)
                {
                    FilterAvi();
                }
            }
        }

        /// <summary> filter load video process from avi format. </summary>
        void FilterAvi()
        {
            string ipcam = "ipcam_10.13.201.10";
            string numPath = "";

            endVtime = endtime;
            if (crossday == 1)
                endVtime = TimeSpan.Parse("23:59:59");

            if (fromTime <= endVtime)
            {
                if (inPath.Contains("avi"))
                {
                    if (crossday == 1)
                    {
                        numPath = Path.Combine(numberPath, FolderName.ToString());
                    }
                    else
                    {
                        numPath = inPath;
                    }
                }
                else
                {
                    numPath = Path.Combine(numberPath, FolderName.ToString());
                }

                DirectoryInfo dconvert = new DirectoryInfo(numPath);
                FileInfo[] ConvertFile = dconvert.GetFiles();

                foreach (FileInfo file in ConvertFile.OrderBy(d => d.Name))
                {
                    dPath = Path.Combine(numPath, file.ToString());
                    dateRec = Path.GetFileName(Path.GetDirectoryName(dPath));

                    string time = Path.GetFileNameWithoutExtension(file.ToString());
                    string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                    var pos = ttime.IndexOf('_');
                    var lengt = ttime.Length;
                    int cek = lengt - pos;

                    if (cek != 7)
                    {
                        VidStatus = "wrong file name format";
                        numStat = 0;
                    }

                    int setind = time.IndexOf('_');
                    string tt = time.Substring(0, setind);
                    type = "nvs";
                    EmptyVideo = "novidnvs.avi";

                    if (file.ToString().Contains(ipcam))
                    {
                        setind = ttime.IndexOf('_');
                        tt = ttime.Substring(0, setind);
                        char cht = tt[tt.Length - 1];
                        tt = Convert.ToString(cht);
                        ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                        type = "ipcam";
                        EmptyVideo = "novidipcam.avi";
                    }

                    string iHour = ttime.Substring(0, 2);
                    string iMinute = ttime.Substring(2, 2);
                    string iSec = ttime.Substring(4, 2);
                    time = iHour + ":" + iMinute + ":" + iSec;
                    TimeSpan nameTime = TimeSpan.Parse(time);

                    if (nameTime >= fromTime && nameTime <= endVtime)
                    {
                        if (tt == "1")
                        {
                            cam1.Add(Path.Combine(dateRec, file.ToString()));
                            mediaInfo(dPath, 1);
                            starttime1.Add(nameTime);
                        }
                        else if (tt == "2")
                        {
                            cam2.Add(Path.Combine(dateRec, file.ToString()));
                            mediaInfo(dPath, 2);
                            starttime2.Add(nameTime);
                        }
                        else if (tt == "3")
                        {
                            cam3.Add(Path.Combine(dateRec, file.ToString()));
                            mediaInfo(dPath, 3);
                            starttime3.Add(nameTime);
                        }
                        else if (tt == "4")
                        {
                            cam4.Add(Path.Combine(dateRec, file.ToString()));
                            mediaInfo(dPath, 4);
                            starttime4.Add(nameTime);
                        }
                        else if (tt == "5")
                        {
                            cam5.Add(Path.Combine(dateRec, file.ToString()));
                            mediaInfo(dPath, 5);
                            starttime5.Add(nameTime);
                        }
                        else if (tt == "6")
                        {
                            cam6.Add(Path.Combine(dateRec, file.ToString()));
                            mediaInfo(dPath, 6);
                            starttime6.Add(nameTime);
                        }
                        else if (tt == "7")
                        {
                            cam7.Add(Path.Combine(dateRec, file.ToString()));
                            mediaInfo(dPath, 7);
                            starttime7.Add(nameTime);
                        }
                        else if (tt == "8")
                        {
                            cam8.Add(Path.Combine(dateRec, file.ToString()));
                            mediaInfo(dPath, 8);
                            starttime8.Add(nameTime);
                        }
                    }
                }

                Console.WriteLine("->>>>>>>>>>>>>");
                foreach (object o in cam1)
                {
                    Console.WriteLine("->> cam1. " + o + "\n");
                }
                foreach (object o in cam2)
                {
                    Console.WriteLine("->> cam2. " + o + "\n");
                }
                foreach (object o in cam3)
                {
                    Console.WriteLine("->> cam3. " + o + "\n");
                }
                foreach (object o in cam4)
                {
                    Console.WriteLine("->> cam4. " + o + "\n");
                }
                foreach (object o in cam5)
                {
                    Console.WriteLine("->> cam5. " + o + "\n");
                }
                foreach (object o in cam6)
                {
                    Console.WriteLine("->> cam6. " + o + "\n");
                }
                foreach (object o in cam7)
                {
                    Console.WriteLine("->> cam7. " + o + "\n");
                }
                foreach (object o in cam8)
                {
                    Console.WriteLine("->> cam8. " + o + "\n");
                }

                Console.WriteLine("->>>>>>>>>>>>>");
                foreach (object o in starttime1)
                {
                    Console.WriteLine("->> starttime1. " + o + "\n");
                }

                foreach (object o in starttime2)
                {
                    Console.WriteLine("->> starttime2. " + o + "\n");
                }
                foreach (object o in starttime3)
                {
                    Console.WriteLine("->> starttime3. " + o + "\n");
                }
                foreach (object o in starttime4)
                {
                    Console.WriteLine("->> starttime4. " + o + "\n");
                }
                foreach (object o in starttime5)
                {
                    Console.WriteLine("->> starttime5. " + o + "\n");
                }
                foreach (object o in starttime6)
                {
                    Console.WriteLine("->> starttime6. " + o + "\n");
                }
                foreach (object o in starttime7)
                {
                    Console.WriteLine("->> starttime7. " + o + "\n");
                }
                foreach (object o in starttime8)
                {
                    Console.WriteLine("->> starttime8. " + o + "\n");
                }

                Console.WriteLine("->>>>>>>>>>>>>");
                foreach (object o in duration1)
                {
                    Console.WriteLine("->> duration1. " + o + "\n");
                }
                foreach (object o in duration2)
                {
                    Console.WriteLine("->> duration2. " + o + "\n");
                }
                foreach (object o in duration3)
                {
                    Console.WriteLine("->> duration3. " + o + "\n");
                }
                foreach (object o in duration4)
                {
                    Console.WriteLine("->> duration4. " + o + "\n");
                }
                foreach (object o in duration5)
                {
                    Console.WriteLine("->> duration5. " + o + "\n");
                }
                foreach (object o in duration6)
                {
                    Console.WriteLine("->> duration6. " + o + "\n");
                }
                foreach (object o in duration7)
                {
                    Console.WriteLine("->> duration7. " + o + "\n");
                }
                foreach (object o in duration8)
                {
                    Console.WriteLine("->> duration8. " + o + "\n");
                }
            }

            /*extend*/
            Console.WriteLine("->>>> extended filter avi");
            if (crossday == 1 && endtime <= TimeSpan.Parse("01:00:00"))
            {
                FolderNameExt = FolderName + 1;
                numPath = Path.Combine(numberPath, FolderNameExt.ToString());

                DirectoryInfo dconvExt = new DirectoryInfo(numPath);
                FileInfo[] ConvertFileExt = dconvExt.GetFiles();

                foreach (FileInfo file in ConvertFileExt.OrderBy(d => d.Name))
                {
                    dPath = Path.Combine(numPath, file.ToString());
                    dateRec = Path.GetFileName(Path.GetDirectoryName(dPath));

                    string time = Path.GetFileNameWithoutExtension(file.ToString());
                    string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                    var pos = ttime.IndexOf('_');
                    var lengt = ttime.Length;
                    int cek = lengt - pos;

                    if (cek != 7)
                    {
                        VidStatus = "wrong file name format";
                        numStat = 0;
                    }

                    int setind = time.IndexOf('_');
                    string tt = time.Substring(0, setind);
                    type = "nvs";
                    EmptyVideo = "novidnvs.avi";

                    if (file.ToString().Contains(ipcam))
                    {
                        setind = ttime.IndexOf('_');
                        tt = ttime.Substring(0, setind);
                        char cht = tt[tt.Length - 1];
                        tt = Convert.ToString(cht);
                        ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                        type = "ipcam";
                        EmptyVideo = "novidipcam.avi";
                    }

                    string iHour = ttime.Substring(0, 2);
                    string iMinute = ttime.Substring(2, 2);
                    string iSec = ttime.Substring(4, 2);
                    time = iHour + ":" + iMinute + ":" + iSec;
                    TimeSpan nameTime = TimeSpan.Parse(time);

                    if (nameTime >= TimeSpan.Parse("00:00:00") && nameTime < endtime)
                    {
                        if (tt == "1")
                        {
                            cam1.Add(Path.Combine(dateRec, file.ToString()));
                            mediaInfo(dPath, 1);
                            starttime1.Add(nameTime.Add(TimeSpan.FromDays(1)));
                        }
                        else if (tt == "2")
                        {
                            cam2.Add(Path.Combine(dateRec, file.ToString()));
                            mediaInfo(dPath, 2);
                            starttime2.Add(nameTime.Add(TimeSpan.FromDays(1)));
                        }
                        else if (tt == "3")
                        {
                            cam3.Add(Path.Combine(dateRec, file.ToString()));
                            mediaInfo(dPath, 3);
                            starttime3.Add(nameTime.Add(TimeSpan.FromDays(1)));
                        }
                        else if (tt == "4")
                        {
                            cam4.Add(Path.Combine(dateRec, file.ToString()));
                            mediaInfo(dPath, 4);
                            starttime4.Add(nameTime.Add(TimeSpan.FromDays(1)));
                        }
                        else if (tt == "5")
                        {
                            cam5.Add(Path.Combine(dateRec, file.ToString()));
                            mediaInfo(dPath, 5);
                            starttime5.Add(nameTime.Add(TimeSpan.FromDays(1)));
                        }
                        else if (tt == "6")
                        {
                            cam6.Add(Path.Combine(dateRec, file.ToString()));
                            mediaInfo(dPath, 6);
                            starttime6.Add(nameTime.Add(TimeSpan.FromDays(1)));
                        }
                        else if (tt == "7")
                        {
                            cam7.Add(Path.Combine(dateRec, file.ToString()));
                            mediaInfo(dPath, 7);
                            starttime7.Add(nameTime.Add(TimeSpan.FromDays(1)));
                        }
                        else if (tt == "8")
                        {
                            cam8.Add(Path.Combine(dateRec, file.ToString()));
                            mediaInfo(dPath, 8);
                            starttime8.Add(nameTime.Add(TimeSpan.FromDays(1)));
                        }
                    }
                }
            }

            Console.WriteLine("->>>>>>>>>>>>>");
            foreach (object o in cam1)
            {
                Console.WriteLine("->> cam1. " + o + "\n");
            }
            foreach (object o in cam2)
            {
                Console.WriteLine("->> cam2. " + o + "\n");
            }
            foreach (object o in cam3)
            {
                Console.WriteLine("->> cam3. " + o + "\n");
            }
            foreach (object o in cam4)
            {
                Console.WriteLine("->> cam4. " + o + "\n");
            }
            foreach (object o in cam5)
            {
                Console.WriteLine("->> cam5. " + o + "\n");
            }
            foreach (object o in cam6)
            {
                Console.WriteLine("->> cam6. " + o + "\n");
            }
            foreach (object o in cam7)
            {
                Console.WriteLine("->> cam7. " + o + "\n");
            }
            foreach (object o in cam8)
            {
                Console.WriteLine("->> cam8. " + o + "\n");
            }

            Console.WriteLine("->>>>>>>>>>>>>");
            foreach (object o in starttime1)
            {
                Console.WriteLine("->> starttime1. " + o + "\n");
            }

            foreach (object o in starttime2)
            {
                Console.WriteLine("->> starttime2. " + o + "\n");
            }
            foreach (object o in starttime3)
            {
                Console.WriteLine("->> starttime3. " + o + "\n");
            }
            foreach (object o in starttime4)
            {
                Console.WriteLine("->> starttime4. " + o + "\n");
            }
            foreach (object o in starttime5)
            {
                Console.WriteLine("->> starttime5. " + o + "\n");
            }
            foreach (object o in starttime6)
            {
                Console.WriteLine("->> starttime6. " + o + "\n");
            }
            foreach (object o in starttime7)
            {
                Console.WriteLine("->> starttime7. " + o + "\n");
            }
            foreach (object o in starttime8)
            {
                Console.WriteLine("->> starttime8. " + o + "\n");
            }

            Console.WriteLine("->>>>>>>>>>>>>");
            foreach (object o in duration1)
            {
                Console.WriteLine("->> duration1. " + o + "\n");
            }
            foreach (object o in duration2)
            {
                Console.WriteLine("->> duration2. " + o + "\n");
            }
            foreach (object o in duration3)
            {
                Console.WriteLine("->> duration3. " + o + "\n");
            }
            foreach (object o in duration4)
            {
                Console.WriteLine("->> duration4. " + o + "\n");
            }
            foreach (object o in duration5)
            {
                Console.WriteLine("->> duration5. " + o + "\n");
            }
            foreach (object o in duration6)
            {
                Console.WriteLine("->> duration6. " + o + "\n");
            }
            foreach (object o in duration7)
            {
                Console.WriteLine("->> duration7. " + o + "\n");
            }
            foreach (object o in duration8)
            {
                Console.WriteLine("->> duration8. " + o + "\n");
            }

            for (int i = 0; i < starttime1.Count(); i++)
            {
                endtime1F.Add(starttime1[i].Add(TimeSpan.FromSeconds(duration1[i])));
            }

            for (int i = 0; i < starttime2.Count(); i++)
            {
                endtime2F.Add(starttime2[i].Add(TimeSpan.FromSeconds(duration2[i])));
            }

            for (int i = 0; i < starttime3.Count(); i++)
            {
                endtime3F.Add(starttime3[i].Add(TimeSpan.FromSeconds(duration3[i])));
            }

            for (int i = 0; i < starttime4.Count(); i++)
            {
                endtime4F.Add(starttime4[i].Add(TimeSpan.FromSeconds(duration4[i])));
            }
            for (int i = 0; i < starttime5.Count(); i++)
            {
                endtime5F.Add(starttime5[i].Add(TimeSpan.FromSeconds(duration5[i])));
            }
            for (int i = 0; i < starttime6.Count(); i++)
            {
                endtime6F.Add(starttime6[i].Add(TimeSpan.FromSeconds(duration6[i])));
            }
            for (int i = 0; i < starttime7.Count(); i++)
            {
                endtime7F.Add(starttime7[i].Add(TimeSpan.FromSeconds(duration7[i])));
            }
            for (int i = 0; i < starttime8.Count(); i++)
            {               
                endtime8F.Add(starttime8[i].Add(TimeSpan.FromSeconds(duration8[i])));
            }


            foreach (object o in endtime1F)
            {
                Console.WriteLine("->> endtime1F. " + o + "\n");
            }
            foreach (object o in endtime2F)
            {
                Console.WriteLine("->> endtime2F. " + o + "\n");
            }
            foreach (object o in endtime3F)
            {
                Console.WriteLine("->> endtime3F. " + o + "\n");
            }
            foreach (object o in endtime4F)
            {
                Console.WriteLine("->> endtime4F. " + o + "\n");
            }
            foreach (object o in endtime5F)
            {
                Console.WriteLine("->> endtime5F. " + o + "\n");
            }
            foreach (object o in endtime6F)
            {
                Console.WriteLine("->> endtime6F. " + o + "\n");
            }
            foreach (object o in endtime7F)
            {
                Console.WriteLine("->> endtime7F. " + o + "\n");
            }
            foreach (object o in endtime8F)
            {
                Console.WriteLine("->> endtime8F. " + o + "\n");
            }
        }

        /// <summary> get unit of video file size. </summary>
        public static string GetSizeReadable(long i)
        {
            string sign = (i < 0 ? "-" : "");
            double readable = (i < 0 ? -i : i);
            string suffix;
            if (i >= 0x100000) // Megabyte
            {
                suffix = "MB";
                readable = (double)(i >> 10);
            }
            else if (i >= 0x400) // Kilobyte
            {
                suffix = "KB";
                readable = (double)i;
            }
            else
            {
                return i.ToString(sign + "0 B"); // Byte
            }
            readable = readable / 1024;

            return sign + readable.ToString("0.### ") + suffix;
        }

        /// <summary> convert time in second (double) to time format hh:mm:ss. </summary>
        public string CalculateTime(double Time)
        {
            string mm, ss, CalculatedTime;
            int h, m, s, T;

            Time = Math.Round(Time);
            T = Convert.ToInt32(Time);

            h = (T / 3600);
            T = T % 3600;
            m = (T / 60);
            s = T % 60;

            if (m < 10)
                mm = string.Format("0{0}", m);
            else
                mm = m.ToString();
            if (s < 10)
                ss = string.Format("0{0}", s);
            else
                ss = s.ToString();

            CalculatedTime = string.Format("{0}:{1}:{2}", h, mm, ss);

            return CalculatedTime;
        }

        /// <summary> check start timer, its used for trackbar to get the position from list to position in video. </summary>
        void cekTimeStart()
        {
            string ipcam = "ipcam_10.13.201.10";
            if (playmode == "single")
            {
                string time = Path.GetFileNameWithoutExtension(pathDir);
                string ttime = time.ToString().Substring(time.ToString().IndexOf('_') + 1);
                var pos = ttime.IndexOf('_');
                var lengt = ttime.Length;
                int cek = lengt - pos;

                if (cek != 7)
                {
                    VidStatus = "wrong file name format";
                    numStat = 0;
                }

                int setind = time.IndexOf('_');
                string tt = time.Substring(0, setind);
                type = "nvs";
                EmptyVideo = "novidnvs.avi";
                if (pathDir.Contains(ipcam))
                {
                    setind = ttime.IndexOf('_');
                    tt = ttime.Substring(0, setind);
                    char cht = tt[tt.Length - 1];
                    tt = Convert.ToString(cht);
                    ttime = ttime.ToString().Substring(ttime.ToString().IndexOf('_') + 1);
                    type = "ipcam";
                    EmptyVideo = "novidipcam.avi";
                }

                string iHour = ttime.Substring(0, 2);
                string iMinute = ttime.Substring(2, 2);
                string iSec = ttime.Substring(4, 2);
                time = iHour + ":" + iMinute + ":" + iSec;

                timestart = TimeSpan.Parse(time);
            }
            else if (playmode == "multiple")
            {
                TimeSpan startt1 = TimeSpan.Parse("23:59:59");
                TimeSpan startt2 = TimeSpan.Parse("23:59:59");
                TimeSpan startt3 = TimeSpan.Parse("23:59:59");
                TimeSpan startt4 = TimeSpan.Parse("23:59:59");
                TimeSpan startt5 = TimeSpan.Parse("23:59:59");
                TimeSpan startt6 = TimeSpan.Parse("23:59:59");
                TimeSpan startt7 = TimeSpan.Parse("23:59:59");
                TimeSpan startt8 = TimeSpan.Parse("23:59:59");

                if (starttime1.Count != 0)
                {
                    startt1 = starttime1[0];
                    Console.WriteLine("startt1 " + startt1);
                }
                if (starttime2.Count != 0)
                {
                    startt2 = starttime2[0];
                    Console.WriteLine("startt2 " + startt2);
                }
                if (starttime3.Count != 0)
                {
                    startt3 = starttime3[0];
                    Console.WriteLine("startt3 " + startt3);
                }
                if (starttime4.Count != 0)
                {
                    startt4 = starttime4[0];
                    Console.WriteLine("startt4 " + startt4);
                }
                if (starttime5.Count != 0)
                {
                    startt5 = starttime5[0];
                    Console.WriteLine("startt5 " + startt5);
                }
                if (starttime6.Count != 0)
                {
                    startt6 = starttime6[0];
                    Console.WriteLine("startt6 " + startt6);
                }
                if (starttime7.Count != 0)
                {
                    startt7 = starttime7[0];
                    Console.WriteLine("startt7 " + startt7);
                }
                if (starttime8.Count != 0)
                {
                    startt8 = starttime8[0];
                    Console.WriteLine("startt8 " + startt8);
                }

                if (startt1 <= timestart)
                {
                    timestart = startt1;
                }
                if (startt2 <= timestart)
                {
                    timestart = startt2;
                }
                if (startt3 <= timestart)
                {
                    timestart = startt3;
                }
                if (startt4 <= timestart)
                {
                    timestart = startt4;
                }
                if (startt5 <= timestart)
                {
                    timestart = startt5;
                }
                if (startt6 <= timestart)
                {
                    timestart = startt6;
                }
                if (startt7 <= timestart)
                {
                    timestart = startt7;
                }
                if (startt8 <= timestart)
                {
                    timestart = startt8;
                }

                timestart = timestart.Subtract(TimeSpan.FromSeconds(1));
            }
            lbStartDur.Text = timestart.ToString();
        }

        /// <summary> check video duration </summary>
        void cektimeduration()
        {
            if (playmode == "single")
            {
                duration = durat;
                //lbEndDur.Text = TimeSpan.Parse(CalculateTime(duration)).ToString();
                lbEndDur.Text = fromTime.Add(TimeSpan.Parse(CalculateTime(duration))).ToString();
            }
            else if (playmode == "multiple")
            {
                if (crossday == 1)
                {
                    duration = endtime.Add(TimeSpan.FromDays(1)).TotalSeconds - fromTime.TotalSeconds;
                }
                else
                {
                    duration = endtime.TotalSeconds - fromTime.TotalSeconds;
                }
                //lbEndDur.Text = TimeSpan.Parse(CalculateTime(duration)).ToString();
                lbEndDur.Text = fromTime.Add(TimeSpan.Parse(CalculateTime(duration))).ToString();
            }

            videoTrackBar.Minimum = 0;
            videoTrackBar.Maximum = Convert.ToInt32(Math.Round(duration));
            //videoTrackBar.Width = Convert.ToInt32(Math.Round(duration));
            //label1.Location = new Point(label1.Location.X + videoTrackBar.Width, label1.Location.Y);

            Console.WriteLine("Duration : " + CalculateTime(Convert.ToInt32(duration)));
        }

        /// <summary> resize preview video window to fill client area in channel 1. </summary>
        void ResizeVideoWindow()
        {
            if (videoWin1 == null)
                return;
            Rectangle rc = splitContainer9.Panel1.ClientRectangle;
            //int hr = videoWin1.SetWindowPosition(rc.Left, rc.Top, rc.Right, rc.Bottom);
            videoWin1.SetWindowPosition(rc.Left, rc.Top, rc.Right, rc.Bottom);

            //if (resize != 0)
            //{
            //int nheight, nwidth;
            //int adleft, adtop;

            //nwidth = (int)(rc.Height * ratio);
            //nheight = rc.Height;
            //adleft = (rc.Width - nwidth) / 2;
            //adtop = (rc.Height - nheight) / 2;

            //int hr = videoWin1.SetWindowPosition(adleft, adtop, nwidth, nheight);
            //}
            //else
            //{
            //}
        }

        /// <summary> resize preview video window to fill client area in channel 2. </summary>
        void ResizeVideoWindow2()
        {
            if (videoWin2 == null)
                return;
            Rectangle rc = splitContainer10.Panel1.ClientRectangle;
            //int hr2 = videoWin2.SetWindowPosition(rc.Left, rc.Top, rc.Right, rc.Bottom);
            videoWin2.SetWindowPosition(rc.Left, rc.Top, rc.Right, rc.Bottom);

            //if (resize != 0)
            //{
            //int nheight, nwidth;
            //int adleft, adtop;

            //nwidth = (int)(rc.Height * ratio);
            //nheight = rc.Height;
            //adleft = (rc.Width - nwidth) / 2;
            //adtop = (rc.Height - nheight) / 2;

            //int hr2 = videoWin2.SetWindowPosition(adleft, adtop, nwidth, nheight);
            //}
            //else
            //{
            //}
        }

        /// <summary> resize preview video window to fill client area in channel 3. </summary>
        void ResizeVideoWindow3()
        {
            if (videoWin3 == null)
                return;
            Rectangle rc = splitContainer9.Panel2.ClientRectangle;
            //int hr = videoWin3.SetWindowPosition(rc.Left, rc.Top, rc.Right, rc.Bottom);
            videoWin3.SetWindowPosition(rc.Left, rc.Top, rc.Right, rc.Bottom);

            //if (resize != 0)
            //{
            //int nheight, nwidth;
            //int adleft, adtop;

            //nwidth = (int)(rc.Height * ratio);
            //nheight = rc.Height;
            //adleft = (rc.Width - nwidth) / 2;
            //adtop = (rc.Height - nheight) / 2;

            //int hr = videoWin3.SetWindowPosition(adleft, adtop, nwidth, nheight);
            //}
            //else
            //{
            //}
        }

        /// <summary> resize preview video window to fill client area in channel 4. </summary>
        void ResizeVideoWindow4()
        {
            if (videoWin4 == null)
                return;
            Rectangle rc = splitContainer4.Panel2.ClientRectangle;
            //int hr = videoWin4.SetWindowPosition(rc.Left, rc.Top, rc.Right, rc.Bottom);
            videoWin4.SetWindowPosition(rc.Left, rc.Top, rc.Right, rc.Bottom);

            //if (resize != 0)
            //{
            //    int nheight, nwidth;
            //    int adleft, adtop;

            //    nwidth = (int)(rc.Height * ratio);
            //    nheight = rc.Height;
            //    adleft = (rc.Width - nwidth) / 2;
            //    adtop = (rc.Height - nheight) / 2;

            //    int hr = videoWin4.SetWindowPosition(adleft, adtop, nwidth, nheight);
            ////}
            //else
            //{
            //}
        }

        /// <summary> resize preview video window to fill client area in channel 4. </summary>
        void ResizeVideoWindow5()
        {
            if (videoWin5 == null)
                return;
            Rectangle rc = splitContainer4.Panel1.ClientRectangle;
            //int hr = videoWin4.SetWindowPosition(rc.Left, rc.Top, rc.Right, rc.Bottom);
            videoWin5.SetWindowPosition(rc.Left, rc.Top, rc.Right, rc.Bottom);

            //if (resize != 0)
            //{
            //    int nheight, nwidth;
            //    int adleft, adtop;

            //    nwidth = (int)(rc.Height * ratio);
            //    nheight = rc.Height;
            //    adleft = (rc.Width - nwidth) / 2;
            //    adtop = (rc.Height - nheight) / 2;

            //    int hr = videoWin4.SetWindowPosition(adleft, adtop, nwidth, nheight);
            ////}
            //else
            //{
            //}
        }

        void ResizeVideoWindow6()
        {
            if (videoWin6 == null)
                return;
            Rectangle rc = splitContainer6.Panel2.ClientRectangle;
            //int hr = videoWin4.SetWindowPosition(rc.Left, rc.Top, rc.Right, rc.Bottom);
            videoWin6.SetWindowPosition(rc.Left, rc.Top, rc.Right, rc.Bottom);
        }
        void ResizeVideoWindow7()
        {
            if (videoWin7 == null)
                return;
            Rectangle rc = splitContainer7.Panel2.ClientRectangle;
            //int hr = videoWin4.SetWindowPosition(rc.Left, rc.Top, rc.Right, rc.Bottom);
            videoWin7.SetWindowPosition(rc.Left, rc.Top, rc.Right, rc.Bottom);
        }
        void ResizeVideoWindow8()
        {
            if (videoWin8 == null)
                return;
            Rectangle rc = splitContainer11.Panel2.ClientRectangle;
            //int hr = videoWin4.SetWindowPosition(rc.Left, rc.Top, rc.Right, rc.Bottom);
            videoWin8.SetWindowPosition(rc.Left, rc.Top, rc.Right, rc.Bottom);
        }

        /// <summary> set video rate. </summary>
        void SetRate(double newRate)
        {
            if (mediaPos1 != null)
            {
                int hr1 = mediaPos1.put_Rate(newRate);
            }

            if (mediaPos2 != null)
            {
                int hr2 = mediaPos2.put_Rate(newRate);
            }

            if (mediaPos3 != null)
            {
                int hr3 = mediaPos3.put_Rate(newRate);
            }

            if (mediaPos4 != null)
            {
                int hr4 = mediaPos4.put_Rate(newRate);
            }
            if (mediaPos5 != null)
            {
                int hr5 = mediaPos5.put_Rate(newRate);
            }
            if (mediaPos6 != null)
            {
                int hr6 = mediaPos6.put_Rate(newRate);
            }
            if (mediaPos7 != null)
            {
                int hr7 = mediaPos7.put_Rate(newRate);
            }
            if (mediaPos8 != null)
            {
                int hr8 = mediaPos8.put_Rate(newRate);
            }
        }

        /// <summary> action when window get resize this also resize video preview </summary>
        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
            {
                formState.Maximize(this);
            }

            ResizeVideoWindow();
            ResizeVideoWindow2();
            ResizeVideoWindow3();
            ResizeVideoWindow4();
            ResizeVideoWindow5();
            ResizeVideoWindow6();
            ResizeVideoWindow7();
            ResizeVideoWindow8();

        }

        private bool CheckForm(Form form)
        {
            form = Application.OpenForms[form.Text];
            if (form != null)
                return true;
            else
                return false;
        }

        /// <summary> load video files from selected directory </summary>
        private void btOpenVideo_Click(object sender, EventArgs e)
        {
            btPause_Click(null, null);
            open = new FilterFile(this);
            open.ShowDialog(this);
        }

        /// <summary> set one video channel window to full screen. </summary>
        public void setFullScreen()
        {
            if (!splitContainer9.Panel2Collapsed)
            {
                splitContainer9.Panel2Collapsed = true;

                btcam1.Enabled = false;
                btcam2.Enabled = false;
                btCam3.Enabled = false;
                btCam4.Enabled = false;
            }

            if (!splitContainer8.Panel2Collapsed)
            {
                splitContainer8.Panel2Collapsed = true;
            }
            if (!splitContainer6.Panel2Collapsed)
            {
                splitContainer6.Panel2Collapsed = true;
            }

            ResizeVideoWindow();
        }

        /// <summary> prepare video for playback for single video. </summary>
        void playSingleVideo()
        {
            Console.WriteLine("play single");
            clipFile1 = pathDir;

            if (playState1 != PlayState.Paused)
            {
                this.mediaDet1 = (IMediaDet)new MediaDet();
                this.mediaDet1.put_Filename(pathDir);
            }

            if (!PlayClip1())
                CloseInterfaces();

            //btPlay.Enabled = true;
            //btFastBackward4x.Enabled = true;
            //btFastBackward8x.Enabled = true;
            //btFast4x.Enabled = true;
            //btFast8x.Enabled = true;
            //btSnapshots.Enabled = true;
        }

        /// <summary> prepare video for playback in channel 1. </summary>
        public void Initializevid1(object sender, EventArgs e)
        {
            Console.WriteLine("In seqno1 " + SeqNo1);
            if (SeqNo1 < cam1Count)
            {
                string FolderPath1 = Path.Combine(numberPath, cam1[SeqNo1].ToString());
                this.mediaDet1 = (IMediaDet)new MediaDet();
                this.mediaDet1.put_Filename(FolderPath1);

                clipFile1 = FolderPath1;
                if (!PlayClip1())
                    CloseInterfaces();
            }
            else
            {
                CloseInterfaces();
            }
        }

        /// <summary> prepare video for playback in channel 1. </summary>
        public void Init1(object sender, EventArgs e)
        {
            Console.WriteLine("In seqno1 " + SeqNo1);
            if (SeqNo1 < cam1Count)
            {
                string FolderPath1 = Path.Combine(numberPath, cam1[SeqNo1].ToString());
                this.mediaDet1 = (IMediaDet)new MediaDet();
                this.mediaDet1.put_Filename(FolderPath1);

                clipFile1 = FolderPath1;
                if (!PlayC1())
                    CloseInterfaces();
            }
            else
            {
                CloseInterfaces();
            }
        }

        /// <summary> prepare video for playback in channel 2. </summary>
        public void Initializevid2(object sender, EventArgs e)
        {
            Console.WriteLine("In seqno2 " + SeqNo2);
            if (SeqNo2 < cam2Count)
            {
                string FolderPath2 = Path.Combine(numberPath, cam2[SeqNo2].ToString());
                this.mediaDet2 = (IMediaDet)new MediaDet();
                this.mediaDet2.put_Filename(FolderPath2);

                clipFile2 = FolderPath2;
                if (!PlayClip2())
                    CloseInterfaces2();
            }
            else
            {
                CloseInterfaces2();
            }
        }

        /// <summary> prepare video for playback in channel 2. </summary>
        public void Init2(object sender, EventArgs e)
        {
            Console.WriteLine("In seqno2 " + SeqNo2);
            if (SeqNo2 < cam2Count)
            {
                string FolderPath2 = Path.Combine(numberPath, cam2[SeqNo2].ToString());
                this.mediaDet2 = (IMediaDet)new MediaDet();
                this.mediaDet2.put_Filename(FolderPath2);

                clipFile2 = FolderPath2;
                if (!PlayC2())
                    CloseInterfaces2();
            }
            else
            {
                CloseInterfaces2();
            }
        }

        /// <summary> prepare video for playback in channel 3. </summary>
        public void Initializevid3(object sender, EventArgs e)
        {
            Console.WriteLine("In seqno1 " + SeqNo3);
            if (SeqNo3 < cam3Count)
            {
                string FolderPath3 = Path.Combine(numberPath, cam3[SeqNo3].ToString());
                Console.WriteLine("FolderPath3 " + FolderPath3);
                this.mediaDet3 = (IMediaDet)new MediaDet();
                this.mediaDet3.put_Filename(FolderPath3);

                clipFile3 = FolderPath3;
                if (!PlayClip3())
                    CloseInterfaces3();
            }
            else
            {
                CloseInterfaces3();
            }
        }

        /// <summary> prepare video for playback in channel 3. </summary>
        public void Init3(object sender, EventArgs e)
        {
            Console.WriteLine("In seqno3 " + SeqNo3);
            if (SeqNo3 < cam3Count)
            {
                string FolderPath3 = Path.Combine(numberPath, cam3[SeqNo3].ToString());
                this.mediaDet3 = (IMediaDet)new MediaDet();
                this.mediaDet3.put_Filename(FolderPath3);

                clipFile3 = FolderPath3;
                if (!PlayC3())
                    CloseInterfaces3();
            }
            else
            {
                CloseInterfaces3();
            }
        }

        /// <summary> prepare video for playback in channel 4. </summary>
        public void Initializevid4(object sender, EventArgs e)
        {
            Console.WriteLine("In seqno4 " + SeqNo4);
            if (SeqNo4 < cam4Count)
            {
                string FolderPath4 = Path.Combine(numberPath, cam4[SeqNo4].ToString());
                this.mediaDet4 = (IMediaDet)new MediaDet();
                this.mediaDet4.put_Filename(FolderPath4);

                clipFile4 = FolderPath4;
                if (!PlayClip4())
                    CloseInterfaces4();
            }
            else
            {
                CloseInterfaces4();
            }
        }

        /// <summary> prepare video for playback in channel 4. </summary>
        public void Init4(object sender, EventArgs e)
        {
            Console.WriteLine("In seqno1 " + SeqNo4);
            if (SeqNo4 < cam4Count)
            {
                string FolderPath4 = Path.Combine(numberPath, cam4[SeqNo4].ToString());
                this.mediaDet4 = (IMediaDet)new MediaDet();
                this.mediaDet4.put_Filename(FolderPath4);

                clipFile4 = FolderPath4;
                if (!PlayC4())
                    CloseInterfaces4();
            }
            else
            {
                CloseInterfaces4();
            }
        }

        public void Initializevid5(object sender, EventArgs e)
        {
            Console.WriteLine("In seqno5 " + SeqNo5);
            if (SeqNo5 < cam5Count)
            {
                string FolderPath5 = Path.Combine(numberPath, cam5[SeqNo5].ToString());
                this.mediaDet5 = (IMediaDet)new MediaDet();
                this.mediaDet5.put_Filename(FolderPath5);

                clipFile5 = FolderPath5;
                if (!PlayClip5())
                    CloseInterfaces5();
            }
            else
            {
                CloseInterfaces5();
            }
        }

        public void Initializevid6(object sender, EventArgs e)
        {
            Console.WriteLine("In seqno6 " + SeqNo6);
            if (SeqNo6 < cam6Count)
            {
                string FolderPath6 = Path.Combine(numberPath, cam6[SeqNo6].ToString());
                this.mediaDet6 = (IMediaDet)new MediaDet();
                this.mediaDet6.put_Filename(FolderPath6);

                clipFile6 = FolderPath6;
                if (!PlayClip6())
                    CloseInterfaces6();
            }
            else
            {
                CloseInterfaces6();
            }
        }

        public void Initializevid7(object sender, EventArgs e)
        {
            Console.WriteLine("In seqno7 " + SeqNo7);
            if (SeqNo7 < cam7Count)
            {
                string FolderPath7 = Path.Combine(numberPath, cam7[SeqNo7].ToString());
                this.mediaDet7 = (IMediaDet)new MediaDet();
                this.mediaDet7.put_Filename(FolderPath7);

                clipFile7 = FolderPath7;
                if (!PlayClip7())
                    CloseInterfaces7();
            }
            else
            {
                CloseInterfaces7();
            }
        }

        public void Initializevid8(object sender, EventArgs e)
        {
            Console.WriteLine("In seqno8 " + SeqNo8);
            if (SeqNo8 < cam8Count)
            {
                string FolderPath8 = Path.Combine(numberPath, cam8[SeqNo8].ToString());
                this.mediaDet8 = (IMediaDet)new MediaDet();
                this.mediaDet8.put_Filename(FolderPath8);

                clipFile8 = FolderPath8;
                if (!PlayClip8())
                    CloseInterfaces8();
            }
            else
            {
                CloseInterfaces8();
            }
        }

        /// <summary> prepare video for playback in channel 5. </summary>
        public void Init5(object sender, EventArgs e)
        {
            Console.WriteLine("In seqno5 " + SeqNo5);
            if (SeqNo5 < cam5Count)
            {
                string FolderPath5 = Path.Combine(numberPath, cam5[SeqNo5].ToString());
                this.mediaDet5 = (IMediaDet)new MediaDet();
                this.mediaDet5.put_Filename(FolderPath5);

                clipFile5 = FolderPath5;
                if (!PlayC5())
                    CloseInterfaces5();
            }
            else
            {
                CloseInterfaces5();
            }
        }
        public void Init6(object sender, EventArgs e)
        {
            Console.WriteLine("In seqno6 " + SeqNo6);
            if (SeqNo6 < cam6Count)
            {
                string FolderPath6 = Path.Combine(numberPath, cam6[SeqNo6].ToString());
                this.mediaDet6 = (IMediaDet)new MediaDet();
                this.mediaDet6.put_Filename(FolderPath6);

                clipFile6 = FolderPath6;
                if (!PlayC6())
                    CloseInterfaces6();
            }
            else
            {
                CloseInterfaces6();
            }
        }
        public void Init7(object sender, EventArgs e)
        {
            Console.WriteLine("In seqno7 " + SeqNo7);
            if (SeqNo7 < cam7Count)
            {
                string FolderPath7 = Path.Combine(numberPath, cam7[SeqNo7].ToString());
                this.mediaDet7 = (IMediaDet)new MediaDet();
                this.mediaDet7.put_Filename(FolderPath7);

                clipFile7 = FolderPath7;
                if (!PlayC7())
                    CloseInterfaces7();
            }
            else
            {
                CloseInterfaces7();
            }
        }
        public void Init8(object sender, EventArgs e)
        {
            Console.WriteLine("In seqno8 " + SeqNo8);
            if (SeqNo8 < cam8Count)
            {
                string FolderPath8 = Path.Combine(numberPath, cam8[SeqNo8].ToString());
                this.mediaDet8 = (IMediaDet)new MediaDet();
                this.mediaDet8.put_Filename(FolderPath8);

                clipFile8 = FolderPath8;
                if (!PlayC8())
                    CloseInterfaces8();
            }
            else
            {
                CloseInterfaces8();
            }
        }


        /// <summary> do cleanup and release DirectShow in channel 1. </summary>
        public void CloseInterfaces()
        {
            int hr1 = 0;
            try
            {
                if (mediaCtrl1 != null)
                {
                    hr1 = mediaCtrl1.StopWhenReady();
                    mediaCtrl1 = null;
                }

                //if (playState1 != PlayState.Stopped)
                //{
                playState1 = PlayState.Stopped;
                //}

                if (mediaEvt1 != null)
                {
                    hr1 = mediaEvt1.SetNotifyWindow(IntPtr.Zero, WM_GRAPHNOTIFY, IntPtr.Zero);
                    mediaEvt1 = null;
                }

                if (videoWin1 != null)
                {
                    hr1 = videoWin1.put_Visible(DsHlp.OAFALSE);
                    hr1 = videoWin1.put_Owner(IntPtr.Zero);
                    videoWin1 = null;
                }

                mediaSeek1 = null;
                mediaPos1 = null;
                basicVideo1 = null;
                videoStep1 = null;
                basicAudio = null;
                //splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                if (graphBuilder1 != null)
                    Marshal.ReleaseComObject(graphBuilder1); graphBuilder1 = null;
            }
            catch (Exception)
            { }
        }

        /// <summary> do cleanup and release DirectShow in channel 2. </summary>
        public void CloseInterfaces2()
        {
            int hr2;
            try
            {
                if (mediaCtrl2 != null)
                {
                    hr2 = mediaCtrl2.StopWhenReady();
                    mediaCtrl2 = null;
                }

                //if (playState2 != PlayState.Stopped)
                //{
                playState2 = PlayState.Stopped;
                //}

                if (mediaEvt2 != null)
                {
                    hr2 = mediaEvt2.SetNotifyWindow(IntPtr.Zero, WM_GRAPHNOTIFY, IntPtr.Zero);
                    mediaEvt2 = null;
                }

                if (videoWin2 != null)
                {
                    hr2 = videoWin2.put_Visible(DsHlp.OAFALSE);
                    hr2 = videoWin2.put_Owner(IntPtr.Zero);
                    videoWin2 = null;
                }

                mediaSeek2 = null;
                mediaPos2 = null;
                basicVideo2 = null;
                videoStep2 = null;
                basicAudio = null;
                //splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                if (graphBuilder2 != null)
                    Marshal.ReleaseComObject(graphBuilder2); graphBuilder2 = null;
            }
            catch (Exception)
            { }
        }

        /// <summary> do cleanup and release DirectShow in channel 3. </summary>
        public void CloseInterfaces3()
        {
            int hr3;
            try
            {
                if (mediaCtrl3 != null)
                {
                    hr3 = mediaCtrl3.StopWhenReady();
                    mediaCtrl3 = null;
                }

                //if (playState3 != PlayState.Stopped)
                //{
                playState3 = PlayState.Stopped;
                //}

                if (mediaEvt3 != null)
                {
                    hr3 = mediaEvt3.SetNotifyWindow(IntPtr.Zero, WM_GRAPHNOTIFY, IntPtr.Zero);
                    mediaEvt3 = null;
                }

                if (videoWin3 != null)
                {
                    hr3 = videoWin3.put_Visible(DsHlp.OAFALSE);
                    hr3 = videoWin3.put_Owner(IntPtr.Zero);
                    videoWin3 = null;
                }

                mediaSeek3 = null;
                mediaPos3 = null;
                basicVideo3 = null;
                videoStep3 = null;
                basicAudio = null;
                //splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                if (graphBuilder3 != null)
                    Marshal.ReleaseComObject(graphBuilder3); graphBuilder3 = null;
            }
            catch (Exception)
            { }
        }

        /// <summary> do cleanup and release DirectShow in channel 4. </summary>
        public void CloseInterfaces4()
        {
            int hr4;
            try
            {
                if (mediaCtrl4 != null)
                {
                    hr4 = mediaCtrl4.StopWhenReady();
                    mediaCtrl4 = null;
                }

                //if (playState4 != PlayState.Stopped)
                //{
                playState4 = PlayState.Stopped;
                //}

                if (mediaEvt4 != null)
                {
                    hr4 = mediaEvt4.SetNotifyWindow(IntPtr.Zero, WM_GRAPHNOTIFY, IntPtr.Zero);
                    mediaEvt4 = null;
                }

                if (videoWin4 != null)
                {
                    hr4 = videoWin4.put_Visible(DsHlp.OAFALSE);
                    hr4 = videoWin4.put_Owner(IntPtr.Zero);
                    videoWin4 = null;
                }

                mediaSeek4 = null;
                mediaPos4 = null;
                basicVideo4 = null;
                videoStep4 = null;
                basicAudio = null;
                //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                if (graphBuilder4 != null)
                    Marshal.ReleaseComObject(graphBuilder4); graphBuilder4 = null;
            }
            catch (Exception)
            { }
        }

        public void CloseInterfaces5()
        {
            int hr5;
            try
            {
                if (mediaCtrl5 != null)
                {
                    hr5 = mediaCtrl5.StopWhenReady();
                    mediaCtrl5 = null;
                }

                //if (playState4 != PlayState.Stopped)
                //{
                playState5 = PlayState.Stopped;
                //}

                if (mediaEvt5 != null)
                {
                    hr5 = mediaEvt5.SetNotifyWindow(IntPtr.Zero, WM_GRAPHNOTIFY, IntPtr.Zero);
                    mediaEvt5 = null;
                }

                if (videoWin5 != null)
                {
                    hr5 = videoWin5.put_Visible(DsHlp.OAFALSE);
                    hr5 = videoWin5.put_Owner(IntPtr.Zero);
                    videoWin5 = null;
                }

                mediaSeek5 = null;
                mediaPos5 = null;
                basicVideo5 = null;
                videoStep5 = null;
                basicAudio = null;
                //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                if (graphBuilder5 != null)
                    Marshal.ReleaseComObject(graphBuilder5); graphBuilder5 = null;
            }
            catch (Exception)
            { }
        }

        public void CloseInterfaces6()
        {
            int hr6;
            try
            {
                if (mediaCtrl6 != null)
                {
                    hr6 = mediaCtrl6.StopWhenReady();
                    mediaCtrl6 = null;
                }

                //if (playState4 != PlayState.Stopped)
                //{
                playState6 = PlayState.Stopped;
                //}

                if (mediaEvt6 != null)
                {
                    hr6 = mediaEvt6.SetNotifyWindow(IntPtr.Zero, WM_GRAPHNOTIFY, IntPtr.Zero);
                    mediaEvt6 = null;
                }

                if (videoWin6 != null)
                {
                    hr6 = videoWin6.put_Visible(DsHlp.OAFALSE);
                    hr6 = videoWin6.put_Owner(IntPtr.Zero);
                    videoWin6 = null;
                }

                mediaSeek6 = null;
                mediaPos6 = null;
                basicVideo6 = null;
                videoStep6 = null;
                basicAudio = null;
                //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                if (graphBuilder6 != null)
                    Marshal.ReleaseComObject(graphBuilder6); graphBuilder6 = null;
            }
            catch (Exception)
            { }
        }

        public void CloseInterfaces7()
        {
            int hr7;
            try
            {
                if (mediaCtrl7 != null)
                {
                    hr7 = mediaCtrl7.StopWhenReady();
                    mediaCtrl7 = null;
                }

                //if (playState4 != PlayState.Stopped)
                //{
                playState7 = PlayState.Stopped;
                //}

                if (mediaEvt7 != null)
                {
                    hr7 = mediaEvt7.SetNotifyWindow(IntPtr.Zero, WM_GRAPHNOTIFY, IntPtr.Zero);
                    mediaEvt7 = null;
                }

                if (videoWin5 != null)
                {
                    hr7 = videoWin7.put_Visible(DsHlp.OAFALSE);
                    hr7 = videoWin7.put_Owner(IntPtr.Zero);
                    videoWin5 = null;
                }

                mediaSeek7 = null;
                mediaPos7 = null;
                basicVideo7 = null;
                videoStep7 = null;
                basicAudio = null;
                //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                if (graphBuilder7 != null)
                    Marshal.ReleaseComObject(graphBuilder7); graphBuilder7 = null;
            }
            catch (Exception)
            { }
        }

        public void CloseInterfaces8()
        {
            int hr8;
            try
            {
                if (mediaCtrl8 != null)
                {
                    hr8 = mediaCtrl8.StopWhenReady();
                    mediaCtrl8 = null;
                }

                //if (playState4 != PlayState.Stopped)
                //{
                playState8 = PlayState.Stopped;
                //}

                if (mediaEvt8 != null)
                {
                    hr8 = mediaEvt8.SetNotifyWindow(IntPtr.Zero, WM_GRAPHNOTIFY, IntPtr.Zero);
                    mediaEvt8 = null;
                }

                if (videoWin8 != null)
                {
                    hr8 = videoWin8.put_Visible(DsHlp.OAFALSE);
                    hr8 = videoWin8.put_Owner(IntPtr.Zero);
                    videoWin5 = null;
                }

                mediaSeek8 = null;
                mediaPos8 = null;
                basicVideo8 = null;
                videoStep8 = null;
                basicAudio = null;
                //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                if (graphBuilder8 != null)
                    Marshal.ReleaseComObject(graphBuilder8); graphBuilder8 = null;
            }
            catch (Exception)
            { }
        }

        void setSpeed()
        {
            if (btFast4x.Checked)
            {
                backwardTimer.Stop();
                SetRate(5);
                timer1.Interval = 2000 / 8;
                this.currentVideoState = VideoState.Forwarding;
            }
            else if (btFast8x.Checked)
            {
                backwardTimer.Stop();
                SetRate(7);
                timer1.Interval = 2000 / 13;
                this.currentVideoState = VideoState.Forwarding;
            }
            else if (btFastBackward4x.Checked)
            {
                timer1.Interval = 1000 / 5;
                backwardTimer.Interval = 1000 / 5;
                backwardTimer.Start();
                SetRate(6);
                this.currentVideoState = VideoState.FastBackwards4x;
            }
            else if (btFastBackward8x.Checked)
            {
                timer1.Interval = 1000 / 10;
                backwardTimer.Interval = 1000 / 10;
                backwardTimer.Start();
                SetRate(12);
                this.currentVideoState = VideoState.FastBackwards8x;
            }
        }

        /// <summary> start all the interfaces, graphs and preview window in channel 1. </summary>
        bool PlayClip1()
        {
            try
            {
                CloseInterfaces();
                if (!GetInterfaces())
                    return false;

                CheckClipType();
                if (clipType1 == ClipType.None)
                    return false;

                int hr1 = mediaEvt1.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                setSpeed();

                if (clipType1 == ClipType.AudioVideo || clipType1 == ClipType.VideoOnly)
                {
                    videoWin1.put_Owner(splitContainer9.Panel1.Handle);
                    videoWin1.put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);
                    InitVideoWindow1(1, 1);
                    //GetFrameStepInterface1();
                }
                else
                    InitPlayerWindow();

                hr1 = mediaCtrl1.Run();
                if (hr1 >= 0)
                    playState1 = PlayState.Running;

                return hr1 >= 0;
            }
            catch
            {
                //MessageBox.Show(this, "Could not start clip\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        /// <summary> start all the interfaces, graphs and preview window in channel 1. </summary>
        bool PlayC1()
        {
            try
            {
                CloseInterfaces();
                if (!GetInterfaces())
                    return false;

                CheckClipType();
                if (clipType1 == ClipType.None)
                    return false;

                int hr1 = mediaEvt1.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                setSpeed();

                if (clipType1 == ClipType.AudioVideo || clipType1 == ClipType.VideoOnly)
                {
                    videoWin1.put_Owner(splitContainer9.Panel1.Handle);
                    videoWin1.put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);
                    InitVideoWindow1(1, 1);
                    //GetFrameStepInterface1();
                }
                else
                    InitPlayerWindow();

                hr1 = mediaCtrl1.Pause();
                if (hr1 >= 0)
                    playState1 = PlayState.Paused;

                return hr1 >= 0;
            }
            catch 
            {
                //MessageBox.Show(this, "Could not start clip\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        /// <summary> start all the interfaces, graphs and preview window in channel 2. </summary>
        bool PlayClip2()
        {
            try
            {
                CloseInterfaces2();
                if (!GetInterfaces2())
                    return false;

                CheckClipType2();
                if (clipType2 == ClipType.None)
                    return false;

                int hr2 = mediaEvt2.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                setSpeed();

                if (clipType2 == ClipType.AudioVideo || clipType2 == ClipType.VideoOnly)
                {
                    videoWin2.put_Owner(splitContainer10.Panel1.Handle);
                    videoWin2.put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

                    InitVideoWindow2(1, 1);
                    //GetFrameStepInterface2();
                }
                else
                    InitPlayerWindow2();

                hr2 = mediaCtrl2.Run();
                if (hr2 >= 0)
                    playState2 = PlayState.Running;

                return hr2 >= 0;
            }
            catch
            {
                //MessageBox.Show(this, "Could not start clip\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        /// <summary> start all the interfaces, graphs and preview window in channel 2. </summary>
        bool PlayC2()
        {
            try
            {
                CloseInterfaces2();
                if (!GetInterfaces2())
                    return false;

                CheckClipType2();
                if (clipType2 == ClipType.None)
                    return false;

                int hr2 = mediaEvt2.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);

                if (clipType2 == ClipType.AudioVideo || clipType2 == ClipType.VideoOnly)
                {
                    videoWin2.put_Owner(splitContainer10.Panel1.Handle);
                    videoWin2.put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

                    InitVideoWindow2(1, 1);
                    //GetFrameStepInterface2();
                }
                else
                    InitPlayerWindow2();

                hr2 = mediaCtrl2.Pause();
                if (hr2 >= 0)
                    playState2 = PlayState.Paused;
                return hr2 >= 0;
            }
            catch
            {
                //MessageBox.Show(this, "Could not start clip\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        /// <summary> start all the interfaces, graphs and preview window in channel 3. </summary>
        bool PlayClip3()
        {
            try
            {
                CloseInterfaces3();
                if (!GetInterfaces3())
                    return false;

                CheckClipType3();
                if (clipType3 == ClipType.None)
                    return false;

                int hr3 = mediaEvt3.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                setSpeed();

                if (clipType3 == ClipType.AudioVideo || clipType3 == ClipType.VideoOnly)
                {
                    videoWin3.put_Owner(splitContainer9.Panel2.Handle);
                    videoWin3.put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

                    InitVideoWindow3(1, 1);
                    //GetFrameStepInterface3();
                }
                else
                    InitPlayerWindow3();

                hr3 = mediaCtrl3.Run();
                if (hr3 >= 0)
                    playState3 = PlayState.Running;

                return hr3 >= 0;
            }
            catch 
            {
                //MessageBox.Show(this, "Could not start clip\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        /// <summary> start all the interfaces, graphs and preview window in channel 3. </summary>
        bool PlayC3()
        {
            try
            {
                CloseInterfaces3();
                if (!GetInterfaces3())
                    return false;

                CheckClipType3();
                if (clipType3 == ClipType.None)
                    return false;

                int hr3 = mediaEvt3.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                if (clipType3 == ClipType.AudioVideo || clipType3 == ClipType.VideoOnly)
                {
                    videoWin3.put_Owner(splitContainer9.Panel2.Handle);
                    videoWin3.put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

                    InitVideoWindow3(1, 1);
                    //GetFrameStepInterface3();
                }
                else
                    InitPlayerWindow3();

                hr3 = mediaCtrl3.Pause();
                if (hr3 >= 0)
                    playState3 = PlayState.Paused;

                return hr3 >= 0;
            }
            catch 
            {
                //MessageBox.Show(this, "Could not start clip\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        /// <summary> start all the interfaces, graphs and preview window in channel 4. </summary>
        bool PlayClip4()
        {
            try
            {
                CloseInterfaces4();
                if (!GetInterfaces4())
                    return false;

                CheckClipType4();
                if (clipType4 == ClipType.None)
                    return false;

                int hr4 = mediaEvt4.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                setSpeed();

                if (clipType4 == ClipType.AudioVideo || clipType4 == ClipType.VideoOnly)
                {
                    videoWin4.put_Owner(splitContainer4.Panel2.Handle);
                    videoWin4.put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

                    InitVideoWindow4(1, 1);
                    //GetFrameStepInterface4();
                }
                else
                    InitPlayerWindow4();

                hr4 = mediaCtrl4.Run();
                if (hr4 >= 0)
                    playState4 = PlayState.Running;

                return hr4 >= 0;
            }
            catch 
            {
                // MessageBox.Show(this, "Could not start clip\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        /// <summary> start all the interfaces, graphs and preview window in channel 4. </summary>
        bool PlayC4()
        {
            try
            {
                CloseInterfaces4();
                if (!GetInterfaces4())
                    return false;

                CheckClipType4();
                if (clipType4 == ClipType.None)
                    return false;

                int hr4 = mediaEvt4.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                if (clipType4 == ClipType.AudioVideo || clipType4 == ClipType.VideoOnly)
                {
                    videoWin4.put_Owner(splitContainer4.Panel2.Handle);
                    videoWin4.put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

                    InitVideoWindow4(1, 1);
                    //GetFrameStepInterface4();
                }
                else
                    InitPlayerWindow4();

                hr4 = mediaCtrl4.Pause();
                if (hr4 >= 0)
                    playState4 = PlayState.Paused;
                return hr4 >= 0;
            }
            catch 
            {
                //MessageBox.Show(this, "Could not start clip\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        bool PlayClip5()
        {
            try
            {
                CloseInterfaces5();
                if (!GetInterfaces5())
                    return false;

                CheckClipType5();
                if (clipType5 == ClipType.None)
                    return false;

                int hr5 = mediaEvt5.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                setSpeed();

                if (clipType5 == ClipType.AudioVideo || clipType5 == ClipType.VideoOnly)
                {
                    videoWin5.put_Owner(splitContainer4.Panel1.Handle);
                    videoWin5.put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

                    InitVideoWindow5(1, 1);
                    //GetFrameStepInterface4();
                }
                else
                    InitPlayerWindow5();

                hr5 = mediaCtrl5.Run();
                if (hr5 >= 0)
                    playState5 = PlayState.Running;

                return hr5 >= 0;
            }
            catch 
            {
                // MessageBox.Show(this, "Could not start clip\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        bool PlayClip6()
        {
            try
            {
                CloseInterfaces6();
                if (!GetInterfaces6())
                    return false;

                CheckClipType6();
                if (clipType6 == ClipType.None)
                    return false;

                int hr6 = mediaEvt6.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                setSpeed();

                if (clipType6 == ClipType.AudioVideo || clipType6 == ClipType.VideoOnly)
                {
                    videoWin6.put_Owner(splitContainer6.Panel2.Handle);
                    videoWin6.put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

                    InitVideoWindow6(1, 1);
                    //GetFrameStepInterface4();
                }
                else
                    InitPlayerWindow6();

                hr6 = mediaCtrl6.Run();
                if (hr6 >= 0)
                    playState6 = PlayState.Running;

                return hr6 >= 0;
            }
            catch
            {
                // MessageBox.Show(this, "Could not start clip\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        bool PlayClip7()
        {
            try
            {
                CloseInterfaces7();
                if (!GetInterfaces7())
                    return false;

                CheckClipType7();
                if (clipType7 == ClipType.None)
                    return false;

                int hr7 = mediaEvt7.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                setSpeed();

                if (clipType7 == ClipType.AudioVideo || clipType7 == ClipType.VideoOnly)
                {
                    videoWin7.put_Owner(splitContainer7.Panel2.Handle);
                    videoWin7.put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

                    InitVideoWindow7(1, 1);
                    //GetFrameStepInterface4();
                }
                else
                    InitPlayerWindow7();

                hr7 = mediaCtrl7.Run();
                if (hr7 >= 0)
                    playState7 = PlayState.Running;

                return hr7 >= 0;
            }
            catch
            {
                // MessageBox.Show(this, "Could not start clip\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        bool PlayClip8()
        {
            try
            {
                CloseInterfaces8();
                if (!GetInterfaces8())
                    return false;

                CheckClipType8();
                if (clipType8 == ClipType.None)
                    return false;

                int hr8 = mediaEvt8.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                setSpeed();

                if (clipType8 == ClipType.AudioVideo || clipType8 == ClipType.VideoOnly)
                {
                    videoWin8.put_Owner(splitContainer11.Panel2.Handle);
                    videoWin8.put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

                    InitVideoWindow8(1, 1);
                    //GetFrameStepInterface4();
                }
                else
                    InitPlayerWindow8();

                hr8 = mediaCtrl8.Run();
                if (hr8 >= 0)
                    playState7 = PlayState.Running;

                return hr8 >= 0;
            }
            catch
            {
                // MessageBox.Show(this, "Could not start clip\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        /// <summary> start all the interfaces, graphs and preview window in channel 4. </summary>
        bool PlayC5()
        {
            try
            {
                //checkpoint
                CloseInterfaces5();
                if (!GetInterfaces5())
                    return false;

                CheckClipType5();
                if (clipType5 == ClipType.None)
                    return false;

                int hr5 = mediaEvt5.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                if (clipType5 == ClipType.AudioVideo || clipType5 == ClipType.VideoOnly)
                {
                    videoWin5.put_Owner(splitContainer4.Panel1.Handle);
                    videoWin5.put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

                    InitVideoWindow5(1, 1);
                    //GetFrameStepInterface4();
                }
                else
                    InitPlayerWindow5();

                hr5 = mediaCtrl5.Pause();
                if (hr5 >= 0)
                    playState5 = PlayState.Paused;
                return hr5 >= 0;
            }
            catch 
            {
                //MessageBox.Show(this, "Could not start clip\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        bool PlayC6()
        {
            try
            {
                //checkpoint
                CloseInterfaces6();
                if (!GetInterfaces6())
                    return false;

                CheckClipType6();
                if (clipType6 == ClipType.None)
                    return false;

                int hr6 = mediaEvt6.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                if (clipType6 == ClipType.AudioVideo || clipType6 == ClipType.VideoOnly)
                {
                    videoWin6.put_Owner(splitContainer6.Panel2.Handle);
                    videoWin6.put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

                    InitVideoWindow6(1, 1);
                    //GetFrameStepInterface4();
                }
                else
                    InitPlayerWindow6();

                hr6 = mediaCtrl6.Pause();
                if (hr6 >= 0)
                    playState6 = PlayState.Paused;
                return hr6 >= 0;
            }
            catch
            {
                //MessageBox.Show(this, "Could not start clip\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        bool PlayC7()
        {
            try
            {
                //checkpoint
                CloseInterfaces7();
                if (!GetInterfaces7())
                    return false;

                CheckClipType7();
                if (clipType7 == ClipType.None)
                    return false;

                int hr7 = mediaEvt7.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                if (clipType7 == ClipType.AudioVideo || clipType7 == ClipType.VideoOnly)
                {
                    videoWin7.put_Owner(splitContainer7.Panel2.Handle);
                    videoWin7.put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

                    InitVideoWindow7(1, 1);
                    //GetFrameStepInterface4();
                }
                else
                    InitPlayerWindow7();

                hr7 = mediaCtrl7.Pause();
                if (hr7 >= 0)
                    playState7 = PlayState.Paused;
                return hr7 >= 0;
            }
            catch
            {
                //MessageBox.Show(this, "Could not start clip\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        bool PlayC8()
        {
            try
            {
                //checkpoint
                CloseInterfaces8();
                if (!GetInterfaces8())
                    return false;

                CheckClipType8();
                if (clipType8 == ClipType.None)
                    return false;

                int hr8 = mediaEvt8.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                if (clipType8 == ClipType.AudioVideo || clipType8 == ClipType.VideoOnly)
                {
                    videoWin8.put_Owner(splitContainer11.Panel2.Handle);
                    videoWin8.put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

                    InitVideoWindow8(1, 1);
                    //GetFrameStepInterface4();
                }
                else
                    InitPlayerWindow8();

                hr8 = mediaCtrl8.Pause();
                if (hr8 >= 0)
                    playState8 = PlayState.Paused;
                return hr8 >= 0;
            }
            catch
            {
                //MessageBox.Show(this, "Could not start clip\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        /// <summary> create the used COM components and get the interfaces in channel 1. </summary>
        bool GetInterfaces()
        {
            Type comtype1 = null;
            object comobj = null;
            try
            {
                comtype1 = Type.GetTypeFromCLSID(Clsid.FilterGraph);
                if (comtype1 == null)
                    throw new NotSupportedException("DirectX (8.1 or higher) not installed?");
                comobj = Activator.CreateInstance(comtype1);
                graphBuilder1 = (DShowNET.IGraphBuilder)comobj; comobj = null;

                int hr1 = graphBuilder1.RenderFile(clipFile1, null);
                if (hr1 < 0)
                    Marshal.ThrowExceptionForHR(hr1);

                mediaCtrl1 = (DShowNET.IMediaControl)graphBuilder1;
                mediaEvt1 = (DShowNET.IMediaEventEx)graphBuilder1;
                mediaSeek1 = (DShowNET.IMediaSeeking)graphBuilder1;
                mediaPos1 = (DShowNET.IMediaPosition)graphBuilder1;

                videoWin1 = graphBuilder1 as DShowNET.IVideoWindow;
                basicVideo1 = graphBuilder1 as DShowNET.IBasicVideo2;
                basicAudio = graphBuilder1 as DShowNET.IBasicAudio;
                basicAudio.put_Volume(-1000);
                return true;
            }
            catch 
            {
                //MessageBox.Show(this, "Could not get interfaces\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            finally
            {
                if (comobj != null)
                    Marshal.ReleaseComObject(comobj); comobj = null;
            }
        }

        /// <summary> create the used COM components and get the interfaces in channel 2. </summary>
        bool GetInterfaces2()
        {
            Type comtype2 = null;
            object comobj = null;
            try
            {
                comtype2 = Type.GetTypeFromCLSID(Clsid.FilterGraph);
                if (comtype2 == null)
                    throw new NotSupportedException("DirectX (8.1 or higher) not installed?");
                comobj = Activator.CreateInstance(comtype2);
                graphBuilder2 = (DShowNET.IGraphBuilder)comobj; comobj = null;

                int hr2 = graphBuilder2.RenderFile(clipFile2, null);
                if (hr2 < 0)
                    Marshal.ThrowExceptionForHR(hr2);

                mediaCtrl2 = (DShowNET.IMediaControl)graphBuilder2;
                mediaEvt2 = (DShowNET.IMediaEventEx)graphBuilder2;
                mediaSeek2 = (DShowNET.IMediaSeeking)graphBuilder2;
                mediaPos2 = (DShowNET.IMediaPosition)graphBuilder2;

                videoWin2 = graphBuilder2 as DShowNET.IVideoWindow;
                basicVideo2 = graphBuilder2 as DShowNET.IBasicVideo2;
                basicAudio = graphBuilder2 as DShowNET.IBasicAudio;
                basicAudio.put_Volume(-1000);
                return true;
            }
            catch 
            {
                //MessageBox.Show(this, "Could not get interfaces\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            finally
            {
                if (comobj != null)
                    Marshal.ReleaseComObject(comobj); comobj = null;
            }
        }

        /// <summary> create the used COM components and get the interfaces in channel 3. </summary>
        bool GetInterfaces3()
        {
            Type comtype3 = null;
            object comobj = null;
            try
            {
                comtype3 = Type.GetTypeFromCLSID(Clsid.FilterGraph);
                if (comtype3 == null)
                    throw new NotSupportedException("DirectX (8.1 or higher) not installed?");
                comobj = Activator.CreateInstance(comtype3);
                graphBuilder3 = (DShowNET.IGraphBuilder)comobj; comobj = null;

                int hr3 = graphBuilder3.RenderFile(clipFile3, null);
                if (hr3 < 0)
                    Marshal.ThrowExceptionForHR(hr3);

                mediaCtrl3 = (DShowNET.IMediaControl)graphBuilder3;
                mediaEvt3 = (DShowNET.IMediaEventEx)graphBuilder3;
                mediaSeek3 = (DShowNET.IMediaSeeking)graphBuilder3;
                mediaPos3 = (DShowNET.IMediaPosition)graphBuilder3;

                videoWin3 = graphBuilder3 as DShowNET.IVideoWindow;
                basicVideo3 = graphBuilder3 as DShowNET.IBasicVideo2;
                basicAudio = graphBuilder3 as DShowNET.IBasicAudio;
                basicAudio.put_Volume(-1000);
                return true;
            }
            catch 
            {
                //MessageBox.Show(this, "Could not get interfaces\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            finally
            {
                if (comobj != null)
                    Marshal.ReleaseComObject(comobj); comobj = null;
            }
        }

        /// <summary> create the used COM components and get the interfaces in channel 4. </summary>
        bool GetInterfaces4()
        {
            Type comtype4 = null;
            object comobj = null;
            try
            {
                comtype4 = Type.GetTypeFromCLSID(Clsid.FilterGraph);
                if (comtype4 == null)
                    throw new NotSupportedException("DirectX (8.1 or higher) not installed?");
                comobj = Activator.CreateInstance(comtype4);
                graphBuilder4 = (DShowNET.IGraphBuilder)comobj; comobj = null;

                int hr4 = graphBuilder4.RenderFile(clipFile4, null);
                if (hr4 < 0)
                    Marshal.ThrowExceptionForHR(hr4);

                mediaCtrl4 = (DShowNET.IMediaControl)graphBuilder4;
                mediaEvt4 = (DShowNET.IMediaEventEx)graphBuilder4;
                mediaSeek4 = (DShowNET.IMediaSeeking)graphBuilder4;
                mediaPos4 = (DShowNET.IMediaPosition)graphBuilder4;

                videoWin4 = graphBuilder4 as DShowNET.IVideoWindow;
                basicVideo4 = graphBuilder4 as DShowNET.IBasicVideo2;
                basicAudio = graphBuilder4 as DShowNET.IBasicAudio;
                basicAudio.put_Volume(-1000);
                return true;
            }
            catch 
            {
                //MessageBox.Show(this, "Could not get interfaces\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            finally
            {
                if (comobj != null)
                    Marshal.ReleaseComObject(comobj); comobj = null;
            }
        }


        // <summary> create the used COM components and get the interfaces in channel 4. </summary>
        bool GetInterfaces5()
        {
            Type comtype5 = null;
            object comobj = null;
            try
            {
                comtype5 = Type.GetTypeFromCLSID(Clsid.FilterGraph);
                if (comtype5 == null)
                    throw new NotSupportedException("DirectX (8.1 or higher) not installed?");
                comobj = Activator.CreateInstance(comtype5);
                graphBuilder5 = (DShowNET.IGraphBuilder)comobj; comobj = null;

                int hr5 = graphBuilder5.RenderFile(clipFile5, null);
                if (hr5 < 0)
                    Marshal.ThrowExceptionForHR(hr5);

                mediaCtrl5 = (DShowNET.IMediaControl)graphBuilder5;
                mediaEvt5 = (DShowNET.IMediaEventEx)graphBuilder5;
                mediaSeek5 = (DShowNET.IMediaSeeking)graphBuilder5;
                mediaPos5 = (DShowNET.IMediaPosition)graphBuilder5;

                videoWin5 = graphBuilder5 as DShowNET.IVideoWindow;
                basicVideo5 = graphBuilder5 as DShowNET.IBasicVideo2;
                basicAudio = graphBuilder5 as DShowNET.IBasicAudio;
                basicAudio.put_Volume(-1000);
                return true;
            }
            catch 
            {
                //MessageBox.Show(this, "Could not get interfaces\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            finally
            {
                if (comobj != null)
                    Marshal.ReleaseComObject(comobj); comobj = null;
            }
        }

        bool GetInterfaces6()
        {
            Type comtype6 = null;
            object comobj = null;
            try
            {
                comtype6 = Type.GetTypeFromCLSID(Clsid.FilterGraph);
                if (comtype6 == null)
                    throw new NotSupportedException("DirectX (8.1 or higher) not installed?");
                comobj = Activator.CreateInstance(comtype6);
                graphBuilder6 = (DShowNET.IGraphBuilder)comobj; comobj = null;

                int hr6 = graphBuilder6.RenderFile(clipFile6, null);
                if (hr6 < 0)
                    Marshal.ThrowExceptionForHR(hr6);

                mediaCtrl6 = (DShowNET.IMediaControl)graphBuilder6;
                mediaEvt6 = (DShowNET.IMediaEventEx)graphBuilder6;
                mediaSeek6 = (DShowNET.IMediaSeeking)graphBuilder6;
                mediaPos6 = (DShowNET.IMediaPosition)graphBuilder6;

                videoWin6 = graphBuilder6 as DShowNET.IVideoWindow;
                basicVideo6 = graphBuilder6 as DShowNET.IBasicVideo2;
                basicAudio = graphBuilder6 as DShowNET.IBasicAudio;
                basicAudio.put_Volume(-1000);
                return true;
            }
            catch
            {
                //MessageBox.Show(this, "Could not get interfaces\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            finally
            {
                if (comobj != null)
                    Marshal.ReleaseComObject(comobj); comobj = null;
            }
        }

        bool GetInterfaces7()
        {
            Type comtype7 = null;
            object comobj = null;
            try
            {
                comtype7 = Type.GetTypeFromCLSID(Clsid.FilterGraph);
                if (comtype7 == null)
                    throw new NotSupportedException("DirectX (8.1 or higher) not installed?");
                comobj = Activator.CreateInstance(comtype7);
                graphBuilder7 = (DShowNET.IGraphBuilder)comobj; comobj = null;

                int hr7 = graphBuilder7.RenderFile(clipFile7, null);
                if (hr7 < 0)
                    Marshal.ThrowExceptionForHR(hr7);

                mediaCtrl7 = (DShowNET.IMediaControl)graphBuilder7;
                mediaEvt7 = (DShowNET.IMediaEventEx)graphBuilder7;
                mediaSeek7 = (DShowNET.IMediaSeeking)graphBuilder7;
                mediaPos7 = (DShowNET.IMediaPosition)graphBuilder7;

                videoWin7 = graphBuilder7 as DShowNET.IVideoWindow;
                basicVideo7 = graphBuilder7 as DShowNET.IBasicVideo2;
                basicAudio = graphBuilder7 as DShowNET.IBasicAudio;
                basicAudio.put_Volume(-1000);
                return true;
            }
            catch
            {
                //MessageBox.Show(this, "Could not get interfaces\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            finally
            {
                if (comobj != null)
                    Marshal.ReleaseComObject(comobj); comobj = null;
            }
        }

        bool GetInterfaces8()
        {
            Type comtype8 = null;
            object comobj = null;
            try
            {
                comtype8 = Type.GetTypeFromCLSID(Clsid.FilterGraph);
                if (comtype8 == null)
                    throw new NotSupportedException("DirectX (8.1 or higher) not installed?");
                comobj = Activator.CreateInstance(comtype8);
                graphBuilder8 = (DShowNET.IGraphBuilder)comobj; comobj = null;

                int hr8 = graphBuilder8.RenderFile(clipFile8, null);
                if (hr8 < 0)
                    Marshal.ThrowExceptionForHR(hr8);

                mediaCtrl8 = (DShowNET.IMediaControl)graphBuilder8;
                mediaEvt8 = (DShowNET.IMediaEventEx)graphBuilder8;
                mediaSeek8 = (DShowNET.IMediaSeeking)graphBuilder8;
                mediaPos8 = (DShowNET.IMediaPosition)graphBuilder8;

                videoWin8 = graphBuilder8 as DShowNET.IVideoWindow;
                basicVideo8 = graphBuilder8 as DShowNET.IBasicVideo2;
                basicAudio = graphBuilder8 as DShowNET.IBasicAudio;
                basicAudio.put_Volume(-1000);
                return true;
            }
            catch
            {
                //MessageBox.Show(this, "Could not get interfaces\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            finally
            {
                if (comobj != null)
                    Marshal.ReleaseComObject(comobj); comobj = null;
            }
        }

        /// <summary> try to detect clip type (video/audio) [not reliable] in chanel 1. </summary>
        void CheckClipType()
        {
            if (basicAudio == null)
                clipType1 = ClipType.None;
            else
                clipType1 = ClipType.AudioOnly;

            if (videoWin1 == null || basicVideo1 == null)
                return;

            int visible;
            int hr1 = videoWin1.get_Visible(out visible);
            if (hr1 < 0)
                return;
            else
            {
                if (basicAudio == null)
                    clipType1 = ClipType.VideoOnly;
                else
                    clipType1 = ClipType.AudioVideo;
            }
        }

        /// <summary> try to detect clip type (video/audio) [not reliable] in chanel 2. </summary>
        void CheckClipType2()
        {
            if (basicAudio == null)
                clipType2 = ClipType.None;
            else
                clipType2 = ClipType.AudioOnly;

            if (videoWin2 == null || basicVideo2 == null)
                return;

            int visible;
            int hr2 = videoWin2.get_Visible(out visible);
            if (hr2 < 0)
                return;
            else
            {
                if (basicAudio == null)
                    clipType2 = ClipType.VideoOnly;
                else
                    clipType2 = ClipType.AudioVideo;
            }
        }

        /// <summary> try to detect clip type (video/audio) [not reliable] in chanel 3. </summary>
        void CheckClipType3()
        {
            if (basicAudio == null)
                clipType3 = ClipType.None;
            else
                clipType3 = ClipType.AudioOnly;

            if (videoWin3 == null || basicVideo3 == null)
                return;

            int visible;
            int hr3 = videoWin3.get_Visible(out visible);
            if (hr3 < 0)
                return;
            else
            {
                if (basicAudio == null)
                    clipType3 = ClipType.VideoOnly;
                else
                    clipType3 = ClipType.AudioVideo;
            }
        }

        /// <summary> try to detect clip type (video/audio) [not reliable] in chanel 4. </summary>
        void CheckClipType4()
        {
            if (basicAudio == null)
                clipType4 = ClipType.None;
            else
                clipType4 = ClipType.AudioOnly;

            if (videoWin4 == null || basicVideo4 == null)
                return;

            int visible;
            int hr4 = videoWin4.get_Visible(out visible);
            if (hr4 < 0)
                return;
            else
            {
                if (basicAudio == null)
                    clipType4 = ClipType.VideoOnly;
                else
                    clipType4 = ClipType.AudioVideo;
            }
        }

        void CheckClipType5()
        {
            if (basicAudio == null)
                clipType5 = ClipType.None;
            else
                clipType5 = ClipType.AudioOnly;

            if (videoWin5 == null || basicVideo5 == null)
                return;

            int visible;
            int hr5 = videoWin5.get_Visible(out visible);
            if (hr5 < 0)
                return;
            else
            {
                if (basicAudio == null)
                    clipType5 = ClipType.VideoOnly;
                else
                    clipType5 = ClipType.AudioVideo;
            }
        }

        void CheckClipType6()
        {
            if (basicAudio == null)
                clipType6 = ClipType.None;
            else
                clipType6 = ClipType.AudioOnly;

            if (videoWin6 == null || basicVideo6 == null)
                return;

            int visible;
            int hr6 = videoWin6.get_Visible(out visible);
            if (hr6 < 0)
                return;
            else
            {
                if (basicAudio == null)
                    clipType6 = ClipType.VideoOnly;
                else
                    clipType6 = ClipType.AudioVideo;
            }
        }

        void CheckClipType7()
        {
            if (basicAudio == null)
                clipType7 = ClipType.None;
            else
                clipType7 = ClipType.AudioOnly;

            if (videoWin7 == null || basicVideo7 == null)
                return;

            int visible;
            int hr7 = videoWin7.get_Visible(out visible);
            if (hr7 < 0)
                return;
            else
            {
                if (basicAudio == null)
                    clipType7 = ClipType.VideoOnly;
                else
                    clipType7 = ClipType.AudioVideo;
            }
        }

        void CheckClipType8()
        {
            if (basicAudio == null)
                clipType8 = ClipType.None;
            else
                clipType8 = ClipType.AudioOnly;

            if (videoWin8 == null || basicVideo8 == null)
                return;

            int visible;
            int hr8 = videoWin8.get_Visible(out visible);
            if (hr8 < 0)
                return;
            else
            {
                if (basicAudio == null)
                    clipType8 = ClipType.VideoOnly;
                else
                    clipType8 = ClipType.AudioVideo;
            }
        }

        /// <summary> configure window for audio playback channel 1. </summary>
        bool InitPlayerWindow()
        {
            splitContainer9.Panel1.ClientSize = new Size(240, 120);
            return true;
        }

        /// <summary> configure window for audio playback channel 2. </summary>
        bool InitPlayerWindow2()
        {
            splitContainer10.Panel1.ClientSize = new Size(240, 120);
            return true;
        }

        /// <summary> configure window for audio playback channel 3. </summary>
        bool InitPlayerWindow3()
        {
            splitContainer9.Panel2.ClientSize = new Size(240, 120);
            return true;
        }

        /// <summary> configure window for audio playback channel 4. </summary>
        bool InitPlayerWindow4()
        {
            splitContainer4.Panel2.ClientSize = new Size(240, 120);
            return true;
        }

        /// <summary> configure window for audio playback channel 5. </summary>
        bool InitPlayerWindow5()
        {
            splitContainer4.Panel1.ClientSize = new Size(240, 120);
            return true;
        }

        /// <summary> configure window for audio playback channel 6. </summary>
        bool InitPlayerWindow6()
        {
            splitContainer6.Panel2.ClientSize = new Size(240, 120);
            return true;
        }

        /// <summary> configure window for audio playback channel 7. </summary>
        bool InitPlayerWindow7()
        {
            splitContainer7.Panel2.ClientSize = new Size(240, 120);
            return true;
        }

        /// <summary> configure window for audio playback channel 8. </summary>
        bool InitPlayerWindow8()
        {
            splitContainer11.Panel2.ClientSize = new Size(240, 120);
            return true;
        }

        /// <summary> configure video preview window in channel 1. </summary>
        bool InitVideoWindow1(int multiplier, int divider)
        {
            if (basicVideo1 == null)
                return false;


            //Account for requests of normal, half, or double size
            //width = splitContainer9.Panel1.Width * multiplier / divider;
            //height = splitContainer9.Panel1.Height * multiplier / divider;

            //splitContainer9.Panel1.ClientSize = new Size(width, height);
            splitContainer9.Panel1.ClientSize = new Size(splitContainer9.Panel1.Width, splitContainer9.Panel1.Height);
            ResizeVideoWindow();
            return true;
        }

        /// <summary> configure video preview window in channel 2. </summary>
        bool InitVideoWindow2(int multiplier, int divider)
        {
            if (basicVideo2 == null)
                return false;


            // Account for requests of normal, half, or double size
            //width = splitContainer10.Panel1.Width * multiplier / divider;
            //height = splitContainer10.Panel1.Height * multiplier / divider;

            //splitContainer10.Panel1.ClientSize = new Size(width, height);
            splitContainer10.Panel1.ClientSize = new Size(splitContainer10.Panel1.Width, splitContainer10.Panel1.Height);

            ResizeVideoWindow2();
            return true;
        }

        /// <summary> configure video preview window in channel 3. </summary>
        bool InitVideoWindow3(int multiplier, int divider)
        {
            if (basicVideo3 == null)
                return false;


            // Account for requests of normal, half, or double size
            //width = splitContainer9.Panel2.Width * multiplier / divider;
            //height = splitContainer9.Panel2.Height * multiplier / divider;

            //splitContainer9.Panel2.ClientSize = new Size(width, height);
            splitContainer9.Panel2.ClientSize = new Size(splitContainer9.Panel2.Width, splitContainer9.Panel2.Height);
            ResizeVideoWindow3();
            return true;
        }

        /// <summary> configure video preview window in channel 4. </summary>
        bool InitVideoWindow4(int multiplier, int divider)
        {
            if (basicVideo4 == null)
                return false;


            // Account for requests of normal, half, or double size
            //width = splitContainer10.Panel2.Width * multiplier / divider;
            //height = splitContainer10.Panel2.Height * multiplier / divider;

            //splitContainer10.Panel2.ClientSize = new Size(width, height);
            splitContainer4.Panel2.ClientSize = new Size(splitContainer4.Panel2.Width, splitContainer4.Panel2.Height);
            ResizeVideoWindow4();
            return true;
        }


        bool InitVideoWindow5(int multiplier, int divider)
        {
            if (basicVideo5 == null)
                return false;

            // Account for requests of normal, half, or double size
            //width = splitContainer10.Panel2.Width * multiplier / divider;
            //height = splitContainer10.Panel2.Height * multiplier / divider;

            //splitContainer10.Panel2.ClientSize = new Size(width, height);
            splitContainer4.Panel1.ClientSize = new Size(splitContainer4.Panel1.Width, splitContainer4.Panel1.Height);
            ResizeVideoWindow5();
            return true;
        }

        bool InitVideoWindow6(int multiplier, int divider)
        {
            if (basicVideo6 == null)
                return false;

            // Account for requests of normal, half, or double size
            //width = splitContainer10.Panel2.Width * multiplier / divider;
            //height = splitContainer10.Panel2.Height * multiplier / divider;

            //splitContainer10.Panel2.ClientSize = new Size(width, height);
            splitContainer6.Panel2.ClientSize = new Size(splitContainer6.Panel2.Width, splitContainer6.Panel2.Height);
            ResizeVideoWindow6();
            return true;
        }

        bool InitVideoWindow7(int multiplier, int divider)
        {
            if (basicVideo7 == null)
                return false;

            // Account for requests of normal, half, or double size
            //width = splitContainer10.Panel2.Width * multiplier / divider;
            //height = splitContainer10.Panel2.Height * multiplier / divider;

            //splitContainer10.Panel2.ClientSize = new Size(width, height);
            splitContainer7.Panel2.ClientSize = new Size(splitContainer7.Panel2.Width, splitContainer7.Panel2.Height);
            ResizeVideoWindow7();
            return true;
        }

        bool InitVideoWindow8(int multiplier, int divider)
        {
            if (basicVideo8 == null)
                return false;

            // Account for requests of normal, half, or double size
            //width = splitContainer10.Panel2.Width * multiplier / divider;
            //height = splitContainer10.Panel2.Height * multiplier / divider;

            //splitContainer10.Panel2.ClientSize = new Size(width, height);
            splitContainer11.Panel2.ClientSize = new Size(splitContainer11.Panel2.Width, splitContainer11.Panel2.Height);
            ResizeVideoWindow8();
            return true;
        }

        /// <summary> try to get the step interfaces in channel 1. </summary>
        bool GetFrameStepInterface1()
        {
            videoStep1 = graphBuilder1 as DShowNET.IVideoFrameStep;
            if (videoStep1 == null)
                return false;

            // Check if this decoder can step
            int hr1 = videoStep1.CanStep(0, null);
            if (hr1 != 0)
            {
                videoStep1 = null;
                return false;
            }
            return true;
        }

        /// <summary> try to get the step interfaces in channel 2. </summary>
        bool GetFrameStepInterface2()
        {
            videoStep2 = graphBuilder2 as DShowNET.IVideoFrameStep;
            if (videoStep2 == null)
                return false;

            // Check if this decoder can step
            int hr2 = videoStep2.CanStep(0, null);
            if (hr2 != 0)
            {
                videoStep2 = null;
                return false;
            }
            return true;
        }

        /// <summary> try to get the step interfaces in channel 3. </summary>
        bool GetFrameStepInterface3()
        {
            videoStep3 = graphBuilder3 as DShowNET.IVideoFrameStep;
            if (videoStep3 == null)
                return false;

            // Check if this decoder can step
            int hr3 = videoStep3.CanStep(0, null);
            if (hr3 != 0)
            {
                videoStep3 = null;
                return false;
            }
            return true;
        }
       /// <summary> try to get the step interfaces in channel 4. </summary>
        bool GetFrameStepInterface4()
        {
            videoStep4 = graphBuilder4 as DShowNET.IVideoFrameStep;
            if (videoStep4 == null)
                return false;

            // Check if this decoder can step
            int hr4 = videoStep4.CanStep(0, null);
            if (hr4 != 0)
            {
                videoStep4 = null;
                return false;
            }
            return true;
        }
        bool GetFrameStepInterface5()
        {
            videoStep5 = graphBuilder5 as DShowNET.IVideoFrameStep;
            if (videoStep5 == null)
                return false;

            // Check if this decoder can step
            int hr5 = videoStep5.CanStep(0, null);
            if (hr5 != 0)
            {
                videoStep5 = null;
                return false;
            }
            return true;
        }
        bool GetFrameStepInterface6()
        {
            videoStep6 = graphBuilder6 as DShowNET.IVideoFrameStep;
            if (videoStep6 == null)
                return false;

            // Check if this decoder can step
            int hr6 = videoStep6.CanStep(0, null);
            if (hr6 != 0)
            {
                videoStep6 = null;
                return false;
            }
            return true;
        }
        bool GetFrameStepInterface7()
        {
            videoStep7 = graphBuilder7 as DShowNET.IVideoFrameStep;
            if (videoStep7 == null)
                return false;

            // Check if this decoder can step
            int hr7 = videoStep7.CanStep(0, null);
            if (hr7 != 0)
            {
                videoStep7 = null;
                return false;
            }
            return true;
        }
        bool GetFrameStepInterface8()
        {
            videoStep8 = graphBuilder8 as DShowNET.IVideoFrameStep;
            if (videoStep8 == null)
                return false;

            // Check if this decoder can step
            int hr8 = videoStep8.CanStep(0, null);
            if (hr8 != 0)
            {
                videoStep8 = null;
                return false;
            }
            return true;
        }

        /// <summary> background process action to load video files. </summary>
        private void doPopulate_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            btPlay.Enabled = false;
            btPause.Enabled = false;
            btFB.Enabled = false;
        }

        /// <summary> background process action when loading files are finished. </summary>
        private void doPopulate_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btPlay.Enabled = true;
            btFast4x.Enabled = true;
            btFast8x.Enabled = true;

            btAll4.Enabled = true;
            btcam1.Enabled = true;
            btcam2.Enabled = true;
            btCam3.Enabled = true;
            btCam4.Enabled = true;
            btCam5.Enabled = true;

            pLoad.Close();
            doPopulate.CancelAsync();

            this.Invoke(new MethodInvoker(delegate()
            {
                if (playmode == "single")
                {
                    countCh = 1;
                    btSnapshots.Location = new Point(69, 3);
                    btOpenVideo.Location = new Point(103, 3);

                    btRec.Visible = true;
                    btRecStop.Visible = true;

                    btAll4.Visible = false;
                    btcam1.Visible = false;
                    btcam2.Visible = false;
                    btCam3.Visible = false;
                    btCam4.Visible = false;
                    btCam5.Visible = false;
                    cektimeduration();
                    cekTimeStart();

                    if (VidStatus == "Not Valid Video")
                    {
                        numStat = 0;
                        btPlay.Enabled = false;
                        btPause.Enabled = false;
                        btFB.Enabled = false;
                        btFastBackward4x.Enabled = false;
                        btFastBackward8x.Enabled = false;
                        btFast4x.Enabled = false;
                        btFast8x.Enabled = false;

                        btSnapshots.Enabled = false;
                        btAll4.Enabled = false;
                        btcam1.Enabled = false;
                        btcam2.Enabled = false;
                        btCam3.Enabled = false;
                        btCam4.Enabled = false;
                        btCam5.Enabled = false;
                    }

                    this.Invoke(new MethodInvoker(delegate()
                    {

                            MessageBox.Show(new Form() { TopMost = true }, VidStatus);
                            if (numStat == 0)
                            {
                                btOpenVideo_Click(sender, e);
                            }
                            else
                            {
                                btOpenVideo.Enabled = true;
                                btPause.Checked = true;
                                this.currentVideoState = VideoState.Forwarding;
                            }

                    }));
                }
                else if (playmode == "multiple")
                {
                    btAll4_Click(null, null);

                    countCh = 0;
                    btRec.Visible = false;
                    btRecStop.Visible = false;

                    btAll4.Visible = true;
                    btcam1.Visible = true;
                    btcam2.Visible = true;
                    btCam3.Visible = true;
                    btCam4.Visible = true;
                    btCam5.Visible = true;

                    if (VidStatus == "No File Loaded" || VidStatus == "Invalid Folder Select")
                    {
                        numStat = 0;
                        btPlay.Enabled = false;
                        btPause.Enabled = false;
                        btFB.Enabled = false;
                        btFastBackward4x.Enabled = false;
                        btFastBackward8x.Enabled = false;
                        btFast4x.Enabled = false;
                        btFast8x.Enabled = false;

                        btSnapshots.Enabled = false;
                        btAll4.Enabled = false;
                        btcam1.Enabled = false;
                        btcam2.Enabled = false;
                        btCam3.Enabled = false;
                        btCam4.Enabled = false;
                        btCam5.Enabled = false;
                    }

                    this.Invoke(new MethodInvoker(delegate()
                    {

                            MessageBox.Show(new Form() { TopMost = true }, VidStatus);
                            if (numStat == 0)
                            {
                                btOpenVideo_Click(sender, e);
                            }
                            else
                            {
                                btOpenVideo.Enabled = true;
                                btAll4_Click(null, null);
                            }

                    }));

                    if (cam1Count <= 0)
                    {
                        splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                    }
                    else
                    {
                        splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.rady;
                    }
                    if (cam2Count <= 0)
                    {
                        splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                    }
                    else
                    {
                        splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.rady;
                    }
                    if (cam3Count <= 0)
                    {
                        splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                    }
                    else
                    {
                        splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.rady;
                    }
                    if (cam4Count <= 0)
                    {
                        splitContainer4.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                    }
                    else
                    {
                        splitContainer4.Panel2.BackgroundImage = mtvplayer.Properties.Resources.rady;
                    }
                    if (cam5Count <= 0)
                    {
                        splitContainer4.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                    }
                    else
                    {
                        splitContainer4.Panel1.BackgroundImage = mtvplayer.Properties.Resources.rady;
                    }

                    if (cam6Count <= 0)
                    {
                        splitContainer6.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                    }
                    else
                    {
                        splitContainer6.Panel2.BackgroundImage = mtvplayer.Properties.Resources.rady;
                    }
                    if (cam7Count <= 0)
                    {
                        splitContainer7.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                    }
                    else
                    {
                        splitContainer7.Panel2.BackgroundImage = mtvplayer.Properties.Resources.rady;
                    }
                    if (cam8Count <= 0)
                    {
                        splitContainer11.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                    }
                    else
                    {
                        splitContainer11.Panel2.BackgroundImage = mtvplayer.Properties.Resources.rady;
                    }
                }
            }));
            this.TopMost = false;
        }

        void mediaInfo1(string FileName)
        {
            Type comtype1 = null;
            object comobj = null;
            comtype1 = Type.GetTypeFromCLSID(Clsid.FilterGraph);
            if (comtype1 == null)
                throw new NotSupportedException("DirectX (8.1 or higher) not installed?");
            comobj = Activator.CreateInstance(comtype1);
            graphBuilder1 = (DShowNET.IGraphBuilder)comobj; comobj = null;

            int hr1 = graphBuilder1.RenderFile(FileName, null);
            if (hr1 < 0)
                Marshal.ThrowExceptionForHR(hr1);

            mediaPos1 = (DShowNET.IMediaPosition)graphBuilder1;
            mediaPos1.get_Duration(out durat);
        }

        /// <summary> get the video information. </summary>
        bool mediaInfo(string FileName, int i)
        {
            Type comtype = null;
            object comobj = null;
            try
            {
                comtype = Type.GetTypeFromCLSID(Clsid.FilterGraph);
                if (comtype == null)
                    throw new NotSupportedException("DirectX (8.1 or higher) not installed?");
                comobj = Activator.CreateInstance(comtype);
                graphBuilder = (DShowNET.IGraphBuilder)comobj; comobj = null;

                int hr = graphBuilder.RenderFile(FileName, null);
                if (hr < 0)
                    Marshal.ThrowExceptionForHR(hr);

                mediaPos = (DShowNET.IMediaPosition)graphBuilder;
                mediaPos.get_Duration(out durat);

                if (i == 1)
                {
                    duration1.Add(Math.Round(durat) / 1);
                }
                else if (i == 2)
                {
                    duration2.Add(Math.Round(durat) / 1);
                }
                else if (i == 3)
                {
                    duration3.Add(Math.Round(durat) / 1);
                }
                else if (i == 4)
                {
                    duration4.Add(Math.Round(durat) / 1);
                }
                else if (i == 5)
                {
                    duration5.Add(Math.Round(durat) / 1);
                }
                else if (i == 6)
                {
                    duration6.Add(Math.Round(durat) / 1);
                }
                else if (i == 7)
                {
                    duration7.Add(Math.Round(durat) / 1);
                }
                else if (i == 8)
                {
                    duration8.Add(Math.Round(durat) / 1);
                }
                return true;
            }
            catch 
            {
                //MessageBox.Show(this, "Could not get interfaces\r\n" + ee.Message, "DirectShow.NET", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            finally
            {
                if (comobj != null)
                    Marshal.ReleaseComObject(comobj); comobj = null;
            }
        }

        private void videoTrackBar_Scroll(object sender, EventArgs e)
        {
            if (playmode == "single")
            {
                if (videoTrackBar.Value > videoTrackBar.Minimum && videoTrackBar.Value < videoTrackBar.Maximum)
                {
                    int timedur = videoTrackBar.Value;
                    TimeSpan tbValue = TimeSpan.FromSeconds(timedur);
                    TimeSpan curPos = fromTime.Add(tbValue);
                    Console.WriteLine("tbValue " + tbValue);
                    Console.WriteLine("curPos " + curPos);
                    Console.WriteLine("A " + a);

                    if (fromTime <= curPos)// && curPos < TimeSpan.Parse(CalculateTime(duration)))
                    {
                        if ((mediaCtrl1 != null) || (mediaSeek1 != null))
                        {
                            mediaPos1.put_CurrentPosition(videoTrackBar.Value);// + videoTrackBar.Value);
                        }
                    }

                    ticky = timedur;
                    Console.WriteLine("ticky--> " + ticky);
                }
                else if (lbStartDur.Text == lbEndDur.Text)
                {
                    btPause_Click(null, null);
                    timer1.Stop();

                    splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                    finish = new finish(this);
                    finish.ShowDialog(this);
                }
            }
            else if (playmode == "multiple")
            {
                if (videoTrackBar.Value <= 0)
                {
                    lbStartDur.Text = fromTime.Add(TimeSpan.Parse(CalculateTime(videoTrackBar.Value))).ToString();
                }
                else if (videoTrackBar.Value >= videoTrackBar.Minimum && videoTrackBar.Value < videoTrackBar.Maximum)
                {
                    //if (lbStartDur.Text != "00:00:00")
                    if (lbStartDur.Text != fromTime.ToString())
                    {
                        btFB.Enabled = true;
                        btFastBackward4x.Enabled = true;
                        btFastBackward8x.Enabled = true;
                    }

                    int timedur = videoTrackBar.Value;
                    Console.WriteLine("timedur " + timedur);
                    TimeSpan tbValue = TimeSpan.FromSeconds(timedur);
                    TimeSpan curPos = fromTime.Add(tbValue);

                    Console.WriteLine("tbValue " + tbValue);
                    Console.WriteLine("curPos " + curPos);
                    Console.WriteLine("A " + a);
                    Console.WriteLine("SeqNo1 " + SeqNo1);

                    //trackbar cam1
                    if (cam1Count != 0)
                    {
                        double dura1 = duration1[cam1Count - 1];
                        TimeSpan val1a = starttime1[a];
                        Console.WriteLine("val1 " + val1a);

                        TimeSpan val2a = val1a.Add(TimeSpan.FromSeconds(duration1[a]));

                        if (val1a <= curPos && curPos < val2a)
                        {
                            Console.WriteLine("val1a <= curPos && curPos < val2a ");
                            //splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                            strtm1 = starttime1[a];

                            if ((mediaCtrl1 == null) || (mediaSeek1 == null))
                            {
                                if (playState1 != PlayState.Paused)
                                    Init1(null, null);
                                Console.WriteLine("val1a <= curPos && curPos < val2a Initializevid1 ");
                            }

                            if ((mediaCtrl1 != null) || (mediaSeek1 != null))
                                mediaPos1.put_CurrentPosition(curPos.TotalSeconds - strtm1.TotalSeconds);
                            //mediaPos1.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm1.TotalSeconds);

                            Console.WriteLine("fromTime.TotalSeconds " + fromTime.TotalSeconds);
                            Console.WriteLine("videoTrackBar.Value " + videoTrackBar.Value);
                            Console.WriteLine("strtm1.TotalSeconds " + strtm1.TotalSeconds);
                            Console.WriteLine("val1a <= curPos && curPos < val2a put_CurrentPosition ");
                            Console.WriteLine((fromTime.TotalSeconds + videoTrackBar.Value) - strtm1.TotalSeconds);
                        }
                        else if (curPos > val2a)
                        {
                            Console.WriteLine("curPos > val2a ");
                            clipFile1 = null;
                            clipType1 = ClipType.None;
                            CloseInterfaces();
                            splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                            if (a < cam1Count - 1)
                            {
                                Console.WriteLine("starttime1 " + starttime1[a + 1]);
                                if (fromTime.Add(curPos) > starttime1[a + 1])
                                {
                                    Console.WriteLine("curPos > val2a < val2a put_CurrentPosition ");
                                    Console.WriteLine("a " + a);
                                    Console.WriteLine("SeqNo1 " + SeqNo1);

                                    a = a + 1;
                                    SeqNo1 = a;

                                    Console.WriteLine("a. " + a);
                                    Console.WriteLine("SeqNo1. " + SeqNo1);

                                    if (curPos >= starttime1[a])
                                    {
                                        //splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                        strtm1 = starttime1[a];

                                        if (playState1 != PlayState.Paused && playState1 != PlayState.Stopped)
                                        {
                                            Console.WriteLine("playState1a " + playState1);
                                            Console.WriteLine("Initializevid1 ");
                                            Initializevid1(null, null);
                                        }
                                        else if (playState1 == PlayState.Paused)
                                        {
                                            Init1(null, null);
                                        }
                                        else if (playState1 == PlayState.Stopped)
                                        {
                                            Init1(null, null);
                                        }

                                        if ((mediaCtrl1 != null) || (mediaSeek1 != null))
                                            mediaPos1.put_CurrentPosition(curPos.TotalSeconds - strtm1.TotalSeconds);
                                        //mediaPos1.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm1.TotalSeconds);

                                        Console.WriteLine("curPos > val2a < val2a put_CurrentPosition Initializevid1");
                                        Console.WriteLine((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm1.TotalSeconds);
                                    }
                                }
                            }
                        }
                        else if (curPos < val1a)
                        {
                            Console.WriteLine("curPos < val1a ");
                            if ((mediaCtrl1 != null) || (mediaSeek1 != null))
                            {
                                clipFile1 = null;
                                clipType1 = ClipType.None;
                                CloseInterfaces();
                                splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                                if (SeqNo1 > 0)
                                {
                                    if (curPos < starttime1[a - 1])
                                    {
                                        Console.WriteLine("curPos < starttime1[a - 1] ");
                                        Console.WriteLine("a " + a);
                                        Console.WriteLine("SeqNo1 " + SeqNo1);

                                        a = a - 1;
                                        SeqNo1 = a;
                                        Console.WriteLine("a. " + a);
                                        Console.WriteLine("SeqNo1. " + SeqNo1);

                                        if (curPos >= starttime1[a])
                                        {
                                            //splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            strtm1 = starttime1[a];
                                            if ((mediaCtrl1 == null) || (mediaSeek1 == null))
                                                if (playState1 != PlayState.Paused && playState1 != PlayState.Stopped)
                                                {
                                                    Initializevid1(null, null);
                                                }
                                                else if (playState1 == PlayState.Paused)
                                                {
                                                    Init1(null, null);
                                                }
                                                else if (playState1 == PlayState.Stopped)
                                                {
                                                    Init1(null, null);
                                                }

                                            if ((mediaCtrl1 != null) || (mediaSeek1 != null))
                                                mediaPos1.put_CurrentPosition(curPos.TotalSeconds - strtm1.TotalSeconds);
                                            //mediaPos1.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm1.TotalSeconds);

                                            Console.WriteLine("curPos < starttime1[a - 1] curPos >= starttime1[a]");
                                            Console.WriteLine((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm1.TotalSeconds);
                                        }
                                    }
                                    else if (curPos < starttime1[a - 1].Add(TimeSpan.FromSeconds(duration1[a - 1])))
                                    {
                                        Console.WriteLine("curPos < starttime1[a - 1].Add(TimeSpan.FromSeconds(duration1[a - 1])) 1 ");
                                        Console.WriteLine("a " + a);
                                        Console.WriteLine("SeqNo1 " + SeqNo1);
                                        //SeqNo1--;
                                        a = a - 1;
                                        SeqNo1 = a;
                                        Console.WriteLine("a. " + a);
                                        Console.WriteLine("SeqNo1. " + SeqNo1);

                                        //splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                        strtm1 = starttime1[a];
                                        if ((mediaCtrl1 == null) || (mediaSeek1 == null))
                                            if (playState1 != PlayState.Paused && playState1 != PlayState.Stopped)
                                            {
                                                Initializevid1(null, null);
                                            }
                                            else if (playState1 == PlayState.Paused)
                                            {
                                                Init1(null, null);
                                            }
                                            else if (playState1 == PlayState.Stopped)
                                            {
                                                Init1(null, null);
                                            }

                                        if ((mediaCtrl1 != null) || (mediaSeek1 != null))
                                            mediaPos1.put_CurrentPosition(curPos.TotalSeconds - strtm1.TotalSeconds);
                                        //mediaPos1.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm1.TotalSeconds);

                                        Console.WriteLine((fromTime.TotalSeconds + videoTrackBar.Value) - strtm1.TotalSeconds);
                                    }
                                }
                            }
                            else
                            {
                                if (SeqNo1 > 0)
                                {
                                    Console.WriteLine("dur" + starttime1[a - 1].Add(TimeSpan.FromSeconds(duration1[a - 1])));
                                    if (curPos > starttime1[a - 1].Add(TimeSpan.FromSeconds(duration1[a - 1])))
                                    {
                                        Console.WriteLine("curPos > starttime1[a - 1].Add(TimeSpan.FromSeconds(duration1[a - 1])) 2");
                                        if (curPos >= starttime1[a])
                                        {
                                            Console.WriteLine("a " + a);
                                            Console.WriteLine("SeqNo1 " + SeqNo1);
                                            //SeqNo1--;
                                            a = a - 1;
                                            SeqNo1 = a;
                                            Console.WriteLine("a. " + a);
                                            Console.WriteLine("SeqNo1. " + SeqNo1);

                                            //splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            strtm1 = starttime1[a];
                                            if ((mediaCtrl1 == null) || (mediaSeek1 == null))
                                                if (playState1 != PlayState.Paused && playState1 != PlayState.Stopped)
                                                {
                                                    Initializevid1(null, null);
                                                }
                                                else if (playState1 == PlayState.Paused)
                                                {
                                                    Init1(null, null);
                                                }
                                                else if (playState1 == PlayState.Stopped)
                                                {
                                                    Init1(null, null);
                                                }

                                            if ((mediaCtrl1 != null) || (mediaSeek1 != null))
                                                mediaPos1.put_CurrentPosition(curPos.TotalSeconds - strtm1.TotalSeconds);
                                            //mediaPos1.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm1.TotalSeconds);

                                            Console.WriteLine((fromTime.TotalSeconds + videoTrackBar.Value) - strtm1.TotalSeconds);
                                        }
                                    }
                                    else if (curPos < starttime1[a - 1].Add(TimeSpan.FromSeconds(duration1[a - 1])))
                                    {
                                        Console.WriteLine("curPos < starttime1[a - 1].Add(TimeSpan.FromSeconds(duration1[a - 1])) ");
                                        Console.WriteLine("a " + a);
                                        Console.WriteLine("SeqNo1 " + SeqNo1);

                                        a = a - 1;
                                        SeqNo1 = a;
                                        Console.WriteLine("a. " + a);
                                        Console.WriteLine("SeqNo1. " + SeqNo1);

                                        //splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                        strtm1 = starttime1[a];
                                        if ((mediaCtrl1 == null) || (mediaSeek1 == null))

                                            if (playState1 != PlayState.Paused && playState1 != PlayState.Stopped)
                                            {
                                                Initializevid1(null, null);
                                            }
                                            else if (playState1 == PlayState.Paused)
                                            {
                                                Init1(null, null);
                                            }
                                            else if (playState1 == PlayState.Stopped)
                                            {
                                                Init1(null, null);
                                            }

                                        if ((mediaCtrl1 != null) || (mediaSeek1 != null))
                                            mediaPos1.put_CurrentPosition(curPos.TotalSeconds - strtm1.TotalSeconds);
                                        //mediaPos1.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm1.TotalSeconds);

                                        Console.WriteLine((fromTime.TotalSeconds + videoTrackBar.Value) - strtm1.TotalSeconds);
                                    }
                                }
                            }
                        }
                    }

                    //trackbar cam2
                    if (cam2Count != 0)
                    {
                        double dura2 = duration2[cam2Count - 1];
                        TimeSpan val1b = starttime2[b];
                        Console.WriteLine("val2 " + val1b);

                        TimeSpan val2b = val1b.Add(TimeSpan.FromSeconds(duration2[b]));

                        if (val1b <= curPos && curPos < val2b)
                        {
                            //splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                            strtm2 = starttime2[b];
                            if ((mediaCtrl2 == null) || (mediaSeek2 == null))
                                if (playState2 != PlayState.Paused)
                                    Init2(null, null);

                            if ((mediaCtrl2 != null) || (mediaSeek2 != null))
                                mediaPos2.put_CurrentPosition(curPos.TotalSeconds - strtm2.TotalSeconds);
                            //mediaPos2.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm2.TotalSeconds);
                        }
                        else if (curPos > val2b)
                        {
                            clipFile2 = null;
                            clipType2 = ClipType.None;
                            CloseInterfaces2();
                            splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                            if (b < cam2Count - 1)
                            {
                                Console.WriteLine(starttime2[b + 1]);
                                if (fromTime.Add(curPos) > starttime2[b + 1])
                                {
                                    b = b + 1;
                                    SeqNo2 = b;

                                    //splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                    if (curPos >= starttime2[b])
                                    {
                                        strtm2 = starttime2[b];

                                        if (playState2 != PlayState.Paused && playState2 != PlayState.Stopped)
                                        {
                                            Initializevid2(null, null);
                                        }
                                        else if (playState2 == PlayState.Paused)
                                        {
                                            Init2(null, null);
                                        }
                                        else if (playState2 == PlayState.Stopped)
                                        {
                                            Init2(null, null);
                                        }

                                        if ((mediaCtrl2 != null) || (mediaSeek2 != null))
                                            mediaPos2.put_CurrentPosition(curPos.TotalSeconds - strtm2.TotalSeconds);
                                        //mediaPos2.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm2.TotalSeconds);
                                    }
                                }
                            }
                        }
                        else if (curPos < val1b)
                        {
                            if ((mediaCtrl2 != null) || (mediaSeek2 != null))
                            {
                                clipFile2 = null;
                                clipType2 = ClipType.None;
                                CloseInterfaces2();
                                splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                                if (SeqNo2 > 0)
                                {
                                    if (curPos < starttime2[b - 1])
                                    {
                                        //SeqNo2--;
                                        b = b - 1;
                                        SeqNo2 = b;

                                        if (curPos >= starttime2[b])
                                        {
                                            //splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            strtm2 = starttime2[b];
                                            if ((mediaCtrl2 == null) || (mediaSeek2 == null))
                                                if (playState2 != PlayState.Paused && playState2 != PlayState.Stopped)
                                                {
                                                    Initializevid2(null, null);
                                                }
                                                else if (playState2 == PlayState.Paused)
                                                {
                                                    Init2(null, null);
                                                }
                                                else if (playState2 == PlayState.Stopped)
                                                {
                                                    Init2(null, null);
                                                }

                                            if ((mediaCtrl2 != null) || (mediaSeek2 != null))
                                                mediaPos2.put_CurrentPosition(curPos.TotalSeconds - strtm2.TotalSeconds);
                                            //mediaPos2.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm2.TotalSeconds);
                                        }
                                    }
                                    else if (curPos < starttime2[b - 1].Add(TimeSpan.FromSeconds(duration2[b - 1])))
                                    {
                                        b = b - 1;
                                        SeqNo2 = b;

                                        //splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                        strtm2 = starttime2[b];
                                        if ((mediaCtrl2 == null) || (mediaSeek2 == null))
                                            if (playState2 != PlayState.Paused && playState2 != PlayState.Stopped)
                                            {
                                                Initializevid2(null, null);
                                            }
                                            else if (playState2 == PlayState.Paused)
                                            {
                                                Init2(null, null);
                                            }
                                            else if (playState2 == PlayState.Stopped)
                                            {
                                                Init2(null, null);
                                            }

                                        if ((mediaCtrl2 != null) || (mediaSeek2 != null))
                                            mediaPos2.put_CurrentPosition(curPos.TotalSeconds - strtm2.TotalSeconds);
                                        //mediaPos2.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm2.TotalSeconds);
                                    }
                                }
                            }
                            else
                            {
                                if (SeqNo2 > 0)
                                {
                                    Console.WriteLine("dur" + starttime2[b - 1].Add(TimeSpan.FromSeconds(duration2[b - 1])));
                                    if (curPos > starttime2[b - 1].Add(TimeSpan.FromSeconds(duration2[b - 1])))
                                    {
                                        if (curPos >= starttime2[b])
                                        {
                                            b = b - 1;
                                            SeqNo2 = b;

                                            //splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            strtm2 = starttime2[b];
                                            if ((mediaCtrl2 == null) || (mediaSeek2 == null))
                                                if (playState2 != PlayState.Paused && playState2 != PlayState.Stopped)
                                                {
                                                    Initializevid2(null, null);
                                                }
                                                else if (playState2 == PlayState.Paused)
                                                {
                                                    Init2(null, null);
                                                }
                                                else if (playState2 == PlayState.Stopped)
                                                {
                                                    Init2(null, null);
                                                }

                                            if ((mediaCtrl2 != null) || (mediaSeek2 != null))
                                                mediaPos2.put_CurrentPosition(curPos.TotalSeconds - strtm2.TotalSeconds);
                                            //mediaPos2.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm2.TotalSeconds);
                                        }
                                    }
                                    else if (curPos < starttime2[b - 1].Add(TimeSpan.FromSeconds(duration2[b - 1])))
                                    {
                                        b = b - 1;
                                        SeqNo2 = b;

                                        //splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                        strtm2 = starttime2[b];
                                        if ((mediaCtrl2 == null) || (mediaSeek2 == null))

                                            if (playState2 != PlayState.Paused && playState2 != PlayState.Stopped)
                                            {
                                                Initializevid2(null, null);
                                            }
                                            else if (playState2 == PlayState.Paused)
                                            {
                                                Init2(null, null);
                                            }
                                            else if (playState2 == PlayState.Stopped)
                                            {
                                                Init2(null, null);
                                            }

                                        if ((mediaCtrl2 != null) || (mediaSeek2 != null))
                                            mediaPos2.put_CurrentPosition(curPos.TotalSeconds - strtm2.TotalSeconds);
                                        //mediaPos2.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm2.TotalSeconds);
                                    }
                                }
                            }
                        }
                    }

                    if (cam3Count != 0)
                    {
                        double dura3 = duration3[cam3Count - 1];
                        TimeSpan val1c = starttime3[c];
                        Console.WriteLine("val3 " + val1c);

                        TimeSpan val2c = val1c.Add(TimeSpan.FromSeconds(duration3[c]));

                        if (val1c <= curPos && curPos < val2c)
                        {
                            Console.WriteLine("diantara");
                            //splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                            strtm3 = starttime3[c];
                            if ((mediaCtrl3 == null) || (mediaSeek3 == null))
                                if (playState3 != PlayState.Paused)
                                    Init3(null, null);

                            if ((mediaCtrl3 != null) || (mediaSeek3 != null))
                                mediaPos3.put_CurrentPosition(curPos.TotalSeconds - strtm3.TotalSeconds);
                            //mediaPos3.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm3.TotalSeconds);
                        }
                        else if (curPos > val2c)
                        {
                            Console.WriteLine("lebih dari");

                            clipFile3 = null;
                            clipType3 = ClipType.None;
                            CloseInterfaces3();
                            splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                            if (c < cam3Count - 1)
                            {
                                Console.WriteLine(starttime3[c + 1]);
                                if (fromTime.Add(curPos) > starttime3[c + 1])
                                {
                                    c = c + 1;
                                    SeqNo3 = c;

                                    if (curPos >= starttime3[c])
                                    {
                                        //splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                        strtm3 = starttime3[c];

                                        if (playState3 != PlayState.Paused && playState3 != PlayState.Stopped)
                                        {
                                            Console.WriteLine("playState3 != PlayState.Paused && playState3 != PlayState.Stopped ");
                                            Initializevid3(null, null);
                                        }
                                        else if (playState3 == PlayState.Paused)
                                        {
                                            Console.WriteLine("playState3 == PlayState.Paused ");
                                            Init3(null, null);
                                        }
                                        else if (playState3 == PlayState.Stopped)
                                        {
                                            Console.WriteLine("playState3 == PlayState.Stopped ");
                                            Init3(null, null);
                                        }

                                        if (mediaCtrl3 != null || mediaSeek3 != null)
                                            mediaPos3.put_CurrentPosition(curPos.TotalSeconds - strtm3.TotalSeconds);
                                        //mediaPos3.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm3.TotalSeconds);
                                    }
                                }
                            }
                        }
                        else if (curPos < val1c)
                        {
                            Console.WriteLine("kurang dari");
                            if ((mediaCtrl3 != null) || (mediaSeek3 != null))
                            {
                                clipFile3 = null;
                                clipType3 = ClipType.None;
                                CloseInterfaces3();
                                splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                                if (SeqNo3 > 0)
                                {
                                    if (curPos < starttime3[c - 1])
                                    {
                                        Console.WriteLine("kurang dari a");
                                        c = c - 1;
                                        SeqNo3 = c;

                                        if (curPos >= starttime3[c])
                                        {
                                            //splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            strtm3 = starttime3[c];
                                            if ((mediaCtrl3 == null) || (mediaSeek3 == null))
                                                if (playState3 != PlayState.Paused && playState3 != PlayState.Stopped)
                                                {
                                                    Console.WriteLine("playState3 != PlayState.Paused && playState3 != PlayState.Stopped ");
                                                    Initializevid3(null, null);
                                                }
                                                else if (playState3 == PlayState.Paused)
                                                {
                                                    Console.WriteLine("playState3 == PlayState.Paused ");
                                                    Init3(null, null);
                                                }
                                                else if (playState3 == PlayState.Stopped)
                                                {
                                                    Console.WriteLine("playState3 == PlayState.Stopped ");
                                                    Init3(null, null);
                                                }

                                            if ((mediaCtrl3 != null) || (mediaSeek3 != null))
                                                mediaPos3.put_CurrentPosition(curPos.TotalSeconds - strtm3.TotalSeconds);
                                            //mediaPos3.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm3.TotalSeconds);
                                        }
                                    }
                                    else if (curPos < starttime3[c - 1].Add(TimeSpan.FromSeconds(duration3[c - 1])))
                                    {
                                        Console.WriteLine("kurang dari b");
                                        c = c - 1;
                                        SeqNo3 = c;

                                        //splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                        strtm3 = starttime3[c];
                                        if ((mediaCtrl3 == null) || (mediaSeek3 == null))
                                            if (playState3 != PlayState.Paused && playState3 != PlayState.Stopped)
                                            {
                                                Console.WriteLine("playState3 != PlayState.Paused && playState3 != PlayState.Stopped ");
                                                Initializevid3(null, null);
                                            }
                                            else if (playState3 == PlayState.Paused)
                                            {
                                                Console.WriteLine("playState3 == PlayState.Paused ");
                                                Init3(null, null);
                                            }
                                            else if (playState3 == PlayState.Stopped)
                                            {
                                                Console.WriteLine("playState3 == PlayState.Stopped ");
                                                Init3(null, null);
                                            }

                                        if ((mediaCtrl3 != null) || (mediaSeek3 != null))
                                            mediaPos3.put_CurrentPosition(curPos.TotalSeconds - strtm3.TotalSeconds);
                                        //    mediaPos3.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm3.TotalSeconds);
                                    }
                                }
                            }
                            else
                            {
                                if (SeqNo3 > 0)
                                {
                                    Console.WriteLine("lebih dari akhir");
                                    Console.WriteLine("dur" + starttime3[c - 1].Add(TimeSpan.FromSeconds(duration3[c - 1])));
                                    if (curPos > starttime3[c - 1].Add(TimeSpan.FromSeconds(duration3[c - 1])))
                                    {
                                        if (curPos >= starttime3[c])
                                        {
                                            c = c - 1;
                                            SeqNo3 = c;

                                            //splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            strtm3 = starttime3[c];
                                            if ((mediaCtrl3 == null) || (mediaSeek3 == null))
                                                if (playState3 != PlayState.Paused && playState3 != PlayState.Stopped)
                                                {
                                                    Console.WriteLine("playState3 != PlayState.Paused && playState3 != PlayState.Stopped ");
                                                    Initializevid3(null, null);
                                                }
                                                else if (playState3 == PlayState.Paused)
                                                {
                                                    Console.WriteLine("playState3 == PlayState.Paused ");
                                                    Init3(null, null);
                                                }
                                                else if (playState3 == PlayState.Stopped)
                                                {
                                                    Console.WriteLine("playState3 == PlayState.Stopped ");
                                                    Init3(null, null);
                                                }

                                            if ((mediaCtrl3 != null) || (mediaSeek3 != null))
                                                mediaPos3.put_CurrentPosition(curPos.TotalSeconds - strtm3.TotalSeconds);
                                            //     mediaPos3.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm3.TotalSeconds);
                                        }
                                    }
                                    else if (curPos < starttime3[c - 1].Add(TimeSpan.FromSeconds(duration3[c - 1])))
                                    {
                                        Console.WriteLine("kurang dari akhir");
                                        c = c - 1;
                                        SeqNo3 = c;

                                        //splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                        strtm3 = starttime3[c];
                                        if ((mediaCtrl3 == null) || (mediaSeek3 == null))
                                            if (playState3 != PlayState.Paused && playState3 != PlayState.Stopped)
                                            {
                                                Console.WriteLine("playState3 != PlayState.Paused && playState3 != PlayState.Stopped ");
                                                Initializevid3(null, null);
                                            }
                                            else if (playState3 == PlayState.Paused)
                                            {
                                                Console.WriteLine("playState3 == PlayState.Paused ");
                                                Init3(null, null);
                                            }
                                            else if (playState3 == PlayState.Stopped)
                                            {
                                                Console.WriteLine("playState3 == PlayState.Stopped ");
                                                Init3(null, null);
                                            }

                                        if ((mediaCtrl3 != null) || (mediaSeek3 != null))
                                            mediaPos3.put_CurrentPosition(curPos.TotalSeconds - strtm3.TotalSeconds);
                                        //mediaPos3.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm3.TotalSeconds);
                                    }
                                }
                            }
                        }
                    }

                    //trackbar cam4
                    if (cam4Count != 0)
                    {
                        double dura4 = duration4[cam4Count - 1];
                        TimeSpan val1d = starttime4[d];
                        Console.WriteLine("val4 " + val1d);

                        TimeSpan val2d = val1d.Add(TimeSpan.FromSeconds(duration4[d]));

                        if (val1d < curPos && curPos < val2d)
                        {
                            Console.WriteLine("diantara");
                            //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                            strtm4 = starttime4[d];
                            if ((mediaCtrl4 == null) || (mediaSeek4 == null))
                                if (playState4 != PlayState.Paused)
                                    Init4(null, null);

                            if ((mediaCtrl4 != null) || (mediaSeek4 != null))
                                mediaPos4.put_CurrentPosition(curPos.TotalSeconds - strtm4.TotalSeconds);
                            //mediaPos4.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm4.TotalSeconds);
                        }
                        else if (curPos > val2d)
                        {
                            Console.WriteLine("lebih dari");
                            clipFile4 = null;
                            clipType4 = ClipType.None;
                            CloseInterfaces4();
                            splitContainer4.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                            if (d < cam4Count - 1)
                            {
                                Console.WriteLine(starttime4[d + 1]);
                                if (fromTime.Add(curPos) > starttime4[d + 1])
                                {
                                    d = d + 1;
                                    SeqNo4 = d;

                                    //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                    if (curPos >= starttime4[d])
                                    {
                                        strtm4 = starttime4[d];
                                        if (playState4 != PlayState.Paused && playState4 != PlayState.Stopped)
                                        {
                                            Initializevid4(null, null);
                                        }
                                        else if (playState4 == PlayState.Paused)
                                        {
                                            Init4(null, null);
                                        }
                                        else if (playState4 == PlayState.Stopped)
                                        {
                                            Init4(null, null);
                                        }

                                        if ((mediaCtrl4 != null) || (mediaSeek4 != null))
                                            mediaPos4.put_CurrentPosition(curPos.TotalSeconds - strtm4.TotalSeconds);
                                        //      mediaPos4.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm4.TotalSeconds);
                                    }
                                }
                            }
                        }
                        else if (curPos < val1d)
                        {
                            Console.WriteLine("kurang dari");
                            if ((mediaCtrl4 != null) || (mediaSeek4 != null))
                            {
                                clipFile4 = null;
                                clipType4 = ClipType.None;
                                CloseInterfaces4();
                                splitContainer4.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                                if (SeqNo4 > 0)
                                {
                                    if (curPos < starttime4[d - 1])
                                    {
                                        d = d - 1;
                                        SeqNo4 = d;

                                        if (curPos >= starttime4[d])
                                        {
                                            //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            strtm4 = starttime4[d];
                                            if ((mediaCtrl4 == null) || (mediaSeek4 == null))
                                                if (playState4 != PlayState.Paused && playState4 != PlayState.Stopped)
                                                {
                                                    Initializevid4(null, null);
                                                }
                                                else if (playState4 == PlayState.Paused)
                                                {
                                                    Init4(null, null);
                                                }
                                                else if (playState4 == PlayState.Stopped)
                                                {
                                                    Init4(null, null);
                                                }

                                            if ((mediaCtrl4 != null) || (mediaSeek4 != null))
                                                mediaPos4.put_CurrentPosition(curPos.TotalSeconds - strtm4.TotalSeconds);
                                            //      mediaPos4.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm4.TotalSeconds);
                                        }
                                    }
                                    else if (curPos < starttime4[d - 1].Add(TimeSpan.FromSeconds(duration4[d - 1])))
                                    {
                                        d = d - 1;
                                        SeqNo4 = d;

                                        //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                        strtm4 = starttime4[d];
                                        if ((mediaCtrl4 == null) || (mediaSeek4 == null))
                                            if (playState4 != PlayState.Paused && playState4 != PlayState.Stopped)
                                            {
                                                Initializevid4(null, null);
                                            }
                                            else if (playState4 == PlayState.Paused)
                                            {
                                                Init4(null, null);
                                            }
                                            else if (playState4 == PlayState.Stopped)
                                            {
                                                Init4(null, null);
                                            }

                                        if ((mediaCtrl4 != null) || (mediaSeek4 != null))
                                            mediaPos4.put_CurrentPosition(curPos.TotalSeconds - strtm4.TotalSeconds);
                                        //   mediaPos4.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm4.TotalSeconds);
                                    }
                                }
                            }
                            else
                            {
                                if (SeqNo4 > 0)
                                {
                                    Console.WriteLine("lebih dari akhir");
                                    Console.WriteLine("dur" + starttime4[d - 1].Add(TimeSpan.FromSeconds(duration4[d - 1])));
                                    if (curPos > starttime4[d - 1].Add(TimeSpan.FromSeconds(duration4[d - 1])))
                                    {
                                        if (curPos >= starttime4[d])
                                        {
                                            d = d - 1;
                                            SeqNo4 = d;

                                            //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            strtm4 = starttime4[d];
                                            if ((mediaCtrl4 == null) || (mediaSeek4 == null))
                                                if (playState4 != PlayState.Paused && playState4 != PlayState.Stopped)
                                                {
                                                    Initializevid4(null, null);
                                                }
                                                else if (playState4 == PlayState.Paused)
                                                {
                                                    Init4(null, null);
                                                }
                                                else if (playState4 == PlayState.Stopped)
                                                {
                                                    Init4(null, null);
                                                }

                                            if ((mediaCtrl4 != null) || (mediaSeek4 != null))
                                                mediaPos4.put_CurrentPosition(curPos.TotalSeconds - strtm4.TotalSeconds);
                                            //       mediaPos4.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm4.TotalSeconds);
                                        }
                                    }
                                    else if (curPos < starttime4[d - 1].Add(TimeSpan.FromSeconds(duration4[d - 1])))
                                    {
                                        Console.WriteLine("kurang dari");
                                        d = d - 1;
                                        SeqNo4 = d;

                                        //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                        strtm4 = starttime4[d];
                                        if ((mediaCtrl4 == null) || (mediaSeek4 == null))
                                            if (playState4 != PlayState.Paused && playState4 != PlayState.Stopped)
                                            {
                                                Initializevid4(null, null);
                                            }
                                            else if (playState4 == PlayState.Paused)
                                            {
                                                Init4(null, null);
                                            }
                                            else if (playState4 == PlayState.Stopped)
                                            {
                                                Init4(null, null);
                                            }

                                        if ((mediaCtrl4 != null) || (mediaSeek4 != null))
                                            mediaPos4.put_CurrentPosition(curPos.TotalSeconds - strtm4.TotalSeconds);
                                        //      mediaPos4.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm4.TotalSeconds);
                                    }
                                }
                            }
                        }
                    }

                    //trackbar cam5
                    if (cam5Count != 0)
                    {
                        double dura5 = duration5[cam5Count - 1];
                        TimeSpan val1e = starttime5[e5];
                        Console.WriteLine("val5 " + val1e);

                        TimeSpan val2e = val1e.Add(TimeSpan.FromSeconds(duration5[e5]));

                        if (val1e < curPos && curPos < val2e)
                        {
                            Console.WriteLine("diantara");
                            //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                            strtm5 = starttime5[e5];
                            if ((mediaCtrl5 == null) || (mediaSeek5 == null))
                                if (playState5 != PlayState.Paused)
                                    Init5(null, null);

                            if ((mediaCtrl5 != null) || (mediaSeek5 != null))
                                mediaPos5.put_CurrentPosition(curPos.TotalSeconds - strtm5.TotalSeconds);
                            //mediaPos4.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm4.TotalSeconds);
                        }
                        else if (curPos > val2e)
                        {
                            Console.WriteLine("lebih dari");
                            clipFile5 = null;
                            clipType5 = ClipType.None;
                            CloseInterfaces5();
                            splitContainer4.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                            if (e5 < cam5Count - 1)
                            {
                                Console.WriteLine(starttime5[e5 + 1]);
                                if (fromTime.Add(curPos) > starttime5[e5 + 1])
                                {
                                    e5 = e5 + 1;
                                    SeqNo5 = e5;

                                    //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                    if (curPos >= starttime5[e5])
                                    {
                                        strtm5 = starttime5[e5];
                                        if (playState5 != PlayState.Paused && playState5 != PlayState.Stopped)
                                        {
                                            Initializevid5(null, null);
                                        }
                                        else if (playState5 == PlayState.Paused)
                                        {
                                            Init5(null, null);
                                        }
                                        else if (playState5 == PlayState.Stopped)
                                        {
                                            Init5(null, null);
                                        }

                                        if ((mediaCtrl5 != null) || (mediaSeek5 != null))
                                            mediaPos5.put_CurrentPosition(curPos.TotalSeconds - strtm5.TotalSeconds);
                                        //      mediaPos4.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm4.TotalSeconds);
                                    }
                                }
                            }
                        }
                        else if (curPos < val1e)
                        {
                            Console.WriteLine("kurang dari");
                            if ((mediaCtrl5 != null) || (mediaSeek5 != null))
                            {
                                clipFile5 = null;
                                clipType5 = ClipType.None;
                                CloseInterfaces5();
                                splitContainer4.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                                if (SeqNo5 > 0)
                                {
                                    if (curPos < starttime5[e5 - 1])
                                    {
                                        e5 = e5 - 1;
                                        SeqNo5 = e5;

                                        if (curPos >= starttime5[e5])
                                        {
                                            //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            strtm5 = starttime5[e5];
                                            if ((mediaCtrl5 == null) || (mediaSeek5 == null))
                                                if (playState5 != PlayState.Paused && playState5 != PlayState.Stopped)
                                                {
                                                    Initializevid5(null, null);
                                                }
                                                else if (playState5 == PlayState.Paused)
                                                {
                                                    Init5(null, null);
                                                }
                                                else if (playState5 == PlayState.Stopped)
                                                {
                                                    Init5(null, null);
                                                }

                                            if ((mediaCtrl5 != null) || (mediaSeek5 != null))
                                                mediaPos5.put_CurrentPosition(curPos.TotalSeconds - strtm5.TotalSeconds);
                                            //      mediaPos4.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm4.TotalSeconds);
                                        }
                                    }
                                    else if (curPos < starttime5[e5 - 1].Add(TimeSpan.FromSeconds(duration5[e5 - 1])))
                                    {
                                        e5 = e5 - 1;
                                        SeqNo5 = e5;

                                        //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                        strtm5 = starttime5[e5];
                                        if ((mediaCtrl5 == null) || (mediaSeek5 == null))
                                            if (playState5 != PlayState.Paused && playState5 != PlayState.Stopped)
                                            {
                                                Initializevid5(null, null);
                                            }
                                            else if (playState5 == PlayState.Paused)
                                            {
                                                Init5(null, null);
                                            }
                                            else if (playState5 == PlayState.Stopped)
                                            {
                                                Init5(null, null);
                                            }

                                        if ((mediaCtrl5 != null) || (mediaSeek5 != null))
                                            mediaPos5.put_CurrentPosition(curPos.TotalSeconds - strtm5.TotalSeconds);
                                        //   mediaPos4.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm4.TotalSeconds);
                                    }
                                }
                            }
                            else
                            {
                                if (SeqNo5 > 0)
                                {
                                    Console.WriteLine("lebih dari akhir");
                                    Console.WriteLine("dur" + starttime5[e5 - 1].Add(TimeSpan.FromSeconds(duration5[e5 - 1])));
                                    if (curPos > starttime5[e5 - 1].Add(TimeSpan.FromSeconds(duration5[e5 - 1])))
                                    {
                                        if (curPos >= starttime5[e5])
                                        {
                                            e5 = e5 - 1;
                                            SeqNo5 = e5;

                                            //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            strtm5 = starttime5[e5];
                                            if ((mediaCtrl5 == null) || (mediaSeek5 == null))
                                                if (playState5 != PlayState.Paused && playState5 != PlayState.Stopped)
                                                {
                                                    Initializevid5(null, null);
                                                }
                                                else if (playState5 == PlayState.Paused)
                                                {
                                                    Init5(null, null);
                                                }
                                                else if (playState5 == PlayState.Stopped)
                                                {
                                                    Init5(null, null);
                                                }

                                            if ((mediaCtrl5 != null) || (mediaSeek5 != null))
                                                mediaPos5.put_CurrentPosition(curPos.TotalSeconds - strtm5.TotalSeconds);
                                            //       mediaPos4.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm4.TotalSeconds);
                                        }
                                    }
                                    else if (curPos < starttime5[e5 - 1].Add(TimeSpan.FromSeconds(duration5[e5 - 1])))
                                    {
                                        Console.WriteLine("kurang dari");
                                        e5 = e5 - 1;
                                        SeqNo5 = e5;

                                        //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                        strtm5 = starttime5[e5];
                                        if ((mediaCtrl5 == null) || (mediaSeek5 == null))
                                            if (playState5 != PlayState.Paused && playState5 != PlayState.Stopped)
                                            {
                                                Initializevid5(null, null);
                                            }
                                            else if (playState5 == PlayState.Paused)
                                            {
                                                Init5(null, null);
                                            }
                                            else if (playState5 == PlayState.Stopped)
                                            {
                                                Init5(null, null);
                                            }

                                        if ((mediaCtrl5 != null) || (mediaSeek5 != null))
                                            mediaPos5.put_CurrentPosition(curPos.TotalSeconds - strtm5.TotalSeconds);
                                        //      mediaPos4.put_CurrentPosition((fromTime.TotalSeconds + videoTrackBar.Value - 2) - strtm4.TotalSeconds);
                                    }
                                }
                            }
                        }
                    }


                    //trackbar cam6
                    if (cam6Count != 0)
                    {
                        double dura6 = duration6[cam6Count - 1];
                        TimeSpan val1f = starttime6[f];
                        Console.WriteLine("val6 " + val1f);

                        TimeSpan val2f = val1f.Add(TimeSpan.FromSeconds(duration6[f]));

                        if (val1f < curPos && curPos < val2f)
                        {
                            Console.WriteLine("diantara");
                            strtm6 = starttime6[f];
                            if ((mediaCtrl6 == null) || (mediaSeek6 == null))
                                if (playState6 != PlayState.Paused)
                                    Init6(null, null);

                            if ((mediaCtrl6 != null) || (mediaSeek6 != null))
                                mediaPos6.put_CurrentPosition(curPos.TotalSeconds - strtm6.TotalSeconds);
                        }
                        else if (curPos > val2f)
                        {
                            Console.WriteLine("lebih dari");
                            clipFile6 = null;
                            clipType6 = ClipType.None;
                            CloseInterfaces6();
                            splitContainer6.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                            if (f < cam6Count - 1)
                            {
                                Console.WriteLine(starttime6[f + 1]);
                                if (fromTime.Add(curPos) > starttime6[f + 1])
                                {
                                    f = f + 1;
                                    SeqNo6 = f;

                                    if (curPos >= starttime6[f])
                                    {
                                        strtm6 = starttime6[f];
                                        if (playState6 != PlayState.Paused && playState6 != PlayState.Stopped)
                                        {
                                            Initializevid6(null, null);
                                        }
                                        else if (playState6 == PlayState.Paused)
                                        {
                                            Init6(null, null);
                                        }
                                        else if (playState6 == PlayState.Stopped)
                                        {
                                            Init6(null, null);
                                        }

                                        if ((mediaCtrl6 != null) || (mediaSeek6 != null))
                                            mediaPos6.put_CurrentPosition(curPos.TotalSeconds - strtm6.TotalSeconds);
                                    }
                                }
                            }
                        }
                        else if (curPos < val1f)
                        {
                            Console.WriteLine("kurang dari");
                            if ((mediaCtrl6 != null) || (mediaSeek6 != null))
                            {
                                clipFile6 = null;
                                clipType6 = ClipType.None;
                                CloseInterfaces6();
                                splitContainer6.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                                if (SeqNo6 > 0)
                                {
                                    if (curPos < starttime6[f - 1])
                                    {
                                        f = f - 1;
                                        SeqNo6 = f;

                                        if (curPos >= starttime6[f])
                                        {
                                            strtm6 = starttime6[f];
                                            if ((mediaCtrl6 == null) || (mediaSeek6 == null))
                                                if (playState6 != PlayState.Paused && playState6 != PlayState.Stopped)
                                                {
                                                    Initializevid6(null, null);
                                                }
                                                else if (playState6 == PlayState.Paused)
                                                {
                                                    Init6(null, null);
                                                }
                                                else if (playState6 == PlayState.Stopped)
                                                {
                                                    Init6(null, null);
                                                }

                                            if ((mediaCtrl6 != null) || (mediaSeek6 != null))
                                                mediaPos6.put_CurrentPosition(curPos.TotalSeconds - strtm6.TotalSeconds);
                                        }
                                    }
                                    else if (curPos < starttime6[f - 1].Add(TimeSpan.FromSeconds(duration6[f - 1])))
                                    {
                                        f = f - 1;
                                        SeqNo6 = f;
                                        strtm6 = starttime6[f];
                                        if ((mediaCtrl6 == null) || (mediaSeek6 == null))
                                            if (playState6 != PlayState.Paused && playState6 != PlayState.Stopped)
                                            {
                                                Initializevid6(null, null);
                                            }
                                            else if (playState6 == PlayState.Paused)
                                            {
                                                Init6(null, null);
                                            }
                                            else if (playState6 == PlayState.Stopped)
                                            {
                                                Init6(null, null);
                                            }

                                        if ((mediaCtrl6 != null) || (mediaSeek6 != null))
                                            mediaPos6.put_CurrentPosition(curPos.TotalSeconds - strtm6.TotalSeconds);
                                    }
                                }
                            }
                            else
                            {
                                if (SeqNo6 > 0)
                                {
                                    Console.WriteLine("lebih dari akhir");
                                    Console.WriteLine("dur" + starttime6[f - 1].Add(TimeSpan.FromSeconds(duration6[f - 1])));
                                    if (curPos > starttime6[f - 1].Add(TimeSpan.FromSeconds(duration6[f - 1])))
                                    {
                                        if (curPos >= starttime6[f])
                                        {
                                            f = f - 1;
                                            SeqNo6 = f;
                                            strtm6 = starttime6[f];
                                            if ((mediaCtrl6 == null) || (mediaSeek6 == null))
                                                if (playState6 != PlayState.Paused && playState6 != PlayState.Stopped)
                                                {
                                                    Initializevid6(null, null);
                                                }
                                                else if (playState6 == PlayState.Paused)
                                                {
                                                    Init6(null, null);
                                                }
                                                else if (playState6 == PlayState.Stopped)
                                                {
                                                    Init6(null, null);
                                                }

                                            if ((mediaCtrl6 != null) || (mediaSeek6 != null))
                                                mediaPos6.put_CurrentPosition(curPos.TotalSeconds - strtm6.TotalSeconds);
                                        }
                                    }
                                    else if (curPos < starttime6[f - 1].Add(TimeSpan.FromSeconds(duration6[f - 1])))
                                    {
                                        Console.WriteLine("kurang dari");
                                        f = f - 1;
                                        SeqNo6 = f;

                                        strtm6 = starttime6[f];
                                        if ((mediaCtrl6 == null) || (mediaSeek6 == null))
                                            if (playState6 != PlayState.Paused && playState6 != PlayState.Stopped)
                                            {
                                                Initializevid6(null, null);
                                            }
                                            else if (playState6 == PlayState.Paused)
                                            {
                                                Init6(null, null);
                                            }
                                            else if (playState6 == PlayState.Stopped)
                                            {
                                                Init6(null, null);
                                            }

                                        if ((mediaCtrl6 != null) || (mediaSeek6 != null))
                                            mediaPos6.put_CurrentPosition(curPos.TotalSeconds - strtm6.TotalSeconds);
                                    }
                                }
                            }
                        }
                    }

                    //trackbar cam7
                    if (cam7Count != 0)
                    {
                        double dura7 = duration7[cam7Count - 1];
                        TimeSpan val1g = starttime7[g];
                        Console.WriteLine("val7 " + val1g);

                        TimeSpan val2g = val1g.Add(TimeSpan.FromSeconds(duration7[g]));

                        if (val1g < curPos && curPos < val2g)
                        {
                            Console.WriteLine("diantara");
                            strtm7 = starttime7[g];
                            if ((mediaCtrl7 == null) || (mediaSeek7 == null))
                                if (playState7 != PlayState.Paused)
                                    Init7(null, null);

                            if ((mediaCtrl7 != null) || (mediaSeek7 != null))
                                mediaPos7.put_CurrentPosition(curPos.TotalSeconds - strtm7.TotalSeconds);
                        }
                        else if (curPos > val2g)
                        {
                            Console.WriteLine("lebih dari");
                            clipFile7 = null;
                            clipType7 = ClipType.None;
                            CloseInterfaces7();
                            splitContainer7.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                            if (g < cam7Count - 1)
                            {
                                Console.WriteLine(starttime7[g + 1]);
                                if (fromTime.Add(curPos) > starttime7[g + 1])
                                {
                                    g = g + 1;
                                    SeqNo7 = g;

                                    if (curPos >= starttime7[g])
                                    {
                                        strtm7 = starttime7[g];
                                        if (playState7 != PlayState.Paused && playState7 != PlayState.Stopped)
                                        {
                                            Initializevid7(null, null);
                                        }
                                        else if (playState7 == PlayState.Paused)
                                        {
                                            Init7(null, null);
                                        }
                                        else if (playState7 == PlayState.Stopped)
                                        {
                                            Init7(null, null);
                                        }

                                        if ((mediaCtrl7 != null) || (mediaSeek7 != null))
                                            mediaPos7.put_CurrentPosition(curPos.TotalSeconds - strtm7.TotalSeconds);
                                    }
                                }
                            }
                        }
                        else if (curPos < val1g)
                        {
                            Console.WriteLine("kurang dari");
                            if ((mediaCtrl7 != null) || (mediaSeek7 != null))
                            {
                                clipFile7 = null;
                                clipType7 = ClipType.None;
                                CloseInterfaces7();
                                splitContainer7.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                                if (SeqNo7 > 0)
                                {
                                    if (curPos < starttime7[g - 1])
                                    {
                                        g = g - 1;
                                        SeqNo7 = g;

                                        if (curPos >= starttime7[g])
                                        {
                                            strtm7 = starttime7[g];
                                            if ((mediaCtrl7 == null) || (mediaSeek7 == null))
                                                if (playState7 != PlayState.Paused && playState7 != PlayState.Stopped)
                                                {
                                                    Initializevid7(null, null);
                                                }
                                                else if (playState7 == PlayState.Paused)
                                                {
                                                    Init7(null, null);
                                                }
                                                else if (playState7 == PlayState.Stopped)
                                                {
                                                    Init7(null, null);
                                                }

                                            if ((mediaCtrl7 != null) || (mediaSeek7 != null))
                                                mediaPos7.put_CurrentPosition(curPos.TotalSeconds - strtm7.TotalSeconds);
                                        }
                                    }
                                    else if (curPos < starttime7[g - 1].Add(TimeSpan.FromSeconds(duration7[g - 1])))
                                    {
                                        g = g - 1;
                                        SeqNo7 = g;
                                        strtm7 = starttime7[g];
                                        if ((mediaCtrl7 == null) || (mediaSeek7 == null))
                                            if (playState7 != PlayState.Paused && playState7 != PlayState.Stopped)
                                            {
                                                Initializevid7(null, null);
                                            }
                                            else if (playState7 == PlayState.Paused)
                                            {
                                                Init7(null, null);
                                            }
                                            else if (playState7 == PlayState.Stopped)
                                            {
                                                Init7(null, null);
                                            }

                                        if ((mediaCtrl7 != null) || (mediaSeek7 != null))
                                            mediaPos7.put_CurrentPosition(curPos.TotalSeconds - strtm7.TotalSeconds);
                                    }
                                }
                            }
                            else
                            {
                                if (SeqNo7 > 0)
                                {
                                    Console.WriteLine("lebih dari akhir");
                                    Console.WriteLine("dur" + starttime7[g - 1].Add(TimeSpan.FromSeconds(duration7[g - 1])));
                                    if (curPos > starttime7[g - 1].Add(TimeSpan.FromSeconds(duration7[g - 1])))
                                    {
                                        if (curPos >= starttime7[g])
                                        {
                                            g = g - 1;
                                            SeqNo7 = g;
                                            strtm7 = starttime7[g];
                                            if ((mediaCtrl7 == null) || (mediaSeek7 == null))
                                                if (playState7 != PlayState.Paused && playState7 != PlayState.Stopped)
                                                {
                                                    Initializevid7(null, null);
                                                }
                                                else if (playState7 == PlayState.Paused)
                                                {
                                                    Init7(null, null);
                                                }
                                                else if (playState7 == PlayState.Stopped)
                                                {
                                                    Init7(null, null);
                                                }

                                            if ((mediaCtrl7 != null) || (mediaSeek7 != null))
                                                mediaPos7.put_CurrentPosition(curPos.TotalSeconds - strtm7.TotalSeconds);
                                        }
                                    }
                                    else if (curPos < starttime7[g - 1].Add(TimeSpan.FromSeconds(duration7[g - 1])))
                                    {
                                        Console.WriteLine("kurang dari");
                                        g = g - 1;
                                        SeqNo7 = g;

                                        strtm7 = starttime7[g];
                                        if ((mediaCtrl7 == null) || (mediaSeek7 == null))
                                            if (playState7 != PlayState.Paused && playState7 != PlayState.Stopped)
                                            {
                                                Initializevid7(null, null);
                                            }
                                            else if (playState7 == PlayState.Paused)
                                            {
                                                Init7(null, null);
                                            }
                                            else if (playState7 == PlayState.Stopped)
                                            {
                                                Init7(null, null);
                                            }

                                        if ((mediaCtrl7 != null) || (mediaSeek7 != null))
                                            mediaPos7.put_CurrentPosition(curPos.TotalSeconds - strtm7.TotalSeconds);
                                    }
                                }
                            }
                        }
                    }


                    //trackbar cam8
                    if (cam8Count != 0)
                    {
                        double dura8 = duration8[cam8Count - 1];
                        TimeSpan val1h = starttime8[h];
                        Console.WriteLine("val8 " + val1h);

                        TimeSpan val2h = val1h.Add(TimeSpan.FromSeconds(duration8[h]));

                        if (val1h < curPos && curPos < val2h)
                        {
                            Console.WriteLine("diantara");
                            strtm8 = starttime8[h];
                            if ((mediaCtrl8 == null) || (mediaSeek8 == null))
                                if (playState8 != PlayState.Paused)
                                    Init8(null, null);

                            if ((mediaCtrl8 != null) || (mediaSeek8 != null))
                                mediaPos8.put_CurrentPosition(curPos.TotalSeconds - strtm8.TotalSeconds);
                        }
                        else if (curPos > val2h)
                        {
                            Console.WriteLine("lebih dari");
                            clipFile8 = null;
                            clipType8 = ClipType.None;
                            CloseInterfaces8();
                            splitContainer8.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                            if (h < cam8Count - 1)
                            {
                                Console.WriteLine(starttime8[h + 1]);
                                if (fromTime.Add(curPos) > starttime8[h + 1])
                                {
                                    h = h + 1;
                                    SeqNo8 = h;

                                    if (curPos >= starttime8[h])
                                    {
                                        strtm8 = starttime8[h];
                                        if (playState8 != PlayState.Paused && playState8 != PlayState.Stopped)
                                        {
                                            Initializevid8(null, null);
                                        }
                                        else if (playState8 == PlayState.Paused)
                                        {
                                            Init8(null, null);
                                        }
                                        else if (playState8 == PlayState.Stopped)
                                        {
                                            Init8(null, null);
                                        }

                                        if ((mediaCtrl8 != null) || (mediaSeek8 != null))
                                            mediaPos8.put_CurrentPosition(curPos.TotalSeconds - strtm8.TotalSeconds);
                                    }
                                }
                            }
                        }
                        else if (curPos < val1h)
                        {
                            Console.WriteLine("kurang dari");
                            if ((mediaCtrl8 != null) || (mediaSeek8 != null))
                            {
                                clipFile8 = null;
                                clipType8 = ClipType.None;
                                CloseInterfaces8();
                                splitContainer8.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                                if (SeqNo8 > 0)
                                {
                                    if (curPos < starttime8[h - 1])
                                    {
                                        h = h - 1;
                                        SeqNo8 = h;

                                        if (curPos >= starttime8[h])
                                        {
                                            strtm8 = starttime8[h];
                                            if ((mediaCtrl8 == null) || (mediaSeek8 == null))
                                                if (playState8 != PlayState.Paused && playState8 != PlayState.Stopped)
                                                {
                                                    Initializevid8(null, null);
                                                }
                                                else if (playState8 == PlayState.Paused)
                                                {
                                                    Init8(null, null);
                                                }
                                                else if (playState8 == PlayState.Stopped)
                                                {
                                                    Init8(null, null);
                                                }

                                            if ((mediaCtrl8 != null) || (mediaSeek8 != null))
                                                mediaPos8.put_CurrentPosition(curPos.TotalSeconds - strtm8.TotalSeconds);
                                        }
                                    }
                                    else if (curPos < starttime8[h - 1].Add(TimeSpan.FromSeconds(duration8[h - 1])))
                                    {
                                        h = h - 1;
                                        SeqNo8 = h;
                                        strtm8 = starttime8[h];
                                        if ((mediaCtrl8 == null) || (mediaSeek8 == null))
                                            if (playState8 != PlayState.Paused && playState8 != PlayState.Stopped)
                                            {
                                                Initializevid8(null, null);
                                            }
                                            else if (playState8 == PlayState.Paused)
                                            {
                                                Init8(null, null);
                                            }
                                            else if (playState8 == PlayState.Stopped)
                                            {
                                                Init8(null, null);
                                            }

                                        if ((mediaCtrl8 != null) || (mediaSeek8 != null))
                                            mediaPos8.put_CurrentPosition(curPos.TotalSeconds - strtm8.TotalSeconds);
                                    }
                                }
                            }
                            else
                            {
                                if (SeqNo8 > 0)
                                {
                                    Console.WriteLine("lebih dari akhir");
                                    Console.WriteLine("dur" + starttime8[h - 1].Add(TimeSpan.FromSeconds(duration8[h - 1])));
                                    if (curPos > starttime8[h - 1].Add(TimeSpan.FromSeconds(duration8[h - 1])))
                                    {
                                        if (curPos >= starttime8[h])
                                        {
                                            h = h - 1;
                                            SeqNo8 = h;
                                            strtm8 = starttime8[h];
                                            if ((mediaCtrl8 == null) || (mediaSeek8 == null))
                                                if (playState8 != PlayState.Paused && playState8 != PlayState.Stopped)
                                                {
                                                    Initializevid8(null, null);
                                                }
                                                else if (playState8 == PlayState.Paused)
                                                {
                                                    Init8(null, null);
                                                }
                                                else if (playState8 == PlayState.Stopped)
                                                {
                                                    Init8(null, null);
                                                }

                                            if ((mediaCtrl8 != null) || (mediaSeek8 != null))
                                                mediaPos8.put_CurrentPosition(curPos.TotalSeconds - strtm8.TotalSeconds);
                                        }
                                    }
                                    else if (curPos < starttime8[h - 1].Add(TimeSpan.FromSeconds(duration8[h - 1])))
                                    {
                                        Console.WriteLine("kurang dari");
                                        h = h - 1;
                                        SeqNo8 = h;

                                        strtm8 = starttime8[h];
                                        if ((mediaCtrl8 == null) || (mediaSeek8 == null))
                                            if (playState8 != PlayState.Paused && playState8 != PlayState.Stopped)
                                            {
                                                Initializevid8(null, null);
                                            }
                                            else if (playState8 == PlayState.Paused)
                                            {
                                                Init8(null, null);
                                            }
                                            else if (playState8 == PlayState.Stopped)
                                            {
                                                Init8(null, null);
                                            }

                                        if ((mediaCtrl8 != null) || (mediaSeek8 != null))
                                            mediaPos8.put_CurrentPosition(curPos.TotalSeconds - strtm8.TotalSeconds);
                                    }
                                }
                            }
                        }
                    }



                    ticky = timedur;
                    Console.WriteLine("ticky--> " + ticky);
                    lbStartDur.Text = fromTime.Add(TimeSpan.Parse(CalculateTime(ticky))).ToString();

                    Console.WriteLine("a--> " + a);
                    Console.WriteLine("b--> " + b);
                    Console.WriteLine("c--> " + c);
                    Console.WriteLine("d--> " + d);
                    Console.WriteLine("e5--> " + e5);
                    Console.WriteLine("f--> " + f);
                    Console.WriteLine("g--> " + g);
                    Console.WriteLine("h--> " + h);
                }
                else if (videoTrackBar.Value >= videoTrackBar.Maximum)
                {
                    btPause_Click(null, null);
                    //btPlay.Enabled = false;
                    //btFast4x.Enabled = false;
                    //btFast8x.Enabled = false;
                    timer1.Stop();

                    splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                    splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                    splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                    splitContainer4.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                    splitContainer4.Panel1.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                    splitContainer6.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                    splitContainer7.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;
                    splitContainer11.Panel2.BackgroundImage = mtvplayer.Properties.Resources.noVid;

                    finish = new finish(this);
                    finish.ShowDialog(this);
                }
            }
        }

        /// <summary> override window fn to handle graph events. </summary>
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_GRAPHNOTIFY)
            {
                if (mediaEvt1 != null)
                    OnGraphNotify1();
                if (mediaEvt2 != null)
                    OnGraphNotify2();
                if (mediaEvt3 != null)
                    OnGraphNotify3();
                if (mediaEvt4 != null)
                    OnGraphNotify4();
                if (mediaEvt5 != null)
                    OnGraphNotify5();
                if (mediaEvt6 != null)
                    OnGraphNotify6();
                if (mediaEvt6 != null)
                    OnGraphNotify6();
                if (mediaEvt6 != null)
                    OnGraphNotify6();
                return;
            }

            base.WndProc(ref m);
        }

        /// <summary> graph event (WM_GRAPHNOTIFY) handler in channel 2. </summary>
        void OnGraphNotify1()
        {
            int p1, p2, hr1 = 0;
            DsEvCode code;
            do
            {
                if (mediaEvt1 == null)
                    break;

                hr1 = mediaEvt1.GetEvent(out code, out p1, out p2, 0);
                if (hr1 < 0)
                    break;
                hr1 = mediaEvt1.FreeEventParams(code, p1, p2);
                if (code == DsEvCode.Complete)
                    OnClipCompleted1();
            }
            while (hr1 == 0);
        }

        /// <summary> graph event (WM_GRAPHNOTIFY) handler in channel 2. </summary>
        void OnGraphNotify2()
        {
            int p1, p2, hr2 = 0;
            DsEvCode code;
            do
            {
                if (mediaEvt2 == null)
                    break;

                hr2 = mediaEvt2.GetEvent(out code, out p1, out p2, 0);
                if (hr2 < 0)
                    break;
                hr2 = mediaEvt2.FreeEventParams(code, p1, p2);
                if (code == DsEvCode.Complete)
                    OnClipCompleted2();
            }
            while (hr2 == 0);
        }

        /// <summary> graph event (WM_GRAPHNOTIFY) handler in channel 3. </summary>
        void OnGraphNotify3()
        {
            int p1, p2, hr3 = 0;
            DsEvCode code;
            do
            {
                if (mediaEvt3 == null)
                    break;

                hr3 = mediaEvt3.GetEvent(out code, out p1, out p2, 0);
                if (hr3 < 0)
                    break;
                hr3 = mediaEvt3.FreeEventParams(code, p1, p2);
                if (code == DsEvCode.Complete)
                    OnClipCompleted3();
            }
            while (hr3 == 0);
        }

        /// <summary> graph event (WM_GRAPHNOTIFY) handler in channel 4. </summary>
        void OnGraphNotify4()
        {
            int p1, p2, hr4 = 0;
            DsEvCode code;
            do
            {
                if (mediaEvt4 == null)
                    break;

                hr4 = mediaEvt4.GetEvent(out code, out p1, out p2, 0);
                if (hr4 < 0)
                    break;
                hr4 = mediaEvt4.FreeEventParams(code, p1, p2);
                if (code == DsEvCode.Complete)
                    OnClipCompleted4();
            }
            while (hr4 == 0);
        }


        void OnGraphNotify5()
        {
            int p1, p2, hr5 = 0;
            DsEvCode code;
            do
            {
                if (mediaEvt5 == null)
                    break;

                hr5 = mediaEvt5.GetEvent(out code, out p1, out p2, 0);
                if (hr5 < 0)
                    break;
                hr5 = mediaEvt5.FreeEventParams(code, p1, p2);
                if (code == DsEvCode.Complete)
                    OnClipCompleted5();
            }
            while (hr5 == 0);
        }

        void OnGraphNotify6()
        {
            int p1, p2, hr6 = 0;
            DsEvCode code;
            do
            {
                if (mediaEvt6 == null)
                    break;

                hr6 = mediaEvt6.GetEvent(out code, out p1, out p2, 0);
                if (hr6 < 0)
                    break;
                hr6 = mediaEvt6.FreeEventParams(code, p1, p2);
                if (code == DsEvCode.Complete)
                    OnClipCompleted6();
            }
            while (hr6 == 0);
        }

        void OnGraphNotify7()
        {
            int p1, p2, hr7 = 0;
            DsEvCode code;
            do
            {
                if (mediaEvt7 == null)
                    break;

                hr7 = mediaEvt7.GetEvent(out code, out p1, out p2, 0);
                if (hr7 < 0)
                    break;
                hr7 = mediaEvt7.FreeEventParams(code, p1, p2);
                if (code == DsEvCode.Complete)
                    OnClipCompleted7();
            }
            while (hr7 == 0);
        }

        void OnGraphNotify8()
        {
            int p1, p2, hr8 = 0;
            DsEvCode code;
            do
            {
                if (mediaEvt8 == null)
                    break;

                hr8 = mediaEvt8.GetEvent(out code, out p1, out p2, 0);
                if (hr8 < 0)
                    break;
                hr8 = mediaEvt8.FreeEventParams(code, p1, p2);
                if (code == DsEvCode.Complete)
                    OnClipCompleted8();
            }
            while (hr8 == 0);
        }

        /// <summary> graph event if clip has finished in channel 1</summary>
        void OnClipCompleted1()
        {
            if (mediaCtrl1 == null || mediaSeek1 == null)
                return;

            DsOptInt64 pos = new DsOptInt64(0);
            int hr1 = mediaSeek1.SetPositions(pos, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);

            if (hr1 == 0)
                return;

            hr1 = mediaCtrl1.Stop();
            clipFile1 = null;
            clipType1 = ClipType.None;
            CloseInterfaces();

            if (SeqNo1 + 1 < cam1Count)
            {
                SeqNo1++;
            }
            else if (SeqNo1 >= cam1Count)
            {
                return;
            }

            if (hr1 < 0)
            {
                return;
            }
        }

        /// <summary> graph event if clip has finished in channel 2</summary>
        void OnClipCompleted2()
        {
            if (mediaCtrl2 == null || mediaSeek2 == null)
                return;

            DsOptInt64 pos = new DsOptInt64(0);
            int hr2 = mediaSeek2.SetPositions(pos, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);

            if (hr2 == 0)
                return;
            hr2 = mediaCtrl2.Stop();

            clipFile2 = null;
            clipType2 = ClipType.None;
            CloseInterfaces2();

            if (SeqNo2 + 1 < cam2Count)
            {
                SeqNo2++;
            }
            else if (SeqNo2 >= cam2Count)
            {
                return;
            }

            if (hr2 < 0)
                return;
        }

        /// <summary> graph event if clip has finished in channel 3</summary>
        void OnClipCompleted3()
        {
            if (mediaCtrl3 == null || mediaSeek3 == null)
                return;

            DsOptInt64 pos = new DsOptInt64(0);
            int hr3 = mediaSeek3.SetPositions(pos, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);

            //Console.WriteLine("hr3 " + hr3);
            if (hr3 == 0)
                return;
            hr3 = mediaCtrl3.Stop();

            clipFile3 = null;
            clipType3 = ClipType.None;
            CloseInterfaces3();

            if (SeqNo3 + 1 < cam3Count)
            {
                SeqNo3++;
                //   Console.WriteLine("SeqNo3 " + SeqNo3);
            }
            else if (SeqNo3 >= cam3Count)
            {
                return;
            }

            if (hr3 < 0)
                return;
        }

        /// <summary> graph event if clip has finished in channel 4</summary>
        void OnClipCompleted4()
        {
            //Console.WriteLine("OnClipCompleted4");
            if (mediaCtrl4 == null || mediaSeek4 == null)
                return;

            DsOptInt64 pos = new DsOptInt64(0);
            int hr4 = mediaSeek4.SetPositions(pos, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);

            // Console.WriteLine("hr4 " + hr4);
            if (hr4 == 0)
            {
                Console.WriteLine("hr4a " + hr4);
                return;
            }
            Console.WriteLine("hr4b " + hr4);
            hr4 = mediaCtrl4.Stop();
            Console.WriteLine("hr4ba " + hr4);

            clipFile4 = null;
            clipType4 = ClipType.None;
            CloseInterfaces4();

            if (SeqNo4 + 1 < cam4Count)
            {
                SeqNo4++;
                //Console.WriteLine("SeqNo4 " + SeqNo4);
            }
            else if (SeqNo4 >= cam4Count)
            {
                //Console.WriteLine("hr4 " + hr4);
                return;
            }

            if (hr4 < 0)
            {
                //Console.WriteLine("hr4c " + hr4);
                return;
            }
        }

        /// <summary> graph event if clip has finished in channel 5</summary>
        void OnClipCompleted5()
        {
            //Console.WriteLine("OnClipCompleted4");
            if (mediaCtrl5 == null || mediaSeek5 == null)
                return;

            DsOptInt64 pos = new DsOptInt64(0);
            int hr5 = mediaSeek5.SetPositions(pos, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);

            // Console.WriteLine("hr4 " + hr4);
            if (hr5 == 0)
            {
                Console.WriteLine("hr5a " + hr5);
                return;
            }
            Console.WriteLine("hr5b " + hr5);
            hr5 = mediaCtrl5.Stop();
            Console.WriteLine("hr5ba " + hr5);

            clipFile5 = null;
            clipType5 = ClipType.None;
            CloseInterfaces5();

            if (SeqNo5 + 1 < cam5Count)
            {
                SeqNo5++;
                //Console.WriteLine("SeqNo4 " + SeqNo4);
            }
            else if (SeqNo5 >= cam5Count)
            {
                //Console.WriteLine("hr4 " + hr4);
                return;
            }

            if (hr5 < 0)
            {
                //Console.WriteLine("hr4c " + hr4);
                return;
            }
        }

        /// <summary> graph event if clip has finished in channel 6</summary>
        void OnClipCompleted6()
        {
            //Console.WriteLine("OnClipCompleted4");
            if (mediaCtrl6 == null || mediaSeek6 == null)
                return;

            DsOptInt64 pos = new DsOptInt64(0);
            int hr6 = mediaSeek6.SetPositions(pos, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);

            // Console.WriteLine("hr4 " + hr4);
            if (hr6 == 0)
            {
                Console.WriteLine("hr6a " + hr6);
                return;
            }
            Console.WriteLine("hr6b " + hr6);
            hr6 = mediaCtrl6.Stop();
            Console.WriteLine("hr6ba " + hr6);

            clipFile6 = null;
            clipType6 = ClipType.None;
            CloseInterfaces6();

            if (SeqNo6 + 1 < cam6Count)
            {
                SeqNo6++;
                //Console.WriteLine("SeqNo4 " + SeqNo4);
            }
            else if (SeqNo6 >= cam6Count)
            {
                //Console.WriteLine("hr4 " + hr4);
                return;
            }

            if (hr6 < 0)
            {
                //Console.WriteLine("hr4c " + hr4);
                return;
            }
        }

        /// <summary> graph event if clip has finished in channel 7</summary>
        void OnClipCompleted7()
        {
            //Console.WriteLine("OnClipCompleted4");
            if (mediaCtrl7 == null || mediaSeek7 == null)
                return;

            DsOptInt64 pos = new DsOptInt64(0);
            int hr7 = mediaSeek7.SetPositions(pos, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);

            // Console.WriteLine("hr4 " + hr4);
            if (hr7 == 0)
            {
                Console.WriteLine("hr7a " + hr7);
                return;
            }
            Console.WriteLine("hr7b " + hr7);
            hr7 = mediaCtrl7.Stop();
            Console.WriteLine("hr7ba " + hr7);

            clipFile7 = null;
            clipType7 = ClipType.None;
            CloseInterfaces7();

            if (SeqNo7 + 1 < cam7Count)
            {
                SeqNo7++;
                //Console.WriteLine("SeqNo4 " + SeqNo4);
            }
            else if (SeqNo7 >= cam7Count)
            {
                //Console.WriteLine("hr4 " + hr4);
                return;
            }

            if (hr7 < 0)
            {
                //Console.WriteLine("hr4c " + hr4);
                return;
            }
        }

        /// <summary> graph event if clip has finished in channel 8</summary>
        void OnClipCompleted8()
        {
            //Console.WriteLine("OnClipCompleted4");
            if (mediaCtrl8 == null || mediaSeek8 == null)
                return;

            DsOptInt64 pos = new DsOptInt64(0);
            int hr8 = mediaSeek8.SetPositions(pos, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);

            // Console.WriteLine("hr4 " + hr4);
            if (hr8 == 0)
            {
                Console.WriteLine("hr8a " + hr8);
                return;
            }
            Console.WriteLine("hr8b " + hr8);
            hr8 = mediaCtrl8.Stop();
            Console.WriteLine("hr8ba " + hr8);

            clipFile8 = null;
            clipType8 = ClipType.None;
            CloseInterfaces8();

            if (SeqNo8 + 1 < cam8Count)
            {
                SeqNo8++;
                //Console.WriteLine("SeqNo4 " + SeqNo4);
            }
            else if (SeqNo8 >= cam8Count)
            {
                //Console.WriteLine("hr4 " + hr4);
                return;
            }

            if (hr8 < 0)
            {
                //Console.WriteLine("hr4c " + hr4);
                return;
            }
        }


        /// <summary> stop the video. </summary>
        public void stop()
        {
            if (playmode == "single")
            {
                if (mediaCtrl1 == null || mediaSeek1 == null)
                    return;

                if (playState1 != PlayState.Paused && playState1 != PlayState.Running)
                    return;

                int hr1;
                //hr1= mediaCtrl1.Stop();
                //playState1 = PlayState.Stopped;

                DsOptInt64 pos1 = new DsOptInt64(0);
                hr1 = mediaSeek1.SetPositions(pos1, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);
                hr1 = mediaCtrl1.Pause();
            }
            else if (playmode == "multiple")
            {
                if (cam1Count != 0)
                {
                    if (mediaCtrl1 == null || mediaSeek1 == null)
                        return;

                    if (playState1 != PlayState.Paused && playState1 != PlayState.Running)
                        return;

                    int hr1;
                    //hr1= mediaCtrl1.Stop();
                    //playState1 = PlayState.Stopped;

                    DsOptInt64 pos1 = new DsOptInt64(0);
                    hr1 = mediaSeek1.SetPositions(pos1, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);
                    hr1 = mediaCtrl1.Pause();
                }

                if (cam2Count != 0)
                {
                    //stop2
                    if (mediaCtrl2 == null || mediaSeek2 == null)
                        return;

                    if (playState2 != PlayState.Paused && playState2 != PlayState.Running)
                        return;

                    int hr2;
                    //hr2= mediaCtrl2.Stop();
                    //playState1 = PlayState.Stopped;

                    DsOptInt64 pos2 = new DsOptInt64(0);
                    hr2 = mediaSeek2.SetPositions(pos2, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);
                    hr2 = mediaCtrl2.Pause();
                }

                if (cam3Count != 0)
                {
                    //stop3
                    if (mediaCtrl3 == null || mediaSeek3 == null)
                        return;

                    if (playState3 != PlayState.Paused && playState3 != PlayState.Running)
                        return;

                    int hr3;
                    //hr3= mediaCtrl3.Stop();
                    //playState3 = PlayState.Stopped;

                    DsOptInt64 pos3 = new DsOptInt64(0);
                    hr3 = mediaSeek3.SetPositions(pos3, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);
                    hr3 = mediaCtrl3.Pause();
                }

                if (cam4Count != 0)
                {
                    //stop4
                    if (mediaCtrl4 == null || mediaSeek4 == null)
                        return;

                    if (playState4 != PlayState.Paused && playState4 != PlayState.Running)
                        return;

                    int hr4;
                    //hr4 = mediaCtrl4.Stop();
                    //playState4 = PlayState.Stopped;

                    DsOptInt64 pos4 = new DsOptInt64(0);
                    hr4 = mediaSeek4.SetPositions(pos4, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);
                    hr4 = mediaCtrl4.Pause();
                }

                if (cam5Count != 0)
                {
                    //stop4
                    if (mediaCtrl5 == null || mediaSeek5 == null)
                        return;

                    if (playState5 != PlayState.Paused && playState5 != PlayState.Running)
                        return;

                    int hr5;
                    //hr4 = mediaCtrl4.Stop();
                    //playState4 = PlayState.Stopped;

                    DsOptInt64 pos5 = new DsOptInt64(0);
                    hr5 = mediaSeek5.SetPositions(pos5, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);
                    hr5 = mediaCtrl5.Pause();
                }

                if (cam6Count != 0)
                {
                    //stop4
                    if (mediaCtrl6 == null || mediaSeek6 == null)
                        return;

                    if (playState6 != PlayState.Paused && playState6 != PlayState.Running)
                        return;

                    int hr6;
                    //hr4 = mediaCtrl4.Stop();
                    //playState4 = PlayState.Stopped;

                    DsOptInt64 pos6 = new DsOptInt64(0);
                    hr6 = mediaSeek6.SetPositions(pos6, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);
                    hr6 = mediaCtrl6.Pause();
                }
                if (cam7Count != 0)
                {
                    //stop4
                    if (mediaCtrl7 == null || mediaSeek7 == null)
                        return;

                    if (playState7 != PlayState.Paused && playState7 != PlayState.Running)
                        return;

                    int hr7;
                    //hr4 = mediaCtrl4.Stop();
                    //playState4 = PlayState.Stopped;

                    DsOptInt64 pos7 = new DsOptInt64(0);
                    hr7 = mediaSeek7.SetPositions(pos7, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);
                    hr7 = mediaCtrl7.Pause();
                }
                if (cam8Count != 0)
                {
                    //stop4
                    if (mediaCtrl8 == null || mediaSeek8 == null)
                        return;

                    if (playState8 != PlayState.Paused && playState8 != PlayState.Running)
                        return;

                    int hr8;
                    //hr4 = mediaCtrl4.Stop();
                    //playState4 = PlayState.Stopped;

                    DsOptInt64 pos8 = new DsOptInt64(0);
                    hr8 = mediaSeek8.SetPositions(pos8, SeekingFlags.AbsolutePositioning, null, SeekingFlags.NoPositioning);
                    hr8 = mediaCtrl8.Pause();
                }

            }
        }

        /// <summary> backward the video in normal speed. </summary>
        //Checkpoint last
        public void Backwards()
        {
            double currentPosition1, currentPosition2, currentPosition3, currentPosition4, currentPosition5, currentPosition6, currentPosition7, currentPosition8;
            double newPosition1 = 0;
            double newPosition2 = 0;
            double newPosition3 = 0;
            double newPosition4 = 0;
            double newPosition5 = 0;
            double newPosition6 = 0;
            double newPosition7 = 0;
            double newPosition8 = 0;

            if (playmode == "single")
            {
                if (playState1 == PlayState.Running || playState1 == PlayState.Paused)
                {
                    mediaCtrl1.Pause();
                    playState1 = PlayState.Paused;
                }

                double currentPosition;
                mediaPos1.get_CurrentPosition(out currentPosition);

                if (currentVideoState == VideoState.Backwards)
                    newPosition1 = Math.Round(currentPosition) - 1;

                mediaPos1.put_CurrentPosition(Math.Max(0, newPosition1));
            }
            else if (playmode == "multiple")
            {
                if (ticky > 0)
                {
                    if (cam1Count > 0)
                    {
                        if (mediaCtrl1 != null || mediaSeek1 != null)
                        {
                            if (playState1 == PlayState.Running)
                            {
                                mediaCtrl1.Pause();
                                playState1 = PlayState.Paused;
                            }

                            if (mediaPos1 != null)
                            {
                                mediaPos1.get_CurrentPosition(out currentPosition1);
                                newPosition1 = Math.Round(currentPosition1) - 1;

                                Console.WriteLine("seqno1 min " + SeqNo1);
                                if (starttime1[SeqNo1].TotalSeconds < starttime1[SeqNo1].TotalSeconds + newPosition1)
                                {
                                    mediaPos1.put_CurrentPosition(Math.Max(0, newPosition1));

                                    Console.WriteLine("starttime1[SeqNo1].TotalSeconds " + starttime1[SeqNo1].TotalSeconds);
                                    Console.WriteLine("starttime1[SeqNo1].TotalSeconds + newPosition1 " + starttime1[SeqNo1].TotalSeconds + newPosition1);
                                }
                                else if (starttime1[SeqNo1].TotalSeconds - newPosition1 <= starttime1[SeqNo1].TotalSeconds)
                                {
                                    if (mediaCtrl1 != null || mediaSeek1 != null)
                                    {
                                        mediaPos1.put_CurrentPosition(0.0);

                                        mediaCtrl1.Stop();
                                        clipFile1 = null;
                                        clipType1 = ClipType.None;
                                        CloseInterfaces();
                                        SeqNo1--;

                                        if (SeqNo1 < 0)
                                        {
                                            SeqNo1 = 0;
                                        }
                                        else if (SeqNo1 >= 0)
                                        {
                                            //splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            Console.WriteLine("start1 " + starttime1[SeqNo1]);

                                            newPosition1 = newPosition1 + duration1[SeqNo1];
                                            if (playState1 == PlayState.Paused || playState1 == PlayState.Stopped)
                                            {
                                                Init1(null, null);
                                            }

                                            //Initializevid1(null, null);
                                            mediaPos1.put_CurrentPosition(Math.Max(0, newPosition1));
                                        }
                                    }
                                }
                                else if (starttime1[SeqNo1].TotalSeconds - newPosition1 < starttime1[SeqNo1].TotalSeconds)
                                { }
                            }
                        }
                        //else if (playState1 == PlayState.Paused || playState1 == PlayState.Stopped)
                        //{
                        //    Console.WriteLine("Backward " + TimeSpan.Parse(lbStartDur.Text));
                        //    Console.WriteLine("endtime " + endtime1F[SeqNo1]);
                        //    Console.WriteLine("< " + endtime1F[SeqNo1]);
                        //    if (TimeSpan.Parse(lbStartDur.Text) <= endtime1F[SeqNo1])
                        //    {
                        //        Init1(null, null);
                        //    }
                        //}
                    }

                    if (cam2Count > 0)
                    {
                        if (mediaCtrl1 != null || mediaSeek1 != null)
                        {
                            if (playState2 == PlayState.Running)
                            {
                                mediaCtrl2.Pause();
                                playState2 = PlayState.Paused;
                            }

                            if (mediaPos2 != null)
                            {
                                mediaPos2.get_CurrentPosition(out currentPosition2);
                                newPosition2 = Math.Round(currentPosition2) - 1;

                                if (starttime2[SeqNo2].TotalSeconds <= starttime2[SeqNo2].TotalSeconds + newPosition2)
                                {
                                    mediaPos2.put_CurrentPosition(Math.Max(0, newPosition2));
                                }
                                else if (starttime2[SeqNo2].TotalSeconds + newPosition2 < starttime2[SeqNo2].TotalSeconds)
                                {
                                    if (mediaCtrl2 != null || mediaSeek2 != null)
                                    {
                                        mediaPos2.put_CurrentPosition(0.0);
                                        mediaCtrl2.Stop();
                                        clipFile2 = null;
                                        clipType2 = ClipType.None;
                                        CloseInterfaces2();
                                        SeqNo2--;

                                        if (SeqNo2 < 0)
                                        {
                                            SeqNo2 = 0;
                                        }
                                        else if (SeqNo2 >= 0)
                                        {
                                            //splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            Console.WriteLine("start2 " + starttime2[SeqNo2]);

                                            newPosition2 = newPosition2 + duration2[SeqNo2];
                                            if (playState2 == PlayState.Paused || playState2 == PlayState.Stopped)
                                            {
                                                Init2(null, null);
                                            }

                                            //Initializevid2(null, null);
                                            mediaPos2.put_CurrentPosition(Math.Max(0, newPosition2));
                                        }
                                    }
                                }
                                else if (starttime2[SeqNo2].TotalSeconds - newPosition2 < starttime2[SeqNo2].TotalSeconds)
                                { }
                            }
                        }
                        //else if (playState2 == PlayState.Paused || playState2 == PlayState.Stopped)
                        //{
                        //    Console.WriteLine("Backward " + TimeSpan.Parse(lbStartDur.Text));
                        //    Console.WriteLine("endtime " + endtime2F[SeqNo2]);
                        //    Console.WriteLine("< " + endtime2F[SeqNo2]);

                        //    if (TimeSpan.Parse(lbStartDur.Text) <= endtime2F[SeqNo2])
                        //    {
                        //        Init2(null, null);
                        //    }
                        //}
                    }

                    if (cam3Count > 0)
                    {
                        if (mediaCtrl3 != null || mediaSeek3 != null)
                        {
                            if (playState3 == PlayState.Running)
                            {
                                mediaCtrl3.Pause();
                                playState3 = PlayState.Paused;
                            }

                            if (mediaPos3 != null)
                            {
                                mediaPos3.get_CurrentPosition(out currentPosition3);
                                newPosition3 = Math.Round(currentPosition3) - 1;
                                //newPosition3 = currentPosition3 - 1;

                                if (starttime3[SeqNo3].TotalSeconds <= starttime3[SeqNo3].TotalSeconds + newPosition3)
                                {
                                    mediaPos3.put_CurrentPosition(Math.Max(0, newPosition3));
                                }
                                else if (starttime3[SeqNo3].TotalSeconds + newPosition3 < starttime3[SeqNo3].TotalSeconds)
                                {
                                    if (mediaCtrl3 != null || mediaSeek3 != null)
                                    {
                                        mediaPos3.put_CurrentPosition(0.0);
                                        mediaCtrl3.Stop();
                                        clipFile3 = null;
                                        clipType3 = ClipType.None;
                                        CloseInterfaces3();
                                        SeqNo3--;

                                        if (SeqNo3 < 0)
                                        {
                                            SeqNo3 = 0;
                                        }
                                        else if (SeqNo3 >= 0)
                                        {
                                            //splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                            Console.WriteLine("start3 " + starttime3[SeqNo3]);

                                            newPosition3 = newPosition3 + duration3[SeqNo3];
                                            if (playState3 == PlayState.Paused || playState3 == PlayState.Stopped)
                                            {
                                                Init3(null, null);
                                            }
                                            //Initializevid3(null, null);
                                            mediaPos3.put_CurrentPosition(Math.Max(0, newPosition3));
                                        }
                                    }
                                }
                                else if (starttime3[SeqNo3].TotalSeconds - newPosition3 < starttime3[SeqNo3].TotalSeconds)
                                { }
                            }
                        }
                        //else if (playState3 == PlayState.Paused || playState3 == PlayState.Stopped)
                        //{
                        //    Console.WriteLine("Backward " + TimeSpan.Parse(lbStartDur.Text));
                        //    Console.WriteLine("endtime " + endtime3F[SeqNo3]);
                        //    Console.WriteLine("< " + endtime3F[SeqNo3]);

                        //    if (TimeSpan.Parse(lbStartDur.Text) <= endtime3F[SeqNo3])
                        //    {
                        //        Init3(null, null);
                        //    }
                        //}
                    }

                    if (cam4Count > 0)
                    {
                        if (mediaCtrl4 != null || mediaSeek4 != null)
                        {
                            if (playState4 == PlayState.Running)
                            {
                                mediaCtrl4.Pause();
                                playState4 = PlayState.Paused;
                            }

                            if (mediaPos4 != null)
                            {
                                mediaPos4.get_CurrentPosition(out currentPosition4);
                                newPosition4 = Math.Round(currentPosition4) - 1;
                                //newPosition4 = currentPosition4 - 1;

                                if (starttime4[SeqNo4].TotalSeconds <= starttime4[SeqNo4].TotalSeconds + newPosition4)
                                {
                                    mediaPos4.put_CurrentPosition(Math.Max(0, newPosition4));
                                }
                                else if (starttime4[SeqNo4].TotalSeconds + newPosition4 < starttime4[SeqNo4].TotalSeconds)
                                {
                                    if (mediaCtrl4 != null || mediaSeek4 != null)
                                    {
                                        mediaPos4.put_CurrentPosition(0.0);
                                        mediaCtrl4.Stop();
                                        clipFile4 = null;
                                        clipType4 = ClipType.None;
                                        CloseInterfaces4();
                                        SeqNo4--;

                                        if (SeqNo4 < 0)
                                        {
                                            SeqNo4 = 0;
                                        }
                                        else if (SeqNo4 >= 0)
                                        {
                                            //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                            Console.WriteLine("start4 " + starttime4[SeqNo4]);

                                            newPosition4 = newPosition4 + duration4[SeqNo4];
                                            if (playState4 == PlayState.Paused || playState4 == PlayState.Stopped)
                                            {
                                                Init4(null, null);
                                            }
                                            //Initializevid4(null, null);
                                            mediaPos4.put_CurrentPosition(Math.Max(0, newPosition4));
                                        }
                                    }
                                }
                                else if (starttime4[SeqNo4].TotalSeconds - newPosition4 < starttime4[SeqNo4].TotalSeconds)
                                { }
                            }
                        }
                        //else if (playState4 == PlayState.Paused || playState4 == PlayState.Stopped)
                        //{
                        //    if (TimeSpan.Parse(lbStartDur.Text) <= endtime4F[SeqNo4])
                        //    {
                        //        Init4(null, null);
                        //    }
                        //}
                    }

                    if (cam5Count > 0)
                    {
                        if (mediaCtrl5 != null || mediaSeek5 != null)
                        {
                            if (playState5 == PlayState.Running)
                            {
                                mediaCtrl5.Pause();
                                playState5 = PlayState.Paused;
                            }

                            if (mediaPos5 != null)
                            {
                                mediaPos5.get_CurrentPosition(out currentPosition5);
                                newPosition5 = Math.Round(currentPosition5) - 1;
                                //newPosition4 = currentPosition4 - 1;

                                if (starttime5[SeqNo5].TotalSeconds <= starttime5[SeqNo5].TotalSeconds + newPosition5)
                                {
                                    mediaPos5.put_CurrentPosition(Math.Max(0, newPosition5));
                                }
                                else if (starttime5[SeqNo5].TotalSeconds + newPosition5 < starttime4[SeqNo5].TotalSeconds)
                                {
                                    if (mediaCtrl5 != null || mediaSeek5 != null)
                                    {
                                        mediaPos5.put_CurrentPosition(0.0);
                                        mediaCtrl5.Stop();
                                        clipFile5 = null;
                                        clipType5 = ClipType.None;
                                        CloseInterfaces5();
                                        SeqNo5--;

                                        if (SeqNo5 < 0)
                                        {
                                            SeqNo5 = 0;
                                        }
                                        else if (SeqNo5 >= 0)
                                        {
                                            //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                            Console.WriteLine("start5 " + starttime5[SeqNo5]);

                                            newPosition5 = newPosition5 + duration5[SeqNo5];
                                            if (playState5 == PlayState.Paused || playState5 == PlayState.Stopped)
                                            {
                                                Init5(null, null);
                                            }
                                            //Initializevid4(null, null);
                                            mediaPos5.put_CurrentPosition(Math.Max(0, newPosition5));
                                        }
                                    }
                                }
                                else if (starttime5[SeqNo5].TotalSeconds - newPosition5 < starttime5[SeqNo5].TotalSeconds)
                                { }
                            }
                        }
                        //else if (playState4 == PlayState.Paused || playState4 == PlayState.Stopped)
                        //{
                        //    if (TimeSpan.Parse(lbStartDur.Text) <= endtime4F[SeqNo4])
                        //    {
                        //        Init4(null, null);
                        //    }
                        //}
                    }

                    if (cam6Count > 0)
                    {
                        if (mediaCtrl6 != null || mediaSeek6 != null)
                        {
                            if (playState6 == PlayState.Running)
                            {
                                mediaCtrl6.Pause();
                                playState6 = PlayState.Paused;
                            }

                            if (mediaPos6 != null)
                            {
                                mediaPos6.get_CurrentPosition(out currentPosition6);
                                newPosition6 = Math.Round(currentPosition6) - 1;

                                if (starttime6[SeqNo6].TotalSeconds <= starttime6[SeqNo6].TotalSeconds + newPosition6)
                                {
                                    mediaPos6.put_CurrentPosition(Math.Max(0, newPosition6));
                                }
                                else if (starttime6[SeqNo6].TotalSeconds + newPosition6 < starttime4[SeqNo6].TotalSeconds)
                                {
                                    if (mediaCtrl6 != null || mediaSeek6 != null)
                                    {
                                        mediaPos6.put_CurrentPosition(0.0);
                                        mediaCtrl6.Stop();
                                        clipFile6 = null;
                                        clipType6 = ClipType.None;
                                        CloseInterfaces6();
                                        SeqNo6--;

                                        if (SeqNo6 < 0)
                                        {
                                            SeqNo6 = 0;
                                        }
                                        else if (SeqNo6 >= 0)
                                        {
                                            Console.WriteLine("start6 " + starttime6[SeqNo6]);

                                            newPosition6 = newPosition6 + duration6[SeqNo6];
                                            if (playState6 == PlayState.Paused || playState6 == PlayState.Stopped)
                                            {
                                                Init6(null, null);
                                            }
                                            mediaPos6.put_CurrentPosition(Math.Max(0, newPosition6));
                                        }
                                    }
                                }
                                else if (starttime6[SeqNo6].TotalSeconds - newPosition6 < starttime6[SeqNo6].TotalSeconds)
                                { }
                            }
                        }

                    }

                    if (cam7Count > 0)
                    {
                        if (mediaCtrl7 != null || mediaSeek7 != null)
                        {
                            if (playState7 == PlayState.Running)
                            {
                                mediaCtrl7.Pause();
                                playState7 = PlayState.Paused;
                            }

                            if (mediaPos7 != null)
                            {
                                mediaPos7.get_CurrentPosition(out currentPosition7);
                                newPosition7 = Math.Round(currentPosition7) - 1;
                                //newPosition4 = currentPosition4 - 1;

                                if (starttime7[SeqNo7].TotalSeconds <= starttime7[SeqNo7].TotalSeconds + newPosition7)
                                {
                                    mediaPos7.put_CurrentPosition(Math.Max(0, newPosition7));
                                }
                                else if (starttime7[SeqNo7].TotalSeconds + newPosition7 < starttime4[SeqNo7].TotalSeconds)
                                {
                                    if (mediaCtrl7 != null || mediaSeek7 != null)
                                    {
                                        mediaPos7.put_CurrentPosition(0.0);
                                        mediaCtrl7.Stop();
                                        clipFile7 = null;
                                        clipType7 = ClipType.None;
                                        CloseInterfaces7();
                                        SeqNo7--;

                                        if (SeqNo7 < 0)
                                        {
                                            SeqNo7 = 0;
                                        }
                                        else if (SeqNo7 >= 0)
                                        {
                                            //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                            Console.WriteLine("start7 " + starttime7[SeqNo7]);

                                            newPosition7 = newPosition7 + duration7[SeqNo7];
                                            if (playState7 == PlayState.Paused || playState7 == PlayState.Stopped)
                                            {
                                                Init7(null, null);
                                            }
                                            //Initializevid4(null, null);
                                            mediaPos7.put_CurrentPosition(Math.Max(0, newPosition7));
                                        }
                                    }
                                }
                                else if (starttime7[SeqNo7].TotalSeconds - newPosition7 < starttime7[SeqNo7].TotalSeconds)
                                { }
                            }
                        }

                    }

                    if (cam8Count > 0)
                    {
                        if (mediaCtrl8 != null || mediaSeek8 != null)
                        {
                            if (playState8 == PlayState.Running)
                            {
                                mediaCtrl8.Pause();
                                playState8 = PlayState.Paused;
                            }

                            if (mediaPos8 != null)
                            {
                                mediaPos8.get_CurrentPosition(out currentPosition8);
                                newPosition8 = Math.Round(currentPosition8) - 1;
                                //newPosition4 = currentPosition4 - 1;

                                if (starttime8[SeqNo8].TotalSeconds <= starttime8[SeqNo8].TotalSeconds + newPosition8)
                                {
                                    mediaPos8.put_CurrentPosition(Math.Max(0, newPosition8));
                                }
                                else if (starttime8[SeqNo8].TotalSeconds + newPosition8 < starttime4[SeqNo8].TotalSeconds)
                                {
                                    if (mediaCtrl8 != null || mediaSeek8 != null)
                                    {
                                        mediaPos8.put_CurrentPosition(0.0);
                                        mediaCtrl8.Stop();
                                        clipFile8 = null;
                                        clipType8 = ClipType.None;
                                        CloseInterfaces8();
                                        SeqNo8--;

                                        if (SeqNo8 < 0)
                                        {
                                            SeqNo8 = 0;
                                        }
                                        else if (SeqNo8 >= 0)
                                        {
                                            //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                            Console.WriteLine("start8 " + starttime8[SeqNo8]);

                                            newPosition8 = newPosition8 + duration8[SeqNo8];
                                            if (playState8 == PlayState.Paused || playState8 == PlayState.Stopped)
                                            {
                                                Init8(null, null);
                                            }
                                            //Initializevid4(null, null);
                                            mediaPos8.put_CurrentPosition(Math.Max(0, newPosition8));
                                        }
                                    }
                                }
                                else if (starttime8[SeqNo8].TotalSeconds - newPosition8 < starttime8[SeqNo8].TotalSeconds)
                                { }
                            }
                        }

                    }


                }
            }
        }

        /// <summary> backward the video in fast speed mode. </summary>
        public void FastBackwards()
        {
            double currentPosition1, currentPosition2, currentPosition3, currentPosition4, currentPosition5, currentPosition6, currentPosition7, currentPosition8;
            double newPosition1 = 0;
            double newPosition2 = 0;
            double newPosition3 = 0;
            double newPosition4 = 0;
            double newPosition5 = 0;
            double newPosition6 = 0;
            double newPosition7 = 0;
            double newPosition8 = 0;

            if (playmode == "single")
            {
                if (playState1 == PlayState.Running || playState1 == PlayState.Paused)
                {
                    mediaCtrl1.Pause();
                    playState1 = PlayState.Paused;
                }

                double currentPosition;
                mediaPos1.get_CurrentPosition(out currentPosition);

                if (currentVideoState == VideoState.FastBackwards4x)
                    newPosition1 = currentPosition - 1;// (rate * 12);

                if (currentVideoState == VideoState.FastBackwards8x)
                    newPosition1 = currentPosition - 1; // (rate * 20);

                mediaPos1.put_CurrentPosition(Math.Max(0, newPosition1));
            }
            else if (playmode == "multiple")
            {
                if (ticky >= 0)
                {
                    if (cam1Count != 0)
                    {
                        if (mediaCtrl1 != null || mediaSeek1 != null)
                        {
                            if (playState1 == PlayState.Running)
                            {
                                mediaCtrl1.Pause();
                                playState1 = PlayState.Paused;
                            }

                            if (mediaPos1 != null)
                            {
                                mediaPos1.get_CurrentPosition(out currentPosition1);
                                newPosition1 = currentPosition1 - 1;

                                if (starttime1[SeqNo1].TotalSeconds <= starttime1[SeqNo1].TotalSeconds + newPosition1)
                                {
                                    mediaPos1.put_CurrentPosition(Math.Max(0, newPosition1));
                                }
                                else if (starttime1[SeqNo1].TotalSeconds + newPosition1 < starttime1[SeqNo1].TotalSeconds)
                                {
                                    if (mediaCtrl1 != null || mediaSeek1 != null)
                                    {
                                        mediaPos1.put_CurrentPosition(0.0);
                                        mediaCtrl1.Stop();
                                        clipFile1 = null;
                                        clipType1 = ClipType.None;
                                        CloseInterfaces();
                                        SeqNo1--;

                                        if (SeqNo1 < 0)
                                        {
                                            SeqNo1 = 0;
                                        }
                                        //else if (SeqNo1 >= 0)
                                        //{
                                        //    //splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                        //    Console.WriteLine("start1 " + starttime1[SeqNo1]);

                                        //    newPosition1 = newPosition1 + duration1[SeqNo1];
                                        //    Initializevid1(null, null);
                                        //    mediaPos1.put_CurrentPosition(Math.Max(0, newPosition1));
                                        //}

                                        else if (SeqNo1 >= 0)
                                        {
                                            //splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            Console.WriteLine("start1 " + starttime1[SeqNo1]);

                                            newPosition1 = newPosition1 + duration1[SeqNo1];
                                            if (playState1 == PlayState.Paused || playState1 == PlayState.Stopped)
                                            {
                                                Init1(null, null);
                                            }

                                            //Initializevid1(null, null);
                                            mediaPos1.put_CurrentPosition(Math.Max(0, newPosition1));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (cam2Count != 0)
                    {
                        if (mediaCtrl2 != null || mediaSeek2 != null)
                        {
                            if (playState2 == PlayState.Running)
                            {
                                mediaCtrl2.Pause();
                                playState2 = PlayState.Paused;
                            }
                            if (mediaPos2 != null)
                            {
                                mediaPos2.get_CurrentPosition(out currentPosition2);
                                newPosition2 = currentPosition2 - 1;

                                if (starttime2[SeqNo2].TotalSeconds <= starttime2[SeqNo2].TotalSeconds + newPosition2)
                                {
                                    mediaPos2.put_CurrentPosition(Math.Max(0, newPosition2));
                                }
                                else if (starttime2[SeqNo2].TotalSeconds + newPosition2 < starttime2[SeqNo2].TotalSeconds)
                                {
                                    if (mediaCtrl2 != null || mediaSeek2 != null)
                                    {
                                        mediaPos2.put_CurrentPosition(0.0);
                                        mediaCtrl2.Stop();
                                        clipFile2 = null;
                                        clipType2 = ClipType.None;
                                        CloseInterfaces2();
                                        SeqNo2--;

                                        if (SeqNo2 < 0)
                                        {
                                            SeqNo2 = 0;
                                        }
                                        //else if (SeqNo2 >= 0)
                                        //{
                                        //    //splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                        //    Console.WriteLine("start2 " + starttime2[SeqNo2]);

                                        //    newPosition2 = newPosition2 + duration2[SeqNo2];
                                        //    Initializevid2(null, null);
                                        //    mediaPos2.put_CurrentPosition(Math.Max(0, newPosition2));
                                        //}

                                        else if (SeqNo2 >= 0)
                                        {
                                            //splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            Console.WriteLine("start2 " + starttime2[SeqNo2]);

                                            newPosition2 = newPosition2 + duration2[SeqNo2];
                                            if (playState2 == PlayState.Paused || playState2 == PlayState.Stopped)
                                            {
                                                Init2(null, null);
                                            }

                                            //Initializevid1(null, null);
                                            mediaPos2.put_CurrentPosition(Math.Max(0, newPosition2));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (cam3Count != 0)
                    {
                        if (mediaCtrl3 != null || mediaSeek3 != null)
                        {
                            if (playState3 == PlayState.Running)
                            {
                                mediaCtrl3.Pause();
                                playState3 = PlayState.Paused;
                            }

                            if (mediaPos3 != null)
                            {
                                mediaPos3.get_CurrentPosition(out currentPosition3);
                                newPosition3 = currentPosition3 - 1;

                                if (starttime3[SeqNo3].TotalSeconds <= starttime3[SeqNo3].TotalSeconds + newPosition3)
                                {
                                    mediaPos3.put_CurrentPosition(Math.Max(0, newPosition3));
                                }
                                else if (starttime3[SeqNo3].TotalSeconds + newPosition3 < starttime3[SeqNo3].TotalSeconds)
                                {
                                    if (mediaCtrl3 != null || mediaSeek3 != null)
                                    {
                                        mediaPos3.put_CurrentPosition(0.0);
                                        mediaCtrl3.Stop();
                                        clipFile3 = null;
                                        clipType3 = ClipType.None;
                                        CloseInterfaces3();
                                        SeqNo3--;

                                        if (SeqNo3 < 0)
                                        {
                                            SeqNo3 = 0;
                                        }
                                        //else if (SeqNo3 >= 0)
                                        //{
                                        //    //splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                        //    Console.WriteLine("start3 " + starttime3[SeqNo3]);

                                        //    newPosition3 = newPosition3 + duration3[SeqNo3];
                                        //    Initializevid3(null, null);
                                        //    mediaPos3.put_CurrentPosition(Math.Max(0, newPosition3));
                                        //}

                                        else if (SeqNo3 >= 0)
                                        {
                                            //splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            Console.WriteLine("start3 " + starttime3[SeqNo3]);

                                            newPosition3 = newPosition3 + duration3[SeqNo3];
                                            if (playState3 == PlayState.Paused || playState3 == PlayState.Stopped)
                                            {
                                                Init3(null, null);
                                            }

                                            mediaPos3.put_CurrentPosition(Math.Max(0, newPosition3));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (cam4Count != 0)
                    {
                        if (mediaCtrl4 != null || mediaSeek4 != null)
                        {
                            if (playState4 == PlayState.Running)
                            {
                                mediaCtrl4.Pause();
                                playState4 = PlayState.Paused;
                            }

                            if (mediaPos4 != null)
                            {
                                mediaPos4.get_CurrentPosition(out currentPosition4);
                                newPosition4 = currentPosition4 - 1;

                                if (starttime4[SeqNo4].TotalSeconds <= starttime4[SeqNo4].TotalSeconds + newPosition4)
                                {
                                    mediaPos4.put_CurrentPosition(Math.Max(0, newPosition4));
                                }
                                else if (starttime4[SeqNo4].TotalSeconds + newPosition4 < starttime4[SeqNo4].TotalSeconds)
                                {
                                    if (mediaCtrl4 != null || mediaSeek4 != null)
                                    {
                                        mediaPos4.put_CurrentPosition(0.0);
                                        mediaCtrl4.Stop();
                                        clipFile4 = null;
                                        clipType4 = ClipType.None;
                                        CloseInterfaces4();
                                        SeqNo4--;

                                        if (SeqNo4 < 0)
                                        {
                                            SeqNo4 = 0;
                                        }
                                        //else if (SeqNo4 >= 0)
                                        //{
                                        //    //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                        //    Console.WriteLine("start4 " + starttime4[SeqNo4]);

                                        //    newPosition4 = newPosition4 + duration4[SeqNo4];
                                        //    Initializevid4(null, null);
                                        //    mediaPos4.put_CurrentPosition(Math.Max(0, newPosition4));
                                        //}

                                        else if (SeqNo4 >= 0)
                                        {
                                            //splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            Console.WriteLine("start4 " + starttime4[SeqNo4]);

                                            newPosition4 = newPosition4 + duration4[SeqNo4];
                                            if (playState4 == PlayState.Paused || playState4 == PlayState.Stopped)
                                            {
                                                Init4(null, null);
                                            }

                                            //Initializevid1(null, null);
                                            mediaPos4.put_CurrentPosition(Math.Max(0, newPosition4));
                                        }
                                    }
                                }
                            }
                        }
                    }


                    if (cam5Count != 0)
                    {
                        if (mediaCtrl5 != null || mediaSeek5 != null)
                        {
                            if (playState5 == PlayState.Running)
                            {
                                mediaCtrl5.Pause();
                                playState5 = PlayState.Paused;
                            }

                            if (mediaPos5 != null)
                            {
                                mediaPos5.get_CurrentPosition(out currentPosition5);
                                newPosition5 = currentPosition5 - 1;

                                if (starttime5[SeqNo5].TotalSeconds <= starttime5[SeqNo5].TotalSeconds + newPosition5)
                                {
                                    mediaPos5.put_CurrentPosition(Math.Max(0, newPosition5));
                                }
                                else if (starttime5[SeqNo5].TotalSeconds + newPosition5 < starttime5[SeqNo5].TotalSeconds)
                                {
                                    if (mediaCtrl5 != null || mediaSeek5 != null)
                                    {
                                        mediaPos5.put_CurrentPosition(0.0);
                                        mediaCtrl5.Stop();
                                        clipFile5 = null;
                                        clipType5 = ClipType.None;
                                        CloseInterfaces5();
                                        SeqNo5--;

                                        if (SeqNo5 < 0)
                                        {
                                            SeqNo5 = 0;
                                        }
                                        //else if (SeqNo4 >= 0)
                                        //{
                                        //    //splitContainer10.Panel2.BackgroundImage = mtvplayer.Properties.Resources.loadVid1;
                                        //    Console.WriteLine("start4 " + starttime4[SeqNo4]);

                                        //    newPosition4 = newPosition4 + duration4[SeqNo4];
                                        //    Initializevid4(null, null);
                                        //    mediaPos4.put_CurrentPosition(Math.Max(0, newPosition4));
                                        //}

                                        else if (SeqNo5 >= 0)
                                        {
                                            //splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.loadVid;
                                            Console.WriteLine("start5 " + starttime5[SeqNo5]);

                                            newPosition5 = newPosition5 + duration5[SeqNo5];
                                            if (playState5 == PlayState.Paused || playState5 == PlayState.Stopped)
                                            {
                                                Init5(null, null);
                                            }

                                            //Initializevid1(null, null);
                                            mediaPos5.put_CurrentPosition(Math.Max(0, newPosition5));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (cam6Count != 0)
                    {
                        if (mediaCtrl6 != null || mediaSeek6 != null)
                        {
                            if (playState6 == PlayState.Running)
                            {
                                mediaCtrl6.Pause();
                                playState6 = PlayState.Paused;
                            }

                            if (mediaPos6 != null)
                            {
                                mediaPos6.get_CurrentPosition(out currentPosition6);
                                newPosition6 = currentPosition6 - 1;

                                if (starttime6[SeqNo6].TotalSeconds <= starttime6[SeqNo6].TotalSeconds + newPosition6)
                                {
                                    mediaPos6.put_CurrentPosition(Math.Max(0, newPosition6));
                                }
                                else if (starttime6[SeqNo6].TotalSeconds + newPosition6 < starttime6[SeqNo6].TotalSeconds)
                                {
                                    if (mediaCtrl6 != null || mediaSeek6 != null)
                                    {
                                        mediaPos6.put_CurrentPosition(0.0);
                                        mediaCtrl6.Stop();
                                        clipFile6 = null;
                                        clipType6 = ClipType.None;
                                        CloseInterfaces6();
                                        SeqNo6--;

                                        if (SeqNo6 < 0)
                                        {
                                            SeqNo6 = 0;
                                        }

                                        else if (SeqNo6 >= 0)
                                        {
                                            Console.WriteLine("start6 " + starttime6[SeqNo6]);

                                            newPosition6 = newPosition6 + duration6[SeqNo6];
                                            if (playState6 == PlayState.Paused || playState6 == PlayState.Stopped)
                                            {
                                                Init6(null, null);
                                            }

                                            mediaPos6.put_CurrentPosition(Math.Max(0, newPosition6));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (cam7Count != 0)
                    {
                        if (mediaCtrl7 != null || mediaSeek7 != null)
                        {
                            if (playState7 == PlayState.Running)
                            {
                                mediaCtrl7.Pause();
                                playState7 = PlayState.Paused;
                            }

                            if (mediaPos7 != null)
                            {
                                mediaPos7.get_CurrentPosition(out currentPosition7);
                                newPosition7 = currentPosition7 - 1;

                                if (starttime7[SeqNo7].TotalSeconds <= starttime7[SeqNo7].TotalSeconds + newPosition7)
                                {
                                    mediaPos7.put_CurrentPosition(Math.Max(0, newPosition7));
                                }
                                else if (starttime7[SeqNo7].TotalSeconds + newPosition7 < starttime7[SeqNo7].TotalSeconds)
                                {
                                    if (mediaCtrl7 != null || mediaSeek7 != null)
                                    {
                                        mediaPos7.put_CurrentPosition(0.0);
                                        mediaCtrl7.Stop();
                                        clipFile7 = null;
                                        clipType7 = ClipType.None;
                                        CloseInterfaces7();
                                        SeqNo7--;

                                        if (SeqNo7 < 0)
                                        {
                                            SeqNo7 = 0;
                                        }

                                        else if (SeqNo7 >= 0)
                                        {
                                            Console.WriteLine("start7 " + starttime7[SeqNo7]);

                                            newPosition7 = newPosition7 + duration7[SeqNo7];
                                            if (playState7 == PlayState.Paused || playState7 == PlayState.Stopped)
                                            {
                                                Init7(null, null);
                                            }
                                            mediaPos7.put_CurrentPosition(Math.Max(0, newPosition7));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (cam8Count != 0)
                    {
                        if (mediaCtrl8 != null || mediaSeek8 != null)
                        {
                            if (playState8 == PlayState.Running)
                            {
                                mediaCtrl8.Pause();
                                playState8 = PlayState.Paused;
                            }

                            if (mediaPos8 != null)
                            {
                                mediaPos8.get_CurrentPosition(out currentPosition8);
                                newPosition8 = currentPosition8 - 1;

                                if (starttime8[SeqNo8].TotalSeconds <= starttime8[SeqNo8].TotalSeconds + newPosition8)
                                {
                                    mediaPos8.put_CurrentPosition(Math.Max(0, newPosition8));
                                }
                                else if (starttime8[SeqNo8].TotalSeconds + newPosition8 < starttime8[SeqNo8].TotalSeconds)
                                {
                                    if (mediaCtrl8 != null || mediaSeek8 != null)
                                    {
                                        mediaPos8.put_CurrentPosition(0.0);
                                        mediaCtrl8.Stop();
                                        clipFile8 = null;
                                        clipType8 = ClipType.None;
                                        CloseInterfaces8();
                                        SeqNo8--;

                                        if (SeqNo8 < 0)
                                        {
                                            SeqNo8 = 0;
                                        }
                                        else if (SeqNo8 >= 0)
                                        {
                                            Console.WriteLine("start8 " + starttime8[SeqNo8]);

                                            newPosition8 = newPosition8 + duration8[SeqNo8];
                                            if (playState8 == PlayState.Paused || playState8 == PlayState.Stopped)
                                            {
                                                Init8(null, null);
                                            }

                                            mediaPos8.put_CurrentPosition(Math.Max(0, newPosition8));
                                        }
                                    }
                                }
                            }
                        }
                    }


                }
            }
        }

        /// <summary> get video screen back to 4 view channel. </summary>
        private void btAll4_Click(object sender, EventArgs e)
        {
            countCh = 0;
            btSnapshots.Location = new Point(1, 3);
            btOpenVideo.Location = new Point(35, 3);
            btRecStop.Visible = false;
            btRec.Visible = false;

            if (splitContainer9.Panel2Collapsed)
            {
                splitContainer9.Panel2Collapsed = false;

                btcam1.Enabled = true;
                btcam2.Enabled = true;
                btCam3.Enabled = true;
                btCam4.Enabled = true;
                btCam5.Enabled = true;
            }
            if (splitContainer8.Panel2Collapsed)
            {
                splitContainer8.Panel2Collapsed = false;
            }
            if (splitContainer3.Panel2Collapsed)
            {
                splitContainer3.Panel2Collapsed = false;
            }
            if (splitContainer6.Panel2Collapsed)
            {
                splitContainer6.Panel2Collapsed = false;
            }
            ResizeVideoWindow();

            if (splitContainer10.Panel2Collapsed)
            {
                splitContainer10.Panel2Collapsed = false;

                btcam1.Enabled = true;
                btcam2.Enabled = true;
                btCam3.Enabled = true;
                btCam4.Enabled = true;
                btCam5.Enabled = true;
            }
            if (splitContainer8.Panel1Collapsed)
            {
                splitContainer8.Panel1Collapsed = false;
            }
            if (splitContainer3.Panel2Collapsed)
            {
                splitContainer3.Panel2Collapsed = false;
            }
            ResizeVideoWindow2();

            if (splitContainer9.Panel1Collapsed)
            {
                splitContainer9.Panel1Collapsed = false;

                btcam1.Enabled = true;
                btcam2.Enabled = true;
                btCam3.Enabled = true;
                btCam4.Enabled = true;
                btCam5.Enabled = true;
            }
            if (splitContainer8.Panel2Collapsed)
            {
                splitContainer8.Panel2Collapsed = false;
            }
            if (splitContainer3.Panel2Collapsed)
            {
                splitContainer3.Panel2Collapsed = false;
            }
            ResizeVideoWindow3();

            if (splitContainer10.Panel1Collapsed)
            {
                splitContainer10.Panel1Collapsed = false;

                btcam1.Enabled = true;
                btcam2.Enabled = true;
                btCam3.Enabled = true;
                btCam4.Enabled = true;
                btCam5.Enabled = true;
            }
            if (splitContainer8.Panel1Collapsed)
            {
                splitContainer8.Panel1Collapsed = false;
            }
            if (splitContainer3.Panel2Collapsed)
            {
                splitContainer3.Panel2Collapsed = false;
            }
            ResizeVideoWindow4();

            if (splitContainer4.Panel2Collapsed)
            {
                splitContainer4.Panel2Collapsed = false;

                btcam1.Enabled = true;
                btcam2.Enabled = true;
                btCam3.Enabled = true;
                btCam4.Enabled = true;
                btCam5.Enabled = true;
            }
            if (splitContainer3.Panel1Collapsed)
            {
                splitContainer3.Panel1Collapsed = false;
            }
            ResizeVideoWindow5();
        }

        public void mediactrl()
        {
            playState1 = PlayState.Stopped;
            playState2 = PlayState.Stopped;
            playState3 = PlayState.Stopped;
            playState4 = PlayState.Stopped;
            playState5 = PlayState.Stopped;
        }

        /// <summary> play video in normal mode. </summary>
        public void btPlay_Click(object sender, EventArgs e)
        {
            splitContainer9.Panel1.BackgroundImage = null;
            splitContainer9.Panel2.BackgroundImage = null;
            splitContainer10.Panel1.BackgroundImage = null;
            splitContainer4.Panel2.BackgroundImage = null;
            splitContainer4.Panel1.BackgroundImage = null;
            splitContainer6.Panel2.BackgroundImage = null;
            splitContainer7.Panel2.BackgroundImage = null;
            splitContainer11.Panel2.BackgroundImage = null;

            backwardTimer.Stop();
            if (playmode == "single")
            {
                btcam1.Enabled = false;
                btcam2.Enabled = false;
                btCam3.Enabled = false;
                btCam4.Enabled = false;
                btCam5.Enabled = false;

                if (mediaCtrl1 != null || mediaSeek1 != null)
                    if (playState1 == PlayState.Paused || playState1 == PlayState.Stopped)
                    {
                        if (mediaCtrl1.Run() == 0)
                            playState1 = PlayState.Running;
                    }

                if (playState1 == PlayState.Running)
                {
                    SetRate(1);
                }

                timer1.Interval = 1000;
                timer1.Start();
            }
            else if (playmode == "multiple")
            {
                if (cam1Count != 0)
                {
                    if (mediaCtrl1 != null || mediaSeek1 != null)
                        if (playState1 == PlayState.Paused)
                        {
                            if (mediaPos1 == null)
                                splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.waiting;

                            if (mediaCtrl1.Run() >= 0)
                            {
                                playState1 = PlayState.Running;
                                SetRate(1);
                            }
                        }
                        else if (playState1 == PlayState.Running)
                        {
                            SetRate(1);
                        }
                }

                if (cam2Count != 0)
                {
                    if (mediaCtrl2 != null || mediaSeek2 != null)
                        if (playState2 == PlayState.Paused)
                        {
                            if (mediaPos2 == null)
                                splitContainer10.Panel1.BackgroundImage = mtvplayer.Properties.Resources.waiting;

                            if (mediaCtrl2.Run() >= 0)
                            {
                                playState2 = PlayState.Running;
                                SetRate(1);
                            }
                        }
                        else if (playState2 == PlayState.Running)
                        {
                            SetRate(1);
                        }
                }

                if (cam3Count != 0)
                {
                    if (mediaCtrl3 != null || mediaSeek3 != null)
                        if (playState3 == PlayState.Paused)
                        {
                            if (mediaPos3 == null)
                                splitContainer9.Panel2.BackgroundImage = mtvplayer.Properties.Resources.waiting;

                            if (mediaCtrl3.Run() >= 0)
                            {
                                playState3 = PlayState.Running;
                                SetRate(1);
                            }
                        }
                        else if (playState3 == PlayState.Running)
                        {
                            SetRate(1);
                        }
                }

                if (cam4Count != 0)
                {
                    if (mediaCtrl4 != null || mediaSeek4 != null)
                        if (playState4 == PlayState.Paused)
                        {
                            if (mediaPos4 == null)
                                splitContainer4.Panel2.BackgroundImage = mtvplayer.Properties.Resources.waiting;

                            if (mediaCtrl4.Run() >= 0)
                            {
                                playState4 = PlayState.Running;
                                SetRate(1);
                            }
                        }
                        else if (playState4 == PlayState.Running)
                        {
                            SetRate(1);
                        }
                }

                if (cam5Count != 0)
                {
                    if (mediaCtrl5 != null || mediaSeek5 != null)
                        if (playState5 == PlayState.Paused)
                        {
                            if (mediaPos5 == null)
                                splitContainer4.Panel1.BackgroundImage = mtvplayer.Properties.Resources.waiting;

                            if (mediaCtrl5.Run() >= 0)
                            {
                                playState5 = PlayState.Running;
                                SetRate(1);
                            }
                        }
                        else if (playState5 == PlayState.Running)
                        {
                            SetRate(1);
                        }
                }

                if (cam6Count != 0)
                {
                    if (mediaCtrl6 != null || mediaSeek6 != null)
                        if (playState6 == PlayState.Paused)
                        {
                            if (mediaPos6 == null)
                                splitContainer6.Panel2.BackgroundImage = mtvplayer.Properties.Resources.waiting;

                            if (mediaCtrl6.Run() >= 0)
                            {
                                playState6 = PlayState.Running;
                                SetRate(1);
                            }
                        }
                        else if (playState6 == PlayState.Running)
                        {
                            SetRate(1);
                        }
                }
                if (cam7Count != 0)
                {
                    if (mediaCtrl7 != null || mediaSeek7 != null)
                        if (playState7 == PlayState.Paused)
                        {
                            if (mediaPos7 == null)
                                splitContainer4.Panel1.BackgroundImage = mtvplayer.Properties.Resources.waiting;

                            if (mediaCtrl7.Run() >= 0)
                            {
                                playState7 = PlayState.Running;
                                SetRate(1);
                            }
                        }
                        else if (playState7 == PlayState.Running)
                        {
                            SetRate(1);
                        }
                }

                if (cam8Count != 0)
                {
                    if (mediaCtrl8 != null || mediaSeek8 != null)
                        if (playState8 == PlayState.Paused)
                        {
                            if (mediaPos8 == null)
                                splitContainer4.Panel1.BackgroundImage = mtvplayer.Properties.Resources.waiting;

                            if (mediaCtrl8.Run() >= 0)
                            {
                                playState8 = PlayState.Running;
                                SetRate(1);
                            }
                        }
                        else if (playState8 == PlayState.Running)
                        {
                            SetRate(1);
                        }
                }

                timer1.Interval = 1000;
            }

            if (!timer1.Enabled)
                timer1.Start();

            if (backwardTimer.Enabled)
                backwardTimer.Stop();

            btPlay.Checked = true;
            btPause.Checked = false;
            btFB.Checked = false;
            btFast4x.Checked = false;
            btFast8x.Checked = false;
            btFastBackward4x.Checked = false;
            btFastBackward8x.Checked = false;

            btRec.Enabled = true;
            if (!btRec.Enabled)
            {
                btRec.Enabled = false;
            }

            btSnapshots.Enabled = true;
            videoTrackBar.Enabled = true;

            btPlay.Enabled = false;
            btFB.Enabled = true;
            btFastBackward4x.Enabled = true;
            btFastBackward8x.Enabled = true;
            btFast4x.Enabled = true;
            btFast8x.Enabled = true;
            btPause.Enabled = true;

            this.currentVideoState = VideoState.Forwarding;
        }

        /// <summary> pause video. </summary>
        private void btPause_Click(object sender, EventArgs e)
        {
            btPlay.Checked = false;
            btFB.Checked = false;
            btFB.Checked = false;
            btFast4x.Checked = false;
            btFast8x.Checked = false;
            btFastBackward4x.Checked = false;
            btFastBackward8x.Checked = false;
            btPause.Checked = true;

            if (playmode == "single")
            {
                if (playState1 == PlayState.Running)
                {
                    if (mediaCtrl1.Pause() >= 0)
                        playState1 = PlayState.Paused;
                }
            }
            else if (playmode == "multiple")
            {
                if (cam1Count != 0)
                {
                    if (playState1 == PlayState.Running)
                    {
                        if (mediaCtrl1.Pause() >= 0)
                            playState1 = PlayState.Paused;
                    }
                }

                if (cam2Count != 0)
                {
                    if (playState2 == PlayState.Running)
                    {
                        if (mediaCtrl2.Pause() >= 0)
                            playState2 = PlayState.Paused;
                    }
                }

                if (cam3Count != 0)
                {
                    if (playState3 == PlayState.Running)
                    {
                        if (mediaCtrl3.Pause() >= 0)
                            playState3 = PlayState.Paused;
                    }
                }

                if (cam4Count != 0)
                {
                    if (playState4 == PlayState.Running)
                    {
                        if (mediaCtrl4.Pause() >= 0)
                            playState4 = PlayState.Paused;
                    }
                }
                if (cam5Count != 0)
                {
                    if (playState5 == PlayState.Running)
                    {
                        if (mediaCtrl5.Pause() >= 0)
                            playState5 = PlayState.Paused;
                    }
                }
                if (cam6Count != 0)
                {
                    if (playState6 == PlayState.Running)
                    {
                        if (mediaCtrl6.Pause() >= 0)
                            playState6 = PlayState.Paused;
                    }
                }
                if (cam7Count != 0)
                {
                    if (playState7 == PlayState.Running)
                    {
                        if (mediaCtrl7.Pause() >= 0)
                            playState7 = PlayState.Paused;
                    }
                }
                if (cam8Count != 0)
                {
                    if (playState8 == PlayState.Running)
                    {
                        if (mediaCtrl8.Pause() >= 0)
                            playState8 = PlayState.Paused;
                    }
                }

            }

            btPause.Enabled = false;

            //if (lbStartDur.Text != "00:00:00")
            if (lbStartDur.Text != fromTime.ToString())
            {
                btPlay.Enabled = true;
                btFast4x.Enabled = true;
                btFast8x.Enabled = true;
                btFB.Enabled = true;
                btFastBackward4x.Enabled = true;
                btFastBackward8x.Enabled = true;
            }

            if (timer1.Enabled)
                timer1.Stop();

            if (backwardTimer.Enabled)
                backwardTimer.Stop();
        }

        private void stops()
        {
            if (playmode == "single")
            {
                if (playState1 == PlayState.Running || playState1 == PlayState.Paused)
                {
                    if (mediaCtrl1.Pause() >= 0)
                        playState1 = PlayState.Stopped;
                }
            }
            else if (playmode == "multiple")
            {
                if (cam1Count > 0)
                {
                    if (playState1 == PlayState.Running || playState1 == PlayState.Paused)
                    {
                        if (mediaCtrl1.Pause() >= 0)
                            playState1 = PlayState.Stopped;
                    }
                }

                if (cam2Count != 0)
                {
                    if (playState2 == PlayState.Running || playState2 == PlayState.Paused)
                    {
                        if (mediaCtrl2.Pause() >= 0)
                            playState2 = PlayState.Stopped;
                    }
                }

                if (cam3Count > 0)
                {
                    if (playState3 == PlayState.Running || playState3 == PlayState.Paused)
                    {
                        if (mediaCtrl3.Pause() >= 0)
                            playState3 = PlayState.Stopped;
                    }
                }

                if (cam4Count > 0)
                {
                    if (playState4 == PlayState.Running || playState4 == PlayState.Stopped)
                    {
                        if (mediaCtrl4.Pause() >= 0)
                            playState4 = PlayState.Stopped;
                    }
                }

                if (cam5Count > 0)
                {
                    if (playState5 == PlayState.Running || playState5 == PlayState.Stopped)
                    {
                        if (mediaCtrl5.Pause() >= 0)
                            playState5 = PlayState.Stopped;
                    }
                }
                if (cam6Count != 0)
                {
                    if (playState6 == PlayState.Running)
                    {
                        if (mediaCtrl6.Pause() >= 0)
                            playState6 = PlayState.Paused;
                    }
                }
                if (cam7Count != 0)
                {
                    if (playState7 == PlayState.Running)
                    {
                        if (mediaCtrl7.Pause() >= 0)
                            playState7 = PlayState.Paused;
                    }
                }
                if (cam8Count != 0)
                {
                    if (playState8 == PlayState.Running)
                    {
                        if (mediaCtrl8.Pause() >= 0)
                            playState8 = PlayState.Paused;
                    }
                }

            }

            if (timer1.Enabled)
                timer1.Stop();
            if (backwardTimer.Enabled)
                backwardTimer.Stop();
        }

        /// <summary> fast backward button. </summary>
        private void btFB_Click(object sender, EventArgs e)
        {
            btPause_Click(sender, e);
            btPlay.Checked = false;
            btPause.Checked = false;
            btFast4x.Checked = false;
            btFast8x.Checked = false;
            btFastBackward4x.Checked = false;
            btFastBackward8x.Checked = false;

            btPlay.Enabled = true;
            btFB.Enabled = false;
            btFastBackward4x.Enabled = true;
            btFastBackward8x.Enabled = true;
            btFast4x.Enabled = true;
            btFast8x.Enabled = true;
            btPause.Enabled = true;

            backwardTimer.Interval = 1200;// / ((int)rate * 1);
            if (!backwardTimer.Enabled)
                backwardTimer.Start();

            timer1.Interval = 1000;
            timer1.Start();

            this.currentVideoState = VideoState.Backwards;
        }

        /// <summary> channel 1 full-screen toggle. </summary>
        private void btCH1_Click(object sender, EventArgs e)
        {
            countCh = 1;
            btRec.Visible = true;
            btRecStop.Visible = true;

            btSnapshots.Location = new Point(69, 3);
            btOpenVideo.Location = new Point(103, 3);

            if (!splitContainer9.Panel2Collapsed)
            {
                splitContainer9.Panel2Collapsed = true;
                btcam1.Enabled = true;
                btcam2.Enabled = false;
                btCam3.Enabled = false;
                btCam4.Enabled = false;
                btCam5.Enabled = false;
                btAll4.Enabled = true;
            }

            if (!splitContainer8.Panel2Collapsed)
            {
                splitContainer8.Panel2Collapsed = true;
            }

            if (!splitContainer3.Panel2Collapsed)
            {
                splitContainer3.Panel2Collapsed = true;
            }

            ResizeVideoWindow();
        }

        /// <summary> channel 2 full-screen toggle. </summary>
        private void btCH2_Click(object sender, EventArgs e)
        {
            countCh = 2;
            btRec.Visible = true;
            btRecStop.Visible = true;

            btSnapshots.Location = new Point(69, 3);
            btOpenVideo.Location = new Point(103, 3);

            if (!splitContainer10.Panel2Collapsed)
            {
                splitContainer10.Panel2Collapsed = true;
                btcam1.Enabled = false;
                btcam2.Enabled = true;
                btCam3.Enabled = false;
                btCam4.Enabled = false;
                btCam5.Enabled = false;
                btAll4.Enabled = true;
            }

            if (!splitContainer8.Panel1Collapsed)
            {
                splitContainer8.Panel1Collapsed = true;
            }
            if (!splitContainer3.Panel2Collapsed)
            {
                splitContainer3.Panel2Collapsed = true;
            }

            ResizeVideoWindow2();
        }

        /// <summary> channel 3 full-screen toggle. </summary>
        private void btCH3_Click(object sender, EventArgs e)
        {
            countCh = 3;
            btRec.Visible = true;
            btRecStop.Visible = true;

            btSnapshots.Location = new Point(69, 3);
            btOpenVideo.Location = new Point(103, 3);

            if (!splitContainer9.Panel1Collapsed)
            {
                splitContainer9.Panel1Collapsed = true;
                btcam1.Enabled = false;
                btcam2.Enabled = false;
                btCam3.Enabled = true;
                btCam4.Enabled = false;
                btCam5.Enabled = false;
                btAll4.Enabled = true;
            }

            if (!splitContainer8.Panel2Collapsed)
            {
                splitContainer8.Panel2Collapsed = true;
            }

            if (!splitContainer3.Panel2Collapsed)
            {
                splitContainer3.Panel2Collapsed = true;
            }

            ResizeVideoWindow3();
        }

        /// <summary> channel 4 full-screen toggle. </summary>
        private void btCH4_Click(object sender, EventArgs e)
        {
            countCh = 4;
            btRec.Visible = true;
            btRecStop.Visible = true;

            btSnapshots.Location = new Point(69, 3);
            btOpenVideo.Location = new Point(103, 3);

            if (!splitContainer10.Panel1Collapsed)
            {
                splitContainer10.Panel1Collapsed = true;
                btcam1.Enabled = false;
                btcam2.Enabled = false;
                btCam3.Enabled = false;
                btCam4.Enabled = true;
                btCam5.Enabled = false;
                btAll4.Enabled = true;
            }

            if (!splitContainer8.Panel1Collapsed)
            {
                splitContainer8.Panel1Collapsed = true;
            }
            if (!splitContainer3.Panel2Collapsed)
            {
                splitContainer3.Panel2Collapsed = true;
            }

            ResizeVideoWindow4();
        }

        private void btCam5_Click(object sender, EventArgs e)
        {
            countCh = 5;
            btRec.Visible = true;
            btRecStop.Visible = true;

            btSnapshots.Location = new Point(69, 3);
            btOpenVideo.Location = new Point(103, 3);

            if (!splitContainer4.Panel2Collapsed)
            {
                splitContainer4.Panel2Collapsed = true;
                btcam1.Enabled = false;
                btcam2.Enabled = false;
                btCam3.Enabled = false;
                btCam4.Enabled = false;
                btCam5.Enabled = true;
                btAll4.Enabled = true;
            }

            if (!splitContainer3.Panel1Collapsed)
            {
                splitContainer3.Panel1Collapsed = true;
            }

            if (!splitContainer8.Panel1Collapsed)
            {
                splitContainer8.Panel1Collapsed = true;
            }

            ResizeVideoWindow5();
        }

        /// <summary> action for trackbar while mouse down. </summary>
        private void videoTrackBar_MouseDown(object sender, MouseEventArgs e)
        {

        }

        /// <summary> action for trackbar while mouse up. </summary>
        private void videoTrackBar_MouseUp(object sender, MouseEventArgs e)
        {
            Console.WriteLine(" ===> mouse is up ");
            if (!btPause.Checked)
            {
                Console.WriteLine(" ===> btPause.Checked ");
                if (currentVideoState == VideoState.Backwards || currentVideoState == VideoState.FastBackwards4x || currentVideoState == VideoState.FastBackwards8x)
                {
                    backwardTimer.Start();
                }
                else if (currentVideoState == VideoState.Forwarding)
                {
                    timer1.Start();
                }

                if (playState1 == PlayState.Paused)
                {
                    if (mediaCtrl1.Run() >= 0)
                        playState1 = PlayState.Running;
                }
                if (playState2 == PlayState.Paused)
                {
                    if (mediaCtrl2.Run() >= 0)
                        playState2 = PlayState.Running;
                }
                if (playState3 == PlayState.Paused)
                {
                    if (mediaCtrl3.Run() >= 0)
                        playState3 = PlayState.Running;
                }
                if (playState4 == PlayState.Paused)
                {
                    if (mediaCtrl4.Run() >= 0)
                        playState4 = PlayState.Running;
                }
                if (playState5 == PlayState.Paused)
                {
                    if (mediaCtrl5.Run() >= 0)
                        playState5 = PlayState.Running;
                }
            }
        }

        /// <summary> create video snapshot, this function can be used for 4 view channel or single view channel. </summary>
        private void btSnapshots_Click(object sender, EventArgs e)
        {
            DialogResult dr;
            if (playState1 != PlayState.Paused || playState2 != PlayState.Paused || playState3 != PlayState.Paused || playState4 != PlayState.Paused
                || playState5 != PlayState.Paused || playState6 != PlayState.Paused || playState7 != PlayState.Paused || playState8 != PlayState.Paused)
            {
                btPause_Click(sender, e);
            }

            if (!string.IsNullOrEmpty(Properties.Settings.Default.snapPath))
            {
                Console.WriteLine("That path is exists.");
                browsePath.SelectedPath = Properties.Settings.Default.snapPath;
                browsePath.ShowNewFolderButton = false;
                dr = browsePath.ShowDialog();
            }
            else
            {
                dr = browsePath.ShowDialog();
            }

            if (dr == DialogResult.OK)
            {
                snapPath = browsePath.SelectedPath;
                Properties.Settings.Default.snapPath = browsePath.SelectedPath;

                pLoad = new progressLoad();
                doCapture.RunWorkerAsync();
                pLoad.Show(this);
            }
            else
            {
                Properties.Settings.Default.snapPath = browsePath.SelectedPath;

                if (currentVideoState == VideoState.Forwarding)
                {
                    btPlay_Click(sender, e);
                }
                else if (currentVideoState == VideoState.Forwarding4x)
                {
                    btFast4x_Click(sender, e);
                }
                else if (currentVideoState == VideoState.Forwarding8x)
                {
                    btFast8x_Click(sender, e);
                }
                else if (currentVideoState == VideoState.Backwards)
                {
                    btFB_Click(sender, e);
                }
                else if (currentVideoState == VideoState.FastBackwards4x)
                {
                    btFastBackward4x_Click(sender, e);
                }
                else if (currentVideoState == VideoState.FastBackwards8x)
                {
                    btFastBackward8x_Click(sender, e);
                }
            }
        }

        /// <summary> snapshot function for channel 1. </summary>
        void capture1(string time)
        {
            //string relativeFolder = Path.Combine(pathdirect, "Maestronic");
            //string snapTarget = Path.Combine(relativeFolder, "snapshot");
            string snapTarget = snapPath;

            string name = "capture_ch1_" + snapTimeTB;
            int fileCount = 0;
            string name1 = "";

            if (mediaPos1 != null)
            {
                mediaPos1.get_CurrentPosition(out snaptime);
                snap = TimeSpan.Parse(CalculateTime(snaptime));

                if (playmode == "single")
                {
                    pathSnap1 = pathDir;
                    name1 = Path.GetFileName(Path.GetDirectoryName(pathDir));
                    name = name + "_" + name1;
                }
                else if (playmode == "multiple")
                {
                    pathSnap1 = Path.Combine(numberPath, cam1[SeqNo1]);
                    name1 = Path.GetDirectoryName(cam1[SeqNo1]);
                    name = name + "_" + name1;
                }

                try
                {
                    if (!Directory.Exists(snapTarget))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(snapTarget);
                        Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(snapTarget));
                    }
                }
                catch (Exception et)
                {
                    Console.WriteLine("The process failed: {0}", et.ToString());
                }

                try
                {
                    if (File.Exists(Path.Combine(snapTarget, name) + ".png"))
                    {
                        do
                        {
                            fileCount++;
                        }
                        while (File.Exists(Path.Combine(snapTarget, name) + (fileCount > 0 ? "_(" + fileCount.ToString() + ").png" : "")));

                        name = name + "_(" + (fileCount).ToString() + ")";
                        Console.WriteLine(File.Exists(Path.Combine(snapTarget, name) + ".png") ? "File exists." : "File does not exist.");
                    }

                    string snapFile = " -i \"" + pathSnap1 + "\" -ss " + snap + " -f image2 -vframes 1 " + "\"" + snapTarget + "\\" + name + ".png\"";
                    Console.WriteLine(snapFile);
                    listCapture.Add(snapFile);
                    listCapture.Sort();
                }
                catch
                {
                    // Log error.
                }
            }

            else if (mediaPos1 == null)
            {
                string folderpath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                folderpath = Path.GetDirectoryName(folderpath);
                string sourceFile = Path.Combine(folderpath, "thirdParty\\noVidg.png");

                TimeSpan times = TimeSpan.Parse(lbStartDur.Text);
                if (Convert.ToInt32(times.TotalSeconds) > 86400)
                {
                    name1 = dateFrom + 1;
                }
                else
                {
                    name1 = dateFrom;
                }
                name = name + "_" + name1;

                if (File.Exists(Path.Combine(snapTarget, name) + ".png"))
                {
                    do
                    {
                        fileCount++;
                    }
                    while (File.Exists(Path.Combine(snapTarget, name) + (fileCount > 0 ? "_(" + fileCount.ToString() + ").png" : "")));

                    name = name + "_(" + (fileCount).ToString() + ")";
                    Console.WriteLine(File.Exists(Path.Combine(snapTarget, name) + ".png") ? "File exists." : "File does not exist.");
                }

                if (!Directory.Exists(snapTarget))
                {
                    DirectoryInfo di = Directory.CreateDirectory(snapTarget);
                    Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(snapTarget));
                }

                string destFile = Path.Combine(snapTarget, name + ".png");
                File.Copy(@sourceFile, @destFile);
                File.SetLastWriteTime(@destFile, DateTime.Now);
            }
        }

        /// <summary> snapshot function for channel 2. </summary>
        void capture2(string time)
        {
            //string relativeFolder = Path.Combine(pathdirect, "Maestronic");
            //string snapTarget = Path.Combine(relativeFolder, "snapshot");
            string snapTarget = snapPath;

            string name = "capture_ch2_" + snapTimeTB;
            int fileCount = 0;
            string name2 = "";

            if (mediaPos2 != null)
            {
                mediaPos2.get_CurrentPosition(out snaptime);
                snap = TimeSpan.Parse(CalculateTime(snaptime));

                pathSnap2 = Path.Combine(numberPath, cam2[SeqNo2]);
                name2 = Path.GetDirectoryName(cam2[SeqNo2]);
                name = name + "_" + name2;

                try
                {
                    if (!Directory.Exists(snapTarget))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(snapTarget);
                        Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(snapTarget));
                    }
                }
                catch (Exception et)
                {
                    Console.WriteLine("The process failed: {0}", et.ToString());
                }

                try
                {
                    if (File.Exists(Path.Combine(snapTarget, name) + ".png"))
                    {
                        do
                        {
                            fileCount++;
                        }
                        while (File.Exists(Path.Combine(snapTarget, name) + (fileCount > 0 ? "_(" + fileCount.ToString() + ").png" : "")));

                        name = name + "_(" + (fileCount).ToString() + ")";
                        Console.WriteLine(File.Exists(Path.Combine(snapTarget, name) + ".png") ? "File exists." : "File does not exist.");
                    }

                    string snapFile = " -i \"" + pathSnap2 + "\" -ss " + snap + " -f image2 -vframes 1 " + "\"" + snapTarget + "\\" + name + ".png\"";
                    listCapture.Add(snapFile);
                    listCapture.Sort();
                }
                catch
                {
                    // Log error.
                }
            }
            else if (mediaPos2 == null)
            {
                string folderpath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                folderpath = Path.GetDirectoryName(folderpath);
                string sourceFile = Path.Combine(folderpath, "thirdParty\\noVidbg.png");

                TimeSpan times = TimeSpan.Parse(lbStartDur.Text);
                if (Convert.ToInt32(times.TotalSeconds) > 86400)
                {
                    name2 = dateFrom + 1;
                }
                else
                {
                    name2 = dateFrom;
                }
                name = name + "_" + name2;

                if (File.Exists(Path.Combine(snapTarget, name) + ".png"))
                {
                    do
                    {
                        fileCount++;
                    }
                    while (File.Exists(Path.Combine(snapTarget, name) + (fileCount > 0 ? "_(" + fileCount.ToString() + ").png" : "")));

                    name = name + "_(" + (fileCount).ToString() + ")";
                    Console.WriteLine(File.Exists(Path.Combine(snapTarget, name) + ".png") ? "File exists." : "File does not exist.");
                }

                if (!Directory.Exists(snapTarget))
                {
                    DirectoryInfo di = Directory.CreateDirectory(snapTarget);
                    Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(snapTarget));
                }

                string destFile = Path.Combine(snapTarget, name + ".png");
                File.Copy(@sourceFile, @destFile);
                File.SetLastWriteTime(@destFile, DateTime.Now);
            }
        }

        /// <summary> snapshot function for channel 3. </summary>
        void capture3(string time)
        {
            //string relativeFolder = Path.Combine(pathdirect, "Maestronic");
            //string snapTarget = Path.Combine(relativeFolder, "snapshot");
            string snapTarget = snapPath;

            string name = "capture_ch3_" + snapTimeTB;
            int fileCount = 0;
            string name3 = "";

            if (mediaPos3 != null)
            {
                mediaPos3.get_CurrentPosition(out snaptime);
                snap = TimeSpan.Parse(CalculateTime(snaptime));

                pathSnap3 = Path.Combine(numberPath, cam3[SeqNo3]);
                name3 = Path.GetDirectoryName(cam3[SeqNo3]);
                name = name + "_" + name3;

                try
                {
                    if (!Directory.Exists(snapTarget))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(snapTarget);
                        Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(snapTarget));
                    }
                }
                catch (Exception et)
                {
                    Console.WriteLine("The process failed: {0}", et.ToString());
                }

                try
                {
                    if (File.Exists(Path.Combine(snapTarget, name) + ".png"))
                    {
                        do
                        {
                            fileCount++;
                        }
                        while (File.Exists(Path.Combine(snapTarget, name) + (fileCount > 0 ? "_(" + fileCount.ToString() + ").png" : "")));

                        name = name + "_(" + (fileCount).ToString() + ")";
                        Console.WriteLine(File.Exists(Path.Combine(snapTarget, name) + ".png") ? "File exists." : "File does not exist.");
                    }

                    string snapFile = " -i \"" + pathSnap3 + "\" -ss " + snap + " -f image2 -vframes 1 " + "\"" + snapTarget + "\\" + name + ".png\"";
                    listCapture.Add(snapFile);
                    listCapture.Sort();
                }
                catch
                {
                    // Log error.
                }
            }
            else if (mediaPos3 == null)
            {
                string folderpath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                folderpath = Path.GetDirectoryName(folderpath);
                string sourceFile = Path.Combine(folderpath, "thirdParty\\noVidbg.png");

                TimeSpan times = TimeSpan.Parse(lbStartDur.Text);
                if (Convert.ToInt32(times.TotalSeconds) > 86400)
                {
                    name3 = dateFrom + 1;
                }
                else
                {
                    name3 = dateFrom;
                }
                name = name + "_" + name3;

                if (File.Exists(Path.Combine(snapTarget, name) + ".png"))
                {
                    do
                    {
                        fileCount++;
                    }
                    while (File.Exists(Path.Combine(snapTarget, name) + (fileCount > 0 ? "_(" + fileCount.ToString() + ").png" : "")));

                    name = name + "_(" + (fileCount).ToString() + ")";
                    Console.WriteLine(File.Exists(Path.Combine(snapTarget, name) + ".png") ? "File exists." : "File does not exist.");
                }

                if (!Directory.Exists(snapTarget))
                {
                    DirectoryInfo di = Directory.CreateDirectory(snapTarget);
                    Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(snapTarget));
                }

                string destFile = Path.Combine(snapTarget, name + ".png");
                File.Copy(@sourceFile, @destFile);
                File.SetLastWriteTime(@destFile, DateTime.Now);
            }
        }

        /// <summary> snapshot function for channel 4. </summary>
        void capture4(string time)
        {
            //string relativeFolder = Path.Combine(pathdirect, "Maestronic");
            //string snapTarget = Path.Combine(relativeFolder, "snapshot");
            string snapTarget = snapPath;

            string name = "capture_ch4_" + snapTimeTB;
            int fileCount = 0;
            string name4 = "";

            if (mediaPos4 != null)
            {
                mediaPos4.get_CurrentPosition(out snaptime);
                snap = TimeSpan.Parse(CalculateTime(snaptime));

                pathSnap4 = Path.Combine(numberPath, cam4[SeqNo4]);
                name4 = Path.GetDirectoryName(cam4[SeqNo4]);
                name = name + "_" + name4;

                try
                {
                    if (!Directory.Exists(snapTarget))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(snapTarget);
                        Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(snapTarget));
                    }
                }
                catch (Exception et)
                {
                    Console.WriteLine("The process failed: {0}", et.ToString());
                }

                try
                {
                    if (File.Exists(Path.Combine(snapTarget, name) + ".png"))
                    {
                        do
                        {
                            fileCount++;
                        }
                        while (File.Exists(Path.Combine(snapTarget, name) + (fileCount > 0 ? "_(" + fileCount.ToString() + ").png" : "")));

                        name = name + "_(" + (fileCount).ToString() + ")";
                        Console.WriteLine(File.Exists(Path.Combine(snapTarget, name) + ".png") ? "File exists." : "File does not exist.");
                    }

                    string snapFile = " -i \"" + pathSnap4 + "\" -ss " + snap + " -f image2 -vframes 1 " + "\"" + snapTarget + "\\" + name + ".png\"";
                    listCapture.Add(snapFile);
                    listCapture.Sort();
                }
                catch
                {
                    // Log error.
                }
            }
            else if (mediaPos4 == null)
            {
                string folderpath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                folderpath = Path.GetDirectoryName(folderpath);
                string sourceFile = Path.Combine(folderpath, "thirdParty\\noVidbg.png");

                TimeSpan times = TimeSpan.Parse(lbStartDur.Text);
                if (Convert.ToInt32(times.TotalSeconds) > 86400)
                {
                    name4 = dateFrom + 1;
                }
                else
                {
                    name4 = dateFrom;
                }
                name = name + "_" + name4;

                if (File.Exists(Path.Combine(snapTarget, name) + ".png"))
                {
                    do
                    {
                        fileCount++;
                    }
                    while (File.Exists(Path.Combine(snapTarget, name) + (fileCount > 0 ? "_(" + fileCount.ToString() + ").png" : "")));

                    name = name + "_(" + (fileCount).ToString() + ")";
                    Console.WriteLine(File.Exists(Path.Combine(snapTarget, name) + ".png") ? "File exists." : "File does not exist.");
                }

                if (!Directory.Exists(snapTarget))
                {
                    DirectoryInfo di = Directory.CreateDirectory(snapTarget);
                    Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(snapTarget));
                }

                string destFile = Path.Combine(snapTarget, name + ".png");
                File.Copy(@sourceFile, @destFile);
                File.SetLastWriteTime(@destFile, DateTime.Now);
            }
        }


        /// <summary> snapshot function for channel 5. </summary>
        void capture5(string time)
        {
            //string relativeFolder = Path.Combine(pathdirect, "Maestronic");
            //string snapTarget = Path.Combine(relativeFolder, "snapshot");
            string snapTarget = snapPath;

            string name = "capture_ch5_" + snapTimeTB;
            int fileCount = 0;
            string name5 = "";

            if (mediaPos5 != null)
            {
                mediaPos5.get_CurrentPosition(out snaptime);
                snap = TimeSpan.Parse(CalculateTime(snaptime));

                pathSnap5 = Path.Combine(numberPath, cam5[SeqNo5]);
                name5 = Path.GetDirectoryName(cam5[SeqNo5]);
                name = name + "_" + name5;

                try
                {
                    if (!Directory.Exists(snapTarget))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(snapTarget);
                        Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(snapTarget));
                    }
                }
                catch (Exception et)
                {
                    Console.WriteLine("The process failed: {0}", et.ToString());
                }

                try
                {
                    if (File.Exists(Path.Combine(snapTarget, name) + ".png"))
                    {
                        do
                        {
                            fileCount++;
                        }
                        while (File.Exists(Path.Combine(snapTarget, name) + (fileCount > 0 ? "_(" + fileCount.ToString() + ").png" : "")));

                        name = name + "_(" + (fileCount).ToString() + ")";
                        Console.WriteLine(File.Exists(Path.Combine(snapTarget, name) + ".png") ? "File exists." : "File does not exist.");
                    }

                    string snapFile = " -i \"" + pathSnap5 + "\" -ss " + snap + " -f image2 -vframes 1 " + "\"" + snapTarget + "\\" + name + ".png\"";
                    listCapture.Add(snapFile);
                    listCapture.Sort();
                }
                catch
                {
                    // Log error.
                }
            }
            else if (mediaPos5 == null)
            {
                string folderpath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                folderpath = Path.GetDirectoryName(folderpath);
                string sourceFile = Path.Combine(folderpath, "thirdParty\\noVidbg.png");

                TimeSpan times = TimeSpan.Parse(lbStartDur.Text);
                if (Convert.ToInt32(times.TotalSeconds) > 86400)
                {
                    name5 = dateFrom + 1;
                }
                else
                {
                    name5 = dateFrom;
                }
                name = name + "_" + name5;

                if (File.Exists(Path.Combine(snapTarget, name) + ".png"))
                {
                    do
                    {
                        fileCount++;
                    }
                    while (File.Exists(Path.Combine(snapTarget, name) + (fileCount > 0 ? "_(" + fileCount.ToString() + ").png" : "")));

                    name = name + "_(" + (fileCount).ToString() + ")";
                    Console.WriteLine(File.Exists(Path.Combine(snapTarget, name) + ".png") ? "File exists." : "File does not exist.");
                }

                if (!Directory.Exists(snapTarget))
                {
                    DirectoryInfo di = Directory.CreateDirectory(snapTarget);
                    Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(snapTarget));
                }

                string destFile = Path.Combine(snapTarget, name + ".png");
                File.Copy(@sourceFile, @destFile);
                File.SetLastWriteTime(@destFile, DateTime.Now);
            }
        }



        /// <summary> starting video recording, set start state for video recording. </summary>
        private void btRec_Click(object sender, EventArgs e)
        {
            string time = CalculateTime(ticky);
            Console.WriteLine("countCh " + countCh);
            strDurName = TimeSpan.Parse(time);
            strTimeTB = lbStartDur.Text.Replace(":", string.Empty);

            if (countCh == 1 && playmode == "single")
            {
                pathrec1a = pathDir;
                strSeq1 = SeqNo1;
                Console.WriteLine("seqNo " + SeqNo1);
                mediaPos1.get_CurrentPosition(out startDuration);
                strRec1a = TimeSpan.Parse(CalculateTime(startDuration));

                IsEmpty = false;
                Console.WriteLine("pathrec1 " + pathrec1a);
                Console.WriteLine("time " + startDuration);
                Console.WriteLine("time calculate " + strRec1a);
            }
            else if (countCh == 1)
            {
                if (cam1.Count() > 0 && SeqNo1 < cam1.Count())
                {
                    if (fromTime.Add(TimeSpan.Parse(time)) > starttime1[SeqNo1] && fromTime.Add(TimeSpan.Parse(time)) <= starttime1[SeqNo1].Add(TimeSpan.FromSeconds(duration1[SeqNo1])))
                    {
                        pathrec1a = Path.Combine(numberPath, cam1[SeqNo1]);
                        strSeq1 = SeqNo1;
                        Console.WriteLine("seqNo " + SeqNo1);
                        mediaPos1.get_CurrentPosition(out startDuration);
                        strRec1a = TimeSpan.Parse(CalculateTime(startDuration));

                        IsEmpty = false;
                        Console.WriteLine("pathrec1 " + pathrec1a);
                        Console.WriteLine("time " + startDuration);
                        Console.WriteLine("time calculate " + strRec1a);
                    }
                    else
                    {
                        IsEmpty = true;
                        strRec1a = TimeSpan.Parse(time);
                    }
                }
            }
            else if (countCh == 2)
            {
                if (cam2.Count() > 0 && SeqNo2 < cam2.Count())
                {
                    if (starttime2[SeqNo2].Add(TimeSpan.Parse(time)) > starttime2[SeqNo2] && starttime2[SeqNo2] <= starttime2[SeqNo2].Add(TimeSpan.FromSeconds(duration2[SeqNo2])))
                    {
                        pathrec2a = Path.Combine(numberPath, cam2[SeqNo2]);
                        strSeq2 = SeqNo2;
                        Console.WriteLine("seqNo " + SeqNo2);
                        mediaPos2.get_CurrentPosition(out startDuration);
                        strRec2a = TimeSpan.Parse(CalculateTime(startDuration));

                        IsEmpty = false;
                        Console.WriteLine("pathrec2 " + pathrec2a);
                        Console.WriteLine("time " + startDuration);
                        Console.WriteLine("time calculate " + strRec2a);
                    }
                    else
                    {
                        IsEmpty = true;
                        strRec2a = TimeSpan.Parse(time);
                    }
                }
            }
            else if (countCh == 3)
            {
                if (cam3.Count() > 0 && SeqNo3 < cam3.Count())
                {
                    if (starttime3[SeqNo3].Add(TimeSpan.Parse(time)) > starttime3[SeqNo3] && starttime3[SeqNo3] <= starttime3[SeqNo3].Add(TimeSpan.FromSeconds(duration3[SeqNo3])))
                    {
                        pathrec3a = Path.Combine(numberPath, cam3[SeqNo3]);
                        strSeq3 = SeqNo3;
                        Console.WriteLine("seqNo " + SeqNo3);
                        mediaPos3.get_CurrentPosition(out startDuration);
                        strRec3a = TimeSpan.Parse(CalculateTime(startDuration));

                        IsEmpty = false;
                        Console.WriteLine("pathrec3 " + pathrec3a);
                        Console.WriteLine("time " + startDuration);
                        Console.WriteLine("time calculate " + strRec3a);
                    }
                    else
                    {
                        IsEmpty = true;
                        strRec3a = TimeSpan.Parse(time);
                    }
                }
            }
            else if (countCh == 4)
            {
                if (cam4.Count() > 0 && SeqNo4 < cam4.Count())
                {
                    if (starttime4[SeqNo4].Add(TimeSpan.Parse(time)) > starttime4[SeqNo4] && starttime4[SeqNo4] <= starttime4[SeqNo4].Add(TimeSpan.FromSeconds(duration4[SeqNo4])))
                    {
                        pathrec4a = Path.Combine(numberPath, cam4[SeqNo4]);
                        strSeq4 = SeqNo4;
                        Console.WriteLine("seqNo " + SeqNo4);
                        mediaPos4.get_CurrentPosition(out startDuration);
                        strRec4a = TimeSpan.Parse(CalculateTime(startDuration));

                        IsEmpty = false;
                        Console.WriteLine("pathrec4 " + pathrec4a);
                        Console.WriteLine("time " + startDuration);
                        Console.WriteLine("time calculate " + strRec4a);
                    }
                    else
                    {
                        IsEmpty = true;
                        strRec4a = TimeSpan.Parse(time);
                    }
                }
            }

            else if (countCh == 5)
            {
                if (cam5.Count() > 0 && SeqNo5 < cam5.Count())
                {
                    if (starttime5[SeqNo5].Add(TimeSpan.Parse(time)) > starttime5[SeqNo5] && starttime5[SeqNo5] <= starttime5[SeqNo5].Add(TimeSpan.FromSeconds(duration5[SeqNo5])))
                    {
                        pathrec5a = Path.Combine(numberPath, cam5[SeqNo5]);
                        strSeq5 = SeqNo5;
                        Console.WriteLine("seqNo " + SeqNo5);
                        mediaPos5.get_CurrentPosition(out startDuration);
                        strRec5a = TimeSpan.Parse(CalculateTime(startDuration));

                        IsEmpty = false;
                        Console.WriteLine("pathrec5 " + pathrec5a);
                        Console.WriteLine("time " + startDuration);
                        Console.WriteLine("time calculate " + strRec5a);
                    }
                    else
                    {
                        IsEmpty = true;
                        strRec5a = TimeSpan.Parse(time);
                    }
                }
            }


            btRecStop.Enabled = true;
            btRec.Enabled = false;
        }

        /// <summary> stop video recording, set end state for video recording and save the video record to the spesific folder in local disk. </summary>
        private void btRecStop_Click(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            //setcbRate();
            System.Globalization.CultureInfo cultureinformation = new System.Globalization.CultureInfo("en-US");

            System.Globalization.DateTimeFormatInfo dateTimeInfo = new System.Globalization.DateTimeFormatInfo();

            dateTimeInfo.DateSeparator = "/";
            dateTimeInfo.LongDatePattern = "dd MMMMM yyyy";
            dateTimeInfo.ShortDatePattern = "dd/MM/yyyy";
            dateTimeInfo.ShortTimePattern = "HH:mm";
            dateTimeInfo.LongTimePattern = "HH:mm:ss";

            //cultureinformation.DateTimeFormat = dateTimeInfo;

            Application.CurrentCulture = cultureinformation;

            Thread.CurrentThread.CurrentCulture = cultureinformation;
            Thread.CurrentThread.CurrentUICulture = cultureinformation;

            DialogResult dr;
            if (playState1 != PlayState.Paused || playState2 != PlayState.Paused || playState3 != PlayState.Paused || playState4 != PlayState.Paused || playState5 != PlayState.Paused)
            {
                btPause_Click(sender, e);
            }

            if (!string.IsNullOrEmpty(Properties.Settings.Default.recPath))
            {
                Console.WriteLine("That path is exists.");
                browsePath.SelectedPath = Properties.Settings.Default.recPath;
                browsePath.ShowNewFolderButton = false;
                dr = browsePath.ShowDialog();
            }
            else
            {
                dr = browsePath.ShowDialog();
            }

            if (dr == DialogResult.OK)
            {
                recPath = browsePath.SelectedPath;
                Properties.Settings.Default.recPath = browsePath.SelectedPath;

                Console.WriteLine("1 " + strSeq1);
                Console.WriteLine("2 " + strSeq2);
                Console.WriteLine("3 " + strSeq3);
                Console.WriteLine("4 " + strSeq4);
                Console.WriteLine("5 " + strSeq5);
                btRecStop.Enabled = false;

                endDurName = TimeSpan.Parse(CalculateTime(ticky));
                endDurStr = lbStartDur.Text.Replace(":", string.Empty);

                Console.WriteLine( "    {0}", CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern);

                if (strDurName <= endDurName)
                {
                    progressDialog = new ProgressRecord();
                    doRecord.RunWorkerAsync();
                    progressDialog.ShowDialog();
                }
                else
                {
                    VidStatus = "record not allow";
                    MessageBox.Show(this, VidStatus);
                    btRec.Enabled = true;
                }
            }
            else
            {
                Properties.Settings.Default.recPath = browsePath.SelectedPath;

                if (currentVideoState == VideoState.Forwarding)
                {
                    btPlay_Click(sender, e);
                }
                else if (currentVideoState == VideoState.FastBackwards4x)
                {
                    btFastBackward4x_Click(sender, e);
                }
                else if (currentVideoState == VideoState.FastBackwards8x)
                {
                    btFastBackward8x_Click(sender, e);
                }
                else if (currentVideoState == VideoState.Forwarding4x)
                {
                    btFast4x_Click(sender, e);
                }
                else if (currentVideoState == VideoState.Forwarding8x)
                {
                    btFast8x_Click(sender, e);
                }
            }
        }

        /// <summary> progress to save video record </summary>
        private void doRecord_DoWork(object sender, DoWorkEventArgs e)
        {
            string comb = "";
            double kosong;
            string folderpath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            folderpath = Path.GetDirectoryName(folderpath);
            string EmptyVideoeosRC = Path.Combine(folderpath, "thirdParty\\" + EmptyVideo);
            string filekosong = "";
            strRec1b = TimeSpan.Parse("00:00:00");
            //CheckPoints
            string recName = "";
            //string relativeFolder = Path.Combine(pathdirect, "Maestronic");
            //string cout = Path.Combine(relativeFolder, "temp_");
            string cout = Path.Combine(pathdirect, "tempVid_");

            try
            {
                if (!Directory.Exists(cout))
                {
                    DirectoryInfo di = Directory.CreateDirectory(cout);
                    di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                    Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(cout));
                }
            }
            catch (Exception et)
            {
                Console.WriteLine("The process failed: {0}", et.ToString());
            }

            int fileCount = 0;
            string time = CalculateTime(ticky);
            TimeSpan timetime = TimeSpan.Parse(time);

            progressDialog.pictureBox1.Image = mtvplayer.Properties.Resources.searching1;
            ProcessStartInfo jointFile = new ProcessStartInfo();
            jointFile.CreateNoWindow = true;
            jointFile.UseShellExecute = false;
            jointFile.FileName = "thirdParty\\ffmpeg.exe";
            jointFile.WindowStyle = ProcessWindowStyle.Hidden;
            Console.WriteLine("cam1.Count()" + cam1.Count());
            Console.WriteLine("cam2.Count()" + cam2.Count());
            Console.WriteLine("cam3.Count()" + cam3.Count());
            Console.WriteLine("cam4.Count()" + cam4.Count());
            Console.WriteLine("cam5.Count()" + cam4.Count());

            if (countCh == 1)
            {
                string execute = "";
                TimeSpan timeKosong;
                string name1 = time.ToString().Replace(@":", "");
                string name = "";

                if (playmode == "single")
                {
                    IsEmpty = false;
                    pathrec1b = pathDir;
                    name = Path.GetDirectoryName(pathrec1b);
                    name = Path.GetFileName(name);
                    
                    recName = "rec_ch1_" + strTimeTB + "-" + endDurStr + "_(" + name + ")";

                    if (mediaPos1 != null)
                    {
                        //if (IsEmpty)
                        //{
                        //    Console.WriteLine("_______");
                        //    timeKosong = TimeSpan.Parse(time) - strRec1a;
                        //    kosong = timeKosong.TotalSeconds;
                        //    filekosong = "";

                        //    if (kosong < 60)
                        //    {
                        //        //trim video
                        //        strRec1b = TimeSpan.Parse("00:00:00");
                        //        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec1b + " -t " + timeKosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "1.avi" + "\"";
                        //        listRec.Add(execute);
                        //        Console.WriteLine("execute " + execute);
                        //    }
                        //    else
                        //    {
                        //        int intkosong = (int)Math.Abs(kosong / 60);
                        //        for (int k = 0; k < intkosong; k++)
                        //        {
                        //            filekosong = filekosong + EmptyVideoeosRC + "|";
                        //        }

                        //        filekosong = filekosong.TrimEnd('|');

                        //        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "1.avi" + "\"";
                        //        listRec.Add(execute);
                        //        Console.WriteLine("execute " + execute);

                        //        kosong = Math.Floor(kosong / 60);
                        //        if (kosong != 0)
                        //        {
                        //            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                        //            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec1b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2.avi" + "\"";
                        //            listRec.Add(execute);
                        //            Console.WriteLine("execute " + execute);
                        //        }
                        //    }
                        //}

                        Console.WriteLine("|||||||||||");
                        mediaPos1.get_CurrentPosition(out endDuration);
                        durRec1 = TimeSpan.Parse(CalculateTime(endDuration)) - strRec1a;
                        //durRec1 = timetime - strRec1a;
                        if (rate.ToString() == "7,27")
                        {
                            
                        }



                        execute = " -i " + "\"" + pathrec1a + "\"" + " -ss " + strRec1a + " -t " + durRec1 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "3.avi" + "\"";
                        listRec.Add(execute);
                        Console.WriteLine("execute " + execute);
                    }
                }

                if (cam1.Count() != 0)
                {
                    if (fromTime.Add(TimeSpan.Parse(time)) > strRec1a)
                    {
                        //if (playmode == "single")
                        //{
                        //    IsEmpty = false;
                        //    pathrec1b = pathDir;
                        //    name = Path.GetDirectoryName(pathrec1b);
                        //    name = Path.GetFileName(name);
                        //}
                        //else if (playmode == "multiple")
                        //{
                        pathrec1b = Path.Combine(numberPath, cam1[SeqNo1]);
                        name = Path.GetDirectoryName(cam1[SeqNo1]);
                        //}

                        recName = "rec_ch1_" + strTimeTB + "-" + endDurStr + "_(" + name + ")";
                        if (strSeq1 == SeqNo1)
                        {
                            if (mediaPos1 != null)
                            {
                                if (IsEmpty)
                                {
                                    Console.WriteLine("_______");
                                    timeKosong = TimeSpan.Parse(time) - strRec1a;
                                    kosong = timeKosong.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        strRec1b = TimeSpan.Parse("00:00:00");
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec1b + " -t " + timeKosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec1b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }

                                Console.WriteLine("|||||||||||");
                                mediaPos1.get_CurrentPosition(out endDuration);
                                durRec1 = TimeSpan.Parse(CalculateTime(endDuration)) - strRec1a;
                                //durRec1 = timetime - strRec1a;

                                execute = " -i " + "\"" + pathrec1a + "\"" + " -ss " + strRec1a + " -t " + durRec1 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "3.avi" + "\"";
                                listRec.Add(execute);
                                Console.WriteLine("execute " + execute);
                            }
                            else if (mediaPos1 == null)
                            {
                                if (IsEmpty)
                                {
                                    Console.WriteLine("______________");
                                    timeKosong = TimeSpan.Parse(time) - strRec1a;
                                    kosong = timeKosong.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        strRec1b = TimeSpan.Parse("00:00:00");
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec1b + " -t " + timeKosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec1b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("|||||||||||__________");
                                    durRec1 = TimeSpan.Parse(CalculateTime(duration1[SeqNo1])) - strRec1a;
                                    execute = " -i " + "\"" + pathrec1a + "\"" + " -ss " + strRec1a + " -t " + durRec1 + " -r " + rate + " -async 1 " + cout + "\\" + "1.avi" + "\"";
                                    listRec.Add(execute);
                                    Console.WriteLine("execute " + execute);

                                    timeKosong = fromTime.Add(TimeSpan.Parse(time)) - starttime1[SeqNo1].Add(TimeSpan.FromSeconds(duration1[SeqNo1]));
                                    if (SeqNo1 > strSeq1)
                                    {
                                        timeKosong = fromTime.Add(TimeSpan.Parse(time)) - starttime1[SeqNo1 - 1].Add(TimeSpan.FromSeconds(duration1[SeqNo1 - 1]));
                                    }

                                    kosong = timeKosong.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec1b + " -t " + timeKosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }
                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec1b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "3.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                            }
                        }
                        else if (strSeq1 < SeqNo1)
                        {
                            if (mediaPos1 == null)
                            {
                                if (!IsEmpty)
                                {
                                    durRec1 = TimeSpan.Parse(CalculateTime(duration1[strSeq1])) - strRec1a;
                                    execute = " -i " + "\"" + pathrec1a + "\"" + " -ss " + strRec1a + " -t " + durRec1 + " -r " + rate + " -async 1 " + cout + "\\" + "1.avi" + "\"";
                                    listRec.Add(execute);
                                    Console.WriteLine("execute " + execute);
                                }

                                int count = SeqNo1 - (strSeq1 + 1);
                                if (count > 0)
                                {
                                    for (int n = strSeq1 + 1; n <= SeqNo1; n++)
                                    {
                                        if (starttime1[n] > starttime1[n - 1].Add(TimeSpan.FromSeconds(duration1[n - 1] + 5)))
                                        {
                                            strRec1b = TimeSpan.Parse("00:00:00");
                                            durRec1 = starttime1[n] - starttime1[strSeq1].Add(TimeSpan.FromSeconds(duration1[strSeq1]));

                                            if (IsEmpty)
                                            {
                                                durRec1 = starttime1[n] - strRec1a;
                                            }

                                            if (n == SeqNo1)
                                            {
                                                durRec1 = TimeSpan.Parse(time) - starttime1[SeqNo1].Add(TimeSpan.FromSeconds(duration1[strSeq1]));
                                            }

                                            kosong = durRec1.TotalSeconds;
                                            filekosong = "";

                                            if (kosong < 60)
                                            {
                                                //trim video
                                                execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec1b + " -t " + durRec1 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + ".avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);
                                            }
                                            else
                                            {
                                                int intkosong = (int)Math.Abs(kosong / 60);
                                                for (int k = 0; k < intkosong; k++)
                                                {
                                                    filekosong = filekosong + EmptyVideoeosRC + "|";
                                                }

                                                filekosong = filekosong.TrimEnd('|');

                                                execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2_" + n + "a.avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);

                                                kosong = Math.Floor(kosong / 60);
                                                if (kosong != 0)
                                                {
                                                    TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                                    execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec1b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + "b.avi" + "\"";
                                                    listRec.Add(execute);
                                                    Console.WriteLine("execute " + execute);
                                                }
                                            }
                                        }

                                        if (n < SeqNo1)
                                        {
                                            strRec1b = TimeSpan.Parse("00:00:00");
                                            durRec1 = starttime1[n].Add(TimeSpan.FromSeconds(duration1[n]));
                                            pathrec1b = Path.Combine(numberPath, cam1[n]);
                                            name1 = Path.GetFileName(cam1[n]);

                                            execute = " -i " + pathrec1b + " -ss " + strRec1b + " -t " + durRec1 + " -r " + rate + " -async 1 " + cout + "\\" + "2_" + n + "c.avi";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                                else
                                {
                                    strRec1b = TimeSpan.Parse("00:00:00");
                                    durRec1 = TimeSpan.FromSeconds(duration1[SeqNo1]);

                                    if (SeqNo1 - 1 == strSeq1)
                                    {
                                        durRec1 = fromTime.Add(TimeSpan.Parse(time)) - starttime1[strSeq1].Add(TimeSpan.FromSeconds(duration1[strSeq1]));
                                    }
                                    else if (IsEmpty)
                                    {
                                        if (strSeq1 + 1 == SeqNo1)
                                            durRec1 = starttime1[SeqNo1] - starttime1[strSeq1].Add(TimeSpan.FromSeconds(duration1[strSeq1]));
                                    }

                                    kosong = durRec1.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        strRec1b = TimeSpan.Parse("00:00:00");
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec1b + " -t " + durRec1 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "4.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "4_a.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec1b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "4_b.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                            }
                            else if (mediaPos1 != null)
                            {
                                if (!IsEmpty)
                                {
                                    Console.WriteLine("|||||||||");
                                    durRec1 = TimeSpan.Parse(CalculateTime(duration1[strSeq1])) - strRec1a;
                                    execute = " -i " + "\"" + pathrec1a + "\"" + " -ss " + strRec1a + " -t " + durRec1 + " -r " + rate + " -async 1 " + cout + "\\" + "1.avi" + "\"";
                                    listRec.Add(execute);
                                    Console.WriteLine("execute " + execute);
                                }

                                int count = SeqNo1 - (strSeq1 + 1);
                                if (count > 0)
                                {
                                    for (int n = strSeq1 + 1; n <= SeqNo1; n++)
                                    {
                                        if (starttime1[n] > starttime1[n - 1].Add(TimeSpan.FromSeconds(duration1[n - 1] + 5)))
                                        {
                                            strRec1b = TimeSpan.Parse("00:00:00");
                                            durRec1 = starttime1[n] - starttime1[n - 1].Add(TimeSpan.FromSeconds(duration1[n - 1]));

                                            if (IsEmpty)
                                            {
                                                durRec1 = starttime1[n] - strRec1a;
                                            }

                                            kosong = durRec1.TotalSeconds;
                                            filekosong = "";

                                            if (kosong < 60)
                                            {
                                                //trim video
                                                execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec1b + " -t " + durRec1 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + ".avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);
                                            }
                                            else
                                            {
                                                int intkosong = (int)Math.Abs(kosong / 60);
                                                for (int k = 0; k < intkosong; k++)
                                                {
                                                    filekosong = filekosong + EmptyVideoeosRC + "|";
                                                }

                                                filekosong = filekosong.TrimEnd('|');

                                                execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2_" + n + "a.avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);

                                                kosong = Math.Floor(kosong / 60);
                                                if (kosong != 0)
                                                {
                                                    TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                                    execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec1b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + "b.avi" + "\"";
                                                    listRec.Add(execute);
                                                    Console.WriteLine("execute " + execute);
                                                }
                                            }
                                        }

                                        if (n < SeqNo1)
                                        {
                                            strRec1b = TimeSpan.Parse("00:00:00");
                                            durRec1 = TimeSpan.FromSeconds(duration1[n]);
                                            pathrec1b = Path.Combine(numberPath, cam1[n]);
                                            name1 = Path.GetFileName(cam1[n]);

                                            execute = " -i " + pathrec1b + " -ss " + strRec1b + " -t " + durRec1 + " -r " + rate + " -async 1 " + cout + "\\" + "2_" + n + "c.avi";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                                else
                                {
                                    if (starttime1[SeqNo1] > starttime1[strSeq1].Add(TimeSpan.FromSeconds(duration1[strSeq1] + 5)))
                                    {
                                        strRec1b = TimeSpan.Parse("00:00:00");
                                        durRec1 = starttime1[SeqNo1] - starttime1[SeqNo1 - 1].Add(TimeSpan.FromSeconds(duration1[SeqNo1 - 1]));

                                        if (IsEmpty)
                                        {
                                            durRec1 = starttime1[SeqNo1] - strRec1a;
                                        }

                                        kosong = durRec1.TotalSeconds;
                                        filekosong = "";

                                        if (kosong < 60)
                                        {
                                            //trim video
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec1b + " -t " + durRec1 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2" + ".avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                        else
                                        {
                                            int intkosong = (int)Math.Abs(kosong / 60);
                                            for (int k = 0; k < intkosong; k++)
                                            {
                                                filekosong = filekosong + EmptyVideoeosRC + "|";
                                            }

                                            filekosong = filekosong.TrimEnd('|');

                                            execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2a" + ".avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);

                                            kosong = Math.Floor(kosong / 60);
                                            if (kosong != 0)
                                            {
                                                TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                                execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec1b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2b" + ".avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);
                                            }
                                        }
                                    }
                                }

                                durRec1 = fromTime.Add(TimeSpan.Parse(time)) - starttime1[SeqNo1];
                                pathrec1a = Path.Combine(numberPath, cam1[SeqNo1]);

                                execute = " -i " + "\"" + pathrec1a + "\"" + " -ss " + strRec1b + " -t " + durRec1 + " -r " + rate + " -async 1 " + cout + "\\" + "3.avi" + "\"";
                                listRec.Add(execute);
                                Console.WriteLine("execute " + execute);
                            }
                        }
                    }
                }
            }

            if (countCh == 2)
            {
                string execute = "";
                TimeSpan timeKosong;
                string name1 = time.ToString().Replace(@":", "");

                if (cam2.Count() != 0)
                {
                    if (fromTime.Add(TimeSpan.Parse(time)) > strRec2a)
                    {
                        pathrec2b = Path.Combine(numberPath, cam2[SeqNo2]);
                        string name = Path.GetDirectoryName(cam2[SeqNo2]);
                        recName = "rec_ch2_" + strTimeTB + "-" + endDurStr + "_(" + name + ")";

                        if (strSeq2 == SeqNo2)
                        {
                            if (mediaPos2 != null)
                            {
                                if (IsEmpty)
                                {
                                    Console.WriteLine("_______");
                                    timeKosong = TimeSpan.Parse(time) - strRec2a;
                                    kosong = timeKosong.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        strRec2b = TimeSpan.Parse("00:00:00");
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec2b + " -t " + timeKosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec2b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }

                                Console.WriteLine("|||||||||||");
                                mediaPos2.get_CurrentPosition(out endDuration);
                                durRec2 = TimeSpan.Parse(CalculateTime(endDuration)) - strRec2a;
                                //durRec2 = timetime - strRec2a;
                                execute = " -i " + "\"" + pathrec2a + "\"" + " -ss " + strRec2a + " -t " + durRec2 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "3.avi" + "\"";
                                listRec.Add(execute);
                                Console.WriteLine("execute " + execute);
                            }
                            else if (mediaPos2 == null)
                            {
                                if (IsEmpty)
                                {
                                    Console.WriteLine("______________");
                                    timeKosong = TimeSpan.Parse(time) - strRec2a;
                                    kosong = timeKosong.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        strRec2b = TimeSpan.Parse("00:00:00");
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec2b + " -t " + timeKosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec2b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("|||||||||||__________");
                                    durRec2 = TimeSpan.Parse(CalculateTime(duration2[SeqNo2])) - strRec2a;
                                    execute = " -i " + "\"" + pathrec1a + "\"" + " -ss " + strRec2a + " -t " + durRec2 + " -r " + rate + " -async 1 " + cout + "\\" + "1.avi" + "\"";
                                    listRec.Add(execute);
                                    Console.WriteLine("execute " + execute);

                                    timeKosong = fromTime.Add(TimeSpan.Parse(time)) - starttime2[SeqNo2].Add(TimeSpan.FromSeconds(duration2[SeqNo2]));
                                    if (SeqNo2 > strSeq2)
                                    {
                                        timeKosong = fromTime.Add(TimeSpan.Parse(time)) - starttime2[SeqNo2 - 1].Add(TimeSpan.FromSeconds(duration2[SeqNo2 - 1]));
                                    }

                                    kosong = timeKosong.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec2b + " -t " + timeKosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec2b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "3.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                            }
                        }
                        else if (strSeq2 < SeqNo2)
                        {
                            if (mediaPos2 == null)
                            {
                                if (!IsEmpty)
                                {
                                    durRec2 = TimeSpan.Parse(CalculateTime(duration2[strSeq2])) - strRec2a;
                                    execute = " -i " + "\"" + pathrec2a + "\"" + " -ss " + strRec2a + " -t " + durRec2 + " -r " + rate + " -async 1 " + cout + "\\" + "1.avi" + "\"";
                                    listRec.Add(execute);
                                    Console.WriteLine("execute " + execute);
                                }

                                int count = SeqNo2 - (strSeq2 + 1);
                                if (count > 0)
                                {
                                    for (int n = strSeq2 + 1; n <= SeqNo2; n++)
                                    {
                                        if (starttime2[n] > starttime2[n - 1].Add(TimeSpan.FromSeconds(duration2[n - 1] + 5)))
                                        {
                                            strRec2b = TimeSpan.Parse("00:00:00");
                                            durRec2 = starttime2[n] - starttime2[strSeq2].Add(TimeSpan.FromSeconds(duration2[strSeq2]));

                                            if (IsEmpty)
                                            {
                                                durRec2 = starttime2[n] - strRec2a;
                                            }

                                            if (n == SeqNo2)
                                            {
                                                durRec2 = TimeSpan.Parse(time) - starttime2[SeqNo2].Add(TimeSpan.FromSeconds(duration2[strSeq2]));
                                            }

                                            kosong = durRec2.TotalSeconds;
                                            filekosong = "";

                                            if (kosong < 60)
                                            {
                                                //trim video
                                                execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec2b + " -t " + durRec2 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + ".avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);
                                            }
                                            else
                                            {
                                                int intkosong = (int)Math.Abs(kosong / 60);
                                                for (int k = 0; k < intkosong; k++)
                                                {
                                                    filekosong = filekosong + EmptyVideoeosRC + "|";
                                                }

                                                filekosong = filekosong.TrimEnd('|');

                                                execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2_" + n + "a.avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);

                                                kosong = Math.Floor(kosong / 60);
                                                if (kosong != 0)
                                                {
                                                    TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                                    execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec2b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + "b.avi" + "\"";
                                                    listRec.Add(execute);
                                                    Console.WriteLine("execute " + execute);
                                                }
                                            }
                                        }

                                        if (n < SeqNo2)
                                        {
                                            strRec2b = TimeSpan.Parse("00:00:00");
                                            durRec2 = starttime2[n].Add(TimeSpan.FromSeconds(duration2[n]));
                                            pathrec2b = Path.Combine(numberPath, cam2[n]);
                                            name1 = Path.GetFileName(cam2[n]);

                                            execute = " -i " + pathrec2b + " -ss " + strRec2b + " -t " + durRec2 + " -r " + rate + " -async 1 " + cout + "\\" + "2_" + n + "c.avi";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                                else
                                {
                                    strRec2b = TimeSpan.Parse("00:00:00");
                                    durRec2 = TimeSpan.FromSeconds(duration2[SeqNo2]);

                                    if (SeqNo2 - 1 == strSeq2)
                                    {
                                        durRec2 = fromTime.Add(TimeSpan.Parse(time)) - starttime2[strSeq2].Add(TimeSpan.FromSeconds(duration2[strSeq2]));
                                    }
                                    else if (IsEmpty)
                                    {
                                        if (strSeq2 + 1 == SeqNo2)
                                            durRec2 = starttime2[SeqNo2] - starttime2[strSeq2].Add(TimeSpan.FromSeconds(duration2[strSeq2]));
                                    }

                                    kosong = durRec2.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        strRec2b = TimeSpan.Parse("00:00:00");
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec2b + " -t " + durRec2 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "4.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "4_a.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec2b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "4_b.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                            }
                            else if (mediaPos2 != null)
                            {
                                if (!IsEmpty)
                                {
                                    Console.WriteLine("|||||||||");
                                    durRec2 = TimeSpan.Parse(CalculateTime(duration2[strSeq2])) - strRec2a;
                                    execute = " -i " + "\"" + pathrec2a + "\"" + " -ss " + strRec2a + " -t " + durRec2 + " -r " + rate + " -async 1 " + cout + "\\" + "1.avi" + "\"";
                                    listRec.Add(execute);
                                    Console.WriteLine("execute " + execute);
                                }

                                int count = SeqNo2 - (strSeq2 + 1);
                                if (count > 0)
                                {
                                    for (int n = strSeq2 + 1; n <= SeqNo2; n++)
                                    {
                                        if (starttime2[n] > starttime2[n - 1].Add(TimeSpan.FromSeconds(duration2[n - 1] + 5)))
                                        {
                                            strRec2b = TimeSpan.Parse("00:00:00");
                                            durRec2 = starttime2[n] - starttime2[n - 1].Add(TimeSpan.FromSeconds(duration2[n - 1]));

                                            if (IsEmpty)
                                            {
                                                durRec2 = starttime2[n] - strRec2a;
                                            }

                                            kosong = durRec2.TotalSeconds;
                                            filekosong = "";

                                            if (kosong < 60)
                                            {
                                                //trim video
                                                execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec2b + " -t " + durRec2 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + ".avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);
                                            }
                                            else
                                            {
                                                int intkosong = (int)Math.Abs(kosong / 60);
                                                for (int k = 0; k < intkosong; k++)
                                                {
                                                    filekosong = filekosong + EmptyVideoeosRC + "|";
                                                }

                                                filekosong = filekosong.TrimEnd('|');

                                                execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2_" + n + "a.avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);

                                                kosong = Math.Floor(kosong / 60);
                                                if (kosong != 0)
                                                {
                                                    TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                                    execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec2b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + "b.avi" + "\"";
                                                    listRec.Add(execute);
                                                    Console.WriteLine("execute " + execute);
                                                }
                                            }
                                        }

                                        if (n < SeqNo2)
                                        {
                                            strRec2b = TimeSpan.Parse("00:00:00");
                                            durRec2 = TimeSpan.FromSeconds(duration2[n]);
                                            pathrec2b = Path.Combine(numberPath, cam2[n]);
                                            name1 = Path.GetFileName(cam2[n]);

                                            execute = " -i " + pathrec2b + " -ss " + strRec2b + " -t " + durRec2 + " -r " + rate + " -async 1 " + cout + "\\" + "2_" + n + "c.avi";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                                else
                                {
                                    if (starttime2[SeqNo2] > starttime2[strSeq2].Add(TimeSpan.FromSeconds(duration2[strSeq2] + 5)))
                                    {
                                        strRec2b = TimeSpan.Parse("00:00:00");
                                        durRec2 = starttime2[SeqNo2] - starttime2[SeqNo2 - 1].Add(TimeSpan.FromSeconds(duration2[SeqNo2 - 1]));

                                        if (IsEmpty)
                                        {
                                            durRec2 = starttime2[SeqNo2] - strRec2a;
                                        }

                                        kosong = durRec2.TotalSeconds;
                                        filekosong = "";

                                        if (kosong < 60)
                                        {
                                            //trim video
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec2b + " -t " + durRec2 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2" + ".avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                        else
                                        {
                                            int intkosong = (int)Math.Abs(kosong / 60);
                                            for (int k = 0; k < intkosong; k++)
                                            {
                                                filekosong = filekosong + EmptyVideoeosRC + "|";
                                            }

                                            filekosong = filekosong.TrimEnd('|');

                                            execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2a" + ".avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);

                                            kosong = Math.Floor(kosong / 60);
                                            if (kosong != 0)
                                            {
                                                TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                                execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec2b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2b" + ".avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);
                                            }
                                        }
                                    }
                                }

                                durRec2 = fromTime.Add(TimeSpan.Parse(time)) - starttime2[SeqNo2];
                                pathrec2a = Path.Combine(numberPath, cam2[SeqNo2]);

                                execute = " -i " + "\"" + pathrec2a + "\"" + " -ss " + strRec2b + " -t " + durRec2 + " -r " + rate + " -async 1 " + cout + "\\" + "3.avi" + "\"";
                                listRec.Add(execute);
                                Console.WriteLine("execute " + execute);
                            }
                        }
                    }
                }
            }

            if (countCh == 3)
            {
                string execute = "";
                TimeSpan timeKosong;
                string name1 = time.ToString().Replace(@":", "");

                if (cam3.Count() != 0)
                {
                    if (fromTime.Add(TimeSpan.Parse(time)) > strRec3a)
                    {
                        pathrec3b = Path.Combine(numberPath, cam3[SeqNo3]);
                        string name = Path.GetDirectoryName(cam3[SeqNo3]);
                        recName = "rec_ch3_" + strTimeTB + "-" + endDurStr + "_(" + name + ")";

                        if (strSeq3 == SeqNo3)
                        {
                            if (mediaPos3 != null)
                            {
                                if (IsEmpty)
                                {
                                    Console.WriteLine("_______");
                                    timeKosong = TimeSpan.Parse(time) - strRec3a;
                                    kosong = timeKosong.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        strRec3b = TimeSpan.Parse("00:00:00");
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec3b + " -t " + timeKosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec3b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }

                                Console.WriteLine("|||||||||||");
                                mediaPos3.get_CurrentPosition(out endDuration);
                                durRec3 = TimeSpan.Parse(CalculateTime(endDuration)) - strRec3a;
                                //durRec3 = timetime - strRec3a;
                                execute = " -i " + "\"" + pathrec3a + "\"" + " -ss " + strRec3a + " -t " + durRec3 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "3.avi" + "\"";
                                listRec.Add(execute);
                                Console.WriteLine("execute " + execute);
                            }
                            else if (mediaPos3 == null)
                            {
                                if (IsEmpty)
                                {
                                    Console.WriteLine("______________");
                                    timeKosong = TimeSpan.Parse(time) - strRec3a;
                                    kosong = timeKosong.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        strRec3b = TimeSpan.Parse("00:00:00");
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec3b + " -t " + timeKosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec3b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("|||||||||||__________");
                                    durRec3 = TimeSpan.Parse(CalculateTime(duration3[SeqNo3])) - strRec3a;
                                    execute = " -i " + "\"" + pathrec3a + "\"" + " -ss " + strRec3a + " -t " + durRec3 + " -r " + rate + " -async 1 " + cout + "\\" + "1.avi" + "\"";
                                    listRec.Add(execute);
                                    Console.WriteLine("execute " + execute);

                                    timeKosong = fromTime.Add(TimeSpan.Parse(time)) - starttime3[SeqNo3].Add(TimeSpan.FromSeconds(duration3[SeqNo3]));
                                    if (SeqNo3 > strSeq3)
                                    {
                                        timeKosong = fromTime.Add(TimeSpan.Parse(time)) - starttime3[SeqNo3 - 1].Add(TimeSpan.FromSeconds(duration3[SeqNo3 - 1]));
                                    }

                                    kosong = timeKosong.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec3b + " -t " + timeKosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec3b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "3.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                            }
                        }
                        else if (strSeq3 < SeqNo3)
                        {
                            if (mediaPos3 == null)
                            {
                                if (!IsEmpty)
                                {
                                    durRec3 = TimeSpan.Parse(CalculateTime(duration3[strSeq3])) - strRec3a;
                                    execute = " -i " + "\"" + pathrec3a + "\"" + " -ss " + strRec3a + " -t " + durRec3 + " -r " + rate + " -async 1 " + cout + "\\" + "1.avi" + "\"";
                                    listRec.Add(execute);
                                    Console.WriteLine("execute " + execute);
                                }

                                int count = SeqNo3 - (strSeq3 + 1);
                                if (count > 0)
                                {
                                    for (int n = strSeq3 + 1; n <= SeqNo3; n++)
                                    {
                                        if (starttime3[n] > starttime3[n - 1].Add(TimeSpan.FromSeconds(duration3[n - 1] + 5)))
                                        {
                                            strRec3b = TimeSpan.Parse("00:00:00");
                                            durRec3 = starttime3[n] - starttime3[strSeq3].Add(TimeSpan.FromSeconds(duration3[strSeq3]));

                                            if (IsEmpty)
                                            {
                                                durRec3 = starttime3[n] - strRec3a;
                                            }

                                            if (n == SeqNo3)
                                            {
                                                durRec3 = TimeSpan.Parse(time) - starttime3[SeqNo3].Add(TimeSpan.FromSeconds(duration3[strSeq3]));
                                            }

                                            kosong = durRec3.TotalSeconds;
                                            filekosong = "";

                                            if (kosong < 60)
                                            {
                                                //trim video
                                                execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec3b + " -t " + durRec3 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + ".avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);
                                            }
                                            else
                                            {
                                                int intkosong = (int)Math.Abs(kosong / 60);
                                                for (int k = 0; k < intkosong; k++)
                                                {
                                                    filekosong = filekosong + EmptyVideoeosRC + "|";
                                                }

                                                filekosong = filekosong.TrimEnd('|');

                                                execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2_" + n + "a.avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);

                                                kosong = Math.Floor(kosong / 60);
                                                if (kosong != 0)
                                                {
                                                    TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                                    execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec3b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + "b.avi" + "\"";
                                                    listRec.Add(execute);
                                                    Console.WriteLine("execute " + execute);
                                                }
                                            }
                                        }

                                        if (n < SeqNo3)
                                        {
                                            strRec3b = TimeSpan.Parse("00:00:00");
                                            durRec3 = starttime3[n].Add(TimeSpan.FromSeconds(duration3[n]));
                                            pathrec3b = Path.Combine(numberPath, cam3[n]);
                                            name1 = Path.GetFileName(cam3[n]);

                                            execute = " -i " + pathrec3b + " -ss " + strRec3b + " -t " + durRec3 + " -r " + rate + " -async 1 " + cout + "\\" + "2_" + n + "c.avi";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                                else
                                {
                                    strRec3b = TimeSpan.Parse("00:00:00");
                                    durRec3 = TimeSpan.FromSeconds(duration3[SeqNo3]);

                                    if (SeqNo3 - 1 == strSeq3)
                                    {
                                        durRec3 = fromTime.Add(TimeSpan.Parse(time)) - starttime3[strSeq3].Add(TimeSpan.FromSeconds(duration3[strSeq3]));
                                    }
                                    else if (IsEmpty)
                                    {
                                        if (strSeq3 + 1 == SeqNo3)
                                            durRec3 = starttime3[SeqNo3] - starttime3[strSeq3].Add(TimeSpan.FromSeconds(duration3[strSeq3]));
                                    }

                                    kosong = durRec3.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        strRec3b = TimeSpan.Parse("00:00:00");
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec3b + " -t " + durRec3 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "4.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "4_a.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec3b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "4_b.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                            }
                            else if (mediaPos3 != null)
                            {
                                if (!IsEmpty)
                                {
                                    Console.WriteLine("|||||||||");
                                    durRec3 = TimeSpan.Parse(CalculateTime(duration3[strSeq3])) - strRec3a;
                                    execute = " -i " + "\"" + pathrec3a + "\"" + " -ss " + strRec3a + " -t " + durRec3 + " -r " + rate + " -async 1 " + cout + "\\" + "1.avi" + "\"";
                                    listRec.Add(execute);
                                    Console.WriteLine("execute " + execute);
                                }

                                int count = SeqNo3 - (strSeq3 + 1);
                                if (count > 0)
                                {
                                    for (int n = strSeq3 + 1; n <= SeqNo3; n++)
                                    {
                                        if (starttime3[n] > starttime3[n - 1].Add(TimeSpan.FromSeconds(duration3[n - 1] + 5)))
                                        {
                                            strRec3b = TimeSpan.Parse("00:00:00");
                                            durRec3 = starttime3[n] - starttime3[n - 1].Add(TimeSpan.FromSeconds(duration3[n - 1]));

                                            if (IsEmpty)
                                            {
                                                durRec3 = starttime3[n] - strRec3a;
                                            }

                                            kosong = durRec3.TotalSeconds;
                                            filekosong = "";

                                            if (kosong < 60)
                                            {
                                                //trim video
                                                execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec3b + " -t " + durRec3 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + ".avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);
                                            }
                                            else
                                            {
                                                int intkosong = (int)Math.Abs(kosong / 60);
                                                for (int k = 0; k < intkosong; k++)
                                                {
                                                    filekosong = filekosong + EmptyVideoeosRC + "|";
                                                }

                                                filekosong = filekosong.TrimEnd('|');

                                                execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2_" + n + "a.avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);

                                                kosong = Math.Floor(kosong / 60);
                                                if (kosong != 0)
                                                {
                                                    TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                                    execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec3b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + "b.avi" + "\"";
                                                    listRec.Add(execute);
                                                    Console.WriteLine("execute " + execute);
                                                }
                                            }
                                        }

                                        if (n < SeqNo3)
                                        {
                                            strRec3b = TimeSpan.Parse("00:00:00");
                                            durRec3 = TimeSpan.FromSeconds(duration3[n]);
                                            pathrec3b = Path.Combine(numberPath, cam3[n]);
                                            name1 = Path.GetFileName(cam3[n]);

                                            execute = " -i " + pathrec3b + " -ss " + strRec3b + " -t " + durRec3 + " -r " + rate + " -async 1 " + cout + "\\" + "2_" + n + "c.avi";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                                else
                                {
                                    if (starttime3[SeqNo3] > starttime3[strSeq3].Add(TimeSpan.FromSeconds(duration3[strSeq3] + 5)))
                                    {
                                        strRec3b = TimeSpan.Parse("00:00:00");
                                        durRec3 = starttime3[SeqNo3] - starttime3[SeqNo3 - 1].Add(TimeSpan.FromSeconds(duration3[SeqNo3 - 1]));

                                        if (IsEmpty)
                                        {
                                            durRec3 = starttime3[SeqNo3] - strRec3a;
                                        }

                                        kosong = durRec3.TotalSeconds;
                                        filekosong = "";

                                        if (kosong < 60)
                                        {
                                            //trim video
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec3b + " -t " + durRec3 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2" + ".avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                        else
                                        {
                                            int intkosong = (int)Math.Abs(kosong / 60);
                                            for (int k = 0; k < intkosong; k++)
                                            {
                                                filekosong = filekosong + EmptyVideoeosRC + "|";
                                            }

                                            filekosong = filekosong.TrimEnd('|');

                                            execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2a" + ".avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);

                                            kosong = Math.Floor(kosong / 60);
                                            if (kosong != 0)
                                            {
                                                TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                                execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec3b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2b" + ".avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);
                                            }
                                        }
                                    }
                                }

                                durRec3 = fromTime.Add(TimeSpan.Parse(time)) - starttime3[SeqNo3];
                                pathrec3a = Path.Combine(numberPath, cam3[SeqNo3]);

                                execute = " -i " + "\"" + pathrec3a + "\"" + " -ss " + strRec3b + " -t " + durRec3 + " -r " + rate + " -async 1 " + cout + "\\" + "3.avi" + "\"";
                                listRec.Add(execute);
                                Console.WriteLine("execute " + execute);
                            }
                        }
                    }
                }
            }

            if (countCh == 4)
            {
                string execute = "";
                TimeSpan timeKosong;
                string name1 = time.ToString().Replace(@":", "");

                if (cam4.Count() != 0)
                {
                    if (fromTime.Add(TimeSpan.Parse(time)) > strRec4a)
                    {
                        pathrec4b = Path.Combine(numberPath, cam4[SeqNo4]);
                        string name = Path.GetDirectoryName(cam4[SeqNo4]);
                        recName = "rec_ch4_" + strTimeTB + "-" + endDurStr + "_(" + name + ")";

                        if (strSeq4 == SeqNo4)
                        {
                            if (mediaPos4 != null)
                            {
                                if (IsEmpty)
                                {
                                    Console.WriteLine("_______");
                                    timeKosong = TimeSpan.Parse(time) - strRec4a;
                                    kosong = timeKosong.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        strRec4b = TimeSpan.Parse("00:00:00");
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec4b + " -t " + timeKosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec4b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }

                                Console.WriteLine("|||||||||||");
                                mediaPos4.get_CurrentPosition(out endDuration);
                                durRec4 = TimeSpan.Parse(CalculateTime(endDuration)) - strRec4a;
                                //durRec4 = timetime - strRec4a;
                                execute = " -i " + "\"" + pathrec4a + "\"" + " -ss " + strRec4a + " -t " + durRec4 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "3.avi" + "\"";
                                listRec.Add(execute);
                                Console.WriteLine("execute " + execute);
                            }
                            else if (mediaPos4 == null)
                            {
                                if (IsEmpty)
                                {
                                    Console.WriteLine("______________");
                                    timeKosong = TimeSpan.Parse(time) - strRec4a;
                                    kosong = timeKosong.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        strRec4b = TimeSpan.Parse("00:00:00");
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec4b + " -t " + timeKosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec4b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("|||||||||||__________");
                                    durRec4 = TimeSpan.Parse(CalculateTime(duration4[SeqNo4])) - strRec4a;
                                    execute = " -i " + "\"" + pathrec4a + "\"" + " -ss " + strRec4a + " -t " + durRec4 + " -r " + rate + " -async 1 " + cout + "\\" + "1.avi" + "\"";
                                    listRec.Add(execute);
                                    Console.WriteLine("execute " + execute);

                                    timeKosong = fromTime.Add(TimeSpan.Parse(time)) - starttime4[SeqNo4].Add(TimeSpan.FromSeconds(duration4[SeqNo4]));
                                    if (SeqNo4 > strSeq4)
                                    {
                                        timeKosong = fromTime.Add(TimeSpan.Parse(time)) - starttime4[SeqNo4 - 1].Add(TimeSpan.FromSeconds(duration4[SeqNo4 - 1]));
                                    }

                                    kosong = timeKosong.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec4b + " -t " + timeKosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec4b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "3.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                            }
                        }
                        else if (strSeq4 < SeqNo4)
                        {
                            if (mediaPos4 == null)
                            {
                                if (!IsEmpty)
                                {
                                    durRec4 = TimeSpan.Parse(CalculateTime(duration4[strSeq4])) - strRec4a;
                                    execute = " -i " + "\"" + pathrec4a + "\"" + " -ss " + strRec4a + " -t " + durRec4 + " -r " + rate + " -async 1 " + cout + "\\" + "1.avi" + "\"";
                                    listRec.Add(execute);
                                    Console.WriteLine("execute " + execute);
                                }

                                int count = SeqNo4 - (strSeq4 + 1);
                                if (count > 0)
                                {
                                    for (int n = strSeq4 + 1; n <= SeqNo4; n++)
                                    {
                                        if (starttime4[n] > starttime4[n - 1].Add(TimeSpan.FromSeconds(duration4[n - 1] + 5)))
                                        {
                                            strRec4b = TimeSpan.Parse("00:00:00");
                                            durRec4 = starttime4[n] - starttime4[strSeq4].Add(TimeSpan.FromSeconds(duration4[strSeq4]));

                                            if (IsEmpty)
                                            {
                                                durRec4 = starttime4[n] - strRec4a;
                                            }

                                            if (n == SeqNo4)
                                            {
                                                durRec4 = TimeSpan.Parse(time) - starttime4[SeqNo4].Add(TimeSpan.FromSeconds(duration4[strSeq4]));
                                            }

                                            kosong = durRec4.TotalSeconds;
                                            filekosong = "";

                                            if (kosong < 60)
                                            {
                                                //trim video
                                                execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec4b + " -t " + durRec4 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + ".avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);
                                            }
                                            else
                                            {
                                                int intkosong = (int)Math.Abs(kosong / 60);
                                                for (int k = 0; k < intkosong; k++)
                                                {
                                                    filekosong = filekosong + EmptyVideoeosRC + "|";
                                                }

                                                filekosong = filekosong.TrimEnd('|');

                                                execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2_" + n + "a.avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);

                                                kosong = Math.Floor(kosong / 60);
                                                if (kosong != 0)
                                                {
                                                    TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                                    execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec4b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + "b.avi" + "\"";
                                                    listRec.Add(execute);
                                                    Console.WriteLine("execute " + execute);
                                                }
                                            }
                                        }

                                        if (n < SeqNo4)
                                        {
                                            strRec4b = TimeSpan.Parse("00:00:00");
                                            durRec4 = starttime4[n].Add(TimeSpan.FromSeconds(duration4[n]));
                                            pathrec4b = Path.Combine(numberPath, cam4[n]);
                                            name1 = Path.GetFileName(cam4[n]);

                                            execute = " -i " + pathrec1b + " -ss " + strRec4b + " -t " + durRec4 + " -r " + rate + " -async 1 " + cout + "\\" + "2_" + n + "c.avi";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                                else
                                {
                                    strRec4b = TimeSpan.Parse("00:00:00");
                                    durRec4 = TimeSpan.FromSeconds(duration4[SeqNo4]);

                                    if (SeqNo4 - 1 == strSeq4)
                                    {
                                        durRec4 = fromTime.Add(TimeSpan.Parse(time)) - starttime4[strSeq4].Add(TimeSpan.FromSeconds(duration4[strSeq4]));
                                    }
                                    else if (IsEmpty)
                                    {
                                        if (strSeq4 + 1 == SeqNo4)
                                            durRec4 = starttime4[SeqNo4] - starttime4[strSeq4].Add(TimeSpan.FromSeconds(duration4[strSeq4]));
                                    }

                                    kosong = durRec4.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        strRec4b = TimeSpan.Parse("00:00:00");
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec4b + " -t " + durRec4 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "4.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "4_a.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec4b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "4_b.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                            }
                            else if (mediaPos4 != null)
                            {
                                if (!IsEmpty)
                                {
                                    Console.WriteLine("|||||||||");
                                    durRec4 = TimeSpan.Parse(CalculateTime(duration4[strSeq4])) - strRec4a;
                                    execute = " -i " + "\"" + pathrec4a + "\"" + " -ss " + strRec4a + " -t " + durRec4 + " -r " + rate + " -async 1 " + cout + "\\" + "1.avi" + "\"";
                                    listRec.Add(execute);
                                    Console.WriteLine("execute " + execute);
                                }

                                int count = SeqNo4 - (strSeq4 + 1);
                                if (count > 0)
                                {
                                    for (int n = strSeq4 + 1; n <= SeqNo4; n++)
                                    {
                                        if (starttime4[n] > starttime4[n - 1].Add(TimeSpan.FromSeconds(duration4[n - 1] + 5)))
                                        {
                                            strRec4b = TimeSpan.Parse("00:00:00");
                                            durRec4 = starttime4[n] - starttime4[n - 1].Add(TimeSpan.FromSeconds(duration4[n - 1]));

                                            if (IsEmpty)
                                            {
                                                durRec4 = starttime4[n] - strRec4a;
                                            }

                                            kosong = durRec4.TotalSeconds;
                                            filekosong = "";

                                            if (kosong < 60)
                                            {
                                                //trim video
                                                execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec4b + " -t " + durRec4 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + ".avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);
                                            }
                                            else
                                            {
                                                int intkosong = (int)Math.Abs(kosong / 60);
                                                for (int k = 0; k < intkosong; k++)
                                                {
                                                    filekosong = filekosong + EmptyVideoeosRC + "|";
                                                }

                                                filekosong = filekosong.TrimEnd('|');

                                                execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2_" + n + "a.avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);

                                                kosong = Math.Floor(kosong / 60);
                                                if (kosong != 0)
                                                {
                                                    TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                                    execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec4b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + "b.avi" + "\"";
                                                    listRec.Add(execute);
                                                    Console.WriteLine("execute " + execute);
                                                }
                                            }
                                        }

                                        if (n < SeqNo4)
                                        {
                                            strRec4b = TimeSpan.Parse("00:00:00");
                                            durRec4 = TimeSpan.FromSeconds(duration4[n]);
                                            pathrec4b = Path.Combine(numberPath, cam4[n]);
                                            name1 = Path.GetFileName(cam4[n]);

                                            execute = " -i " + pathrec4b + " -ss " + strRec4b + " -t " + durRec4 + " -r " + rate + " -async 1 " + cout + "\\" + "2_" + n + "c.avi";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                                else
                                {
                                    if (starttime4[SeqNo4] > starttime4[strSeq4].Add(TimeSpan.FromSeconds(duration4[strSeq4] + 5)))
                                    {
                                        strRec4b = TimeSpan.Parse("00:00:00");
                                        durRec4 = starttime4[SeqNo4] - starttime4[SeqNo4 - 1].Add(TimeSpan.FromSeconds(duration4[SeqNo4 - 1]));

                                        if (IsEmpty)
                                        {
                                            durRec4 = starttime4[SeqNo4] - strRec4a;
                                        }

                                        kosong = durRec4.TotalSeconds;
                                        filekosong = "";

                                        if (kosong < 60)
                                        {
                                            //trim video
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec4b + " -t " + durRec4 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2" + ".avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                        else
                                        {
                                            int intkosong = (int)Math.Abs(kosong / 60);
                                            for (int k = 0; k < intkosong; k++)
                                            {
                                                filekosong = filekosong + EmptyVideoeosRC + "|";
                                            }

                                            filekosong = filekosong.TrimEnd('|');

                                            execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2a" + ".avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);

                                            kosong = Math.Floor(kosong / 60);
                                            if (kosong != 0)
                                            {
                                                TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                                execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec4b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2b" + ".avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);
                                            }
                                        }
                                    }
                                }

                                durRec4 = fromTime.Add(TimeSpan.Parse(time)) - starttime4[SeqNo4];
                                pathrec4a = Path.Combine(numberPath, cam4[SeqNo4]);

                                execute = " -i " + "\"" + pathrec4a + "\"" + " -ss " + strRec4b + " -t " + durRec4 + " -r " + rate + " -async 1 " + cout + "\\" + "3.avi" + "\"";
                                listRec.Add(execute);
                                Console.WriteLine("execute " + execute);
                            }
                        }
                    }
                }
            }

            //Process Record cam5
            if (countCh == 5)
            {
                string execute = "";
                TimeSpan timeKosong;
                string name1 = time.ToString().Replace(@":", "");

                if (cam5.Count() != 0)
                {
                    if (fromTime.Add(TimeSpan.Parse(time)) > strRec5a)
                    {
                        pathrec5b = Path.Combine(numberPath, cam5[SeqNo5]);
                        string name = Path.GetDirectoryName(cam5[SeqNo5]);
                        recName = "rec_ch5_" + strTimeTB + "-" + endDurStr + "_(" + name + ")";

                        if (strSeq5 == SeqNo5)
                        {
                            if (mediaPos5 != null)
                            {
                                if (IsEmpty)
                                {
                                    Console.WriteLine("_______");
                                    timeKosong = TimeSpan.Parse(time) - strRec5a;
                                    kosong = timeKosong.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        strRec5b = TimeSpan.Parse("00:00:00");
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec5b + " -t " + timeKosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec5b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }

                                Console.WriteLine("|||||||||||");
                                mediaPos5.get_CurrentPosition(out endDuration);
                                durRec5 = TimeSpan.Parse(CalculateTime(endDuration)) - strRec5a;
                                //durRec5 = timetime - strRec5a;
                                execute = " -i " + "\"" + pathrec5a + "\"" + " -ss " + strRec5a + " -t " + durRec5 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "3.avi" + "\"";
                                listRec.Add(execute);
                                Console.WriteLine("execute " + execute);
                            }
                            else if (mediaPos5 == null)
                            {
                                if (IsEmpty)
                                {
                                    Console.WriteLine("______________");
                                    timeKosong = TimeSpan.Parse(time) - strRec5a;
                                    kosong = timeKosong.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        strRec5b = TimeSpan.Parse("00:00:00");
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec5b + " -t " + timeKosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "1.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec5b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("|||||||||||__________");
                                    durRec5 = TimeSpan.Parse(CalculateTime(duration5[SeqNo5])) - strRec5a;
                                    execute = " -i " + "\"" + pathrec5a + "\"" + " -ss " + strRec5a + " -t " + durRec5 + " -r " + rate + " -async 1 " + cout + "\\" + "1.avi" + "\"";
                                    listRec.Add(execute);
                                    Console.WriteLine("execute " + execute);

                                    timeKosong = fromTime.Add(TimeSpan.Parse(time)) - starttime5[SeqNo5].Add(TimeSpan.FromSeconds(duration5[SeqNo5]));
                                    if (SeqNo5 > strSeq5)
                                    {
                                        timeKosong = fromTime.Add(TimeSpan.Parse(time)) - starttime5[SeqNo5 - 1].Add(TimeSpan.FromSeconds(duration5[SeqNo5 - 1]));
                                    }

                                    kosong = timeKosong.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec5b + " -t " + timeKosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec5b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "3.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                            }
                        }
                        else if (strSeq5 < SeqNo5)
                        {
                            if (mediaPos5 == null)
                            {
                                if (!IsEmpty)
                                {
                                    durRec5 = TimeSpan.Parse(CalculateTime(duration5[strSeq5])) - strRec5a;
                                    execute = " -i " + "\"" + pathrec5a + "\"" + " -ss " + strRec5a + " -t " + durRec5 + " -r " + rate + " -async 1 " + cout + "\\" + "1.avi" + "\"";
                                    listRec.Add(execute);
                                    Console.WriteLine("execute " + execute);
                                }

                                int count = SeqNo5 - (strSeq5 + 1);
                                if (count > 0)
                                {
                                    for (int n = strSeq5 + 1; n <= SeqNo5; n++)
                                    {
                                        if (starttime5[n] > starttime5[n - 1].Add(TimeSpan.FromSeconds(duration5[n - 1] + 5)))
                                        {
                                            strRec5b = TimeSpan.Parse("00:00:00");
                                            durRec5 = starttime5[n] - starttime5[strSeq5].Add(TimeSpan.FromSeconds(duration5[strSeq5]));

                                            if (IsEmpty)
                                            {
                                                durRec5 = starttime5[n] - strRec5a;
                                            }

                                            if (n == SeqNo5)
                                            {
                                                durRec5 = TimeSpan.Parse(time) - starttime5[SeqNo5].Add(TimeSpan.FromSeconds(duration5[strSeq5]));
                                            }

                                            kosong = durRec5.TotalSeconds;
                                            filekosong = "";

                                            if (kosong < 60)
                                            {
                                                //trim video
                                                execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec5b + " -t " + durRec5 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + ".avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);
                                            }
                                            else
                                            {
                                                int intkosong = (int)Math.Abs(kosong / 60);
                                                for (int k = 0; k < intkosong; k++)
                                                {
                                                    filekosong = filekosong + EmptyVideoeosRC + "|";
                                                }

                                                filekosong = filekosong.TrimEnd('|');

                                                execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2_" + n + "a.avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);

                                                kosong = Math.Floor(kosong / 60);
                                                if (kosong != 0)
                                                {
                                                    TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                                    execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec5b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + "b.avi" + "\"";
                                                    listRec.Add(execute);
                                                    Console.WriteLine("execute " + execute);
                                                }
                                            }
                                        }

                                        if (n < SeqNo5)
                                        {
                                            strRec5b = TimeSpan.Parse("00:00:00");
                                            durRec5 = starttime5[n].Add(TimeSpan.FromSeconds(duration5[n]));
                                            pathrec5b = Path.Combine(numberPath, cam5[n]);
                                            name1 = Path.GetFileName(cam5[n]);

                                            execute = " -i " + pathrec1b + " -ss " + strRec5b + " -t " + durRec5 + " -r " + rate + " -async 1 " + cout + "\\" + "2_" + n + "c.avi";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                                else
                                {
                                    strRec5b = TimeSpan.Parse("00:00:00");
                                    durRec5 = TimeSpan.FromSeconds(duration5[SeqNo5]);

                                    if (SeqNo5 - 1 == strSeq5)
                                    {
                                        durRec5 = fromTime.Add(TimeSpan.Parse(time)) - starttime5[strSeq5].Add(TimeSpan.FromSeconds(duration5[strSeq5]));
                                    }
                                    else if (IsEmpty)
                                    {
                                        if (strSeq5 + 1 == SeqNo5)
                                            durRec5 = starttime5[SeqNo5] - starttime5[strSeq5].Add(TimeSpan.FromSeconds(duration5[strSeq5]));
                                    }

                                    kosong = durRec5.TotalSeconds;
                                    filekosong = "";

                                    if (kosong < 60)
                                    {
                                        //trim video
                                        strRec5b = TimeSpan.Parse("00:00:00");
                                        execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec5b + " -t " + durRec5 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "4.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);
                                    }
                                    else
                                    {
                                        int intkosong = (int)Math.Abs(kosong / 60);
                                        for (int k = 0; k < intkosong; k++)
                                        {
                                            filekosong = filekosong + EmptyVideoeosRC + "|";
                                        }

                                        filekosong = filekosong.TrimEnd('|');

                                        execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "4_a.avi" + "\"";
                                        listRec.Add(execute);
                                        Console.WriteLine("execute " + execute);

                                        kosong = Math.Floor(kosong / 60);
                                        if (kosong != 0)
                                        {
                                            TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec5b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "4_b.avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                            }
                            else if (mediaPos5 != null)
                            {
                                if (!IsEmpty)
                                {
                                    Console.WriteLine("|||||||||");
                                    durRec5 = TimeSpan.Parse(CalculateTime(duration5[strSeq5])) - strRec5a;
                                    execute = " -i " + "\"" + pathrec5a + "\"" + " -ss " + strRec5a + " -t " + durRec5 + " -r " + rate + " -async 1 " + cout + "\\" + "1.avi" + "\"";
                                    listRec.Add(execute);
                                    Console.WriteLine("execute " + execute);
                                }

                                int count = SeqNo5 - (strSeq5 + 1);
                                if (count > 0)
                                {
                                    for (int n = strSeq5 + 1; n <= SeqNo5; n++)
                                    {
                                        if (starttime5[n] > starttime5[n - 1].Add(TimeSpan.FromSeconds(duration5[n - 1] + 5)))
                                        {
                                            strRec5b = TimeSpan.Parse("00:00:00");
                                            durRec5 = starttime5[n] - starttime5[n - 1].Add(TimeSpan.FromSeconds(duration5[n - 1]));

                                            if (IsEmpty)
                                            {
                                                durRec5 = starttime5[n] - strRec5a;
                                            }

                                            kosong = durRec5.TotalSeconds;
                                            filekosong = "";

                                            if (kosong < 60)
                                            {
                                                //trim video
                                                execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec5b + " -t " + durRec5 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + ".avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);
                                            }
                                            else
                                            {
                                                int intkosong = (int)Math.Abs(kosong / 60);
                                                for (int k = 0; k < intkosong; k++)
                                                {
                                                    filekosong = filekosong + EmptyVideoeosRC + "|";
                                                }

                                                filekosong = filekosong.TrimEnd('|');

                                                execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2_" + n + "a.avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);

                                                kosong = Math.Floor(kosong / 60);
                                                if (kosong != 0)
                                                {
                                                    TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                                    execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec5b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2_" + n + "b.avi" + "\"";
                                                    listRec.Add(execute);
                                                    Console.WriteLine("execute " + execute);
                                                }
                                            }
                                        }

                                        if (n < SeqNo5)
                                        {
                                            strRec5b = TimeSpan.Parse("00:00:00");
                                            durRec5 = TimeSpan.FromSeconds(duration5[n]);
                                            pathrec5b = Path.Combine(numberPath, cam5[n]);
                                            name1 = Path.GetFileName(cam5[n]);

                                            execute = " -i " + pathrec5b + " -ss " + strRec5b + " -t " + durRec5 + " -r " + rate + " -async 1 " + cout + "\\" + "2_" + n + "c.avi";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                    }
                                }
                                else
                                {
                                    if (starttime5[SeqNo5] > starttime5[strSeq5].Add(TimeSpan.FromSeconds(duration5[strSeq5] + 5)))
                                    {
                                        strRec5b = TimeSpan.Parse("00:00:00");
                                        durRec5 = starttime5[SeqNo5] - starttime5[SeqNo5 - 1].Add(TimeSpan.FromSeconds(duration5[SeqNo5 - 1]));

                                        if (IsEmpty)
                                        {
                                            durRec5 = starttime5[SeqNo5] - strRec5a;
                                        }

                                        kosong = durRec5.TotalSeconds;
                                        filekosong = "";

                                        if (kosong < 60)
                                        {
                                            //trim video
                                            execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec5b + " -t " + durRec5 + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2" + ".avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);
                                        }
                                        else
                                        {
                                            int intkosong = (int)Math.Abs(kosong / 60);
                                            for (int k = 0; k < intkosong; k++)
                                            {
                                                filekosong = filekosong + EmptyVideoeosRC + "|";
                                            }

                                            filekosong = filekosong.TrimEnd('|');

                                            execute = " -i " + "\"concat:" + filekosong + "\" -c copy " + "\"" + cout + "\\" + "2a" + ".avi" + "\"";
                                            listRec.Add(execute);
                                            Console.WriteLine("execute " + execute);

                                            kosong = Math.Floor(kosong / 60);
                                            if (kosong != 0)
                                            {
                                                TimeSpan sisakosong = TimeSpan.Parse(CalculateTime(kosong));
                                                execute = " -i " + "\"" + EmptyVideoeosRC + "\"" + " -ss " + strRec5b + " -t " + sisakosong + " -r " + rate + " -async 1 " + "\"" + cout + "\\" + "2b" + ".avi" + "\"";
                                                listRec.Add(execute);
                                                Console.WriteLine("execute " + execute);
                                            }
                                        }
                                    }
                                }

                                durRec5 = fromTime.Add(TimeSpan.Parse(time)) - starttime5[SeqNo5];
                                pathrec5a = Path.Combine(numberPath, cam5[SeqNo5]);

                                execute = " -i " + "\"" + pathrec5a + "\"" + " -ss " + strRec5b + " -t " + durRec5 + " -r " + rate + " -async 1 " + cout + "\\" + "3.avi" + "\"";
                                listRec.Add(execute);
                                Console.WriteLine("execute " + execute);
                            }
                        }
                    }
                }
            }



            //Create Video file
            foreach (string d in listRec)
            {
                if (d.Contains(","))
                {
                    string d_replace;
                    d_replace = d.Replace(',', '.');
                    jointFile.Arguments = d_replace;
                }
                else
                {
                    jointFile.Arguments = d;
                }

                try
                {
                    using (Process exeProcess = Process.Start(jointFile))
                    {
                        exeProcess.WaitForExit();
                        Console.WriteLine("capture completed " + d);
                    }
                }
                catch (Exception et)
                {
                    Console.WriteLine("The process failed: {0}", et.ToString());
                }
            }

            //joining file
            DirectoryInfo dinfo = new DirectoryInfo(cout);
            FileInfo[] Files = dinfo.GetFiles();
            string jointFiles = "";
            string jfiles = "";
            //recFolder = Path.Combine(pathdirect, "Maestronic");
            //recFolder = Path.Combine(recFolder, "recording");
            recFolder = recPath;

            try
            {
                if (!Directory.Exists(recFolder))
                {
                    System.IO.Directory.CreateDirectory(recFolder);
                    DirectoryInfo di = Directory.CreateDirectory(recFolder);
                }
            }
            catch (Exception et)
            {
                Console.WriteLine("The process failed: {0}", et.ToString());
            }

            comb = Path.Combine(recFolder, recName);
            if (File.Exists(comb))
            {
                do
                {
                    fileCount++;
                }
                while (File.Exists(Path.Combine(recFolder, recName) + (fileCount > 0 ? "_(" + fileCount.ToString() + ").avi" : "")));

                comb = comb + "_(" + (fileCount).ToString() + ")";
                Console.WriteLine(File.Exists(Path.Combine(recFolder, recName) + ".avi") ? "File exists." : "File does not exist.");
            }

            foreach (FileInfo file in Files.OrderBy(d => d.Name))
            {
                jfiles = jfiles + Path.Combine(cout, file.ToString()) + "|";
            }

            jfiles = jfiles.TrimEnd('|');
            jointFiles = " -i " + "\"concat:" + jfiles + "\" -c copy " + "\"" + comb + ".avi" + "\"";
            jointFile.Arguments = jointFiles;
            Console.WriteLine("jointFiles " + jointFiles);

            try
            {
                if (!File.Exists(comb))
                {
                    using (Process exeProcess = Process.Start(jointFile))
                    {
                        exeProcess.WaitForExit();
                    }
                }
                else
                {
                    Console.WriteLine(File.Exists(comb) ? "File exists." : "File does not exist.");
                }
            }
            catch
            {
                // Log error.
            }

            string[] filePaths = Directory.GetFiles(cout);
            foreach (string filePath in filePaths)
                File.Delete(filePath);

            listRec.Clear();
        }

        /// <summary> doing action when recording is completed </summary>
        private void doRecord_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btRec.Enabled = true;
            progressDialog.pictureBox1.Image = null;
            doRecord.CancelAsync();
            progressDialog.label1.Text = "Recording Accomplished";
            progressDialog.btDone.Visible = true;

            if (currentVideoState == VideoState.Forwarding)
            {
                btPlay_Click(sender, e);
            }
            else if (currentVideoState == VideoState.FastBackwards4x)
            {
                btFastBackward4x_Click(sender, e);
            }
            else if (currentVideoState == VideoState.FastBackwards8x)
            {
                btFastBackward8x_Click(sender, e);
            }
            else if (currentVideoState == VideoState.Forwarding4x)
            {
                btFast4x_Click(sender, e);
            }
            else if (currentVideoState == VideoState.Forwarding8x)
            {
                btFast8x_Click(sender, e);
            }
        }

        /// <summary> timer for handle backward. </summary>
        private void backwardTimer_Tick(object sender, EventArgs e)
        {
            if (currentVideoState == VideoState.Backwards)
                Backwards();
            if (currentVideoState == VideoState.FastBackwards4x)
                FastBackwards();
            if (currentVideoState == VideoState.FastBackwards8x)
                FastBackwards();
        }

        /// <summary> background process action to take snapshots. </summary>
        private void doCapture_DoWork(object sender, DoWorkEventArgs e)
        {
            this.Invoke(new MethodInvoker(delegate()
                {
                    btSnapshots.Enabled = false;
                }));

            string time = CalculateTime(ticky);
            Console.WriteLine("countCh " + countCh);
            snap = TimeSpan.Parse(time);
            //snapTimeTB = snap.ToString().Replace(@":", "");
            snapTimeTB = lbStartDur.Text.Replace(":", string.Empty);

            if (countCh == 0)
            {
                capture1(time);
                capture2(time);
                capture3(time);
                capture4(time);
                capture5(time);
            }
            else if (countCh == 1)
            {
                capture1(time);
            }
            else if (countCh == 2)
            {
                capture2(time);
            }
            else if (countCh == 3)
            {
                capture3(time);
            }
            else if (countCh == 4)
            {
                capture4(time);
            }
            else if (countCh == 5)
            {
                capture5(time);
            }

            ProcessStartInfo snapInto = new ProcessStartInfo();
            snapInto.CreateNoWindow = true;
            snapInto.UseShellExecute = false;
            snapInto.FileName = "thirdParty\\ffmpeg.exe";
            snapInto.WindowStyle = ProcessWindowStyle.Hidden;

            foreach (string d in listCapture)
            {
                snapInto.Arguments = d;
                using (Process exeProcess = Process.Start(snapInto))
                {
                    exeProcess.WaitForExit();
                    Console.WriteLine("capture completed " + d);
                }
            }
            listCapture.Clear();
        }

        /// <summary> background process action when take snapshots is complete. </summary>
        private void doCapture_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Invoke(new MethodInvoker(delegate()
            {
                btSnapshots.Enabled = true;
            }));

            pLoad.Close();
            doCapture.CancelAsync();
            btSnapshots.Enabled = true;

            if (playState1 == PlayState.Paused || playState2 == PlayState.Paused || playState3 == PlayState.Paused || playState4 == PlayState.Paused || playState5 == PlayState.Paused)
            {
                btPause_Click(sender, e);
            }

            if (currentVideoState == VideoState.Forwarding)
            {
                btPlay_Click(sender, e);
            }
            else if (currentVideoState == VideoState.Forwarding4x)
            {
                btFast4x_Click(sender, e);
            }
            else if (currentVideoState == VideoState.Forwarding8x)
            {
                btFast8x_Click(sender, e);
            }
            else if (currentVideoState == VideoState.Backwards)
            {
                btFB_Click(sender, e);
            }
            else if (currentVideoState == VideoState.FastBackwards4x)
            {
                btFastBackward4x_Click(sender, e);
            }
            else if (currentVideoState == VideoState.FastBackwards8x)
            {
                btFastBackward8x_Click(sender, e);
            }

            //Process.Start(Path.Combine(pathdirect, "Maestronic\\snapshot"));
            Process.Start(snapPath);
        }

        /// <summary> keyboard handling for shortcuts. </summary>
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                formState.Restore(this);
            }
        }

        /// <summary> button handling to run video forward with 4x speed. </summary>
        private void btFast4x_Click(object sender, EventArgs e)
        {
            btPause_Click(sender, e);
            btPlay.Checked = false;
            btPause.Checked = false;
            btFB.Checked = false;
            btFast4x.Checked = true;
            btFast8x.Checked = false;
            btFastBackward4x.Checked = false;
            btFastBackward8x.Checked = false;

            btRec.Enabled = true;
            btSnapshots.Enabled = true;
            videoTrackBar.Enabled = true;

            btPlay.Enabled = true;
            btFB.Enabled = true;
            btFastBackward4x.Enabled = true;
            btFastBackward8x.Enabled = true;
            btFast4x.Enabled = false;
            btFast8x.Enabled = true;
            btPause.Enabled = true;

            splitContainer9.Panel1.BackgroundImage = null;
            splitContainer9.Panel2.BackgroundImage = null;
            splitContainer10.Panel1.BackgroundImage = null;
            splitContainer4.Panel2.BackgroundImage = null;
            splitContainer4.Panel1.BackgroundImage = null;

            if (playmode == "single")
            {
                splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.waiting;

                if (mediaCtrl1 != null || mediaSeek1 != null)
                    if (playState1 == PlayState.Paused)
                    {
                        if (mediaCtrl1.Run() >= 0)
                            playState1 = PlayState.Running;
                    }
                if (playState1 == PlayState.Running)
                {
                    SetRate(5);
                }

                timer1.Interval = 2000 / 8;

                btcam1.Enabled = false;
                btcam2.Enabled = false;
                btCam3.Enabled = false;
                btCam4.Enabled = false;
                btCam5.Enabled = false;
            }
            else if (playmode == "multiple")
            {
                if (cam1Count != 0)
                {
                    if (mediaCtrl1 != null || mediaSeek1 != null)
                        if (playState1 == PlayState.Paused)
                        {
                            if (mediaCtrl1.Run() >= 0)
                                playState1 = PlayState.Running;
                        }
                    if (playState1 == PlayState.Running)
                    {
                        SetRate(5);
                    }
                }

                if (cam2Count != 0)
                {
                    if (mediaCtrl2 != null || mediaSeek2 != null)
                        if (playState2 == PlayState.Paused)
                        {
                            if (mediaCtrl2.Run() >= 0)
                                playState2 = PlayState.Running;
                        }
                    if (playState2 == PlayState.Running)
                    {
                        SetRate(5);
                    }
                }

                if (cam3Count != 0)
                {
                    if (mediaCtrl3 != null || mediaSeek3 != null)
                        if (playState3 == PlayState.Paused)
                        {
                            if (mediaCtrl3.Run() >= 0)
                                playState3 = PlayState.Running;
                        }
                    if (playState3 == PlayState.Running)
                    {
                        SetRate(5);
                    }
                }

                if (cam4Count != 0)
                {
                    if (mediaCtrl4 != null || mediaSeek4 != null)
                        if (playState4 == PlayState.Paused)
                        {
                            if (mediaCtrl4.Run() >= 0)
                                playState4 = PlayState.Running;
                        }
                    if (playState4 == PlayState.Running)
                    {
                        SetRate(5);
                    }
                }
                if (cam5Count != 0)
                {
                    if (mediaCtrl5 != null || mediaSeek5 != null)
                        if (playState5 == PlayState.Paused)
                        {
                            if (mediaCtrl5.Run() >= 0)
                                playState5 = PlayState.Running;
                        }
                    if (playState5 == PlayState.Running)
                    {
                        SetRate(5);
                    }
                }
                timer1.Interval = 2000 / 8;
            }

            if (backwardTimer.Enabled)
                backwardTimer.Stop();

            if (!timer1.Enabled)
                timer1.Start();

            this.currentVideoState = VideoState.Forwarding4x;
        }

        /// <summary> button handling to run video forward with 8x speed. </summary>
        private void btFast8x_Click(object sender, EventArgs e)
        {
            btPause_Click(sender, e);
            btPlay.Checked = false;
            btPause.Checked = false;
            btFast4x.Checked = false;
            btFast8x.Checked = true;
            btFB.Checked = false;
            btFastBackward4x.Checked = false;
            btFastBackward8x.Checked = false;

            btRec.Enabled = true;
            btSnapshots.Enabled = true;
            videoTrackBar.Enabled = true;

            btPlay.Enabled = true;
            btFB.Enabled = true;
            btFastBackward4x.Enabled = true;
            btFastBackward8x.Enabled = true;
            btFast4x.Enabled = true;
            btFast8x.Enabled = false;
            btPause.Enabled = true;

            splitContainer9.Panel1.BackgroundImage = null;
            splitContainer9.Panel2.BackgroundImage = null;
            splitContainer10.Panel1.BackgroundImage = null;
            splitContainer4.Panel2.BackgroundImage = null;
            splitContainer4.Panel1.BackgroundImage = null;

            if (playmode == "single")
            {
                splitContainer9.Panel1.BackgroundImage = mtvplayer.Properties.Resources.waiting;

                if (mediaCtrl1 != null || mediaSeek1 != null)
                    if (playState1 == PlayState.Paused)
                    {
                        if (mediaCtrl1.Run() >= 0)
                            playState1 = PlayState.Running;
                    }

                if (playState1 == PlayState.Running)
                {
                    SetRate(7);
                }

                timer1.Interval = 2000 / 13;

                btcam1.Enabled = false;
                btcam2.Enabled = false;
                btCam3.Enabled = false;
                btCam4.Enabled = false;
                btCam5.Enabled = false;
            }
            else if (playmode == "multiple")
            {
                if (cam1Count != 0)
                {
                    if (mediaCtrl1 != null || mediaSeek1 != null)
                        if (playState1 == PlayState.Paused)
                        {
                            if (mediaCtrl1.Run() >= 0)
                                playState1 = PlayState.Running;
                        }
                    if (playState1 == PlayState.Running)
                    {
                        SetRate(7);
                    }
                }

                if (cam2Count != 0)
                {
                    if (mediaCtrl2 != null || mediaSeek2 != null)
                        if (playState2 == PlayState.Paused)
                        {
                            if (mediaCtrl2.Run() >= 0)
                                playState2 = PlayState.Running;
                        }
                    if (playState2 == PlayState.Running)
                    {
                        SetRate(7);
                    }
                }

                if (cam3Count != 0)
                {
                    if (mediaCtrl3 != null || mediaSeek3 != null)
                        if (playState3 == PlayState.Paused)
                        {
                            if (mediaCtrl3.Run() >= 0)
                                playState3 = PlayState.Running;
                        }
                    if (playState3 == PlayState.Running)
                    {
                        SetRate(7);
                    }
                }

                if (cam4Count != 0)
                {
                    if (mediaCtrl4 != null || mediaSeek4 != null)
                        if (playState4 == PlayState.Paused)
                        {
                            if (mediaCtrl4.Run() >= 0)
                                playState4 = PlayState.Running;
                        }
                    if (playState4 == PlayState.Running)
                    {
                        SetRate(7);
                    }

                }

                if (cam5Count != 0)
                {
                    if (mediaCtrl5 != null || mediaSeek5 != null)
                        if (playState5 == PlayState.Paused)
                        {
                            if (mediaCtrl5.Run() >= 0)
                                playState5 = PlayState.Running;
                        }
                    if (playState5 == PlayState.Running)
                    {
                        SetRate(7);
                    }

                }

                timer1.Interval = 2000 / 13;
            }

            if (backwardTimer.Enabled)
                backwardTimer.Stop();
            if (!timer1.Enabled)
                timer1.Start();

            this.currentVideoState = VideoState.Forwarding8x;
        }

        /// <summary> button handling to run video backward with 4x speed. </summary>
        private void btFastBackward4x_Click(object sender, EventArgs e)
        {
            btPause_Click(sender, e);
            btPlay.Checked = false;
            btPause.Checked = false;
            btFast4x.Checked = false;
            btFast8x.Checked = false;
            btFB.Checked = false;
            btFastBackward4x.Checked = true;
            btFastBackward8x.Checked = false;

            btPlay.Enabled = true;
            btFB.Enabled = true;
            btFastBackward4x.Enabled = false;
            btFastBackward8x.Enabled = true;
            btFast4x.Enabled = true;
            btFast8x.Enabled = true;
            btPause.Enabled = true;

            timer1.Interval = 1000 / 5;
            timer1.Start();
            backwardTimer.Interval = 1000 / 5;
            backwardTimer.Start();
            SetRate(6);
            this.currentVideoState = VideoState.FastBackwards4x;
        }

        /// <summary> button handling to run video backward with 8x speed. </summary>
        private void btFastBackward8x_Click(object sender, EventArgs e)
        {
            btPause_Click(sender, e);
            btPlay.Checked = false;
            btPause.Checked = false;
            btFast4x.Checked = false;
            btFast8x.Checked = false;
            btFastBackward4x.Checked = false;
            btFastBackward8x.Checked = true;
            btFB.Checked = false;

            btPlay.Enabled = true;
            btFB.Enabled = true;
            btFastBackward4x.Enabled = true;
            btFastBackward8x.Enabled = false;
            btFast4x.Enabled = true;
            btFast8x.Enabled = true;
            btPause.Enabled = true;

            timer1.Interval = 1000 / 10;
            timer1.Start();
            backwardTimer.Interval = 1000 / 10;
            backwardTimer.Start();
            SetRate(12);
            this.currentVideoState = VideoState.FastBackwards8x;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (System.IO.Directory.Exists(Path.Combine(pathdirect, "avi")))
            {
                foreach (var process in Process.GetProcessesByName("thirdParty\\ffmpeg.exe"))
                {
                    process.Kill();
                }

                foreach (var process in Process.GetProcessesByName("thirdParty\\avc2avi.exe"))
                {
                    process.Kill();
                }
                stop();

                CloseInterfaces();
                CloseInterfaces2();
                CloseInterfaces3();
                CloseInterfaces4();
                CloseInterfaces5();

                var dir = new DirectoryInfo(Path.Combine(pathdirect, "avi"));
                try
                {
                    Directory.Delete(Path.Combine(pathdirect, "avi"), true);
                    bool directoryExists = Directory.Exists(Path.Combine(pathdirect, "avi"));
                    Console.WriteLine("top-level directory exists: " + directoryExists);
                }
                catch (Exception z)
                {
                    Console.WriteLine("The process failed: {0}", z.Message);
                }

                string dir1 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "MaesTronic");
                try
                {
                    bool directoryExists = Directory.Exists(dir1);
                    Console.WriteLine("top-level directory exists: " + directoryExists);

                    Directory.Delete(dir1, true);
                }
                catch (Exception z)
                {
                    Console.WriteLine("The process failed: {0}", z.Message);
                }
            }

            //Properties.Settings.Default.Save();
        }

        private void rbNVS_CheckedChanged(object sender, EventArgs e)
        {
            if (rbNVS.Checked)
            {
                rbIPCAM.Checked = false;
                setcbRate();
            }
        }

        private void rbIPCAM_CheckedChanged(object sender, EventArgs e)
        {
            if (rbIPCAM.Checked)
            {
                rbNVS.Checked = false;
                setcbRate();
            }
        }

        public void setcbRate()
        {
            cbRate.Items.Clear();

            if (rbIPCAM.Checked)
            {
                cbRate.Items.Add("7.27");
                cbRate.Items.Add("6.27");
                cbRate.Items.Add("5.27");
                cbRate.Items.Add("4.27");
                cbRate.Items.Add("3.27");
                cbRate.Items.Add("2.27");
            }
            else if (rbNVS.Checked)
            {
                cbRate.Items.Add("7");
                cbRate.Items.Add("6");
                cbRate.Items.Add("5");
                cbRate.Items.Add("4");
                cbRate.Items.Add("3");
                cbRate.Items.Add("2");
            }

            cbRate.SelectedIndex = 0;
        }

        private void cbRate_SelectedIndexChanged(object sender, EventArgs e)
        {
            drate = Convert.ToDouble(cbRate.Text);
        }

        private void splitContainer6_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void splitContainer6_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }
        private void splitContainer6_DoubleClick(object sender, EventArgs e)
        {
            countCh = 1;
            btRec.Visible = true;
            btRecStop.Visible = true;

            btSnapshots.Location = new Point(69, 3);
            btOpenVideo.Location = new Point(103, 3);

            if (!splitContainer9.Panel2Collapsed)
            {
                splitContainer9.Panel2Collapsed = true;
                btcam1.Enabled = true;
                btcam2.Enabled = false;
                btCam3.Enabled = false;
                btCam4.Enabled = false;
                btCam5.Enabled = false;
                btAll4.Enabled = true;
            }

            if (!splitContainer8.Panel2Collapsed)
            {
                splitContainer8.Panel2Collapsed = true;
            }

            if (!splitContainer3.Panel2Collapsed)
            {
                splitContainer3.Panel2Collapsed = true;
            }
            if (!splitContainer6.Panel2Collapsed)
            {
                splitContainer6.Panel2Collapsed = true;
            }

            ResizeVideoWindow();
        }
       
        
    }
}