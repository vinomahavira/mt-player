﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace mtvplayer
{
    /// <summary>
    /// this form is used to divert screen, while load progress or capture progress is happen
    /// </summary>
    public partial class progressLoad : Form
    {
        /// <summary> initialize progressLoad component. </summary>
        public progressLoad()
        {
            InitializeComponent();
        }
    }
}
